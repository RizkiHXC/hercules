//
//  UIColor+Style.h
//  Hercules
//
//  Created by Rizki Calame on 14-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Style)

+ (UIColor *)hcWhite;
+ (UIColor *)hcBlack;
+ (UIColor *)hcOceanicCoralBlue;
+ (UIColor *)hcOctopusBlue;
+ (UIColor *)hcDeepSeaBlue;
+ (UIColor *)hcStarFishRed;

@end
