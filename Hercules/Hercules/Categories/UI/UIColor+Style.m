//
//  UIColor+Style.m
//  Hercules
//
//  Created by Rizki Calame on 14-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "UIColor+Style.h"

@implementation UIColor (Style)

+ (UIColor *)hcBlack
{
    return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1];
}

+ (UIColor *)hcWhite
{
    return [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
}

+ (UIColor *)hcDeepSeaBlue
{
    return [UIColor colorWithRed:(34.0f/255.0f) green:(105.0f/255.0f) blue:(136.0f/255.0f) alpha:1];
}

+ (UIColor *)hcOceanicCoralBlue
{
    return [UIColor colorWithRed:(165.0f/255.0f) green:(253.0f/255.0f) blue:(254.0f/255.0f) alpha:1];
}

+ (UIColor *)hcOctopusBlue
{
    return [UIColor colorWithRed:(10.0f/255.0f) green:(91.0f/255.0f) blue:(127.0f/255.0f) alpha:1];
}

+ (UIColor *)hcStarFishRed
{
    return [UIColor colorWithRed:(139.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1];
}



@end
