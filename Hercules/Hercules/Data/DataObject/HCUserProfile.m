//
//  HCUserProfile.m
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCUserProfile.h"

@implementation HCUserProfile

@synthesize fullName = fullName_;
@synthesize emailAddress = emailAddress_;
@synthesize safetyStatus = safetyStatus_;
@synthesize profilePicturePath = profilePicturePath_;

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        
    }
    
    return self;
}

@end
