// In AppDelegate:
//-----------------------

/*
    // Set up iRate
    iRate *rate = [iRate sharedInstance];
    [rate setAppStoreCountry:@"nl"];
    [rate setDisableAlertViewResizing:YES];
    [rate setDaysUntilPrompt:14.0];
 
    [rate setMessageTitle:NSLocalizedString(@"Vind je dit ook zo'n handige app?", @"iRate message title")];
    [rate setMessage:NSLocalizedString(@"Neem even de tijd om het te beoordelen in de App Store. Dank u voor uw steun.", @"iRate message")];
    [rate setCancelButtonLabel:NSLocalizedString(@"Nee, dank je", @"iRate decline button")];
    [rate setRemindButtonLabel:NSLocalizedString(@"Herinner mij later", @"iRate remind button")];
    [rate setRateButtonLabel:NSLocalizedString(@"Beoordeel het nu", @"iRate accept button")];
    [rate setVerboseLogging:NO];
 */
#ifdef DEBUG
    //[rate setPreviewMode:YES];
#endif

//-----------------------
