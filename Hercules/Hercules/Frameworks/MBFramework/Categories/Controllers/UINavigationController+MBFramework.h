//
//  UINavigationController+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 26/05/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  This category adds methods to the UIKit framework’s UINavigationController class.
 */
@interface UINavigationController (MBFramework)

/**
 *  The view controller at the root of the navigation stack. (read-only)
 */
@property(nonatomic,readonly) UIViewController *rootViewController;

@end
