//
//  UINavigationController+MBFramework.m
//  MBFramework
//
//  Created by Arno Woestenburg on 26/05/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "UINavigationController+MBFramework.h"

@implementation UINavigationController (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UIViewController *)rootViewController
{
    return [self.viewControllers firstObject];
}

@end
