//
//  UIViewController+MBUserInterfaceIdiom.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/6/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  This category adds methods to the UIKit framework’s UIViewController class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface UIViewController (MBUserInterfaceIdiom)

+ (id)allocForUserInterfaceIdiom;

@end
