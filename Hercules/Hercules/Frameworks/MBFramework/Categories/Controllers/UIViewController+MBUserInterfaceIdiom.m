//
//  UIViewController+MBUserInterfaceIdiom.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/6/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "UIViewController+MBUserInterfaceIdiom.h"
#import "MBClassFactory.h"

@implementation UIViewController (MBUserInterfaceIdiom)

+ (id)allocForUserInterfaceIdiom {
    return [[MBClassFactory fromClass:[self class]] alloc];
}

@end
