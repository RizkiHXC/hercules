
/**
 *  This category adds methods to the Foundation’s NSArray class.
 */
@interface NSArray (MBFramework)

/**
 *  Returns the object previous to the given object in the array order.
 *
 *  @param object Object that is located after the returning object in the array.
 *
 *  @return The object previous to the given object in the array order. If there is no previous object in the array, the method returns nil.
 */
- (id)objectPreceding:(id)object;


/**
 *  Returns the object previous to the given object in the array order.
 *
 *  @param object Object that is located before the returning object in the array.
 *
 *  @return The object previous to the given object in the array order. If there is no object following in the array, the method returns nil.
 */
- (id)objectFollowing:(id)object;


/**
 *  Returns the first object in the array that passes a test in a given Block.
 *
 *  @param predicate The block to apply to elements in the array.
 *  The block takes three arguments:
 *  obj
 *  The element in the array.
 *  idx
 *  The index of the element in the array.
 *  stop
 *  A reference to a Boolean value. The block can set the value to YES to stop further processing of the array. The stop argument is an out-only argument. You should only ever set this Boolean to YES within the Block.
 *
 *  @return The first object whose corresponding value in the array passes the test specified by predicate. If no objects in the array pass the test, returns nil.
 */
- (id)objectPassingTest:(BOOL (^)(id obj, NSUInteger idx, BOOL *stop))predicate NS_AVAILABLE(10_6, 4_0);

@end
