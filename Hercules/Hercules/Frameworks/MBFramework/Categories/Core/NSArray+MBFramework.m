
#import "NSArray+MBFramework.h"

@implementation NSArray (MBFramework)

- (id)objectPreceding:(id)object
{
    NSUInteger index = [self indexOfObject:object];
    return (NSNotFound != index && (index - 1) > 0) ? [self objectAtIndex:(index - 1)] : nil;
}

- (id)objectFollowing:(id)object
{
    NSUInteger index = [self indexOfObject:object];
    return (NSNotFound != index && (index + 1) < self.count) ? [self objectAtIndex:(index + 1)] : nil;
}

- (id)objectPassingTest:(BOOL (^)(id obj, NSUInteger idx, BOOL *stop))predicate
{
    NSUInteger index = [self indexOfObjectPassingTest:predicate];
    return (index != NSNotFound ? [self objectAtIndex:index] : nil);
}

@end
