//
//  NSBundle+MBProperties.h
//  MBFramework
//
//  Created by Arno Woestenburg on 3/8/13.
//
//

#import <Foundation/Foundation.h>

/**
 This category adds methods to the Foundation’s NSBundle class.
 */
@interface NSBundle (MBProperties)

/**
 A string formatted with the build year specified in the buildDate property
 @param format The date format string used by the receiver.
 @return A formatted build date string.
 @discussion A key with the name CFBuildDate is required in your Info.plist file.
 */
- (NSString *)buildDateWithFormat:(NSString *)format;

/**
 Specifies the copyright notice for the bundle.
 @discussion When a year date template ('YYYY') is provided in the copyright string, the copyright string is automatically formatted with the build year specified in the buildDate property.
 @discussion A key with the name NSHumanReadableCopyright is required in your Info.plist file.
 */
@property(nonatomic,readonly) NSString *copyright;
/**
 The actual name of the bundle.
 @discussion A key with the name CFBundleDisplayName is required in your Info.plist file.
 */
@property(nonatomic,readonly) NSString *displayName;
/**
 Name of the bundle’s executable file.
 @discussion A key with the name CFBundleExecutable is required in your Info.plist file.
 */
@property(nonatomic,readonly) NSString *executable;
/**
 The short display name of the bundle.
 @discussion This name should be less than 16 characters long and be suitable for displaying in the menu bar and the app’s Info window.
 @discussion A key with the name CFBundleName is required in your Info.plist file.
 */
@property(nonatomic,readonly) NSString *name;
/**
 The build-version-number string for the bundle.
 @discussion A key with the name CFBundleVersion is required in your Info.plist file.
 */
@property(nonatomic,readonly) NSString *version;
/**
 The release-version-number string for the bundle.
 @discussion A key with the name CFBundleShortVersionString is required in your Info.plist file.
 */
@property(nonatomic,readonly) NSString *shortVersion;

// Custom keys.
/**
 The short display name of a specific build.
 @discussion A key with the name CFAppBuildTitle is required in your Info.plist file.
 */
@property(nonatomic,readonly) NSString *buildTitle;
/**
 Specifies the build date of the bundle.
 @discussion A key with the name CFBuildDate is required in your Info.plist file.
 */
@property(nonatomic,readonly) NSDate *buildDate;

@end
