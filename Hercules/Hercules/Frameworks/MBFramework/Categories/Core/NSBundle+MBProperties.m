//
//  NSBundle+MBProperties.m
//  MBFramework
//
//  Created by Arno Woestenburg on 3/8/13.
//
//

#import "NSBundle+MBProperties.h"

@implementation NSBundle (MBProperties)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (id)MB_objectForBundleInfoKey:(NSString *)key
{
    NSAssert([self objectForInfoDictionaryKey:key], @"Forgot to add %@?", key);
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:key];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSString *)buildTitle
{
    // Need to have an Info object with the name CFAppBuildTitle for the buildTitle
    return [self MB_objectForBundleInfoKey:@"CFAppBuildTitle"];
}

- (NSString *)displayName
{
    // The actual name of the bundle.
    return [self MB_objectForBundleInfoKey:@"CFBundleDisplayName"];
}

- (NSString *)executable
{
    // Name of the bundle’s executable file.
    return [self MB_objectForBundleInfoKey:@"CFBundleExecutable"];
}

- (NSString *)name
{
    // This name should be less than 16 characters long and be suitable for displaying in the menu bar and the app’s Info window.
    return [self MB_objectForBundleInfoKey:@"CFBundleName"];
}

- (NSString *)version
{
    // The build-version-number string for the bundle. 
    return [self MB_objectForBundleInfoKey:@"CFBundleVersion"];
}

- (NSString *)shortVersion
{
    // The release-version-number string for the bundle.
    return [self MB_objectForBundleInfoKey:@"CFBundleShortVersionString"];
}

- (NSString *)copyright
{
    NSString *copyright = [self MB_objectForBundleInfoKey:@"NSHumanReadableCopyright"];
    NSString *dateFormat = @"yyyy"; // Year
    NSRange dateRange = [copyright rangeOfString:dateFormat options:NSCaseInsensitiveSearch];
    // When a year date template is provided in the copyright string, format the copyright with the build year
    if (dateRange.location != NSNotFound) {
        NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        [dateFormatter setDateFormat:[copyright substringWithRange:dateRange]];
        NSString *date = [dateFormatter stringFromDate:[[NSBundle mainBundle] buildDate]];
        copyright = [copyright stringByReplacingCharactersInRange:dateRange withString:date];
    }
    return copyright;
}

- (NSDate *)buildDate {
    
    return [self MB_objectForBundleInfoKey:@"CFBuildDate"];
}

- (NSString *)buildDateWithFormat:(NSString *)format
{
    NSString *result = nil;
    
    NSDate* buildDate = self.buildDate;
    if (buildDate) {
        NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        [dateFormatter setDateFormat:format];
        result = [dateFormatter stringFromDate:buildDate];
    }
    return result;
}


@end
