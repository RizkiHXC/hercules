//
//  NSDate+MBDateCategory.h
//  MBFramework
//
//  Created by Marco Jonker on 6/25/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

/**
 *  This category adds methods to the Foundation’s NSDate class.
 */
@interface NSDate (MBDateCategory)

- (BOOL)mbIsToday;
- (BOOL)mbIsTomorrow;
- (BOOL)mbIsDayAfterTomorrow;

- (NSDate *)yesterday;
- (NSDate *)tomorrow;

+ (NSDate *)dateWithComponents:(NSCalendarUnit)unitFlags fromDate:(NSDate *)date;
/**
 *  Returns a Boolean value that indicates whether a given object is an NSDate object and exactly the same day as the receiver.
 *
 *  @param anotherDate The date to compare with the receiver.
 *
 *  @return YES if the anotherDate is an NSDate object and is exactly the same day as the receiver, otherwise NO.
 */
- (BOOL)isSameDay:(NSDate *)anotherDate;

/**
 *  Returns a Boolean value that indicates whether a given object is an NSDate object and the same month as the receiver.
 *
 *  @param anotherDate The date to compare with the receiver.
 *
 *  @return YES if the anotherDate is an NSDate object and is the same month as the receiver, otherwise NO.
 */
- (BOOL)isSameMonth:(NSDate *)anotherDate;

/**
 *  Returns an NSComparisonResult value that indicates the ordering (using the the specified components) of the receiver and another given date.
 *
 *  @param anotherDate The date with which to compare the receiver. This value must not be nil.
 *  @param unitFlags The components into which to decompose date—a bitwise OR of NSCalendarUnit constants.
 *
 *  @return If:
 ** The receiver and anotherDate tested components are exactly equal to each other, NSOrderedSame
 ** The tested components of the receiver are later in time than anotherDate, NSOrderedDescending
 ** The tested components of the receiver are earlier in time than anotherDate, NSOrderedAscending.
 */
- (NSComparisonResult)compare:(NSDate *)anotherDate withComponents:(NSCalendarUnit)unitFlags;

@end
