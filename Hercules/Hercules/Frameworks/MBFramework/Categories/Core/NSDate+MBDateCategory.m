//
//  NSDate+MBDateCategory.m
//  MBFramework
//
//  Created by Marco Jonker on 6/25/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "NSDate+MBDateCategory.h"
#import "MBComparator.h"

@implementation NSDate (MBDateCategory)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (NSDate *)dateWithComponents:(NSCalendarUnit)unitFlags fromDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    return [calendar dateFromComponents:comps];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

- (BOOL)mbIsToday {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    // Today
    NSDateComponents *components = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDate *today = [calendar dateFromComponents:components];
    // Compare date
    components = [calendar components:unitFlags fromDate:self];
    NSDate *otherDate = [calendar dateFromComponents:components];
    return ([today isEqualToDate:otherDate]);
}

- (BOOL)mbIsTomorrow {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    // Today
    NSDateComponents *components = [calendar components:unitFlags fromDate:[NSDate date]];
    components.day += 1;
    NSDate *today = [calendar dateFromComponents:components];
    // Compare date
    components = [calendar components:unitFlags fromDate:self];
    NSDate *otherDate = [calendar dateFromComponents:components];
    return ([today isEqualToDate:otherDate]);
}

- (BOOL)mbIsDayAfterTomorrow {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    // Today
    NSDateComponents *components = [calendar components:unitFlags fromDate:[NSDate date]];
    components.day += 2;
    NSDate *today = [calendar dateFromComponents:components];
    // Compare date
    components = [calendar components:unitFlags fromDate:self];
    NSDate *otherDate = [calendar dateFromComponents:components];
    return ([today isEqualToDate:otherDate]);
}

- (NSDate *)yesterday {
    NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
    dayComponent.day = -1;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *yesterday = [calendar dateByAddingComponents:dayComponent toDate:self options:0];
    
    return yesterday;
}

- (NSDate *)tomorrow {
    NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
    dayComponent.day = 1;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *tomorrow = [calendar dateByAddingComponents:dayComponent toDate:self options:0];
    
    return tomorrow;
}

- (BOOL)isSameDay:(NSDate *)anotherDate
{
    BOOL result = NO;
    if (anotherDate) {
        NSCalendarUnit unitFlags = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
        result = ([self compare:anotherDate withComponents:unitFlags] == NSOrderedSame);
    }
    return result;
}

- (BOOL)isSameMonth:(NSDate *)anotherDate
{
    BOOL result = NO;
    if (anotherDate) {
        NSCalendarUnit unitFlags = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth;
        result = ([self compare:anotherDate withComponents:unitFlags] == NSOrderedSame);
    }
    return result;
}

- (NSComparisonResult)compare:(NSDate *)anotherDate withComponents:(NSCalendarUnit)unitFlags
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSComparisonResult compareResult = NSOrderedSame;
    NSDateComponents *components = [calendar components:unitFlags fromDate:self];
    NSDateComponents *compareComponents = [calendar components:unitFlags fromDate:anotherDate];
   
    // Era
    if (compareResult == NSOrderedSame && (unitFlags & NSCalendarUnitEra) == NSCalendarUnitEra) {
        compareResult = MBCompareInteger(components.era, compareComponents.era);
    }
    // Year
    if (compareResult == NSOrderedSame && (unitFlags & NSCalendarUnitYear) == NSCalendarUnitYear) {
        compareResult = MBCompareInteger(components.year, compareComponents.year);
    }
    // Month
    if (compareResult == NSOrderedSame && (unitFlags & NSCalendarUnitMonth) == NSCalendarUnitMonth) {
        compareResult = MBCompareInteger(components.month, compareComponents.month);
    }
    // Day
    if (compareResult == NSOrderedSame && (unitFlags & NSCalendarUnitDay) == NSCalendarUnitDay) {
        compareResult = MBCompareInteger(components.day, compareComponents.day);
    }
    // Hour
    if (compareResult == NSOrderedSame && (unitFlags & NSCalendarUnitHour) == NSCalendarUnitHour) {
        compareResult = MBCompareInteger(components.hour, compareComponents.hour);
    }
    // Minute
    if (compareResult == NSOrderedSame && (unitFlags & NSCalendarUnitMinute) == NSCalendarUnitMinute) {
        compareResult = MBCompareInteger(components.minute, compareComponents.minute);
    }
    // Second
    if (compareResult == NSOrderedSame && (unitFlags & NSCalendarUnitSecond) == NSCalendarUnitSecond) {
        compareResult = MBCompareInteger(components.second, compareComponents.second);
    }
    return compareResult;
}

@end
