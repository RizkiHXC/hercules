//
//  NSDictionary+MBFramework.h
//  MBFramework
//
//  Created by Marco Jonker on 12/13/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  This category adds methods to the Foundation’s NSDictionary class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface NSDictionary (MBFramework)

-(NSInteger)mbIntegerValueForKey:(NSString*)key defaultValue:(NSInteger)defaultValue;
-(CGFloat)mbFloatValueForKey:(NSString*)key defaultValue:(CGFloat)defaultValue;
-(NSString*)mbStringValueForKey:(NSString*)key defaultValue:(id)defaultValue;
-(NSNumber*)mbNumberValueForKey:(NSString*)key defaultValue:(id)defaultValue;
-(NSDictionary*)mbArrayValueForKey:(NSString*)key defaultValue:(id)defaultValue;
-(NSDate*)mbDateValueForKey:(NSString*)key defaultValue:(id)defaultValue;
-(NSDate*)mbDateValueForStringKey:(NSString*)key dateFormatter:(NSDateFormatter*)dateFormatter defaultValue:(id)defaultValue;

@end
