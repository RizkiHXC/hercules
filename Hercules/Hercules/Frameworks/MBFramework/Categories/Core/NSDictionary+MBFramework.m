//
//  NSDictionary+MBFramework.m
//  MBFramework
//
//  Created by Marco Jonker on 12/13/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "NSDictionary+MBFramework.h"

@implementation NSDictionary (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(NSInteger)mbIntegerValueForKey:(NSString*)key defaultValue:(NSInteger)defaultValue {
    id value = [self valueForKey:key];
    return (value != nil && [value isKindOfClass:[NSNumber class]]) ? [value intValue] : defaultValue;
}

-(CGFloat)mbFloatValueForKey:(NSString*)key defaultValue:(CGFloat)defaultValue {
    id value = [self valueForKey:key];
    return (value != nil && [value isKindOfClass:[NSNumber class]]) ? [value floatValue] : defaultValue;
}

-(NSString*)mbStringValueForKey:(NSString*)key defaultValue:(id)defaultValue {
    id value = [self valueForKey:key];
    return (value != nil && [value isKindOfClass:[NSString class]]) ? (NSString*)value : defaultValue;
}

-(NSNumber*)mbNumberValueForKey:(NSString*)key defaultValue:(id)defaultValue {
    id value = [self valueForKey:key];
    return (value != nil && [value isKindOfClass:[NSNumber class]]) ? value : defaultValue;
}

-(NSDictionary*)mbArrayValueForKey:(NSString*)key defaultValue:(id)defaultValue {
    id value = [self valueForKey:key];
    return (value != nil && [value isKindOfClass:[NSDictionary class]]) ? value : defaultValue;
}

-(NSDate*)mbDateValueForKey:(NSString*)key defaultValue:(id)defaultValue {
    id value = [self valueForKey:key];
    return (value != nil && [value isKindOfClass:[NSDate class]]) ? value : defaultValue;
}

-(NSDate*)mbDateValueForStringKey:(NSString*)key dateFormatter:(NSDateFormatter*)dateFormatter defaultValue:(id)defaultValue {
    NSString* dateString = [self mbStringValueForKey:key defaultValue:nil];
    NSDate* date = nil;
    if(dateString != nil && dateFormatter != nil) {
        date = [dateFormatter dateFromString:dateString];
    }
    
    return (date != nil) ? date : defaultValue;
}

@end
