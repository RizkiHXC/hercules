//
//  NSError+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 29/10/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 This category adds methods to the Foundation’s NSError class.
 */
@interface NSError (MBFramework)

/**
 The recommended standard way for embedded NSErrors from underlying calls.
 */
@property(nonatomic,readonly) NSError *underlyingError;

@end
