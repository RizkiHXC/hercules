//
//  NSError+MBFramework.m
//  MBFramework
//
//  Created by Arno Woestenburg on 29/10/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "NSError+MBFramework.h"

@implementation NSError (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSError *)underlyingError
{
    return [[self userInfo] valueForKey:NSUnderlyingErrorKey];
}


@end
