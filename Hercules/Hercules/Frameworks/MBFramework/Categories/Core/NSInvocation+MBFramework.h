//  Copyright 2010 Alejandro Isaza.
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not
//  use this file except in compliance with the License.  You may obtain a copy
//  of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
//  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
//  License for the specific language governing permissions and limitations under
//  the License.

#import <Foundation/Foundation.h>

/**
 *  This category adds methods to the Foundation’s NSInvocation class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface NSInvocation (MBFramework)

/**
 *  Returns an initialized invocation from a target and a selector.
 *
 *  @param targetObject Sets the receiver’s target.
 *  @param selector     Sets the receiver’s selector.
 *
 *  @return An initialized invocation from a target and a selector.
 */
+ (id)invocationWithTarget:(NSObject*)targetObject selector:(SEL)selector;

/**
 *  Returns an invocation from a class and a selector. The selector property of the invocation is initialized but you still need to set the target before invoking it.
 *
 *  @param targetClass Sets the receiver’s target class.
 *  @param selector    Sets the receiver’s selector.
 *
 *  @return An invocation from a class and a selector.
 */
+ (id)invocationWithClass:(Class)targetClass selector:(SEL)selector;

/**
 *  Returns an invocation from a protocol and a selector. The selector property of the invocation is initialized but you still need to set the target before invoking it.
 *
 *  @param targetProtocol A protocol for the receiver’s target.
 *  @param selector       Sets the receiver’s selector.
 *
 *  @return An invocation from a protocol and a selector.
 */
+ (id)invocationWithProtocol:(Protocol*)targetProtocol selector:(SEL)selector;

@end