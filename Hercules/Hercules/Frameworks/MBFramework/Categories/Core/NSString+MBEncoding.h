
/**
 This category adds methods to the Foundation’s NSString class.
 */
@interface NSString (MBEncoding)

/**
 Returns a representation of the receiver using a given encoding to determine the percent escapes necessary to convert the receiver into a legal URL string with the specified encoding.
 @param encoding The encoding to use for the returned string. If you are uncertain of the correct encoding you should use NSUTF8StringEncoding.
 @return A representation of the receiver using encoding to determine the percent escapes necessary to convert the receiver into a legal URL string. Returns nil if encoding cannot encode a particular character.
 */
- (NSString *)stringByEncodingURLCharactersUsingEncoding:(NSStringEncoding)encoding;


// Deprecated methods
- (NSString *)stringByURLEncoding DEPRECATED_MSG_ATTRIBUTE("Use stringByEncodingURLCharactersUsingEncoding: instead");
+ (NSString*)sha1HashForString:(NSString*)string DEPRECATED_MSG_ATTRIBUTE("Use an MBHash instance with MBHashAlgorithmSHA1 as the hash algorithm");
+ (NSString*)sha1HashForStringHex:(NSString*)string DEPRECATED_MSG_ATTRIBUTE("Use an MBHash instance with MBHashAlgorithmSHA1 as the hash algorithm");

@end
