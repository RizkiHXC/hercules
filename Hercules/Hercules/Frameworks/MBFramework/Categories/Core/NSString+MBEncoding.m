
#import "NSString+MBEncoding.h"
#include <CommonCrypto/CommonDigest.h>

@implementation NSString (MBEncoding)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

- (NSString *)stringByURLEncoding {
    // Deprecated. Use stringByEncodingURLCharactersWithEncoding instead.
    return [[self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
}

- (NSString *)stringByEncodingURLCharactersUsingEncoding:(NSStringEncoding)encoding
{
    NSString *encodedString = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                  (CFStringRef)self,
                                                                                  NULL,
                                                                                  (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                  CFStringConvertNSStringEncodingToEncoding(encoding));
    return [encodedString autorelease];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public static functions


+(NSString*)sha1HashForString:(NSString*)string
{
    NSData *data = [string dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1 (data.bytes, (CC_LONG)data.length, digest);
    NSMutableString* hashedString = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [hashedString appendFormat:@"%c", digest[i]];
    return hashedString;
}

+(NSString*)sha1HashForStringHex:(NSString*)string
{
    NSData *data = [string dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    NSMutableString* hashedString = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [hashedString appendFormat:@"%02X", digest[i]];
    return hashedString;
}

@end
