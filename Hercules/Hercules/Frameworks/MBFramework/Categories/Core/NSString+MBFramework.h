//
//  NSString+MBFramework.h
//  MBFramework
//
//  Created by Marco Jonker on 2/27/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The following constants are provided by NSString as possible capitalisation options.
 */
typedef NS_OPTIONS(NSInteger, MBCapitalizeOptions)
{
    /**
     A sentence style capitalized representation of the receiver.
     */
    MBCapitalizeSentence = 1 << 0,
};


/**
 This category adds methods to the Foundation’s NSString class.
 */
@interface NSString (MBFramework)

/**
 Returns a new string and check if a path separator needs to be added to the end of a string to create the correct syntax for a path.
 @return A new string with a path separator added to the end if needed.
 */
- (NSString *)stringByAppendingPathSeparator;
/**
 Removes the trailing backslash from a given path.
 @return A string without a trailing backslash.
 */
- (NSString *)stringByRemovingPathSeparator;
/**
 Returns a new string made by removing all characters contained in a given character set.
 @param set A character set containing the characters to remove from the receiver. set must not be nil.
 @return A new string made by removing all characters contained in set. If the receiver is composed entirely of characters from set, the empty string is returned.
 */
- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet *)set;
/**
 Returns a new string made by removing all characters that are not allowed in a file name.
 @return A new string made by removing all characters that are not allowed in a file name. If the receiver is composed entirely of characters from set, the empty string is returned.
 */
- (NSString *)stringByRemovingIllegalFilenameCharacters;
/**
 A capitalized representation of the receiver.
 @param options Capitalisation options as defined in MBCapitalizeOptions.
 @return A capitalized string.
 @see MBCapitalizeOptions
 */
- (NSString *)capitalizedStringWithOptions:(MBCapitalizeOptions)options;


@end
