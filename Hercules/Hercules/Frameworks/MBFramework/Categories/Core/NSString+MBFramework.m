//
//  NSString+MBFramework.m
//  MBFramework
//
//  Created by Marco Jonker on 2/27/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "NSString+MBFramework.h"

#define PATH_SEPARATOR @"/"

@implementation NSString (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

- (NSString *)stringByAppendingPathSeparator
{
    NSString *result = self;
    
    if (self.length > 0 && [self compare:PATH_SEPARATOR options:NSCaseInsensitiveSearch range:NSMakeRange(self.length - 1, 1)] != NSOrderedSame) {
        result = [self stringByAppendingString:PATH_SEPARATOR];
    }
    return result;
}

- (NSString *)stringByRemovingPathSeparator
{
    NSString *result = self;
    
    if(self.length > 0 && [self compare:PATH_SEPARATOR options:NSCaseInsensitiveSearch range:NSMakeRange(self.length - 1, 1)] == NSOrderedSame) {
        result = [self substringWithRange:NSMakeRange(0,self.length - 1)];
    }
    return result;
}

- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet *)set
{
    return [[self componentsSeparatedByCharactersInSet:set] componentsJoinedByString:@""];
}

- (NSString *)stringByRemovingIllegalFilenameCharacters
{
    return [self stringByRemovingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" /"]];
}

- (NSString *)capitalizedStringWithOptions:(MBCapitalizeOptions)options
{
    NSString *result = self;

    if ((options && MBCapitalizeSentence) == MBCapitalizeSentence) {
        // Sentence style capitalisation
        if (self.length >= 1) {
            NSString *uppercase = [[self substringToIndex:1] uppercaseString];
            NSString *lowercase = [[self substringFromIndex:1] lowercaseString];
            result = [uppercase stringByAppendingString:lowercase];
        }
    }
    
    return result;
}

@end
