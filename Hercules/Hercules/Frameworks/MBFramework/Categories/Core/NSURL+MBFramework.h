//
//  NSURL+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 12/6/12.
//
//

#import <Foundation/Foundation.h>

/**
 *  This category adds methods to the Foundation’s NSURL class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface NSURL (MBFramework)

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

+ (NSURL *)phoneURLWithString:(NSString *)URLString;

+ (NSURL *)URLWithLocation:(NSString *)location parameters:(NSDictionary *)parameters;
+ (NSDictionary *)URLQueryParameters:(NSURL *)url;
+ (NSURL*)mbURLWithHttp:(NSString*)urlString;
+ (NSURL*)mbURLWithHttps:(NSString*)urlString;

@end
