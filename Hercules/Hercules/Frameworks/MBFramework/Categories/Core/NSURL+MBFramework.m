//
//  NSURL+Phone.m
//  MBFramework
//
//  Created by Arno Woestenburg on 12/6/12.
//
//

#import "NSURL+MBFramework.h"
#import "NSString+MBEncoding.h"
#import <sys/xattr.h>

@implementation NSURL (Phone)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public static functions

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    if (&NSURLIsExcludedFromBackupKey == nil) { // iOS <= 5.0.1
        const char* filePath = [[URL path] fileSystemRepresentation];
        
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    } else { // iOS >= 5.1
        return [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
    }
}

+ (NSURL *)phoneURLWithString:(NSString *)URLString {
    
    NSMutableString* phoneString = [NSMutableString stringWithFormat:@"tel:%@", URLString];
    [phoneString replaceOccurrencesOfString:@"(0)" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [phoneString length])];
    NSString *phoneStringEscaped = [phoneString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:phoneStringEscaped];
}

+ (NSURL *)URLWithLocation:(NSString *)location parameters:(NSDictionary *)parameters {
    if(0 < [parameters count]) {
        NSString *parameterString = @"";
        
        if([location rangeOfString:@"?"].location == NSNotFound) {
            parameterString = @"?";
        } else {
            parameterString = @"&";
        }
        
        for(NSString *key in parameters) {
            id value              = [parameters objectForKey:key];
            NSString *valueString = @"";
            if([value isKindOfClass:[NSArray class]]) {
                valueString = [value componentsJoinedByString:@","];
            } else {
                valueString = [NSString stringWithFormat:@"%@", value];
            }
            if(0 < [valueString length]) {
                parameterString = [parameterString stringByAppendingFormat:@"%@=%@&", key, [valueString stringByEncodingURLCharactersUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", location, [parameterString substringWithRange:NSMakeRange(0, parameterString.length-1)]]];
    }
    else {
        return [NSURL URLWithString:location];
    }
}

+ (NSDictionary *)URLQueryParameters:(NSURL *)url {
    NSString *queryString = [url query];
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    NSArray *parameters = [queryString componentsSeparatedByString:@"&"];
    for (NSString *parameter in parameters)
    {
        NSArray *parts = [parameter componentsSeparatedByString:@"="];
        NSString *key = [[parts objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if ([parts count] > 1)
        {
            id value = [[parts objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [result setObject:value forKey:key];
        }
    }
    return result;
}

+(NSURL*)mbURLWithHttp:(NSString*)urlString {
    NSURL* url = [NSURL URLWithString:urlString];
    
    if([urlString rangeOfString:@"http:"].location == NSNotFound &&
       [urlString rangeOfString:@"https:"].location == NSNotFound) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", urlString]];
    }
    
    return url;
}

+(NSURL*)mbURLWithHttps:(NSString*)urlString {
    NSURL* url = [NSURL URLWithString:urlString];
    
    if([urlString rangeOfString:@"http:"].location == NSNotFound &&
       [urlString rangeOfString:@"https:"].location == NSNotFound) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@", urlString]];
    }
    
    return url;
}

@end
