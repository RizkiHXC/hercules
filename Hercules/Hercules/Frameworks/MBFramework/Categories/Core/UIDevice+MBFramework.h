//
//  UIDevice+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 03/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 *  This category adds methods to the UIKit framework’s UIDevice class.
 */
@interface UIDevice (MBFramework)


/**
 *  A Boolean value indicating whether current version of the operating system is equal to the passed system version. (read-only)
 *
 *  @param version Operating system version to compare the current version of the operating system with.
 *
 *  @discussion An example of the system version is @”7.1”.
 *
 *  @return YES if the current version of the operating system is equal to the passed system version. NO if not.
 */
- (BOOL)systemVersionEqual:(NSString *)version;

/**
 *  A Boolean value indicating whether current version of the operating system is equal or higher to the passed system version. (read-only)
 *
 *  @param version Operating system version to compare the current version of the operating system with.
 *
 *  @discussion An example of the system version is @”7.1”.
 *
 *  @return YES if the current version of the operating system is equal or higher to the passed system version. NO if not.
 */
- (BOOL)systemVersionEqualHigher:(NSString *)version;

/**
 *  A Boolean value indicating whether current version of the operating system is higher than passed system version. (read-only)
 *
 *  @param version Operating system version to compare the current version of the operating system with.
 *
 *  @discussion An example of the system version is @”7.1”.
 *
 *  @return YES if the current version of the operating system is higher than the passed system version. NO if not.
 */
- (BOOL)systemVersionHigher:(NSString *)version;

/**
 *  A Boolean value indicating whether current version of the operating system is equal or lower than the passed system version. (read-only)
 *
 *  @param version Operating system version to compare the current version of the operating system with.
 *
 *  @discussion An example of the system version is @”7.1”.
 *
 *  @return YES if the current version of the operating system is equal or lower than the passed system version. NO if not.
 */
- (BOOL)systemVersionEqualLower:(NSString *)version;

/**
 *  A Boolean value indicating whether current version of the operating system is lower than the passed system version. (read-only)
 *
 *  @param version Operating system version to compare the current version of the operating system with.
 *
 *  @discussion An example of the system version is @”7.1”.
 *
 *  @return YES if the current version of the operating system is equal lower than the passed system version. NO if not.
 */
- (BOOL)systemVersionLower:(NSString *)version;

@end
