//
//  UIDevice+MBFramework.m
//  MBFramework
//
//  Created by Arno Woestenburg on 03/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "UIDevice+MBFramework.h"

@implementation UIDevice (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (BOOL)systemVersionEqual:(NSString *)version
{
    return ([self MB_compareSystemVersionWithVersion:version] == NSOrderedSame);
}

- (BOOL)systemVersionEqualHigher:(NSString *)version
{
    NSComparisonResult compareResult = [self MB_compareSystemVersionWithVersion:version];
    return (compareResult == NSOrderedSame || compareResult == NSOrderedDescending);
}

- (BOOL)systemVersionHigher:(NSString *)version
{
    return ([self MB_compareSystemVersionWithVersion:version] == NSOrderedDescending);
}

- (BOOL)systemVersionEqualLower:(NSString *)version
{
    NSComparisonResult compareResult = [self MB_compareSystemVersionWithVersion:version];
    return (compareResult == NSOrderedSame || compareResult == NSOrderedAscending);
}

- (BOOL)systemVersionLower:(NSString *)version
{
    return ([self MB_compareSystemVersionWithVersion:version] == NSOrderedAscending);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSComparisonResult)MB_compareSystemVersionWithVersion:(NSString *)version
{
    NSCharacterSet *wildcardCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"x*"];
    NSArray *systemComponent = [self.systemVersion componentsSeparatedByString:@"."];
    NSArray *versionComponents = [version componentsSeparatedByString:@"."];
    NSString *category = nil;
    NSString *category2 = nil;
    NSUInteger version1 = 0;
    NSUInteger version2 = 0;
    NSInteger index = 0;
    NSInteger shift = 0;
    BOOL skip = NO;
    
    // Number of characters to check
    NSUInteger count = versionComponents.count;
    
    while (index < count && skip == NO) {
        shift = (count - index) * 8;
    
        category = (systemComponent.count > index) ? [systemComponent objectAtIndex:index] : @"";
        category2 = (versionComponents.count > index) ? [versionComponents objectAtIndex:index] : @"";
        
        // Skip category and stop when wildcard character is used
        skip = ([category rangeOfCharacterFromSet:wildcardCharacterSet].location != NSNotFound ||
                [category2 rangeOfCharacterFromSet:wildcardCharacterSet].location != NSNotFound);

        if (!skip) {
            version1 |= ([category integerValue] << shift);
            version2 |= ([category2 integerValue] << shift);
        }
        index++;
    }
    return (version1 > version2 ? NSOrderedDescending : (version1 < version2 ? NSOrderedAscending : NSOrderedSame));
}


@end
