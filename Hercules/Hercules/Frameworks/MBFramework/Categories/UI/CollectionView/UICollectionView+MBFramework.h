//
//  UICollectionView+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 14/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  This category adds methods to the UIKit framework’s UICollectionView class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface UICollectionView (MBFramework)

- (BOOL)indexPathLastInSection:(NSIndexPath *)indexPath;

@end
