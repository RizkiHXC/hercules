//
//  UICollectionView+MBFramework.m
//  MBFramework
//
//  Created by Arno Woestenburg on 14/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "UICollectionView+MBFramework.h"

@implementation UICollectionView (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (BOOL)indexPathLastInSection:(NSIndexPath *)indexPath
{
    return (indexPath.row == ([self numberOfItemsInSection:indexPath.section] - 1));
}


@end
