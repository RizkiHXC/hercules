//
//  UICollectionViewFlowLayout+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 14/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  This category adds methods to the UIKit framework’s UICollectionViewFlowLayout class.
 */
@interface UICollectionViewFlowLayout (MBFramework)

/**
 *  Asks the flow layout if the cell with specified indexPath is located in the first line of its section.
 *
 *  @param indexPath An index path object that identifies a cell by its index and its section index.
 *
 *  @return YES if the cell with specified indexPath is in the first line of its section or NO if not.
 */
- (BOOL)indexPathInFirstLine:(NSIndexPath *)indexPath;
/**
 *  Asks the flow layout if the cell with specified indexPath is located as the first item of its line.
 *
 *  @param indexPath indexPath An index path object that identifies a cell by its index and its section index.
 *
 *  @return YES if the cell with specified indexPath is the first item of its line or NO if not.
 */
- (BOOL)indexPathFirstInLine:(NSIndexPath *)indexPath;
/**
 *  Asks the flow layout if the cell with specified indexPath is located in the last line of its section.
 *
 *  @param indexPath indexPath An index path object that identifies a cell by its index and its section index.
 *
 *  @return YES if the cell with specified indexPath is in the last line of its section or NO if not.
 */
- (BOOL)indexPathInLastLine:(NSIndexPath *)indexPath;
/**
 *  Asks the flow layout if the cell with specified indexPath is located as the last item of its line.
 *
 *  @param indexPath indexPath An index path object that identifies a cell by its index and its section index.
 *
 *  @return YES if the cell with specified indexPath is the last item of its line or NO if not.
 */
- (BOOL)indexPathLastInLine:(NSIndexPath *)indexPath;

@end
