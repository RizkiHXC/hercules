//
//  UICollectionViewFlowLayout+MBFramework.m
//  MBFramework
//
//  Created by Arno Woestenburg on 14/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "UICollectionViewFlowLayout+MBFramework.h"
#import "UICollectionView+MBFramework.h"

@implementation UICollectionViewFlowLayout (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (BOOL)indexPathInFirstLine:(NSIndexPath *)indexPath
{
    NSIndexPath *firstItem = [NSIndexPath indexPathForItem:0 inSection:indexPath.section];
    UICollectionViewLayoutAttributes *firstItemAttributes = [self layoutAttributesForItemAtIndexPath:firstItem];
    UICollectionViewLayoutAttributes *thisItemAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    
    return (firstItemAttributes.frame.origin.y == thisItemAttributes.frame.origin.y);
}

- (BOOL)indexPathFirstInLine:(NSIndexPath *)indexPath
{
    NSInteger previousItem = indexPath.item-1;
    BOOL result = NO;
    
    if (previousItem >= 0) {
        UICollectionViewLayoutAttributes *cellAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
        UICollectionViewLayoutAttributes *previousCellAttributes = [self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:previousItem inSection:indexPath.section]];
        
        result = (cellAttributes.frame.origin.y != previousCellAttributes.frame.origin.y);
    } else {
        result = YES;
    }
    return result;
}

- (BOOL)indexPathInLastLine:(NSIndexPath *)indexPath
{
    NSInteger lastItemRow = ([self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:indexPath.section] - 1);
    NSIndexPath *lastItem = [NSIndexPath indexPathForItem:lastItemRow inSection:indexPath.section];
    UICollectionViewLayoutAttributes *lastItemAttributes = [self layoutAttributesForItemAtIndexPath:lastItem];
    UICollectionViewLayoutAttributes *thisItemAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    
    return (lastItemAttributes.frame.origin.y == thisItemAttributes.frame.origin.y);
}

- (BOOL)indexPathLastInLine:(NSIndexPath *)indexPath
{
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:indexPath.item+1 inSection:indexPath.section];
    
    UICollectionViewLayoutAttributes *cellAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    UICollectionViewLayoutAttributes *nextCellAttributes = [self layoutAttributesForItemAtIndexPath:nextIndexPath];
    
    return (cellAttributes.frame.origin.y != nextCellAttributes.frame.origin.y);
}

@end
