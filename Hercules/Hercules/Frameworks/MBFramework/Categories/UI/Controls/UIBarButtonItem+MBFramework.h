//
//  UIBarButtonItem+MBFramework.h
//  MBFramework
//
//  Created by Marco Jonker on 11/22/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (MBFramework)

+ (UIBarButtonItem*)barButtonItemWithImage:(UIImage *)normalImage highlightedImage:(UIImage*)highlightedImage target:(id)target action:(SEL)action;

- (CGRect)frameInView:(UIView *)view;
- (UIView *)barButtonView;

@end
