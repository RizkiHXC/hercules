//
//  UIBarButtonItem+MBFramework.m
//  MBFramework
//
//  Created by Marco Jonker on 11/22/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "UIBarButtonItem+MBFramework.h"

@implementation UIBarButtonItem (MBFramework)

+ (UIBarButtonItem*)barButtonItemWithImage:(UIImage *)normalImage highlightedImage:(UIImage*)highlightedImage target:(id)target action:(SEL)action {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:normalImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0,0,normalImage.size.width, normalImage.size.height);
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[[UIBarButtonItem alloc]initWithCustomView:button]autorelease];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (CGRect)frameInView:(UIView *)view
{
    CGRect frame = CGRectZero;
    UIView *theView = [self barButtonView];
    
    UIView *parentView = theView.superview;
    NSArray *subviews = parentView.subviews;
    
    NSUInteger indexOfView = [subviews indexOfObject:theView];
    NSUInteger subviewCount = subviews.count;
    
    if (subviewCount > 0 && indexOfView != NSNotFound) {
        UIView *button = [parentView.subviews objectAtIndex:indexOfView];
        frame = [button convertRect:button.bounds toView:view];
    }
    return frame;
}

- (UIView *)barButtonView
{
    UIView *theView = self.customView;
    if (!theView && [self respondsToSelector:@selector(view)]) {
        theView = [self performSelector:@selector(view)];
    }
    return theView;
}


@end
