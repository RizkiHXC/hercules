//
//  UIControl+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 04/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (MBFramework)

- (void)setEnabled:(BOOL)enabled animated:(BOOL)animated;

@end
