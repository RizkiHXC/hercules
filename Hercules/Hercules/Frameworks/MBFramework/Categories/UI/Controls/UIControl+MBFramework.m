//
//  UIControl+MBFramework.m
//  MBFramework
//
//  Created by Arno Woestenburg on 04/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "UIControl+MBFramework.h"

@implementation UIControl (MBFramework)

- (void)setEnabled:(BOOL)enabled animated:(BOOL)animated
{
    if (self.isEnabled != enabled) {
        if (animated) {
            [UIView transitionWithView:self
                              duration:0.2
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:nil];
            [self setEnabled:enabled];
        } else {
            [self setEnabled:enabled];
        }
    }
}

@end
