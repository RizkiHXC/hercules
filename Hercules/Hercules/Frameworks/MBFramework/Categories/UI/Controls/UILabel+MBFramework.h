//
//  UILabel+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 11/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (MBFramework)

- (void)setAdjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth minimumFontSize:(CGFloat)minimumFontSize;
- (void)setAdjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth minimumFontSize:(CGFloat)minimumFontSize baselineAdjustment:(UIBaselineAdjustment)baselineAdjustment;

@end
