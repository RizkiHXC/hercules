//
//  UILabel+MBFramework.m
//  MBFramework
//
//  Created by Arno Woestenburg on 11/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "UILabel+MBFramework.h"

@implementation UILabel (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setAdjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth minimumFontSize:(CGFloat)minimumFontSize
{
    [self setAdjustsFontSizeToFitWidth:adjustsFontSizeToFitWidth];
    
    if (adjustsFontSizeToFitWidth == YES) {
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0
        // >= iOS 6
        [self setMinimumScaleFactor:minimumFontSize / self.font.pointSize];
#else
        if ([self respondsToSelector:@selector(setMinimumScaleFactor:)]) {
            // >= iOS 6
            [self setMinimumScaleFactor:minimumFontSize / self.font.pointSize];
        } else {
            // <= iOS 5
            [self setMinimumFontSize:minimumFontSize];
        }
#endif
    }
}

- (void)setAdjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth minimumFontSize:(CGFloat)minimumFontSize baselineAdjustment:(UIBaselineAdjustment)baselineAdjustment
{
    [self setAdjustsFontSizeToFitWidth:adjustsFontSizeToFitWidth minimumFontSize:minimumFontSize];
    // Default is UIBaselineAdjustmentAlignBaselines
    [self setBaselineAdjustment:baselineAdjustment];
}


@end
