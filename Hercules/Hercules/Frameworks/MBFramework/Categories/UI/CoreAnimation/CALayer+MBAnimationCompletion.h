//
//  CALayer+MBAnimationCompletion.h
//  MBFramework
//
//  Created by Arno Woestenburg on 28/04/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

typedef void(^CAAnimationCompletion)(BOOL finished);

/**
 This category adds methods to the UIKit framework’s CALayer class.
 */
@interface CALayer (AnimationCompletion)

/**
 Add the specified animation object to the layer’s render tree.
 @param animation The animation to be added to the render tree. This object is copied by the render tree, not referenced. Therefore, subsequent modifications to the object are not propagated into the render tree.
 @param completion A block object to be executed when the animation sequence ends. This block has no return value and takes a single Boolean argument that indicates whether or not the animations actually finished before the completion handler was called. This parameter may be NULL.
 */
- (void)addAnimation:(CAAnimation *)animation completion:(CAAnimationCompletion)completion;

@end
