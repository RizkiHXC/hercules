//
//  CALayer+MBAnimationCompletion.m
//  MBFramework
//
//  Created by Arno Woestenburg on 28/04/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "CALayer+MBAnimationCompletion.h"
#import <objc/runtime.h>

@implementation CALayer (AnimationCompletion)

static id kCALayer_animationCompletionAssociationKey;
static NSString *kDefaultAnimationKey = @"CALayer_defaultAnimationKey";

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)addAnimation:(CAAnimation *)animation completion:(CAAnimationCompletion)completion
{
    // Set runtime association of object
    // Param - source object for association, association key, association value, policy of association
    objc_setAssociatedObject(self, &kCALayer_animationCompletionAssociationKey, completion, OBJC_ASSOCIATION_COPY_NONATOMIC);

    [animation setDelegate:self];
    [animation setRemovedOnCompletion:NO];
    [self addAnimation:animation forKey:kDefaultAnimationKey];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - CAAnimation delegate

- (void)animationDidStart:(CAAnimation *)animation
{ }

- (void)animationDidStop:(CAAnimation *)animation finished:(BOOL)finished
{
    if (animation == [self animationForKey:kDefaultAnimationKey]) {
        CAAnimationCompletion completion = objc_getAssociatedObject(self, &kCALayer_animationCompletionAssociationKey);
        if (completion) {
            completion(finished);
        }
        // Clean up block reference and remove animation
        objc_setAssociatedObject(self, &kCALayer_animationCompletionAssociationKey, nil, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self removeAnimationForKey:kDefaultAnimationKey];
    }
}

@end
