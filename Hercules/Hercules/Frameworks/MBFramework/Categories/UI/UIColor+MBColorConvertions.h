//
//  UIColor+MBColorConvertions.h
//  MBFramework
//
//  Created by Marco Jonker on 2/19/13.
//
//

#import <UIKit/UIKit.h>

#define RGB(r, g, b) \
[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBA(r, g, b, a) \
[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

/**
 *  This category adds methods to the UIKit framework’s UIColor class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface UIColor (MBColorConvertions)

@property (nonatomic, readonly) CGFloat red;
@property (nonatomic, readonly) CGFloat green;
@property (nonatomic, readonly) CGFloat blue;
@property (nonatomic, readonly) CGFloat alpha;
@property (nonatomic, readonly) CGFloat hue         NS_AVAILABLE_IOS(5_0);
@property (nonatomic, readonly) CGFloat saturation  NS_AVAILABLE_IOS(5_0);
@property (nonatomic, readonly) CGFloat brightness  NS_AVAILABLE_IOS(5_0);

+(UIColor*)colorWithHexString:(NSString*)hexString;
+(NSString*)rgbStringWithColor:(UIColor*)color;

@end
