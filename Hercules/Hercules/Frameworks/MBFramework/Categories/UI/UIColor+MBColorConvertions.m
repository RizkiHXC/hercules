//
//  UIColor+MBColorConvertions.m
//  MBFramework
//
//  Created by Marco Jonker on 2/19/13.
//
//

#import "UIColor+MBColorConvertions.h"

@implementation UIColor (MBColorConvertions)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

-(void)MB_getRed:(CGFloat *)red green:(CGFloat *)green blue:(CGFloat *)blue alpha:(CGFloat *)alpha {
    if ([self respondsToSelector:@selector(getRed:green:blue:alpha:)]) {
        [self getRed:red green:green blue:blue alpha:alpha];
    } else {
        // < iOS 5
        const CGFloat *components = CGColorGetComponents(self.CGColor);
        *red     = components[0];
        *green   = components[1];
        *blue    = components[2];
        *alpha   = components[3];
    }
}

-(void)MB_getHue:(CGFloat *)hue saturation:(CGFloat *)saturation brightness:(CGFloat *)brightness alpha:(CGFloat *)alpha {
    if ([self respondsToSelector:@selector(getHue:saturation:brightness:alpha:)]) {
        [self getHue:hue saturation:saturation brightness:brightness alpha:alpha];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(CGFloat)red {
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
    [self MB_getRed:&red green:&green blue:&blue alpha:&alpha];
    return red;
}

-(CGFloat)green {
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
    [self MB_getRed:&red green:&green blue:&blue alpha:&alpha];
    return green;
}

-(CGFloat)blue {
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
    [self MB_getRed:&red green:&green blue:&blue alpha:&alpha];
    return blue;
}

-(CGFloat)alpha {
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
    [self MB_getRed:&red green:&green blue:&blue alpha:&alpha];
    return alpha;
}

-(CGFloat)hue {
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    [self MB_getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    return hue;
}

-(CGFloat)saturation {
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    [self MB_getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    return saturation;
}

-(CGFloat)brightness {
    CGFloat hue = 0.0, saturation = 0.0, brightness = 0.0, alpha = 0.0;
    [self MB_getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    return brightness;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static functions

+(UIColor*)colorWithHexString:(NSString*)hexString {
    NSString *tempString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    //Convert tree chacterstring to 6 character string if needed
    if([tempString length] == 3) {
        tempString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [tempString substringWithRange:NSMakeRange(0, 1)],[tempString substringWithRange:NSMakeRange(0, 1)],
                       [tempString substringWithRange:NSMakeRange(1, 1)],[tempString substringWithRange:NSMakeRange(1, 1)],
                       [tempString substringWithRange:NSMakeRange(2, 1)],[tempString substringWithRange:NSMakeRange(2, 1)]];
    }
    
    //Add alpha characters if not available
    if([tempString length] == 6) {
        tempString = [tempString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:tempString] scanHexInt:&baseValue];
    
    //Convert characters to RGB
    float red   = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue  = ((baseValue >> 8)  & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0)  & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+(NSString*)rgbStringWithColor:(UIColor*)color {
    unsigned int red    = (unsigned int)(255.0 * color.red);
    unsigned int green  = (unsigned int)(255.0 * color.green);
    unsigned int blue   = (unsigned int)(255.0 * color.blue);
    
    return [NSString stringWithFormat:@"rgb(%i,%i,%i)", red, green, blue];
}

@end
