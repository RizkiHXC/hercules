//
//  UIImage+MBFramework.h
//  MBFramework
//
//  Created by Arno Woestenburg on 20/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This category adds methods to the UIKit framework’s UIImage class. 
 */
@interface UIImage (MBFramework)

// Class methods

/**
 Creates and returns a resizable image object associated with the specified filename.
 @param name The name of the file. If this is the first time the image is being loaded, the method looks for an image with the specified name in the application’s main bundle.
 @return The image object for the specified file, or nil if the method could not find the specified image.
 @discussion You use this method to add cap insets to an image or to change the existing cap insets of an image. In both cases, you get back a new image and the original image remains untouched.
 
 The cap widths area calculated by subtracting one from the image's width then dividing by 2. 
 
 During scaling or resizing of the image, areas covered by a cap are not scaled or resized. Instead, the pixel area not covered by the cap in each direction is tiled, left-to-right and top-to-bottom, to resize the image. This technique is often used to create variable-width buttons, which retain the same rounded corners but whose center region grows or shrinks as needed. For best performance, use a tiled area that is a 1x1 pixel area in size.
 */
+ (UIImage *)resizableImageNamed:(NSString *)name;
/**
 Creates and returns a resizable image object associated with the specified filename.
 @param name The name of the file. If this is the first time the image is being loaded, the method looks for an image with the specified name in the application’s main bundle.
 @param resizingMode The mode with which the interior of the image is resized.
 @return The image object for the specified file, or nil if the method could not find the specified image.
 @discussion You use this method to add cap insets to an image or to change the existing cap insets of an image. In both cases, you get back a new image and the original image remains untouched.
 
 The cap widths area calculated by subtracting one from the image's width then dividing by 2. 
 
 During scaling or resizing of the image, areas covered by a cap are not scaled or resized. Instead, the pixel area not covered by the cap in each direction is tiled, left-to-right and top-to-bottom, to resize the image. This technique is often used to create variable-width buttons, which retain the same rounded corners but whose center region grows or shrinks as needed. For best performance, use a tiled area that is a 1x1 pixel area in size.
 */
+ (UIImage *)resizableImageNamed:(NSString *)name resizingMode:(UIImageResizingMode)resizingMode;

/**
 Creates and returns an image object with the specified size and color.
 @param color The fill color for the image.
 @param size  The dimensions of the image measured in points.
 @discussion The scale factor is set to the scale factor of the device’s main screen.
 @return A new image object specified size and color, or nil if the image couldn't be created.
 */
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
/**
 Creates and returns an image object with the specified size and color.
 @param color The fill color for the image.
 @param size  The dimensions of the image measured in points.
 @param cornerRadius The radius used to draw the image.
 @discussion The scale factor is set to the scale factor of the device’s main screen.
 @return A new image object specified size and color, or nil if the image couldn't be created.
 */
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size cornerRadius:(CGFloat)cornerRadius;

// Instance methods

/**
 Create a alpha-blended version of this image.
 @param alpha The desired opacity of the image, specified as a value between 0.0 and 1.0. A value of 0.0 renders the image totally transparent while 1.0 renders it fully opaque. Values larger than 1.0 are interpreted as 1.0.
 @return A new image object with the specified alpha.
 */
- (UIImage *)imageWithAlpha:(CGFloat)alpha;
/**
 Creates and returns a new image rendered with the specified tint color.
 @param tintColor A color used to tint the image.
 @return A new image object rendered with the specified tint color.
 */
- (UIImage *)imageWithTintColor:(UIColor *)tintColor;

@end
