//
//  UIImage+MBFramework.m
//  MBFramework
//
//  Created by Arno Woestenburg on 20/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "UIImage+MBFramework.h"

@implementation UIImage (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (UIImage *)resizableImageNamed:(NSString *)name
{
    UIImage *image = [UIImage imageNamed:name];
    CGFloat horzInset = floorf((image.size.width - 1) * 0.5);
    CGFloat vertInset = floorf((image.size.height - 1) * 0.5);
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(vertInset, horzInset, vertInset, horzInset)];
}

+ (UIImage *)resizableImageNamed:(NSString *)name resizingMode:(UIImageResizingMode)resizingMode
{
    UIImage *image = [UIImage imageNamed:name];
    CGFloat horzInset = floorf((image.size.width - 1) * 0.5);
    CGFloat vertInset = floorf((image.size.height - 1) * 0.5);
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0
    // >= iOS 6
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(vertInset, horzInset, vertInset, horzInset) resizingMode:resizingMode];
#else 
    // < iOS 6
    if ([[UIImage class] instancesRespondToSelector:@selector(resizableImageWithCapInsets:resizingMode:)]) {
        image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(vertInset, horzInset, vertInset, horzInset) resizingMode:resizingMode];
    } else {
        image = [image stretchableImageWithLeftCapWidth:horzInset topCapHeight:vertInset];
    }
#endif
    return image;
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    return [self imageWithColor:color size:size cornerRadius:0];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size cornerRadius:(CGFloat)cornerRadius
{
    CGRect rect = CGRectMake(0.0, 0.0, size.width, size.height);
    if (color != nil) {
        // Image is opaque if alpha is 1.0 and the image has no corner radius.
        CGFloat alpha = 0.0f;
        [color getRed:nil green:nil blue:nil alpha:&alpha];
        BOOL opaque = (alpha >= 1.0f) && (cornerRadius <= 0.0f);
        // Image with fill color; the scale factor is automatically set to the scale factor of the device’s main screen.
        UIGraphicsBeginImageContextWithOptions(rect.size, opaque, 0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        if (cornerRadius > 0) {
            // Add a clip before drawing anything, in the shape of an rounded rect
            [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:cornerRadius] addClip];
        }
        
        CGContextSetFillColorWithColor(context, [color CGColor]);
        CGContextFillRect(context, rect);
    } else {
        // Transparent image
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0f);
    }
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Instance methods

- (UIImage *)imageWithAlpha:(CGFloat)alpha
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, self.size.width, self.size.height);

    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -area.size.height);

    CGContextSetBlendMode(context, kCGBlendModeMultiply);

    CGContextSetAlpha(context, alpha);

    CGContextDrawImage(context, area, self.CGImage);

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return newImage;
}

- (UIImage *)imageWithTintColor:(UIColor *)tintColor
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextClipToMask(context, rect, self.CGImage);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


@end
