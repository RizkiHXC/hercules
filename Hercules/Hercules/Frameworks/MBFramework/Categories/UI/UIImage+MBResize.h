//
//  UIImage+MBResize.h
//  MBFramework
//
//  Created by Arno Woestenburg on 31/01/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Specifies the possible resize options for an image.
 */
typedef NS_OPTIONS(NSUInteger, MBImageResizeOptions)
{
    /**
     Default. Image scaled to fit.
     */
    MBImageResizeOptionScaleToFill          = 0 << 0,
    /**
     Image scaled to fit with fixed aspect.
     */
    MBImageResizeOptionScaleAspectFit       = 1 << 0,
    /**
     Image scaled to fill with fixed aspect. Some portion of content may be clipped.
     */
    MBImageResizeOptionScaleAspectFill      = 2 << 0,
    /**
     Don't enlarge the image when it's smaller than the required size
     */
    MBImageResizeOptionNoScaleIfSmaller     = 1 << 4,
    /**
     Let the context decide interpolation quality.
     */
    MBImageResizeOptionInterpolationDefault = 0 << 8,
    /**
     Never interpolate.
     */
    MBImageResizeOptionInterpolationNone    = 1 << 8,
    /**
     Low quality, fast interpolation.
     */
    MBImageResizeOptionInterpolationLow     = 2 << 8,
    /**
     Medium quality, slower than MBImageResizeOptionInterpolationLow.
     */
    MBImageResizeOptionInterpolationMedium  = 3 << 8,
    /**
     Highest quality, slower than MBImageResizeOptionInterpolationMedium.
     */
    MBImageResizeOptionInterpolationHigh    = 4 << 8,
};

#define MBImageResizeScaleMask              (MBImageResizeOptionScaleToFill | MBImageResizeOptionScaleAspectFit | MBImageResizeOptionScaleAspectFill)
#define MBImageResizeInterpolationMask      (MBImageResizeOptionInterpolationDefault | MBImageResizeOptionInterpolationNone | MBImageResizeOptionInterpolationLow | MBImageResizeOptionInterpolationMedium | MBImageResizeOptionInterpolationHigh)

/**
 This category adds methods to the UIKit framework’s UIImage class. The methods in this category provide support for resizing images to a specified size.
 */
@interface UIImage(MBResize)

/**
 Creates and returns a new image object with the specified size.
 @param size The desired dimensions of the image.
 @return A new image object with the specified size.
 */
- (UIImage *)resizedImageToSize:(CGSize)size;
/**
 Creates and returns a new image object with the specified size and options.
 @param size The desired dimensions of the image.
 @param options The resize option to use when resizing the image.
 @return A new image object with the specified size.
 @see MBImageResizeOptions
 */
- (UIImage *)resizedImageToSize:(CGSize)size withResizeOptions:(MBImageResizeOptions)options;
/**
 Creates and returns a new image object cropped with the specified rectangle.
 @param rect The rectangle (in the coordinate system of the graphics context) in which to crop the image.
 @return A new image object cropped with the specified rectangle.
 */
- (UIImage *)croppedImageWithRect:(CGRect)rect;

@end
