//
//  UIImage+MBResize.h
//  MBFramework
//
//  Created by Arno Woestenburg on 31/01/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//
//  This code is based, although heavily modified, on the Image resize category
//  created by Olivier Halligon on 12/08/09
//

#import "UIImage+MBResize.h"

@implementation UIImage (MBResize)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (UIImage *)resizedImageToSize:(CGSize)size withResizeOptions:(MBImageResizeOptions)options
{
	// Get the image size (independant of imageOrientation)
	CGImageRef imageRef = self.CGImage;
    // Not equivalent to self.size (which depends on the imageOrientation)
	CGSize originalSize = CGSizeMake(CGImageGetWidth(imageRef), CGImageGetHeight(imageRef));
    originalSize.width /= self.scale;
    originalSize.height /= self.scale;
    
	// Adjust size to make it independant on imageOrientation too for farther computations
	switch (self.imageOrientation) {
		case UIImageOrientationLeft:
		case UIImageOrientationRight:
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRightMirrored:
			size = CGSizeMake(size.height, size.width);
			break;
        default:
            break;
	}
    
    NSInteger scaleMode = (options & MBImageResizeScaleMask);
    
	// Compute the target CGRect in order to keep aspect-ratio
	CGRect targetRect = CGRectZero;
    CGSize scaleRatio = CGSizeMake(size.width / originalSize.width, size.height / originalSize.height);
	
	if ( (options & MBImageResizeOptionNoScaleIfSmaller) && (originalSize.width < size.width) && (originalSize.height < size.height) ) {
        // No resize (we could directly return 'self' here, but we draw the image anyway to take image orientation into account)
		targetRect.size = originalSize;
    } else {
        
        switch (scaleMode) {
            case MBImageResizeOptionScaleAspectFit:
                // Image scaled to fit with fixed aspect.
                if (scaleRatio.width < scaleRatio.height) {
                    //NSLog(@"Width imposed, Height scaled ; ratio = %f",wRatio);
                    scaleRatio.height = scaleRatio.width;
                    targetRect.size = CGSizeMake(size.width, floorf(originalSize.height * scaleRatio.width));
                } else {
                    //NSLog(@"Height imposed, Width scaled ; ratio = %f",hRatio);
                    scaleRatio.width = scaleRatio.height;
                    targetRect.size = CGSizeMake(floorf(originalSize.width * scaleRatio.height), size.height);
                }
                break;
            case MBImageResizeOptionScaleAspectFill:
                // Image scaled to fill with fixed aspect. Some portion of content may be clipped.
                if (scaleRatio.width > scaleRatio.height) {
                    scaleRatio.height = scaleRatio.width;
                    //NSLog(@"Width imposed, Height scaled ; ratio = %f",wRatio);
                    targetRect.size = CGSizeMake(size.width, floorf(originalSize.height * scaleRatio.width));
                    targetRect.origin.y = roundf((size.height - targetRect.size.height) * 0.5);
                    targetRect.size.height = MIN(size.height, targetRect.size.height);
                } else {
                    scaleRatio.width = scaleRatio.height;
                    //NSLog(@"Height imposed, Width scaled ; ratio = %f",hRatio);
                    targetRect.size = CGSizeMake(floorf(originalSize.width * scaleRatio.height), size.height);
                    targetRect.origin.x = roundf((size.width - targetRect.size.width) * 0.5);
                    targetRect.size.width = MIN(size.width, targetRect.size.width);
                }
                break;
            default:
                // MBImageResizeOptionScaleToFill
                targetRect.size = size;
                break;
        }
	}
	
    // Don't resize if we already meet the required destination size.
    if (CGSizeEqualToSize(originalSize, targetRect.size)) {
        return self;
    }
    
	CGAffineTransform transform = CGAffineTransformIdentity;
	switch (self.imageOrientation) {
			
		case UIImageOrientationUp: //EXIF = 1
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
			transform = CGAffineTransformMakeTranslation(originalSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
			transform = CGAffineTransformMakeTranslation(originalSize.width, originalSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
			transform = CGAffineTransformMakeTranslation(0.0, originalSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
			targetRect.size = CGSizeMake(targetRect.size.height, targetRect.size.width);
			transform = CGAffineTransformMakeTranslation(originalSize.height, originalSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI_2);
			break;
			
		case UIImageOrientationLeft: //EXIF = 6
			targetRect.size = CGSizeMake(targetRect.size.height, targetRect.size.width);
			transform = CGAffineTransformMakeTranslation(0.0, originalSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI_2);
			break;
			
		case UIImageOrientationRightMirrored: //EXIF = 7
			targetRect.size = CGSizeMake(targetRect.size.height, targetRect.size.width);
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
			break;
			
		case UIImageOrientationRight: //EXIF = 8
			targetRect.size = CGSizeMake(targetRect.size.height, targetRect.size.width);
			transform = CGAffineTransformMakeTranslation(originalSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
			break;
			
		default:
			[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			
	}
	
	// Draw the image on a new context, applying a transform matrix
	UIGraphicsBeginImageContextWithOptions(targetRect.size, NO, self.scale);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Interpolation mode
    switch (options & MBImageResizeInterpolationMask) {
        case MBImageResizeOptionInterpolationNone:
            CGContextSetInterpolationQuality(context, kCGInterpolationNone);
            break;
        case MBImageResizeOptionInterpolationLow:
            CGContextSetInterpolationQuality(context, kCGInterpolationLow);
            break;
        case MBImageResizeOptionInterpolationMedium:
            CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
            break;
        case MBImageResizeOptionInterpolationHigh:
            CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
            break;
        default:
            // MBImageResizeOptionInterpolationDefault
            CGContextSetInterpolationQuality(context, kCGInterpolationDefault);
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationRight:
        case UIImageOrientationLeft:
            CGContextScaleCTM(context, -scaleRatio.width, scaleRatio.height);
            CGContextTranslateCTM(context, -originalSize.height, 0);
            break;
        default:
            CGContextScaleCTM(context, scaleRatio.width, -scaleRatio.height);
            CGContextTranslateCTM(context, 0, -originalSize.height);
            break;
    }
    
    if (scaleMode == MBImageResizeOptionScaleAspectFill) {
        // Translate the filled image
        CGSize translate = CGSizeZero;
        translate.height = targetRect.origin.y / scaleRatio.height;
        translate.width = targetRect.origin.x / scaleRatio.width;
        CGContextTranslateCTM(context, translate.width, -translate.height);
    }
    
	CGContextConcatCTM(context, transform);
	
	// originalSize is used (and not size) as the size to specify is in user space (and the CTM is used to apply a scaleRatio).
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, originalSize.width, originalSize.height), imageRef);
	UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	return resizedImage;
}

- (UIImage *)resizedImageToSize:(CGSize)size
{
    return [self resizedImageToSize:size withResizeOptions:MBImageResizeOptionScaleAspectFit | MBImageResizeOptionNoScaleIfSmaller];
}

- (UIImage *)croppedImageWithRect:(CGRect)rect
{
    if (self.scale != 1.0) {
        rect.origin.x *= self.scale;
        rect.origin.y *= self.scale;
        rect.size.width *= self.scale;
        rect.size.height *= self.scale;
    }
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return croppedImage;
}


@end
