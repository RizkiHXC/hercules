//
//  UIView+MBFramework.h
//  MBFramework
//
//  Created by Marco Jonker on 2/24/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  This category adds methods to the UIKit framework’s UIView class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface UIView (MBFramework)

- (void)setHidden:(BOOL)hidden animated:(BOOL)animated;

-(UIView*)subViewOfKindOfClass:(Class)class deepSearch:(BOOL)deepSearch;
+(UIView*)subViewInView:(UIView*)view ofKindOfClass:(Class)class deepSearch:(BOOL)deepSearch;

-(UIView*)findFirstResponder;
+(UIView*)findFirstResponderInView:(UIView*)view;

@end
