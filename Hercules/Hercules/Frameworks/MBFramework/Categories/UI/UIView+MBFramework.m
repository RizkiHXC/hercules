//
//  UIView+MBFramework.m
//  MBFramework
//
//  Created by Marco Jonker on 2/24/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "UIView+MBFramework.h"

@implementation UIView (MBFramework)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

- (void)setHidden:(BOOL)hidden animated:(BOOL)animated
{
    if (self.isHidden != hidden) {
        if (animated) {
            [UIView transitionWithView:self
                              duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:nil];
            [self setHidden:hidden];
        } else {
            [self setHidden:hidden];
        }
    }
}

-(UIView*)subViewOfKindOfClass:(Class)class deepSearch:(BOOL)deepSearch {
    UIView* resultView = nil;
    
    for(UIView* subView in self.subviews) {
        if([subView isKindOfClass:class]) {
            resultView = subView;
            break;
        } else if(deepSearch == YES){
            resultView = [subView subViewOfKindOfClass:class deepSearch:deepSearch];
        }
        
        if(resultView != nil) {
            break;
        }
        
    }
    
    return resultView;
}

- (UIView*)findFirstResponder
{
    UIView* resultView = nil;
    
    for(UIView* subView in self.subviews) {
        if(subView.isFirstResponder == YES) {
            resultView = subView;
        } else {
            resultView = [subView findFirstResponder];
        }
        
        if(resultView != nil) {
            break;
        }
    }
    
    return resultView;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public static functions

+(UIView*)subViewInView:(UIView*)view ofKindOfClass:(Class)class deepSearch:(BOOL)deepSearch {
    return [view subViewOfKindOfClass:class deepSearch:deepSearch];
}

+(UIView*)findFirstResponderInView:(UIView*)view {
    return [view findFirstResponder];
}

@end
