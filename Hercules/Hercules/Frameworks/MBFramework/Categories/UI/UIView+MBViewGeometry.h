//
//  UIView+MBViewGeometry.h
//  MBFramework
//
//  Created by Arno Woestenburg on 04/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  This category adds methods to the UIKit framework’s UIView class. The methods in this category provide convenience methods for view geometry and layout properties.
 */
@interface UIView (MBViewGeometry)

///------------------------------------------------
/// @name Configuring the Bounds and Frame Rectangles
///------------------------------------------------

/**
 *  A point that specifies the coordinates of the view's origin.
 *  @return The coordinates of the view's origin.
 */
- (CGPoint)origin;
/**
 *  Sets the coordinates of the view's origin.
 *  @param origin The coordinates of the view's origin.
 */
- (void)setOrigin:(CGPoint)origin;

/**
 *  A point that specifies the X-coordinate of the view's origin.
 *  @return The X-coordinate of the view's origin.
 */
- (CGFloat)originX;
/**
 *  Sets the X-coordinate of the view's origin.
 *  @param originX The X-coordinate of the view's origin.
 */
- (void)setOriginX:(CGFloat)originX;

/**
 *  A point that specifies the Y-coordinate of the view's origin.
 *  @return The Y-coordinate of the view's origin.
 */
- (CGFloat)originY;
/**
 *  Sets the Y-coordinate of the view's origin.
 *  @param originY The Y-coordinate of the view's origin.
 */
- (void)setOriginY:(CGFloat)originY;

/**
 *  A size that specifies the height and width of the view.
 *  @return The size that specifies the height and width of the view.
 */
- (CGSize)size;
/**
 *  Sets the size that specifies the height and width of the view.
 *  @param size The height and width of the view.
 */
- (void)setSize:(CGSize)size;

/**
 *  Specifies the width of the view.
 *  @return The width of the view.
 */
- (CGFloat)width;
/**
 *  Sets the width of the view.
 *  @param width The width of the view.
 */
- (void)setWidth:(CGFloat)width;

/**
 *  Specifies the height of the view.
 *  @return The height of the view.
 */
- (CGFloat)height;
/**
 *  Sets the height of the view.
 *  @param height The height of the view.
 */
- (void)setHeight:(CGFloat)height;

///------------------------------------------------
/// @name Other methods
///------------------------------------------------

/**
 *  Asks the view to calculate and return the size that best fits its subviews.
 */
- (CGSize)sizeThatFitsSubviews;
/**
 *  Resizes and moves the view so it just encloses its subviews.
 */
- (void)sizeToFitSubviews;

@end
