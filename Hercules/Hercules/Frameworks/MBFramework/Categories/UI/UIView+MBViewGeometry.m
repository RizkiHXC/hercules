//
//  UIView+MBViewGeometry.m
//  MBFramework
//
//  Created by Arno Woestenburg on 04/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "UIView+MBViewGeometry.h"

@implementation UIView (MBViewGeometry)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (CGPoint)origin
{
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin
{
    [self setFrame:CGRectMake(origin.x, origin.y, self.frame.size.width, self.frame.size.height)];
}

- (CGFloat)originX
{
    return self.frame.origin.x;
}

- (void)setOriginX:(CGFloat)originX
{
    [self setFrame:CGRectMake(originX, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
}

- (CGFloat)originY
{
    return self.frame.origin.y;
}

- (void)setOriginY:(CGFloat)originY
{
    [self setFrame:CGRectMake(self.frame.origin.x, originY, self.frame.size.width, self.frame.size.height)];
}

- (CGSize)size
{
    return self.bounds.size;
}

- (void)setSize:(CGSize)size
{
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height)];
}

- (CGFloat)width
{
    return self.bounds.size.width;
}

- (void)setWidth:(CGFloat)width
{
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, width, self.frame.size.height)];
}

- (CGFloat)height
{
    return self.bounds.size.height;
}

- (void)setHeight:(CGFloat)height
{
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height)];
}

- (CGSize)sizeThatFitsSubviews
{
    CGRect bounds = CGRectZero;
    for (UIView *subview in [self subviews]) {
        bounds = CGRectUnion(bounds, subview.frame);
    }
    return bounds.size;
}

- (void)sizeToFitSubviews
{
    CGRect frame = self.frame;
    frame.size = [self sizeThatFitsSubviews];
    [self setFrame:frame];
}


@end
