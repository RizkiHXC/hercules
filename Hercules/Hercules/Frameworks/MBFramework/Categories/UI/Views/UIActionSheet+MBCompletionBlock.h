//
//  UIActionSheet+MBCompletionBlock.h
//  MBFramework
//
//  Created by Arno Woestenburg on 8/12/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UIActionSheetCallBackHandler)(NSInteger clickedButtonIndex);

/**
 *  This category adds methods to the UIKit framework’s UIActionSheet class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface UIActionSheet (MBCompletionBlock) <UIActionSheetDelegate>

- (void)showFromToolbar:(UIToolbar *)view completion:(UIActionSheetCallBackHandler)completion;
- (void)showFromTabBar:(UITabBar *)view completion:(UIActionSheetCallBackHandler)completion;
- (void)showFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated completion:(UIActionSheetCallBackHandler)completion;
- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated completion:(UIActionSheetCallBackHandler)completion;
- (void)showInView:(UIView *)view completion:(UIActionSheetCallBackHandler)completion;

@end
