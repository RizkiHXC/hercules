//
//  UIActionSheet+MBCompletionBlock.m
//  MBFramework
//
//  Created by Arno Woestenburg on 8/12/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "UIActionSheet+MBCompletionBlock.h"
#import <objc/runtime.h>

@implementation UIActionSheet (MBCompletionBlock)

static id UIActionSheet_completionAssociationKey;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)showFromToolbar:(UIToolbar *)view completion:(UIActionSheetCallBackHandler)completion
{
    // Set runtime association of object
    // Param - source object for association, association key, association value, policy of association
    objc_setAssociatedObject(self, &UIActionSheet_completionAssociationKey, completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [self setDelegate:self];
    [self showFromToolbar:view];
}

- (void)showFromTabBar:(UITabBar *)view completion:(UIActionSheetCallBackHandler)completion
{
    // Set runtime association of object
    // Param - source object for association, association key, association value, policy of association
    objc_setAssociatedObject(self, &UIActionSheet_completionAssociationKey, completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [self setDelegate:self];
    [self showFromTabBar:view];
}

- (void)showFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated completion:(UIActionSheetCallBackHandler)completion
{
    // Set runtime association of object
    // Param - source object for association, association key, association value, policy of association
    objc_setAssociatedObject(self, &UIActionSheet_completionAssociationKey, completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [self setDelegate:self];
    [self showFromBarButtonItem:item animated:animated];
}

- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated completion:(UIActionSheetCallBackHandler)completion
{
    // Set runtime association of object
    // Param - source object for association, association key, association value, policy of association
    objc_setAssociatedObject(self, &UIActionSheet_completionAssociationKey, completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [self setDelegate:self];
    [self showFromRect:rect inView:view animated:animated];
}

- (void)showInView:(UIView *)view completion:(UIActionSheetCallBackHandler)completion
{
    // Set runtime association of object
    // Param - source object for association, association key, association value, policy of association
    objc_setAssociatedObject(self, &UIActionSheet_completionAssociationKey, completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [self setDelegate:self];
    [self showInView:view];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Completion handler
    UIActionSheetCallBackHandler completion = objc_getAssociatedObject(self, &UIActionSheet_completionAssociationKey);
    if (completion) {
        completion(buttonIndex);
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Clean up block reference
    UIActionSheetCallBackHandler completion = objc_getAssociatedObject(self, &UIActionSheet_completionAssociationKey);
    if (completion) {
        objc_setAssociatedObject(self, &UIActionSheet_completionAssociationKey, nil, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
}



@end
