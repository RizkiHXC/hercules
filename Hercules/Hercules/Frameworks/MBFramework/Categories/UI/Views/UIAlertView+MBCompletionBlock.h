//
//  UIAlertView+MBCompletionBlock.h
//  MBFramework
//
//  Created by Arno Woestenburg on 8/12/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UIAlertViewCallBackHandler)(NSInteger clickedButtonIndex);

/**
 *  This category adds methods to the UIKit framework’s UIAlertView class.
 *
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface UIAlertView (MBCompletionBlock) <UIAlertViewDelegate>

- (void)showWithCompletion:(UIAlertViewCallBackHandler)completion;

@end
