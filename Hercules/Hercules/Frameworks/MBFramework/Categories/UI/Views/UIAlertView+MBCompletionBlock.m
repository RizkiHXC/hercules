//
//  UIAlertView+MBCompletionBlock.m
//  MBFramework
//
//  Created by Arno Woestenburg on 8/12/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "UIAlertView+MBCompletionBlock.h"
#import <objc/runtime.h>

@implementation UIAlertView (MBCompletionBlock)

static id kUIAlertView_completionAssociationKey;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)showWithCompletion:(UIAlertViewCallBackHandler)completion
{
    // Set runtime association of object
    // Param - source object for association, association key, association value, policy of association
    objc_setAssociatedObject(self, &kUIAlertView_completionAssociationKey, completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    [self setDelegate:self];
    [self show];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIAlertViewCallBackHandler completion = objc_getAssociatedObject(self, &kUIAlertView_completionAssociationKey);
    if (completion) {
        completion(buttonIndex);
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Clean up block reference
    UIAlertViewCallBackHandler completion = objc_getAssociatedObject(self, &kUIAlertView_completionAssociationKey);
    if (completion) {
        objc_setAssociatedObject(self, &kUIAlertView_completionAssociationKey, nil, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
}


@end
