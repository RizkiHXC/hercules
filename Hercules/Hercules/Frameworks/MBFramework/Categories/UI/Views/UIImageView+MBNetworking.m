//
//  UIImageView+MBNetworking.m
//  MBFramework
//
//  Created by Arno Woestenburg on 01/10/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "UIImageView+MBNetworking.h"
#import "MBRectFunctions.h"

@implementation UIImageView (MBNetworking)

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholderImage
                success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self setImageWithURLRequest:request placeholderImage:placeholderImage success:success failure:failure];
}

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholderImage
  activityIndicatorView:(UIView *)activityIndicatorView
                success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure
{
    if (url.absoluteString.length > 0) {
        if (activityIndicatorView) {
            [activityIndicatorView setFrame:[MBRectFunctions alignRect:activityIndicatorView.bounds toRect:self.bounds alignMode:MBAlignmentCenter]];
            [activityIndicatorView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin];
            [self addSubview:activityIndicatorView];
            SEL startAnimateSelector = @selector(startAnimating);
            // Start animating if supported
            if ([activityIndicatorView respondsToSelector:startAnimateSelector]) {
                [activityIndicatorView performSelector:startAnimateSelector];
            }
        }
        
        [self setImageWithURL:url
             placeholderImage:placeholderImage
                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                          [activityIndicatorView removeFromSuperview];
                          if (success) {
                              success(request, response, image);
                          }
                      }
                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                          [activityIndicatorView removeFromSuperview];
                          if (failure) {
                              failure(request, response, error);
                          }
                      }];
    } else {
        // No url
        [self setImage:placeholderImage];
    }
}


@end
