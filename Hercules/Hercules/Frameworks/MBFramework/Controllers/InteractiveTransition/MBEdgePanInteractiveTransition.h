//
//  MBEdgePanInteractiveTransition.h
//  MBFramework
//
//  Created by Arno Woestenburg on 28/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBInteractiveTransition.h"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBEdgePanInteractiveTransition : MBInteractiveTransition
{
    
}

@end
