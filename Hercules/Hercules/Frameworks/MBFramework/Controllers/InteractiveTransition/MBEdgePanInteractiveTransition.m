//
//  MBEdgePanInteractiveTransition.m
//  MBFramework
//
//  Created by Arno Woestenburg on 28/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBEdgePanInteractiveTransition.h"

@implementation MBEdgePanInteractiveTransition

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithViewController:(UIViewController *)viewController
{
    self = [super initWithViewController:viewController];
    if (self) {

    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (UIGestureRecognizer *)recognizerForInteraction
{
    UIScreenEdgePanGestureRecognizer *edgePanRecognizer = [[[UIScreenEdgePanGestureRecognizer alloc] init] autorelease];
    [edgePanRecognizer setEdges:UIRectEdgeLeft];
    return edgePanRecognizer;
}

- (void)handlePopRecognizer:(UIScreenEdgePanGestureRecognizer *)gestureRecognizer
{
    UIView *view = gestureRecognizer.view;
    CGPoint point = [gestureRecognizer translationInView:view];
    CGFloat progress = point.x / view.bounds.size.width;
    progress = MIN(1.0, MAX(0.0, progress));

    switch (gestureRecognizer.state) {
        case UIGestureRecognizerStateBegan:
            [self beginInteractiveTransition];
            break;
        case UIGestureRecognizerStateChanged:
            // Update the interactive transition's progress
            [self updateInteractiveTransition:progress];
            break;
        case UIGestureRecognizerStateEnded:
            // Calculate final progress with velocity
            progress = (point.x + [gestureRecognizer velocityInView:view].x) / view.bounds.size.width;
            progress = MIN(1.0, MAX(0.0, progress));
            
            // Finish or cancel the interactive transition
            if (progress > 0.5) {
                [self finishInteractiveTransition];
            } else {
                [self cancelInteractiveTransition];
            }
            break;
        case UIGestureRecognizerStateCancelled:
            // Cancel the interactive transition
            [self cancelInteractiveTransition];
            break;
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStatePossible:
        default:
            // No action
            break;
    }
}



@end
