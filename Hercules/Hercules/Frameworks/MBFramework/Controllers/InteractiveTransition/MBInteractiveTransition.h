//
//  MBInteractiveTransition.h
//  MBFramework
//
//  Created by Arno Woestenburg on 03/03/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBInteractiveTransition : UIPercentDrivenInteractiveTransition

/**
 *  Initializes and returns a newly allocated InteractiveTransition object with the specified frame rectangle.
 *
 *  @param viewController UIViewController object used for transition
 *
 *  @return An initialized InteractiveTransition object or nil if the object couldn't be created.
 */
- (instancetype)initWithViewController:(UIViewController *)viewController;

- (UIGestureRecognizer *)recognizerForInteraction;
- (void)handlePopRecognizer:(UIGestureRecognizer *)recognizer;
- (void)beginInteractiveTransition;

@property(nonatomic,assign) UIViewController *viewController;
@property(nonatomic,assign,getter = isInteractive) BOOL interactive;
@property(nonatomic,retain,readonly) UIGestureRecognizer *gestureRecognizer;

@end
