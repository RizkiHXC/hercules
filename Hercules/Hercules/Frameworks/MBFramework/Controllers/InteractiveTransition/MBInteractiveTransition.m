//
//  MBInteractiveTransition.m
//  MBFramework
//
//  Created by Arno Woestenburg on 03/03/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBInteractiveTransition.h"

@interface MBInteractiveTransition ()

@property(nonatomic,retain) UIGestureRecognizer *gestureRecognizer;

@end

@implementation MBInteractiveTransition

@synthesize gestureRecognizer = gestureRecognizer_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithViewController:(UIViewController *)viewController
{
    self.viewController = viewController;
    self = [super init];
    if (self) {
        [self MB_initialize];
    }
    return self;
}

- (instancetype)init
{
    NSAssert(self.viewController != nil, @"Use initWithViewController to initialize the class");
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    [self.viewController.view removeGestureRecognizer:self.gestureRecognizer];
    self.gestureRecognizer = nil;
    self.viewController = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_initialize
{
    NSAssert(self.viewController != nil, @"Use initWithViewController to initialize the class");
    
    self.gestureRecognizer = [self recognizerForInteraction];
    NSAssert(self.gestureRecognizer != nil, nil);
    
    // Remove all targets and actions from the receiver.
    [self.gestureRecognizer removeTarget:nil action:NULL];
    // Specify target
    [self.gestureRecognizer addTarget:self action:@selector(handlePopRecognizer:)];
    [self.viewController.view addGestureRecognizer:self.gestureRecognizer];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (UIGestureRecognizer *)recognizerForInteraction
{
    // Needs to be overridden by a subclass
    return nil;
}

- (void)handlePopRecognizer:(UIGestureRecognizer *)recognizer
{
    // Overridable by subclass
}

- (void)beginInteractiveTransition
{
    // Note: This function has no [super beginInteractiveTransition]
    self.interactive = YES;
    // Create a interactive transition and pop the view controller
    [self.viewController.navigationController popViewControllerAnimated:YES];
}

- (void)cancelInteractiveTransition
{
    [super cancelInteractiveTransition];
    self.interactive = NO;
}

- (void)finishInteractiveTransition
{
    [super finishInteractiveTransition];
    self.interactive = NO;
}

@end
