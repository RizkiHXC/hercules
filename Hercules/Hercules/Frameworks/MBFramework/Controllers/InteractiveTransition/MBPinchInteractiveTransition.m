//
//  MBPinchInteractiveTransition.m
//  MBFramework
//
//  Created by Arno Woestenburg on 28/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBPinchInteractiveTransition.h"

@interface MBPinchInteractiveTransition ()
{
    BOOL pinchWillPopViewController_;
    BOOL pinchDidFinishTransition_;
}
@end

@implementation MBPinchInteractiveTransition

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithViewController:(UIViewController *)viewController
{
    self = [super initWithViewController:viewController];
    if (self) {
        // Threshold value for pop when this progress value has been reached
        self.thresholdValueForPop = 0.8;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (UIGestureRecognizer *)recognizerForInteraction
{
    UIPinchGestureRecognizer *pinchRecognizer = [[[UIPinchGestureRecognizer alloc] init] autorelease];
    return pinchRecognizer;
}

- (void)handlePopRecognizer:(UIPinchGestureRecognizer *)gestureRecognizer
{
    CGFloat progress = (1.0 - gestureRecognizer.scale);
    progress = MIN(1.0, MAX(0.0, progress));
    
    switch (gestureRecognizer.state) {
        case UIGestureRecognizerStateBegan:
            [self beginInteractiveTransition];
            pinchWillPopViewController_ = NO;
            pinchDidFinishTransition_ = NO;
            break;
        case UIGestureRecognizerStateChanged:
            // Pinch will pop only when pinch scale direction is getting smaller (has a velocity)
            pinchWillPopViewController_ = (gestureRecognizer.velocity < 0);
            
            if (progress > self.thresholdValueForPop || progress >= 1.0) {
                pinchDidFinishTransition_ = YES; // Threshold value passed
                [self.gestureRecognizer setEnabled:NO];
            } else {
                [self updateInteractiveTransition:progress];
            }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
            if (pinchWillPopViewController_ || pinchDidFinishTransition_) {
                [self finishInteractiveTransition];
            } else {
                [self cancelInteractiveTransition];
            }
            [self.gestureRecognizer setEnabled:YES];
            break;
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStatePossible:
        default:
            // No action
            break;
    }
}


@end
