//
//  MBClassFactory.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/6/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBClassFactory : NSObject

+ (Class)classNamed:(NSString *)className;
+ (Class)fromClass:(Class)aClass;

@end
