//
//  MBClassFactory.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/6/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBClassFactory.h"

@implementation MBClassFactory

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

+ (Class)fromClass:(Class)aClass
{
    return [MBClassFactory classNamed:NSStringFromClass(aClass)];
}

+ (Class)classNamed:(NSString *)className
{
    Class class = nil;
    
    // Check if there's a specific class for the device. If not, try to load the default class.
    switch (UI_USER_INTERFACE_IDIOM()) {
        case UIUserInterfaceIdiomPad:
            class = NSClassFromString([NSString stringWithFormat:@"%@%@", className, @"iPad"]);
            break;
        case UIUserInterfaceIdiomPhone:
            class = NSClassFromString([NSString stringWithFormat:@"%@%@", className, @"iPhone"]);
            break;
        default:
            break;
    }
    if (!class) {
        class = NSClassFromString(className);
    }
    return class;
}

@end
