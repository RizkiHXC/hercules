//
//  MBObserverController.h
//  MBFramework
//
//  Created by Marco Jonker on 1/17/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MBObserverControllerProtocol <NSObject>

@required
/**
 Adds an entry to the receiver’s dispatch table with an observer.
 @param observer Object registering as an observer. This value must not be nil.
 */
- (void)addObserver:(id<NSObject>)observer;
/**
 Removes the observer from the receiver’s dispatch table.
 @param observer Object registering as an observer. This value must not be nil.
 */
- (void)removeObserver:(id<NSObject>)observer;

@optional
/**
 Returns a Boolean value that indicates whether a given observer is present in the receiver’s dispatch table.
 @param observer Object registering as an observer. This value must not be nil.
 @return YES if observer is present in the receiver’s dispatch table, otherwise NO.
 */
- (BOOL)containsObserver:(id<NSObject>)observer;

@end

/**
 An MBObserverController object  provides a mechanism for broadcasting information within a program to a set of observer classes. An MBObserverController object is essentially a delegate method dispatch table.
 */
@interface MBObserverController : NSObject <MBObserverControllerProtocol>

/**
 Adds an entry to the receiver’s dispatch table with an observer.
 @param observer Object registering as an observer. This value must not be nil.
 */
- (void)addObserver:(id<NSObject>)observer;
/**
 Removes the observer from the receiver’s dispatch table.
 @param observer Object registering as an observer. This value must not be nil.
 */
- (void)removeObserver:(id<NSObject>)observer;
/**
 Returns a Boolean value that indicates whether a given observer is present in the receiver’s dispatch table.
 @param observer Object registering as an observer. This value must not be nil.
 @return YES if observer is present in the receiver’s dispatch table, otherwise NO.
 */
- (BOOL)containsObserver:(id<NSObject>)observer;

/**
 Executes a given block with each observer.
 @param notifyBlock The block to apply to elements in the array. Block must not be nil.
 The block takes two arguments:
 obj
 The observer in the receiver’s dispatch table.
 stop
 A reference to a Boolean value. The block can set the value to YES to stop further processing. The stop argument is an out-only argument. You should only ever set this Boolean to YES within the Block.
 */
- (void)notifyObserversUsingBlock:(void (^)(id<NSObject> observer, BOOL *stop))notifyBlock;

// Deprecated methods
- (void)notifyObserversWithSelector:(SEL)selector retainArguments:(BOOL)retainArguments, ... DEPRECATED_MSG_ATTRIBUTE("64-bit not supported. Use notifyObserversWithBlock: instead");


@end
