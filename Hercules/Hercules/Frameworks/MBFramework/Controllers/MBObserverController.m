//
//  MBObserverController.m
//  MBFramework
//
//  Created by Marco Jonker on 1/17/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBObserverController.h"
#import "NSInvocation+MBFramework.h"

@interface MBObserverController() {
    NSMutableSet* observers_;
    NSMutableSet* pendingAdds_;
    NSMutableSet* pendingRemoves_;
    BOOL notifying_;
}
@end

@implementation MBObserverController

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        observers_      = [[NSMutableSet alloc] init];
        pendingAdds_    = [[NSMutableSet alloc] init];
        pendingRemoves_ = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [pendingAdds_ release];
    [pendingRemoves_ release];
    [observers_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

- (void)MB_commitPending {
    NSAssert(!notifying_, @"Tried to commit pending observers while notifying");
    for (id<NSObject> observer in pendingRemoves_)
        [observers_ removeObject:observer];
    [pendingRemoves_ removeAllObjects];
    
    for (id<NSObject> observer in pendingAdds_)
        [observers_ addObject:observer];
    [pendingAdds_ removeAllObjects];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

- (void)addObserver:(id<NSObject>)observer {
    if (notifying_) {
        // The main set cannot be mutated while iterating, add to a secondary set
        // to be processed when the iteration finishes
        [pendingRemoves_ removeObject:observer];
        [pendingAdds_ addObject:observer];
    } else {
        [observers_ addObject:observer];
    }
}

- (void)removeObserver:(id<NSObject>)observer {
    if (notifying_) {
        // The main set cannot be mutated while iterating, add to a secondary set
        // to be processed when the iteration finishes
        [pendingAdds_ removeObject:observer];
        [pendingRemoves_ addObject:observer];
    } else {
        [observers_ removeObject:observer];
    }
}

- (BOOL)containsObserver:(id<NSObject>)observer {
    return ([observers_ containsObject:observer] && ![pendingRemoves_ containsObject:observer]) ||
    [pendingAdds_ containsObject:observer];
}

- (void)notifyObserversUsingBlock:(void (^)(id<NSObject> observer, BOOL *stop))notifyBlock
{
    if (!notifyBlock) {
        [NSException raise:NSInvalidArgumentException format:@"block must not be nil"];
    }
    
    @synchronized(self) {
        notifying_ = YES;
        BOOL stop = NO;
        
        for (id<NSObject> observer in observers_) {
            if ([pendingRemoves_ containsObject:observer] == NO) {
                // Callback block
                notifyBlock(observer, &stop);
                if (stop) {
                    break;
                }
            }
        }
        notifying_ = NO;
        [self MB_commitPending];
    }
}


//https://developer.apple.com/library/ios/documentation/Cocoa/Reference/Foundation/Classes/NSInvocation_Class/Reference/Reference.html
//For efficiency, newly created NSInvocation objects don’t retain or copy their arguments, nor do they retain their targets, copy C strings, or copy any associated blocks. You should instruct an NSInvocation object to retain its arguments if you intend to cache it, because the arguments may otherwise be released before the invocation is invoked. NSTimer objects always instruct their invocations to retain their arguments, for example, because there’s usually a delay before a timer fires.

- (void)notifyObserversWithSelector:(SEL)selector retainArguments:(BOOL)retainArguments, ...; {
    // WARNING: this function is deprecated! Use notifyObserversWithBlock instead
    @synchronized(self) {
        notifying_ = YES;
        for (id<NSObject> observer in observers_) {
            if (![pendingRemoves_ containsObject:observer] && [observer respondsToSelector:selector]) {
                va_list argumentsPointer;
                va_start(argumentsPointer, retainArguments);
                char* args = (char*)argumentsPointer;
                
                NSInvocation* invocation = [NSInvocation invocationWithTarget:observer selector:selector];
                NSMethodSignature* signature = invocation.methodSignature;
                if (retainArguments) {
                    [invocation retainArguments];
                }
                
                for (int index = 2; index < [signature numberOfArguments]; index++) {
                    const char *type = [signature getArgumentTypeAtIndex:index];
                    NSUInteger size, align;
                    NSGetSizeAndAlignment(type, &size, &align);
                    NSUInteger mod = (NSUInteger)args % align;
                    if (mod != 0) {
                        args += (align - mod);
                    }
                    [invocation setArgument:args atIndex:index];
                    args += size;
                }
                va_end(argumentsPointer);
                [invocation invoke];
            }
        }
        notifying_ = NO;
        [self MB_commitPending];
    }
}

@end
