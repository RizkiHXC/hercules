//
//  MBPushNotificationCenter.h
//  MBFramework
//
//  Created by Arno Woestenburg on 7/19/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBPushNotificationCenter : NSObject
{
    NSURL *registerApiURL_;
    NSMutableDictionary *parameters_;
}

+ (instancetype)sharedInstance;

- (NSData *)registerForRemoteNotificationsWithDeviceToken:(NSData *)devToken;

@property(nonatomic,retain) NSURL *registerApiURL;
@property(nonatomic,retain) NSString *scheme;
@property(nonatomic,retain) NSString *path;
@property(nonatomic,retain) NSMutableDictionary *parameters;

@end
