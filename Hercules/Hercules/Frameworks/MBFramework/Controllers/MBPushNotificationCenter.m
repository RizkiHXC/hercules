//
//  MBPushNotificationCenter.m
//  MBFramework
//
//  Created by Arno Woestenburg on 7/19/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBPushNotificationCenter.h"
#import "MBApiPortal.h"

static id sharedInstance_ = nil;

@implementation MBPushNotificationCenter

@synthesize registerApiURL = registerApiURL_;
@synthesize parameters = parameters_;

#define KEY_APPNAME         @"appName"
#define KEY_APPID           @"appId"
#define KEY_APPVERSION      @"appVersion"
#define KEY_DEVICEBRAND     @"deviceBrand"
#define KEY_DEVICEMODEL     @"deviceModel"
#define KEY_DEVICENAME      @"deviceName"
#define KEY_DEVICETOKEN     @"deviceToken"
#define KEY_DEVICEUIID      @"deviceUid"
#define KEY_DEVICEVERSION   @"deviceVersion"
#define KEY_PUSHALERT       @"pushAlert"
#define KEY_PUSHBADGE       @"pushBadge"
#define KEY_PUSHSOUND       @"pushSound"
#define KEY_TASK            @"task"

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (instancetype)sharedInstance
{
    if (!sharedInstance_) {
        sharedInstance_ = [[self alloc] init];
    }
    return sharedInstance_;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.scheme = @"http";
        self.parameters = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)dealloc
{
    self.registerApiURL = nil;
    self.parameters = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSData *)registerForRemoteNotificationsWithDeviceToken:(NSData *)devToken
{
    
#if TARGET_IPHONE_SIMULATOR
    NSAssert(FALSE, @"Don't call this function from the simulator");
#endif

    // Check what Notifications the user has turned on.  We registered for all three, but they may have manually disabled some or all of them.
    NSUInteger rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];

    // Set the defaults to disabled unless we find otherwise...
    BOOL pushBadge = NO;
    BOOL pushAlert = NO;
    BOOL pushSound = NO;

    // Check what Registered Types are turned on. 
    pushBadge = ((rntypes & UIRemoteNotificationTypeBadge) == UIRemoteNotificationTypeBadge);
    pushAlert = ((rntypes & UIRemoteNotificationTypeAlert) == UIRemoteNotificationTypeAlert);
    pushSound = ((rntypes & UIRemoteNotificationTypeSound) == UIRemoteNotificationTypeSound);

    // Get the users Device Model, Display Name, Unique ID, Token & Version Number
    UIDevice *device = [UIDevice currentDevice];

    // Prepare the Device Token for Registration (remove spaces and < >)
    NSString *deviceToken = [devToken description];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@"<"withString:@""];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString: @" " withString: @""];

    // Post paramaters
    NSMutableDictionary* parameters = [[[NSMutableDictionary alloc] initWithDictionary:parameters_] autorelease];

    // Some parameters can be set from outside the class, via parameters property.
    if (nil == [parameters objectForKey:KEY_APPNAME]) {
        [parameters setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"] forKey:KEY_APPNAME];
    }
    if (nil == [parameters objectForKey:KEY_APPVERSION]) {
        [parameters setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:KEY_APPVERSION];
    }
    [parameters setObject:[self MB_getDeviceUid] forKey:KEY_DEVICEUIID];
    [parameters setObject:deviceToken forKey:KEY_DEVICETOKEN];
    [parameters setObject:device.name forKey:KEY_DEVICENAME];
    [parameters setObject:device.model forKey:KEY_DEVICEMODEL];
    [parameters setObject:device.systemVersion forKey:KEY_DEVICEVERSION];
    [parameters setObject:@"apple" forKey:KEY_DEVICEBRAND];
    [parameters setObject:(pushBadge ? @"true" : @"false") forKey:KEY_PUSHBADGE];
    [parameters setObject:(pushAlert ? @"true" : @"false") forKey:KEY_PUSHALERT];
    [parameters setObject:(pushSound ? @"true" : @"false") forKey:KEY_PUSHSOUND];
    
    // http://mediabunker-propaganda.herokuapp.com/api/push/register?secret=ReRbkcGsX3jvu7K4Y3z9YgDQ
    
    MBApiRequest *request = [MBApiRequest POSTWithUrl:self.registerApiURL parameters:parameters];
    [[MBApiPortal sharedInstance] processApiRequest:request];

    return nil;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSString *)MB_getDeviceUid
{
    NSUserDefaults  *userDefaults    = [NSUserDefaults standardUserDefaults];
    NSString        *deviceUuid      = [userDefaults objectForKey:@"deviceUuid"];
    
    if (!deviceUuid) {
        // Create universally unique identifier (object)
        CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
        
        // Get the string representation of CFUUID object.
        deviceUuid = [(NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuidObject) autorelease];
        CFRelease(uuidObject);
        
        [userDefaults setObject:deviceUuid forKey:@"deviceUuid"];
        [userDefaults synchronize];
    }
    return deviceUuid;
}

@end
