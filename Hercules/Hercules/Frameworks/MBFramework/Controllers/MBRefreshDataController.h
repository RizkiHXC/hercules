//
//  MBRefreshDataController.h
//  MBFramework
//
//  Created by Arno Woestenburg on 8/12/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MBRefreshDataController;

/**
 This protocol defines delegate methods for MBRefreshDataController objects.
 */
@protocol MBRefreshDataControllerDelegate <NSObject>

/**
 Tells the delegate to refresh its data.
 @param controller The refresh controller.
 */
- (void)refreshDataControllerRefresh:(MBRefreshDataController *)controller;

@end

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBRefreshDataController : NSObject {
 
    NSDate *lastRefreshTimeStamp_;
    id<MBRefreshDataControllerDelegate> delegate_;
    NSTimeInterval refreshTimeSpan_;
    NSTimeInterval refreshTimeInterval_;
    BOOL needRefresh_;
}

- (instancetype)initWithDelegate:(id<MBRefreshDataControllerDelegate>)delegate;

- (void)startWithTimeSpan:(NSTimeInterval)refreshTimeSpan;

/**
 *  Starts a timer with the given refreshTimeInterval and calls the delegate
 *  when refresh is needed.
 *
 *  @param refreshTimeInterval
 */
- (void)startTimerWithTimeInterval:(NSTimeInterval)refreshTimeInterval;

- (void)stop;
- (void)refresh;
- (void)refreshIfNeeded;

@property(nonatomic,retain) NSDate *lastRefreshTimeStamp;
/**
 The object that acts as the delegate of the receiving refresh controller.
 @discussion The delegate must adopt the MBRefreshDataControllerDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBRefreshDataControllerDelegate> delegate;
@property(nonatomic,assign) BOOL needRefresh;

@end
