//
//  MBRefreshDataController.m
//  MBFramework
//
//  Created by Arno Woestenburg on 8/12/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBRefreshDataController.h"

@interface MBRefreshDataController ()
{
    NSDate *lastCheckTimeStamp_;
    NSTimer *timer_;
}
@end

const NSInteger kNoRefreshTimeInterval = -1;

@implementation MBRefreshDataController

@synthesize delegate = delegate_;
@synthesize lastRefreshTimeStamp = lastRefreshTimeStamp_;
@synthesize needRefresh=needRefresh_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithDelegate:(id<MBRefreshDataControllerDelegate>)delegate
{
    self = [super init];
    if (self) {
        refreshTimeInterval_ = kNoRefreshTimeInterval;
        
        needRefresh_ = NO;
        [self setDelegate:delegate];
    }
    return self;
}

- (void)dealloc
{
    [self stop];
    [lastRefreshTimeStamp_ release];
    [lastCheckTimeStamp_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (BOOL)MB_refreshNeeded
{
    BOOL result = NO;
    NSDate *timeStamp = nil;
    
    if(needRefresh_ == NO) {
        if (lastRefreshTimeStamp_ || lastCheckTimeStamp_) {
            if (lastRefreshTimeStamp_) {
                timeStamp = lastRefreshTimeStamp_;
            } else if (lastCheckTimeStamp_) {
                timeStamp = lastCheckTimeStamp_;
            }
            if (timeStamp) {
                result = ([[NSDate date] timeIntervalSinceDate:timeStamp] > refreshTimeSpan_);
            }
        } else {
            // First refresh
            result = YES;
        }
    } else {
        result = YES;
    }
    
    [lastCheckTimeStamp_ release];
    lastCheckTimeStamp_ = [[NSDate date] retain];
    return result;
}

- (void)MB_subscribeForNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MB_applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MB_applicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
}

- (void)MB_unsubscribeForNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)startWithTimeSpan:(NSTimeInterval)refreshTimeSpan
{
    [self MB_subscribeForNotifications];
    
    refreshTimeSpan_ = refreshTimeSpan;
    
    [self refreshIfNeeded];
}

- (void)startTimerWithTimeInterval:(NSTimeInterval)refreshTimeInterval
{
    refreshTimeInterval_ = refreshTimeInterval;
    
    [timer_ invalidate];
    timer_ = [NSTimer scheduledTimerWithTimeInterval:refreshTimeInterval_ target:self selector:@selector(refresh) userInfo:nil repeats:YES];
}

- (void)stop
{
    [timer_ invalidate];
    timer_ = nil;
    [self MB_unsubscribeForNotifications];
}

- (void)refreshIfNeeded {
    if([self MB_refreshNeeded] == YES) {
        [self refresh];
    }
}

- (void)refresh
{
    if ([delegate_ respondsToSelector:@selector(refreshDataControllerRefresh:)]) {
        [delegate_ refreshDataControllerRefresh:self];
    }
    needRefresh_ = NO;
    [lastRefreshTimeStamp_ release];
    lastRefreshTimeStamp_ = [[NSDate date] retain];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSNotificationCenter

- (void)MB_applicationDidBecomeActive:(UIApplication *)application
{
    // Start the timer if needed.
    if (refreshTimeInterval_ != kNoRefreshTimeInterval) {
        [self startTimerWithTimeInterval:refreshTimeInterval_];
    }
    
    if ([self MB_refreshNeeded]) {
        [self refresh];
    }
}

- (void)MB_applicationWillResignActive:(UIApplication *)application
{
    // Stop the timer.
    [timer_ invalidate];
    timer_ = nil;
}

@end
