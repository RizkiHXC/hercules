//
//  MBToolBarDisplayController.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/19/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MBToolBarDisplayDelegate;

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBToolBarDisplayController : NSObject
{
    UIScrollView *scrollView_;
    UIView *toolBar_;
    id<MBToolBarDisplayDelegate> delegate_;
}

/**
 *  Initializes and returns a newly allocated MBToolBarDisplayDelegate object with the specified properties.
 *
 *  @param toolBar An UIView based object which will be managed by this class.
 *  @param scrollView An UIScrollView object.
 *
 *  @return An initialized TextInputController object or nil if the object couldn't be created.
 */
- (instancetype)initWithToolBar:(UIView *)toolBar scrollView:(UIScrollView *)scrollView;

- (void)setToolBarHidden:(BOOL)hidden animated:(BOOL)animated;
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

@property(nonatomic,readonly) UIScrollView *scrollView;
@property(nonatomic,readonly) UIView *toolBar;
/**
 *  The object that acts as the delegate of the receiving toolbar displaycontroller.
 *
 *  @discussion The delegate must adopt the MBToolBarDisplayDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBToolBarDisplayDelegate> delegate;

@end

@protocol MBToolBarDisplayDelegate <NSObject>
@optional
- (void)toolBarDisplayController:(MBToolBarDisplayController *)controller willShowToolBar:(UIView *)toolBar;
- (void)toolBarDisplayController:(MBToolBarDisplayController *)controller didShowToolBar:(UIView *)toolBar;
- (void)toolBarDisplayController:(MBToolBarDisplayController *)controller willHideToolBar:(UIView *)toolBar;
- (void)toolBarDisplayController:(MBToolBarDisplayController *)controller didHideToolBar:(UIView *)toolBar;
@end

