//
//  MBToolBarDisplayController.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/19/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBToolBarDisplayController.h"
#import "UIView+MBViewGeometry.h"

typedef NS_ENUM(NSInteger, MBToolBarDisplayState) {
    MBToolBarDisplayStateHidden          = 0,
    MBToolBarDisplayStateIntermediate    = 1,
    MBToolBarDisplayStateVisible         = 2,
};

@interface MBToolBarDisplayController ()
{
    CGPoint previousContentOffset_;
    CGFloat displayChangeStartOffset_;
    CGFloat displayChangeStartInset_;
    BOOL isMovingToolbar_;
    BOOL isToolbarOpen_;
    BOOL animating_;
    MBToolBarDisplayState toolBarDisplayState_;
    //UIEdgeInsets defaultInsets_;
}
@end




@implementation MBToolBarDisplayController


@synthesize delegate = delegate_;
@synthesize scrollView = scrollView_;
@synthesize toolBar = toolBar_;

static CGFloat toolBarVelocity = 750; // Pan gesture velocity required to show the toolbar

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithToolBar:(UIView *)toolBar scrollView:(UIScrollView *)scrollView
{
    self = [super init];
    if (self) {
    	scrollView_ = [scrollView retain];
    	toolBar_ = [toolBar retain];
        [self setToolBarHidden:YES animated:NO];
        toolBarDisplayState_ = MBToolBarDisplayStateHidden;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    	NSAssert(toolBar_ != nil && scrollView_ != nil, @"Use initWithToolBar:scrollView: to set the toolBar and scrollView");
    }
    return self;
}

- (void)dealloc
{
    [scrollView_ release];
    [toolBar_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_notifyWillShowToolBar
{
	if ([delegate_ respondsToSelector:@selector(toolBarDisplayController:willShowToolBar:)]) {
		[delegate_ toolBarDisplayController:self willShowToolBar:toolBar_];
	}
}

- (void)MB_notifyDidShowToolBar
{
	if ([delegate_ respondsToSelector:@selector(toolBarDisplayController:didShowToolBar:)]) {
		[delegate_ toolBarDisplayController:self didShowToolBar:toolBar_];
	}
}

- (void)MB_notifyWillHideToolBar
{
	if ([delegate_ respondsToSelector:@selector(toolBarDisplayController:willHideToolBar:)]) {
		[delegate_ toolBarDisplayController:self willHideToolBar:toolBar_];
	}
}

- (void)MB_notifyDidHideToolBar
{
	if ([delegate_ respondsToSelector:@selector(toolBarDisplayController:didHideToolBar:)]) {
		[delegate_ toolBarDisplayController:self didHideToolBar:toolBar_];
	}
}

-(void)MB_setToolBarDisplayState:(MBToolBarDisplayState)toolBarDisplayState sendNotification:(BOOL)sendNotification {
    if(toolBarDisplayState_ != toolBarDisplayState) {
        if(sendNotification == YES) {
            MBToolBarDisplayState prevState = toolBarDisplayState_;
            toolBarDisplayState_ = toolBarDisplayState;
            
            switch(toolBarDisplayState) {
                case MBToolBarDisplayStateHidden:
                    [self MB_notifyDidHideToolBar];
                    break;
                case MBToolBarDisplayStateIntermediate:
                    if(prevState == MBToolBarDisplayStateHidden) {
                        [self MB_notifyWillShowToolBar];
                    } else if(prevState == MBToolBarDisplayStateVisible) {
                        [self MB_notifyWillHideToolBar];
                    }
                    break;
                case MBToolBarDisplayStateVisible:
                    [self MB_notifyDidShowToolBar];
                    break;
            }
        } else {
            toolBarDisplayState_ = toolBarDisplayState;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

-(void)setToolBarPosition:(CGFloat)cy {
    UIEdgeInsets scrollViewInsets = scrollView_.contentInset;
    
    switch(toolBarDisplayState_) {
        case MBToolBarDisplayStateHidden:
            if(cy > 0) {
                if(cy >= toolBar_.height - displayChangeStartInset_) {
                    [self MB_setToolBarDisplayState:MBToolBarDisplayStateVisible sendNotification:YES];
                    scrollViewInsets.top = toolBar_.height;
                } else {
                    [self MB_setToolBarDisplayState:MBToolBarDisplayStateIntermediate sendNotification:YES];
                    scrollViewInsets.top = displayChangeStartInset_ + cy;
                }
            }
            break;
        case MBToolBarDisplayStateIntermediate:
            if(cy + displayChangeStartInset_ >= toolBar_.height) {
                [self MB_setToolBarDisplayState:MBToolBarDisplayStateVisible sendNotification:YES];
                scrollViewInsets.top = toolBar_.height;
            } else if(cy + displayChangeStartInset_ <= 0) {
                [self MB_setToolBarDisplayState:MBToolBarDisplayStateHidden sendNotification:YES];
                scrollViewInsets.top = 0;
            } else if(cy != 0) {
                scrollViewInsets.top = displayChangeStartInset_ + cy;
            }
            
            break;
        case MBToolBarDisplayStateVisible:
            if(cy < 0) {
                if(cy <= -displayChangeStartInset_) {
                    [self MB_setToolBarDisplayState:MBToolBarDisplayStateHidden sendNotification:YES];
                    scrollViewInsets.top = 0;
                } else {
                    [self MB_setToolBarDisplayState:MBToolBarDisplayStateIntermediate sendNotification:YES];
                    scrollViewInsets.top = displayChangeStartInset_ + cy;
                }
            }
            break;
    }
    
    isMovingToolbar_            = (toolBarDisplayState_ == MBToolBarDisplayStateIntermediate);
    scrollView_.contentInset    = scrollViewInsets;
    toolBar_.frame              = CGRectMake(toolBar_.originX, scrollViewInsets.top - toolBar_.height, toolBar_.width, toolBar_.height);
}

- (void)setToolBarHidden:(BOOL)hidden animated:(BOOL)animated
{
    isMovingToolbar_ = NO;
    animating_       = YES;
    CGRect       newToolBarFrame = CGRectZero;
    UIEdgeInsets newScrollInsets = scrollView_.contentInset;
        
    if (hidden) {
        newToolBarFrame     = CGRectMake(0, -toolBar_.height,toolBar_.width,toolBar_.height);
        newScrollInsets.top = 0;
        [self MB_notifyWillHideToolBar];
    } else {
        newToolBarFrame     = CGRectMake(toolBar_.originX,0,toolBar_.width,toolBar_.height);
        newScrollInsets.top = toolBar_.height;
        [self MB_notifyWillShowToolBar];
    }
    
    CGPoint newContentOffest =  scrollView_.contentOffset;
    if(scrollView_.contentOffset.y <= toolBar_.height) {
        newContentOffest.y +=  -newScrollInsets.top - scrollView_.contentOffset.y;
    }
    
    if (animated) {
        // First animate transform
        [UIView animateWithDuration:0.3
                         animations:^{
                             [toolBar_ setFrame:newToolBarFrame];
                             [scrollView_ setContentInset:newScrollInsets];
                             [scrollView_ setScrollIndicatorInsets:newScrollInsets];
                             scrollView_.contentOffset = newContentOffest;
                             
                         }];
        
        // Then animate fade in/ -out
        [UIView animateWithDuration:0.2
                              delay:hidden ? 0.2 : 0.0
                            options:UIViewAnimationOptionCurveLinear
                         animations: ^{
                             [toolBar_ setAlpha:hidden ? 1.0 : 1.0];
                         }
                         completion:^(BOOL finished) {
                             if (finished) {
                                 if(hidden) {
                                     [self MB_setToolBarDisplayState:MBToolBarDisplayStateHidden sendNotification:YES];
                                 } else {
                                     [self MB_setToolBarDisplayState:MBToolBarDisplayStateVisible sendNotification:YES];
                                 }
                             }
                         }];

    } else {
        [toolBar_ setFrame:newToolBarFrame];
        [scrollView_ setContentInset:newScrollInsets];
        [scrollView_ setScrollIndicatorInsets:newScrollInsets];

        if(hidden) {
            [self MB_setToolBarDisplayState:MBToolBarDisplayStateHidden sendNotification:YES];
        } else {
            [self MB_setToolBarDisplayState:MBToolBarDisplayStateVisible sendNotification:YES];
        }
    }
    animating_ = NO;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollViewDelegate -

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSAssert(scrollView == scrollView_, @"Must be the controlling scrollView");
    
    if(decelerate == NO && animating_ == NO && toolBarDisplayState_ == MBToolBarDisplayStateIntermediate) {
        BOOL hidden = (CGRectGetMaxY(toolBar_.frame) < toolBar_.height * 0.5);
        [self setToolBarHidden:hidden animated:YES];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSAssert(scrollView == scrollView_, @"Must be the controlling scrollView");
    
    if(animating_ == NO && toolBarDisplayState_ == MBToolBarDisplayStateIntermediate) {
        BOOL hidden = (CGRectGetMaxY(toolBar_.frame) < toolBar_.height * 0.5);
        [self setToolBarHidden:hidden animated:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    NSAssert(scrollView == scrollView_, @"Must be the controlling scrollView");
    previousContentOffset_ = scrollView.contentOffset;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //Check if the scrollview did not reach the bottom
    if(scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.bounds.size.height)) {
        if(animating_ == NO) {
            NSAssert(scrollView == scrollView_, @"Must be the controlling scrollView");
            
            CGFloat panVelocity     = [[scrollView panGestureRecognizer] velocityInView:scrollView.superview].y;
            CGPoint contentOffset   = scrollView.contentOffset;
        
            if(toolBarDisplayState_ != MBToolBarDisplayStateIntermediate && isMovingToolbar_ == NO ) {
                CGFloat offset              = scrollView.contentOffset.y - previousContentOffset_.y;
                displayChangeStartOffset_   = scrollView.contentOffset.y;
                displayChangeStartInset_    = scrollView.contentInset.top;

                if(contentOffset.y <= 0 && contentOffset.y >= -toolBar_.height) {
                    isMovingToolbar_    = YES;
                    displayChangeStartInset_        = scrollView.contentInset.top;
                } else
                if (offset < 0 && contentOffset.y > toolBar_.height) {
                    // Scroll direction is up
                    if (panVelocity >= toolBarVelocity) {
                        isMovingToolbar_ = YES;
                    }
                } else if (offset > 0 && contentOffset.y > toolBar_.height) {
                    isMovingToolbar_     = YES;
                }
            } else{
                [self setToolBarPosition:displayChangeStartOffset_ - scrollView.contentOffset.y];
            }
            previousContentOffset_ = scrollView.contentOffset;
        }
    }
}


@end
