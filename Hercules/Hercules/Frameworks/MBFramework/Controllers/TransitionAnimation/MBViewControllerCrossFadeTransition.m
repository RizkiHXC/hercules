//
//  MBViewControllerCrossFadeTransition.m
//  MBFramework
//
//  Created by Arno Woestenburg on 28/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBViewControllerCrossFadeTransition.h"

@implementation MBViewControllerCrossFadeTransition

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MBViewControllerTransitionAnimation

- (void)animatePresentTransitionWithContext:(id<UIViewControllerContextTransitioning>)transitionContext fromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    // Get references to the view hierarchy
    UIView *containerView = [transitionContext containerView];
    
    // Add view to the hierarchy
    [toViewController.view setAlpha:0.0];
    [containerView insertSubview:toViewController.view aboveSubview:fromViewController.view];
    
    // Restore the CrossFade
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                     animations:^{
                         [toViewController.view setAlpha:1.0];
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                     }];
}

- (void)animateDismissTransitionWithContext:(id<UIViewControllerContextTransitioning>)transitionContext fromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    // Get references to the view hierarchy
    UIView *containerView = [transitionContext containerView];

    // Add CrossFaded view to the hierarchy
    [containerView insertSubview:toViewController.view belowSubview:fromViewController.view];

    // CrossFade the 'from' view down until it disappears
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                     animations:^{
                         [fromViewController.view setAlpha:0.0];
                     }
                     completion:^(BOOL finished) {
                         
                         
                         [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
                     }];
}

@end
