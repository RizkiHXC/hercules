//
//  MBViewControllerScaleTransition.h
//  MBFramework
//
//  Created by Arno Woestenburg on 28/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBViewControllerTransitionAnimation.h"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBViewControllerScaleTransition : MBViewControllerTransitionAnimation
{
    
}

@property(nonatomic,retain) UIView *startViewForAnimation;

@end
