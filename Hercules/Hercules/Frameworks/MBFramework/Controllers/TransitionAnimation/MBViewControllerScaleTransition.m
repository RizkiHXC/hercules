//
//  MBViewControllerScaleTransition.m
//  MBFramework
//
//  Created by Arno Woestenburg on 28/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBViewControllerScaleTransition.h"

@implementation MBViewControllerScaleTransition

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    self.startViewForAnimation = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (CGAffineTransform)MB_translatedAndScaledTransformUsingViewRect:(CGRect)viewRect fromRect:(CGRect)fromRect
{
    CGSize scales = CGSizeMake(viewRect.size.width / fromRect.size.width, viewRect.size.height / fromRect.size.height);
    CGPoint offset = CGPointMake(CGRectGetMidX(viewRect) - CGRectGetMidX(fromRect), CGRectGetMidY(viewRect) - CGRectGetMidY(fromRect));
    return CGAffineTransformMake(scales.width, 0, 0, scales.height, offset.x, offset.y);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MBViewControllerTransitionAnimation

- (void)animatePresentTransitionWithContext:(id<UIViewControllerContextTransitioning>)transitionContext fromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    // Get references to the view hierarchy
    UIView *containerView = [transitionContext containerView];
    
    // Add scaled view to the hierarchy
    [toViewController.view setAlpha:0.7];
    [containerView insertSubview:toViewController.view aboveSubview:fromViewController.view];
    
    CGRect frame = CGRectZero;
    
    if (self.startViewForAnimation) {
        frame = [self.startViewForAnimation.superview convertRect:self.startViewForAnimation.frame toView:nil];
    }
    
    CGAffineTransform transform = [self MB_translatedAndScaledTransformUsingViewRect:frame fromRect:toViewController.view.frame];
    [toViewController.view setTransform:transform];
    
    // Restore the scale
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                     animations:^{
                         [toViewController.view setTransform:CGAffineTransformIdentity];
                         [toViewController.view setAlpha:1.0];
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                     }];
}

- (void)animateDismissTransitionWithContext:(id<UIViewControllerContextTransitioning>)transitionContext fromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{

}

@end
