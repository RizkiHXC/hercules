//
//  MBViewControllerTransitionAnimation.h
//  MBFramework
//
//  Created by Arno Woestenburg on 28/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Identifies the type of transition for the view controller.
 */
typedef NS_ENUM(NSInteger, MBTransitionAnimationType)
{
    /**
     Specifies that the animation is used when presenting the view controller.
     */
    MBTransitionAnimationTypePresent,
    /**
     Specifies that the animation is used when dismissing the view controller.
     */
    MBTransitionAnimationTypeDismiss,
};

/**
 Implements the animations for a custom view controller transition.
 */
@interface MBViewControllerTransitionAnimation : NSObject <UIViewControllerAnimatedTransitioning>
{
    
}

/**
 Identifies the type of transition for the view controller.
 */
@property(nonatomic,assign) MBTransitionAnimationType animationType;
/**
 The duration of the transition animation, measured in seconds.
 */
@property(nonatomic,assign) CGFloat transitionDuration;

@end
