//
//  MBViewControllerTransitionAnimation.m
//  MBFramework
//
//  Created by Arno Woestenburg on 28/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBViewControllerTransitionAnimation.h"

@implementation MBViewControllerTransitionAnimation

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.transitionDuration = 0.3;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)animatePresentTransitionWithContext:(id<UIViewControllerContextTransitioning>)transitionContext fromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    [NSException raise:NSInternalInconsistencyException format:@"%@ Must be handled by a subclass", NSStringFromSelector(_cmd)];
}

- (void)animateDismissTransitionWithContext:(id<UIViewControllerContextTransitioning>)transitionContext fromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    [NSException raise:NSInternalInconsistencyException format:@"%@ Must be handled by a subclass", NSStringFromSelector(_cmd)];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewControllerAnimatedTransitioning

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];

    switch (self.animationType) {
        case MBTransitionAnimationTypePresent:
            [self animatePresentTransitionWithContext:transitionContext fromViewController:fromViewController toViewController:toViewController];
            break;
        case MBTransitionAnimationTypeDismiss:
            [self animateDismissTransitionWithContext:transitionContext fromViewController:fromViewController toViewController:toViewController];
            break;
        default:
            NSAssert(FALSE, @"Unhandled case");
            break;
    }
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return self.transitionDuration;
}

@end
