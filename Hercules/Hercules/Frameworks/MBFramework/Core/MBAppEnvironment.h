//
//  MBAppEnvironment.h
//  MBFramework
//
//  Created by Arno Woestenburg on 11/04/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

/**
 An MBAppEnvironment object represents a set of environment dependant settings that can be used in a program.
 @discussion Typically you build an application using one of these project types: Live, Staging, Develop/Local.
 */
@interface MBAppEnvironment : NSObject
{
    
}

/**
 Creates a MBAppEnvironment instance that has the specified bundle identifier.
 @param identifier An identifier for the environment type.
 @return A newly created MBAppEnvironment instance.
 */
+ (instancetype)environmentWithIdentifier:(NSString *)identifier;
/**
 Creates a MBAppEnvironment instance that has the specified bundle identifier which is retreived from a Bundle setting with the identifier "CFEnvironmentType".
 @return A newly created MBAppEnvironment instance.
 */
+ (instancetype)mainEnvironment;

/**
 Initializes and returns a newly allocated AppEnvironment object with the specified identifier.
 @param identifier The identifier for the environment.
 @return An initialized AppEnvironment object or nil if the object couldn't be created.
 */
- (instancetype)initWithIdentifier:(NSString *)identifier;
/**
 Initializes and returns a newly allocated AppEnvironment object with the specified identifier.
 @param identifier The identifier for the environment.
 @param bundleURL The full URL of the receiver’s bundle directory.
 @return An initialized AppEnvironment object or nil if the object couldn't be created.
 */
- (instancetype)initWithIdentifier:(NSString *)identifier bundleURL:(NSURL *)bundleURL;
/**
 The identifier for the environment.
 @return The identifier for the environment.
 */
- (NSString *)environmentIdentifier;
/**
 Returns a dictionary that contains information about the receiver.
 @return A dictionary, constructed from the environment type's .plist file, that contains information about the receiver.
 */
- (NSDictionary *)infoDictionary;
/**
 Returns the value associated with the specified key in the receiver's information property list.
 @param key A key in the receiver's property list.
 @return The value associated with key in the receiver's property list.
 */
- (id)objectForInfoDictionaryKey:(NSString *)key;

@end