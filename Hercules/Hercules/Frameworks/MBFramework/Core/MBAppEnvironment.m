//
//  MBAppEnvironment.m
//  MBFramework
//
//  Created by Arno Woestenburg on 11/04/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBAppEnvironment.h"

@interface MBAppEnvironment ()
{
    NSString *environmentIdentifier_;
    NSDictionary *infoDictionary_;
}
@end

static id mainEnvironment_ = nil;

@implementation MBAppEnvironment

static NSString * const kEnvironmentType = @"CFEnvironmentType";
static NSString * const kEnvironmentSettingsBundleFileName = @"EnvironmentSettings.bundle";

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (instancetype)environmentWithIdentifier:(NSString *)identifier
{
    if(!mainEnvironment_) {
        mainEnvironment_ = [[self alloc] initWithIdentifier:identifier];
    }
    return mainEnvironment_;
}

+ (instancetype)mainEnvironment
{
    if (!mainEnvironment_) {
        // Initialize environment type
        NSString *identifier = [[NSBundle mainBundle] objectForInfoDictionaryKey:kEnvironmentType];
        
        NSString *settingsBundlePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:kEnvironmentSettingsBundleFileName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:settingsBundlePath]) {
            mainEnvironment_ = [[self alloc] initWithIdentifier:identifier bundleURL:[NSURL URLWithString:settingsBundlePath]];
        } else {
            mainEnvironment_ = [[self alloc] initWithIdentifier:identifier];
        }
    }
    return mainEnvironment_;
}


//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithIdentifier:(NSString *)identifier
{
    self = [super init];
    if (self) {
        environmentIdentifier_ = [identifier copy];
        
        // Load info dictionairy
        NSString *resourceFileName = [NSString stringWithFormat:@"%@-environment", environmentIdentifier_];
        NSString *path = [[NSBundle mainBundle] pathForResource:resourceFileName ofType:@"plist"];
        infoDictionary_ = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        NSAssert(infoDictionary_ != nil, @"Invalid environment");
    }
    return self;
}

- (instancetype)initWithIdentifier:(NSString *)identifier bundleURL:(NSURL *)bundleURL
{
    self = [super init];
    if (self) {
        
        environmentIdentifier_ = [identifier copy];
        
        // Load base environment settings dictionairy
        NSString *bundlePath = [bundleURL absoluteString];
        NSString *fileName = @"Root.plist";
        
        NSString *filePath = [bundlePath stringByAppendingPathComponent:fileName];
        
        NSMutableDictionary *settings = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
        
        fileName = [identifier stringByAppendingPathExtension:@"plist"];
        
        filePath = [bundlePath stringByAppendingPathComponent:fileName];
        // Load environment specific settings dictionairy
        
        NSMutableDictionary *environmentSettings = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
        // If both dictionaries contain the same key, the settings dictionary’s value object is replaced by the environmentSettings key value.
        [settings addEntriesFromDictionary:environmentSettings];
        
        infoDictionary_ = [[NSDictionary alloc] initWithDictionary:settings];
        NSAssert(infoDictionary_ != nil, @"Invalid environment");
        
        
    }
    return self;
}

- (void)dealloc
{
    [environmentIdentifier_ release];
    [infoDictionary_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSString *)environmentIdentifier
{
    return environmentIdentifier_;
}

- (NSDictionary *)infoDictionary
{
    return infoDictionary_;
}

- (id)objectForInfoDictionaryKey:(NSString *)key
{
    return [infoDictionary_ objectForKey:key];
}


@end
