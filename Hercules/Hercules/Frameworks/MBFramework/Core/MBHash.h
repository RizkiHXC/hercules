//
//  MBHash.h
//  MBFramework
//
//  Created by Arno Woestenburg on 16/10/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The following constants specify predefined hash algorithm for generating hashes.
 */
typedef NS_ENUM(NSInteger, MBHashAlgorithm)
{
    /**
     MD5 hash algorithm.
     */
    MBHashAlgorithmMD5,
    /**
     SHA1 hash algorithm.
     */
    MBHashAlgorithmSHA1,
};

/**
 Instances of MBHash create string hash representations.
 */
@interface MBHash : NSObject <NSCopying>

/**
 Initializes and returns an MBHash instance that uses the given hash algorithm for its conversions.
 @param string The string from which to generate the hash from. This value must not be nil.
 @param hashAlgorithm The hash algorithm for the conversion.
 @return An initialized MBHash instance that uses the given hash algorithm for its conversions.
 @discussion The input string interpreted using UTF-8 encoding.
 @see MBHashAlgorithm
 */
- (instancetype)initWithString:(NSString *)string hashAlgorithm:(MBHashAlgorithm)hashAlgorithm;
/**
 The hash algorithm used by the receiver.
 */
@property(nonatomic,assign) MBHashAlgorithm hashAlgorithm;
/**
 Returns a hash representation of a given string interpreted using the receiver’s current settings.
 */
@property(nonatomic,readonly) NSString *hashString;

@end
