//
//  MBHash.m
//  MBFramework
//
//  Created by Arno Woestenburg on 16/10/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBHash.h"
#import <CommonCrypto/CommonDigest.h>

@interface MBHash ()
{
    NSString *inputString_;
}
@end

@implementation MBHash

@synthesize hashAlgorithm = hashAlgorithm_;
@synthesize hashString = hashString_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithString:(NSString *)string hashAlgorithm:(MBHashAlgorithm)hashAlgorithm
{
    self = [super init];
    if (self) {
        NSParameterAssert(string != nil);
        inputString_ = [string copy];
        hashAlgorithm_ = hashAlgorithm;
        hashString_ = [self MB_generateHash];
    }
    return self;
}

- (void)dealloc
{
    [inputString_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSString *)MB_createMD5HashFromString:(NSString *)string usingEncoding:(NSStringEncoding)encoding
{
    const char *cStr = [string cStringUsingEncoding:encoding];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return  output;
}

- (NSString *)MB_createSHA1HashFromString:(NSString *)string usingEncoding:(NSStringEncoding)encoding
{
    const char *cStr = [string cStringUsingEncoding:encoding];
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(cStr, (CC_LONG)strlen(cStr), result);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH];
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", result[i]];
    }
    return [NSString stringWithString:output];
}

- (NSString *)MB_generateHash
{
    NSString *hashResult = nil;
    if (inputString_.length > 0) {
        switch (hashAlgorithm_) {
            case MBHashAlgorithmMD5:
                hashResult = [self MB_createMD5HashFromString:inputString_ usingEncoding:NSUTF8StringEncoding];
                break;
            case MBHashAlgorithmSHA1:
                hashResult = [self MB_createSHA1HashFromString:inputString_ usingEncoding:NSUTF8StringEncoding];
                break;
            default:
                break;
        }
    }
    return hashResult;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying protocol

- (id)copyWithZone:(NSZone *)zone
{
    return [[[self class] allocWithZone:zone] initWithString:inputString_ hashAlgorithm:hashAlgorithm_];
}


@end
