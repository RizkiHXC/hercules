//
//  MBMutableCollection.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/5/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The MBMutableCollection class declares the programmatic interface to objects that manage a modifiable 2 dimensional array of objects. 
 
 The collection class is typically used in combination with UIKit's table view or with a collection view.
 */
@interface MBMutableCollection : NSObject

/**
 Creates and returns an empty collection.
 @return A new empty collection.
 */
+ (MBMutableCollection *)collection;

// Adding objects
/**
 Inserts a given object at the end of the array in the specified section.
 @param object  The object to add to the end of the array's content. This value must not be nil.
 @param section An index number that identifies a section of the object.
 */
- (void)addObject:(id)object atSection:(NSInteger)section;

/**
 Replaces the array of objects at section with the objects in the array.
 @param section The section of the objects to be replaced.
 @param array   The objects with which to replace the objects at the section in the collection. This value must not be nil.
 */
- (void)replaceArrayAtSection:(NSInteger)section withArray:(NSArray *)array;
/**
 Inserts an array of objects into the collection at the given section index.
 @param section The objects to add to the collection's content. This value must not be nil.
 @param array   An index number that identifies the section at which to insert the objects.
 */
- (void)insertArray:(NSArray *)array atSection:(NSInteger)section;

// Counting objects
/**
 Returns the number of objects currently in the array in the specified section.
 @param section An index number that identifies a section of the objects.
 @return The number of objects currently in the section.
 */
- (NSUInteger)numberOfObjectsInSection:(NSInteger)section;
/**
 Returns the number of sections currently in the collection.
 @return The number of sections currently in the collection.
 */
- (NSUInteger)numberOfSections;

// Finding Objects
/**
 Returns an index path representing the index and section of a given object in the collection.
 @param object An object of the collection.
 @return An index path representing the index and section of the object or nil if the index path is invalid.
 */
- (NSIndexPath *)indexPathOfObject:(id)object;

// Querying the collection
/**
 Returns a Boolean value that indicates whether a given object is present in the collection.
 @param object An object.
 @return YES if anObject is present in the collection, otherwise NO.
 */
- (BOOL)containsObject:(id)object;
/**
 Returns the array of object slocated at the specified section.
 @param section An index number that identifies a section of the object array.
 @return The array objects located at section.
 */
- (NSArray *)arrayAtSection:(NSInteger)section;
/**
 Returns a new array containing all the objects in the collection.
 @return A new array containing the collection's objects, or an empty array if the collection has no objects.
 */
- (NSArray *)allObjects;
/**
 Returns the object at the specified index path.
 @param indexPath The index path locating the index and section in the receiver.
 @return An object or nil if indexPath is out of range.
 */
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;
/**
 Returns a new array containing all the sections in the collection.
 @return A new array containing the collection's sections, or an empty array if the collection has no sections.
 */
- (NSArray *)sections;


@end
