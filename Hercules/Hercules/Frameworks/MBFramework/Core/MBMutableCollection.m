//
//  MBMutableCollection.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/5/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBMutableCollection.h"

@interface MBMutableCollection ()
{
    NSMutableArray *sections_;
}
@end

@implementation MBMutableCollection

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (MBMutableCollection *)collection
{
    return [[[MBMutableCollection alloc] init] autorelease];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        sections_ = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [sections_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Adding Objects

- (void)addObject:(id)object atSection:(NSInteger)section
{
    NSMutableArray *array = [sections_ objectAtIndex:section];
    if (!array) {
        array = [NSMutableArray array];
        [sections_ addObject:array];
    }
    [array addObject:object];
}

- (void)replaceArrayAtSection:(NSInteger)section withArray:(NSArray *)array
{
    [sections_ replaceObjectAtIndex:section withObject:array];
}

- (void)insertArray:(NSArray *)array atSection:(NSInteger)section
{
    [sections_ insertObject:array atIndex:section];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Counting objects

- (NSUInteger)numberOfObjectsInSection:(NSInteger)section
{
    return [[sections_ objectAtIndex:section] count];
}

- (NSUInteger)numberOfSections
{
    return sections_.count;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Finding Objects

- (NSIndexPath *)indexPathOfObject:(id)object
{
    NSIndexPath *indexPath = nil;
    NSInteger   section = 0;
    NSInteger   numberOfSections = sections_.count;
    NSArray     *array = nil;
    NSUInteger  indexOfObject   = 0;
    
    while (section < numberOfSections && indexPath == nil) {
        array = [sections_ objectAtIndex:section];
        
        indexOfObject = [array indexOfObject:object];
        
        if (indexOfObject != NSNotFound) {
            indexPath = [NSIndexPath indexPathForRow:indexOfObject inSection:section];
        }
        section++;
    }
    return indexPath;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Querying the collection

- (BOOL)containsObject:(id)object
{
    BOOL result = NO;
    NSUInteger index = 0;
    NSUInteger count = sections_.count;
    
    while (index < count && result == NO) {
        NSArray *array = [sections_ objectAtIndex:index];
        result = [array containsObject:object];
        index++;
    }
    return result;
}

- (NSArray *)arrayAtSection:(NSInteger)section
{
    return [sections_ objectAtIndex:section];
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *array = [self arrayAtSection:indexPath.section];
    return [array objectAtIndex:indexPath.row];
}

- (NSArray *)allObjects
{
    NSMutableArray *allObjects = [NSMutableArray array];
    for (NSArray *array in sections_) {
        [allObjects addObjectsFromArray:array];
    }
    return [NSArray arrayWithArray:allObjects];
}

- (NSArray *)sections
{
    return [NSArray arrayWithArray:sections_];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Finding Objects in the collection





@end
