//
//  MBAppDeprecatedDataAdapter.h
//  MBFrameworkCatalog
//
//  Created by Marco Jonker on 2/14/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBBaseDataAdapter.h"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBAppDeprecatedDataAdapter : MBBaseDataAdapter

@end
