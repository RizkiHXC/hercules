//
//  MBAppDeprecatedDataAdapter.m
//  MBFrameworkCatalog
//
//  Created by Marco Jonker on 2/14/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBAppDeprecatedDataAdapter.h"
#import "MBDataMapping.h"
#import "MBAppDeprecatedDataObject.h"

@implementation MBAppDeprecatedDataAdapter

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

-(instancetype)init {
    NSArray* mappings = [NSArray arrayWithObjects:
                         [MBDataMapping typeMappingForField:@"url"                              property:@"url"                            type:MBDataMappingTypeUrl],
                         [MBDataMapping typeMappingForField:@"message"                          property:@"message"                         type:MBDataMappingTypeString],
                         nil];
    
    self = [super initWithObjectClass:[MBAppDeprecatedDataObject class] andMappings:mappings];
    
    if(self != nil) {
    }
    
    return self;
}

@end
