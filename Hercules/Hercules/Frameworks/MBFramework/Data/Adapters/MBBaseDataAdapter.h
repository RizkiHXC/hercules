//
//  MBBaseDataAdapter.h
//  MBFramework
//
//  Created by Marco Jonker on 3/25/13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBBaseDataAdapter : NSObject {
    NSString* objectField_;
    NSArray* mappings_;
    Class objectClass_;
}

@property (nonatomic, retain) NSString* objectField;
@property (nonatomic, readonly) NSArray* mappings;
@property (nonatomic, readonly) Class objectClass;

+(instancetype)sharedInstance;
+(instancetype)create;
-(instancetype)initWithObjectClass:(Class)objectClass andMappings:(NSArray*)mappings;


+(int)nullableValueToInt:(id)value defaultValue:(id)defaultValue;
+(NSInteger)nullableValueToInteger:(id)value defaultValue:(id)defaultValue;
+(NSString*)nullableValueToString:(id)value defaultValue:(id)defaultValue;
+(NSURL*)nullableValueToURL:(id)value defaultValue:(id)defaultValue;
+(NSArray*)nullableValueToArray:(id)value defaultValue:(id)defaultValue;
+(NSDate*)nullableValueToDate:(id)value format:(NSDateFormatter*)dateFormatter defaultValue:(id)defaultValue;
+(NSNumber*)nullableValueToNumber:(id)value defaultValue:(id)defaultValue;
+(NSDecimalNumber*)nullableValueToDecimalNumber:(id)value defaultValue:(id)defaultValue;
+(bool)nullableValueToBool:(id)value defaultValue:(id)defaultValue;
+(float)nullableValueToFloat:(id)value defaultValue:(id)defaultValue;
+(NSDictionary*)nullableValueToDictionary:(id)value defaultValue:(id)defaultValue;

-(NSArray*)fromArray:(NSArray*)array;
-(id)fromDictionary:(NSDictionary*)dictionary;
-(NSDictionary*)toDictionary:(id)object;
-(NSArray*)toArray:(NSArray*)array;
@end

@protocol MBBaseDataAdapterProtocol
-(id)fromDictionary:(NSDictionary*)dictionary;
-(NSDictionary*)toDictionary:(id)object;
@end
