//
//  MBBaseDataAdapter.m
//  MBFramework
//
//  Created by Marco Jonker on 3/25/13.
//
//

#import "MBBaseDataAdapter.h"
#import "MBDataMapping.h"

@implementation MBBaseDataAdapter

static NSMutableDictionary* sharedInstancesMap_ = nil;

@synthesize objectField=objectField_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(instancetype)initWithObjectClass:(Class)objectClass andMappings:(NSArray*)mappings {
    self  = [super init];
    
    if(self != nil) {
        mappings_ = [mappings retain];
        objectClass_ = objectClass;
        
    }
    return self;
}

+(instancetype)sharedInstance {
    //Prevent multiple threads from creating the instances map
    @synchronized(self) {
        if(sharedInstancesMap_ == nil)        {
            sharedInstancesMap_ = [[NSMutableDictionary alloc] init];
        }
    }
    
    Class       class           = [self class];
    NSString*   className       = NSStringFromClass(class);
    id          sharedInstance  = [sharedInstancesMap_ objectForKey:className];
    
    //Prevent multiple threads from creating the same instance
    @synchronized(self) {
        if(sharedInstance == nil) {
            sharedInstance = [[[class alloc] init] autorelease];
            [sharedInstancesMap_ setObject:sharedInstance forKey:className];
        }
    }
    return sharedInstance;
}

-(void) dealloc {
    Class      class        = [self class];
    NSString*  className    = NSStringFromClass(class);
    
    //Prevent multiple threads from creating the same instance
    @synchronized(self) {
        [sharedInstancesMap_ removeObjectForKey:className];
        
        if(sharedInstancesMap_.count == 0 ) {
            [sharedInstancesMap_ dealloc];
            sharedInstancesMap_ = nil;
        }
    }
    
    [objectField_ release];
    [mappings_ release];
    [super dealloc];
}

+(instancetype)create {
    return [[[self alloc]init]autorelease];
}

-(NSArray*)fromArray:(NSArray*)array{
    NSMutableArray* itemArray = [[[NSMutableArray alloc]init]autorelease];
    
    for(NSDictionary* dictionary in array) {
        [itemArray addObject:[self fromDictionary:dictionary]];
    }
    
    return [NSArray arrayWithArray:itemArray];
}

-(id)fromDictionary:(NSDictionary*)dictionary {
    id item = nil;
    
    if(dictionary != nil) {
        
        item = [[[objectClass_ alloc]init]autorelease];
        
        for(MBDataMapping* dataMapping in mappings_) {
            if(![[dictionary valueForKeyPath:dataMapping.field] isKindOfClass:[NSNull class]]) {
                [item setValue:[self MB_valueFromDictionary:dictionary withDataMapping:dataMapping] forKeyPath:dataMapping.property];
            } else if(dataMapping.required == YES) {
            }
        }
    }
    return item;
}

-(NSArray*)toArray:(NSArray*)array{
    NSMutableArray* itemArray = [[[NSMutableArray alloc]init]autorelease];
    
    for(id object in array) {
        [itemArray addObject:[self toDictionary:object]];
    }
    
    return [NSArray arrayWithArray:itemArray];
}

-(NSDictionary*)toDictionary:(id)object {
    NSDictionary* result = nil;
    
    if(object != nil) {
        
        NSMutableDictionary* itemDictionary = [NSMutableDictionary dictionary];
        
        for(MBDataMapping* dataMapping in mappings_) {
            [self MB_dictionary:itemDictionary setValue:[self MB_valueToDictionary:object withDataMapping:dataMapping] forKeyPath:dataMapping.field];
            //        [itemDictionary setValue:[self MB_valueToDictionary:object withDataMapping:dataMapping] forKeyPath:dataMapping.field];
        }
        result = [NSDictionary dictionaryWithDictionary:itemDictionary];
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public static functions

+(int)nullableValueToInt:(id)value defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSNumber class]]) ? [value intValue] : [defaultValue intValue];
}

+(NSInteger)nullableValueToInteger:(id)value defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSNumber class]]) ? [value integerValue] : [defaultValue intValue];
}

+(NSString*)nullableValueToString:(id)value defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSString class]]) ? (NSString*)value : defaultValue;
}

+(NSArray*)nullableValueToArray:(id)value defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSArray class]]) ? value : defaultValue;
}

+(NSDate*)nullableValueToDate:(id)value format:(NSDateFormatter*)dateFormatter defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSString class]]) ? [dateFormatter dateFromString:(NSString*)value] : defaultValue;
}

+(NSNumber*)nullableValueToNumber:(id)value defaultValue:(id)defaultValue {
    NSNumber* number = nil;
    
    if(value != nil &&  [value isKindOfClass:[NSNumber class]]) {
        number = value;
    } else if(value != nil &&  [value isKindOfClass:[NSString class]] ) {
        NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        number = [numberFormatter numberFromString:value];
        [numberFormatter release];
    }
    
    if(number == nil) {
        number = defaultValue;
    }
    return number;
}

+(NSDecimalNumber*)nullableValueToDecimalNumber:(id)value defaultValue:(id)defaultValue {
    NSDecimalNumber* number = nil;
    
    if(value != nil &&  [value isKindOfClass:[NSDecimalNumber class]]) {
        number = value;
    } else if(value != nil &&  [value isKindOfClass:[NSNumber class]]) {
        number = [NSDecimalNumber decimalNumberWithDecimal:[value decimalValue]];
    } else if(value != nil &&  [value isKindOfClass:[NSString class]] ) {
        number = [NSDecimalNumber decimalNumberWithString:value];
    }
    
    if(number == nil) {
        number = defaultValue;
    }
    return number;
}

+(bool)nullableValueToBool:(id)value defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSNumber class]]) ? [value boolValue] : [defaultValue boolValue];
}

+(float)nullableValueToFloat:(id)value defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSNumber class]]) ? [value floatValue] : [defaultValue floatValue];
}

+(NSDictionary*)nullableValueToDictionary:(id)value  defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSDictionary class]]) ? value : defaultValue;
}

+(NSURL*)nullableValueToURL:(id)value defaultValue:(id)defaultValue {
    return (value != nil && [value isKindOfClass:[NSString class]]) ? [NSURL URLWithString:(NSString *)value] : defaultValue;
}


//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

-(id)MB_valueFromDictionary:(NSDictionary*)dictionary withDataMapping:(MBDataMapping*)dataMapping {
    id value = nil;
    
    switch (dataMapping.type) {
        case MBDataMappingTypeInteger:
        case MBDataMappingTypeFloat:
        case MBDataMappingTypeNumber:
        case MBDataMappingTypeBool:
            value = [MBBaseDataAdapter nullableValueToNumber:[dictionary valueForKeyPath:dataMapping.field] defaultValue:dataMapping.defaultValue];
            break;
        case MBDataMappingTypeDecimalNumber:
            value = [MBBaseDataAdapter nullableValueToDecimalNumber:[dictionary valueForKeyPath:dataMapping.field] defaultValue:dataMapping.defaultValue];
            break;
        case MBDataMappingTypeString:
            value = [MBBaseDataAdapter nullableValueToString:[dictionary valueForKeyPath:dataMapping.field] defaultValue:dataMapping.defaultValue];
            break;
        case MBDataMappingTypeUrl:
            value = [MBBaseDataAdapter nullableValueToURL:[dictionary valueForKeyPath:dataMapping.field] defaultValue:dataMapping.defaultValue];
            break;
        case MBDataMappingTypeDate:
        {
            id date = [dictionary valueForKeyPath:dataMapping.field];
            if (date != nil && [date isKindOfClass:[NSString class]] && ![date isEqualToString:@"null"]) {
                // Don't use nullableValueToDate when date is nil because it sets today as default date
                value = [MBBaseDataAdapter nullableValueToDate:date format:dataMapping.dateFormatter defaultValue:dataMapping.defaultValue];
            }
            break;
        }
        case MBDataMappingTypeArray:
            value = [MBBaseDataAdapter nullableValueToArray:[dictionary valueForKeyPath:dataMapping.field] defaultValue:dataMapping.defaultValue];
            if(dataMapping.dataAdapterClass != nil) {
                value = [[dataMapping.dataAdapterClass create] fromArray:value];
            }
            break;
        case MBDataMappingTypeDictionary:
            value = [[dataMapping.dataAdapterClass create] fromDictionary:[MBBaseDataAdapter nullableValueToDictionary:[dictionary valueForKeyPath:dataMapping.field] defaultValue:dataMapping.defaultValue]];
            break;
        default:
            break;
    }
    
    return value;
}

-(id)MB_valueToDictionary:(id)object withDataMapping:(MBDataMapping*)dataMapping {
    id value = nil;
    
    switch (dataMapping.type) {
        case MBDataMappingTypeNumber:
        case MBDataMappingTypeDecimalNumber:
        case MBDataMappingTypeString:
        case MBDataMappingTypeBool:
        case MBDataMappingTypeInteger:
        case MBDataMappingTypeFloat:
            value = [object valueForKeyPath:dataMapping.property];
            break;
        case MBDataMappingTypeUrl:
            value = ((NSURL*)[object valueForKeyPath:dataMapping.property]).absoluteString;
            break;
        case MBDataMappingTypeDate:
            value = [dataMapping.dateFormatter stringFromDate:[object valueForKeyPath:dataMapping.property]];
            break;
        case MBDataMappingTypeArray:
            value = [object valueForKeyPath:dataMapping.property];
            if(dataMapping.dataAdapterClass != nil) {
                value = [[dataMapping.dataAdapterClass create] toArray:value];
            }
            break;
        case MBDataMappingTypeDictionary:
            value = [[dataMapping.dataAdapterClass create] toDictionary:[object valueForKeyPath:dataMapping.property]];
            break;
        default:
            break;
    }
    
    return value;
}

- (void)MB_dictionary:(NSMutableDictionary *)dictionary setValue:(id)value forKeyPath:(NSString *)keyPath {
    
    // Check if keypath exists
    NSMutableDictionary *node = nil;
    NSMutableDictionary *parent = dictionary;
    
    NSArray *keys = [keyPath componentsSeparatedByString:@"."];
    
    if (keys.count > 1) {
        for (NSUInteger index = 0; index < keys.count; index++) {
            NSString *key = [keys objectAtIndex:index];
            
            node = [parent objectForKey:key];
            
            if (![node isKindOfClass:[NSDictionary class]] && index < (keys.count - 1)) {
                node = [NSMutableDictionary dictionary];
                [parent setObject:node forKey:key];
            }
            parent = node;
        }
    }
    
    [dictionary setValue:value forKeyPath:keyPath];
}



@end
