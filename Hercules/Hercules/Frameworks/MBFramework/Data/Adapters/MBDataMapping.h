//
//  MBDataMapping.h
//  MBFramework
//
//  Created by Marco Jonker on 6/26/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBBaseDataAdapter.h"

/**
 MBDataMappingType describes the type where the reciever should map it's content to.
 */
typedef NS_ENUM(NSInteger, MBDataMappingType) {
    /**
     The receiver’s value is mapped to type NSInteger.
     */
    MBDataMappingTypeInteger,
    /**
     The receiver’s value is mapped to type float.
     */
    MBDataMappingTypeFloat,
    /**
     The receiver’s value is mapped to type BOOL.
     */
    MBDataMappingTypeBool,
    /**
     The receiver’s value is mapped to type NSNumber.
     */
    MBDataMappingTypeNumber,
    /**
     The receiver’s value is mapped to type NSString.
     */
    MBDataMappingTypeString,
    /**
     The receiver’s value is mapped to type NSURL.
     */
    MBDataMappingTypeUrl,
    /**
     The receiver’s value is mapped to type NSDate.
     */
    MBDataMappingTypeDate,
    /**
     The receiver’s value is mapped to type NSArray.
     */
    MBDataMappingTypeArray,
    /**
     The receiver’s value is mapped to type NSDictionairy.
     */
    MBDataMappingTypeDictionary,
    /**
     The receiver’s value is mapped to type NSDecimalNumber.
     */
    MBDataMappingTypeDecimalNumber
};

/**
 A data mapper is a class that is used to map JSON based data to an Objective-C object.
 */
@interface MBDataMapping : NSObject {
    NSString*               field_;
    NSString*               property_;
    MBDataMappingType       type_;
    NSDateFormatter*        dateFormatter_;
    Class                   dataAdapterClass_;
    bool                    required_;
    bool                    isObject_;
    id                      defaultValue_;
}
/**
 Name of the JSON field used for mapping.
 */
@property (nonatomic, retain) NSString*             field;
/**
 Property name for the data to be mapped to.
 @discussion When property is left empty, the value in field is used for the property name.
 */
@property (nonatomic, retain) NSString*             property;
/**
 Describes the type where the reciever should map it's content to.
 */
@property (nonatomic, assign) MBDataMappingType     type;
/**
 Date formatter used to parse a date when field contains a date value.
 */
@property (nonatomic, retain) NSDateFormatter*      dateFormatter;
/**
 A MBDataAdapter based class to be used to create a mapped class.
 */
@property (nonatomic, assign) Class                 dataAdapterClass;
/**
 A Boolean value that indicates if the mapping is required.
 */
@property (nonatomic, assign) bool                  required;
/**
 A Boolean value that indicates if the object is an object that inherits from NSObject.
 */
@property (nonatomic, readonly) bool                isObject;
/**
 A value that can be used to provide a default value for the object when the object can't be successfully mapped.
 */
@property (nonatomic, retain) id                    defaultValue;


/**
 Maps the field to the specified datatype.
 @param field    Name of the JSON field used for mapping.
 @param type     The type where the reciever should map it's content to.
 @return A mapped datatype or nil if the data object can't be mapped
 */
+(MBDataMapping*)typeMappingForField:(NSString*)field type:(MBDataMappingType)type;
/**
 Maps the field to the specified datatype.
 @param field    Name of the JSON field used for mapping.
 @param property Optional property name for the data to be mapped to.
 @param type     The type where the reciever should map it's content to.
 @return A mapped datatype or nil if the data object can't be mapped
 */
+(MBDataMapping*)typeMappingForField:(NSString*)field property:(NSString*)property type:(MBDataMappingType)type;
/**
 Maps the field to the a date value.
 @param field    Name of the JSON field used for mapping.
 @param dateFormatter     Date formatter used to parse the date.
 @return A mapped datatype or nil if the data object can't be mapped
 */
+(MBDataMapping*)dateMappingForField:(NSString*)field dateFormatter:(NSDateFormatter*)dateFormatter;
/**
 Maps the field to the a date value.
 @param field    Name of the JSON field used for mapping.
 @param property Optional property name for the data to be mapped to.
 @param dateFormatter     Date formatter used to parse the date.
 @return A mapped datatype or nil if the data object can't be mapped
 */
+(MBDataMapping*)dateMappingForField:(NSString*)field property:(NSString*)property dateFormatter:(NSDateFormatter*)dateFormatter;
/**
 Maps the field to an array of objects with a data adapter.
 @param field    Name of the JSON field used for mapping.
 @param dataAdapterClass     A MBDataAdapter based class to be used to create a mapped class..
 @return A mapped datatype or nil if the data object can't be mapped
 */
+(MBDataMapping*)arrayMappingForField:(NSString*)field dataAdapter:(Class)dataAdapterClass;
/**
 Maps the field to an array of objects with a data adapter.
 @param field    Name of the JSON field used for mapping.
 @param property Optional property name for the data to be mapped to.
 @param dataAdapterClass     A MBDataAdapter based class to be used to create a mapped class..
 @return A mapped datatype or nil if the data object can't be mapped
 */
+(MBDataMapping*)arrayMappingForField:(NSString*)field property:(NSString*)property dataAdapter:(Class)dataAdapterClass;
/**
 Maps the field to a dictionairy of objects with a data adapter.
 @param field    Name of the JSON field used for mapping.
 @param dataAdapterClass     A MBDataAdapter based class to be used to create a mapped class..
 @return A mapped datatype or nil if the data object can't be mapped
 */
+(MBDataMapping*)dictionaryMappingForField:(NSString*)field dataAdapter:(Class)dataAdapterClass;
/**
 Maps the field to a dictionairy of objects with a data adapter.
 @param field    Name of the JSON field used for mapping.
 @param property Optional property name for the data to be mapped to.
 @param dataAdapterClass     A MBDataAdapter based class to be used to create a mapped class..
 @return A mapped datatype or nil if the data object can't be mapped
 */
+(MBDataMapping*)dictionaryMappingForField:(NSString*)field property:(NSString*)property dataAdapter:(Class)dataAdapterClass;

@end
