
//
//  MBDataMapping.m
//  MBFramework
//
//  Created by Marco Jonker on 6/26/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBDataMapping.h"

@implementation MBDataMapping

@synthesize field=field_;
@synthesize property=property_;
@synthesize type=type_;
@synthesize dateFormatter=dateFormatter_;
@synthesize dataAdapterClass=dataAdapterClass_;
@synthesize required=required_;
@synthesize isObject=isObject_;
@synthesize defaultValue=defaultValue_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (void)dealloc {
    self.field          = nil;
    self.property       = nil;
    self.dateFormatter  = nil;
    self.defaultValue   = nil;
    [super dealloc];
}

-(id)MB_standardDefaultValueForType:(MBDataMappingType)type{
    id value = nil;
   
    switch(type) {
        case MBDataMappingTypeInteger:
        case MBDataMappingTypeFloat:
        case MBDataMappingTypeBool:
            value = [NSNumber numberWithInt:0];
            break;
        case MBDataMappingTypeNumber:
        case MBDataMappingTypeDecimalNumber:
            value = nil;
            break;
        case MBDataMappingTypeString:
            value = @"";
            break;
        case MBDataMappingTypeUrl:
            value = [NSURL URLWithString:@""];
            break;
        case MBDataMappingTypeDate:
            value = [NSDate date];
            break;
        case MBDataMappingTypeArray:
            value = [NSArray array];
            break;
        case MBDataMappingTypeDictionary:
            value = [NSDictionary dictionary];
            break;
        default:
            value = nil;
            break;
            
    }
    return value;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(void)setType:(MBDataMappingType)type {
    type_ = type;
    if(type_ == MBDataMappingTypeInteger ||
       type_ == MBDataMappingTypeFloat ||
       type_ == MBDataMappingTypeBool) {
        isObject_ = NO;
    } else {
        isObject_ = YES;
    }
}

+(MBDataMapping*)typeMappingForField:(NSString*)field property:(NSString*)property type:(MBDataMappingType)type{
    MBDataMapping* dataMapping = [[[MBDataMapping alloc]init]autorelease];
    dataMapping.field        = field;
    dataMapping.property     = property;
    dataMapping.type         = type;
    dataMapping.required     = NO;
    dataMapping.defaultValue = [dataMapping MB_standardDefaultValueForType:type];
    return dataMapping;
}

+(MBDataMapping*)typeMappingForField:(NSString*)field type:(MBDataMappingType)type {
    return [MBDataMapping typeMappingForField:field property:field type:type];
}

+(MBDataMapping*)dateMappingForField:(NSString*)field property:(NSString*)property dateFormatter:(NSDateFormatter*)dateFormatter {
    MBDataMapping* dataMapping = [MBDataMapping typeMappingForField:field property:property type:MBDataMappingTypeDate];
    dataMapping.dateFormatter   = dateFormatter;
    return dataMapping;
}

+(MBDataMapping*)dateMappingForField:(NSString*)field dateFormatter:(NSDateFormatter*)dateFormatter {
    return [MBDataMapping dateMappingForField:field property:field dateFormatter:dateFormatter];
}

+(MBDataMapping*)arrayMappingForField:(NSString*)field property:(NSString*)property dataAdapter:(Class)dataAdapterClass {
    MBDataMapping* dataMapping = [MBDataMapping typeMappingForField:field property:property type:MBDataMappingTypeArray];
    dataMapping.dataAdapterClass   = dataAdapterClass;
    return dataMapping;
}

+(MBDataMapping*)arrayMappingForField:(NSString*)field dataAdapter:(Class)dataAdapterClass {
    return [MBDataMapping arrayMappingForField:field property:field dataAdapter:dataAdapterClass];
}

+(MBDataMapping*)dictionaryMappingForField:(NSString*)field property:(NSString*)property dataAdapter:(Class)dataAdapterClass{
    MBDataMapping* dataMapping = [MBDataMapping typeMappingForField:field property:property type:MBDataMappingTypeDictionary];
    dataMapping.dataAdapterClass   = dataAdapterClass;
    return dataMapping;
}

+(MBDataMapping*)dictionaryMappingForField:(NSString*)field dataAdapter:(Class)dataAdapterClass {
    return [MBDataMapping dictionaryMappingForField:field property:field dataAdapter:dataAdapterClass];
}



@end
