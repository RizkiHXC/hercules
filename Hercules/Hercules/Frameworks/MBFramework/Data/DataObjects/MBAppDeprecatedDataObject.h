//
//  MBAppDeprecatedDataObject.h
//  MBFrameworkCatalog
//
//  Created by Marco Jonker on 2/14/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBBaseDataObject.h"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBAppDeprecatedDataObject : MBBaseDataObject{
    NSURL* url_;
    NSString* message_;
}

@property(nonatomic,retain) NSURL* url;
@property(nonatomic,retain) NSString* message;

@end
