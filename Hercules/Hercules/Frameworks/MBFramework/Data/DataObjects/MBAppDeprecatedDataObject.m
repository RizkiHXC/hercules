//
//  MBAppDeprecatedDataObject.m
//  MBFrameworkCatalog
//
//  Created by Marco Jonker on 2/14/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBAppDeprecatedDataObject.h"

@implementation MBAppDeprecatedDataObject

@synthesize url=url_;
@synthesize message=message_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

-(void)dealloc {
    [url_ release];
    [message_ release];
    [super dealloc];
}

@end
