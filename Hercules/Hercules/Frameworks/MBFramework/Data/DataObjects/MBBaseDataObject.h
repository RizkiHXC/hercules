//
//  MBBaseDataObject.h
//  MBFramework
//
//  Created by Marco Jonker on 3/25/13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 MBBaseDataObject is a generic class that implements all the basic behavior required of an api data model model object.
 */
@interface MBBaseDataObject : NSObject <NSCopying>

/**
 Returns a new instance that’s a copy of the receiver. (required)
 @param zone The zone identifies an area of memory from which to allocate for the new instance. If zone is NULL, the new instance is allocated from the default zone, which is returned from the function NSDefaultMallocZone.
 @return The returned object is implicitly retained by the sender, who is responsible for releasing it. The copy returned is immutable if the consideration “immutable vs. mutable” applies to the receiving object; otherwise the exact nature of the copy is determined by the class.
 */
- (id)copyWithZone:(NSZone *)zone;

@end
