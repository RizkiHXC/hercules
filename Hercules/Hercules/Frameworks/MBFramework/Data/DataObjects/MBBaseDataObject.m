//
//  MBBaseDataObject.m
//  MBFramework
//
//  Created by Marco Jonker on 3/25/13.
//
//

#import "MBBaseDataObject.h"

@implementation MBBaseDataObject

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone
{
    return [[[self class] allocWithZone:zone] init];
}

@end
