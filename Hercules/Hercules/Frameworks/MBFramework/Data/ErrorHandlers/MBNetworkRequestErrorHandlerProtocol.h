//
//  MBNetworkRequestErrorHandlerProtocol.h
//  MBFramework
//
//  Created by Marco Jonker on 3/26/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBApiRequest.h"

@protocol MBNetworkRequestErrorHandlerProtocol <NSObject>

//If the result is true the error will be market as handled and will reach the completion handler
-(BOOL)handleRequest:(MBApiRequest*)request error:(NSError*)error responseData:(id)response;

@end
