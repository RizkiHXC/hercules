//
//  MBStandardNetworkRequestErrorHandler.h
//  MBFramework
//
//  Created by Marco Jonker on 3/26/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBNetworkRequestErrorHandlerProtocol.h"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBStandardNetworkRequestErrorHandler : NSObject <MBNetworkRequestErrorHandlerProtocol>

@end
