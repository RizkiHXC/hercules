//
//  MBStandardNetworkRequestErrorHandler.m
//  MBFramework
//
//  Created by Marco Jonker on 3/26/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBStandardNetworkRequestErrorHandler.h"
#import "MBAppDeprecatedDataObject.h"
#import "NSBundle+MBProperties.h"
#import "UIAlertView+MBCompletionBlock.h"

@implementation MBStandardNetworkRequestErrorHandler

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MBNetworkRequestErrorHandlerProtocol

-(BOOL)handleRequest:(MBApiRequest *)request error:(NSError *)error responseData:(id)response {
    BOOL result = NO;
    if(request.isAppDeprecated) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString*   title   = [[NSBundle mainBundle]displayName];
            NSString*   message = @"Your version App is deprecated, download the new version in the App store";
            NSURL*      url     = nil;
            
            if([response isKindOfClass:[MBAppDeprecatedDataObject class]]) {
                MBAppDeprecatedDataObject* appDeprecatedObject = (MBAppDeprecatedDataObject*)response;
                
                if(appDeprecatedObject.url != nil && appDeprecatedObject.url.absoluteString.length > 0) {
                    url = appDeprecatedObject.url;
                }

                if(appDeprecatedObject.message != nil && appDeprecatedObject.message.length > 0) {
                    message = appDeprecatedObject.message;
                }
            }
            
            UIAlertView* alertView = [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil]autorelease];
 
            if(url.absoluteString.length > 0) {
                
                [alertView showWithCompletion:^(NSInteger clickedButtonIndex) {
                    [[UIApplication sharedApplication] openURL:url];
                }];
            } else {
                [alertView show];
            }
        });

        result = YES;
    }
    
    return result;
}

@end
