/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBDownloadQueue : NSObject {
    NSMutableDictionary *operationQueues_;
    NSInteger maxConcurrentOperationCount_;
}

+(instancetype)sharedInstance;
-(void)cancelAllOperationsForQueueWithName:(NSString*)queueName;
-(void)cancelAllOperations;
-(void)addQueue:(NSString *)queueName withMaxConcurrentOperations:(NSInteger)maxConcurrentOperations;
-(void)setMaxConcurrentOperations:(NSInteger)maxConcurrentOperations forQueueWithNAme:(NSString *)queueName;
-(void)addOperation:(NSOperation *)request forQueueWithName:(NSString*)queueName;

@property(nonatomic,assign) NSInteger maxConcurrentOperationCount;

@end
