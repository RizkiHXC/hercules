
#import "MBDownloadQueue.h"

#define DEFAULT_CONCURRENT_OPERATIONS 4

static MBDownloadQueue *sharedInstance_ = nil;

@implementation MBDownloadQueue

@synthesize maxConcurrentOperationCount = maxConcurrentOperationCount_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject
+ (instancetype)sharedInstance {
    if(!sharedInstance_)
        sharedInstance_ = [[self alloc] init];
    return sharedInstance_;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        operationQueues_ = [[NSMutableDictionary alloc]init];
        maxConcurrentOperationCount_ = DEFAULT_CONCURRENT_OPERATIONS;
    }
    return self;
}

- (void)dealloc {
    [operationQueues_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

-(void)cancelAllOperationsForQueueWithName:(NSString*)queueName {
    if([operationQueues_ objectForKey:queueName] == nil) {
        NSOperationQueue* queue = (NSOperationQueue*)[operationQueues_ valueForKey:queueName];
        [queue cancelAllOperations];
    }
}

- (void)cancelAllOperations {
    NSEnumerator *enumerator = [operationQueues_ keyEnumerator];
    NSString* queueName;
    while ((queueName = [enumerator nextObject])) {
        [self cancelAllOperationsForQueueWithName:queueName];
    }
}

- (void)addQueue:(NSString *)queueName withMaxConcurrentOperations:(NSInteger)maxConcurrentOperations {
    if([operationQueues_ objectForKey:queueName] == nil) {
        NSOperationQueue* queue = [[[NSOperationQueue alloc] init]autorelease];
        [queue setMaxConcurrentOperationCount:maxConcurrentOperations];
        [operationQueues_ setValue:queue forKey:queueName];
    }
}

- (void)setMaxConcurrentOperations:(NSInteger)maxConcurrentOperations forQueueWithNAme:(NSString *)queueName {
    if([operationQueues_ objectForKey:queueName] == nil) {
        NSOperationQueue* queue = (NSOperationQueue*)[operationQueues_ valueForKey:queueName];
        [queue setMaxConcurrentOperationCount:maxConcurrentOperations];
    }
}

- (void)addOperation:(NSOperation *)request forQueueWithName:(NSString*)queueName{
    if([operationQueues_ objectForKey:queueName] == nil) {
        [self addQueue:queueName withMaxConcurrentOperations:maxConcurrentOperationCount_];
        [operationQueues_ setValue:[[[NSOperationQueue alloc] init]autorelease] forKey:queueName];
    }
    
    NSOperationQueue* queue =[operationQueues_ valueForKey:queueName];
    [queue addOperation:request];
}
@end
