//
//  MBApiPortal.h
//  MBFrameworkCatalog
//
//  Created by Marco Jonker on 2/14/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBApiRequest.h"
#import "MBNetworkRequestErrorHandlerProtocol.h"

@protocol MBApiPortalObserver;

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBApiPortal : NSObject

@property (nonatomic, assign) NSURLRequestCachePolicy   POSTCachePolicy;
@property (nonatomic, assign) BOOL                      POSTFallbackToCacheOnFailure;
@property (nonatomic, assign) NSTimeInterval            POSTTimeOutSeconds;
@property (nonatomic, assign) NSURLRequestCachePolicy   GETCachePolicy;
@property (nonatomic, assign) BOOL                      GETFallbackToCacheOnFailure;
@property (nonatomic, assign) NSTimeInterval            GETTimeOutSeconds;
@property (nonatomic, assign) NSInteger                 concurrentOperations;
@property (nonatomic, retain) NSString*                 defaultApiKey;
@property (nonatomic, retain) id<MBNetworkRequestErrorHandlerProtocol> errorHandler;


+(instancetype)sharedInstance;
+(bool)isAppDeprecatedError:(NSError*)error;

-(void)useCacheWithMemoryCapacity:(NSInteger)memory diskCapacity:(NSInteger)capacity;
-(void)processApiRequest:(MBApiRequest*)request;
-(void)processApiRequest:(MBApiRequest*)request completion:(MBApiRequestCompletionHandler)completionHandler;

@end
