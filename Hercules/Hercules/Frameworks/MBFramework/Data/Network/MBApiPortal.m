//
//  MBApiPortal.m
//  MBFrameworkCatalog
//
//  Created by Marco Jonker on 2/14/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBApiPortal.h"
#import "MBServerOperationsQueue.h"
#import "MBHTTPRequestOperation.h"
#import "MBAppDeprecatedDataAdapter.h"
#import "MBAppDeprecatedDataObject.h"
#import "AFNetworkActivityIndicatorManager.h"

#define SERVER_OPERATION_REQUEST       @"requests"
#define CACHE_NAME      @"mbserverportal_cache"

static const NSInteger kDefaultConcurrentOperations = 4;

static id sharedInstance_ = nil;

@interface MBApiPortal() {
    NSMutableArray* requests_;
}
@end

@implementation MBApiPortal

@synthesize GETCachePolicy=GETCachePolicy_;
@synthesize GETTimeOutSeconds=GETTimeOutSeconds_;
@synthesize GETFallbackToCacheOnFailure=GETFallbackToCacheOnFailure_;
@synthesize concurrentOperations=concurrentOperations_;
@synthesize defaultApiKey=defaultApiKey_;
@synthesize errorHandler=errorHandler_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init {
    self = [super init];
    if (self) {
        requests_ = [[NSMutableArray alloc] init];
        self.GETTimeOutSeconds                      = 15;
        self.GETCachePolicy                         = NSURLRequestUseProtocolCachePolicy;
        self.GETFallbackToCacheOnFailure            = NO;
        self.concurrentOperations                   = kDefaultConcurrentOperations;
        self.defaultApiKey                          = @"";
        
        // AFNetworkActivityIndicatorManager manages the state of the network activity indicator in the status bar.
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    }
    return self;
}

- (void)dealloc {
    [requests_ makeObjectsPerformSelector:@selector(clearDelegatesAndCancel)];
    [requests_ release];
    [super dealloc];
}

+ (instancetype)sharedInstance {
    if(!sharedInstance_) {
        sharedInstance_ = [[self alloc] init];
    }
    
    return sharedInstance_;
}

-(void)useCacheWithMemoryCapacity:(NSInteger)memory diskCapacity:(NSInteger)capacity {
    NSURLCache *cache = [[[NSURLCache alloc] initWithMemoryCapacity:memory diskCapacity:capacity diskPath:CACHE_NAME]autorelease];
    [NSURLCache setSharedURLCache:cache];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

-(void)MB_startRequest:(MBApiRequest *)request {
    if(request.request.URL != nil && request.request.URL.absoluteString.length > 0 ) {
        
        if(![requests_ containsObject:request]) {
            [requests_ addObject:request];
            [[MBServerOperationsQueue sharedInstance] setMaxConcurrentOperations:self.concurrentOperations forQueueWithName:SERVER_OPERATION_REQUEST];
            [[MBServerOperationsQueue sharedInstance] addOperation:request forQueueWithName:SERVER_OPERATION_REQUEST];
        } else {
            NSAssert(FALSE, @"Same request object can not be fired twice");
        }
    }
}

- (void)MB_cleanupRequest:(MBApiRequest*)request {
    [requests_ removeObject:request];
}

-(void)MB_handleError:(NSError*)error forRequest:(MBApiRequest*)request withData:(id)data  completion:(MBApiRequestCompletionHandler)completionHandler{
    if(errorHandler_ == nil || [errorHandler_ handleRequest:request error:error responseData:data] == NO) {
        [request callDidReceiveErrorDelegate:error withData:data];
        if (completionHandler) {
            completionHandler(request, data, error);
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

+(bool)isAppDeprecatedError:(NSError*)error {
    NSError* underlyingError = [error.userInfo valueForKey:NSUnderlyingErrorKey];
    return (error.code == MBRequestErrApiResult && underlyingError != nil && underlyingError.code == MBBaseApiErrAppVersionDeprecated);
}

-(void)processApiRequest:(MBApiRequest*)request completion:(MBApiRequestCompletionHandler)completionHandler
{
    __block id              me             = self;
    __block MBApiRequest*   currentRequest = request;
    
    [request setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(operation.isCancelled == NO) {
            NSError* outError = nil;
            id data = [request handleRequestFinished:responseObject outError:&outError];
            
            if(outError == nil) {
                [request callDidReceiveObjectDelegate:data];
                
                if (completionHandler) {
                    completionHandler(currentRequest, data, nil);
                }
            } else {
                [me MB_handleError:outError forRequest:currentRequest withData:data completion:completionHandler];
            }
        }
        
        [me MB_cleanupRequest:currentRequest];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if([operation isCancelled] == NO) {
            NSError* outError = nil;
            [currentRequest handleRequestFailed:error outError:&outError];
            [me MB_handleError:outError forRequest:currentRequest withData:nil completion:completionHandler];
        }
        [me MB_cleanupRequest:currentRequest];
    }];
    
    [self MB_startRequest:request];
}

-(void)processApiRequest:(MBApiRequest*)request {
    [self processApiRequest:request completion:nil];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setConcurrentOperations:(NSInteger)concurrentOperations
{
    concurrentOperations_ = concurrentOperations;
    [[MBServerOperationsQueue sharedInstance] setMaxConcurrentOperations:concurrentOperations forQueueWithName:SERVER_OPERATION_REQUEST];
}


@end
