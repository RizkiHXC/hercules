//
//  MBHTTPRequestOperation.h
//  afnetworkingtest
//
//  Created by Marco Jonker on 10/21/13.
//  Copyright (c) 2013 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AFHTTPRequestOperation.h"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBHTTPRequestOperation : AFHTTPRequestOperation {
    bool fallbackToCacheOnFailure_;
}

@property (nonatomic, assign) bool fallbackToCacheOnFailure;

@end
