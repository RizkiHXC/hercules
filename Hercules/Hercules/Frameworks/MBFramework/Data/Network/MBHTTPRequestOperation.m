//
//  MBHTTPRequestOperation.m
//  afnetworkingtest
//
//  Created by Marco Jonker on 10/21/13.
//  Copyright (c) 2013 Mediabunker. All rights reserved.
//

#import "MBHTTPRequestOperation.h"
#import "MBJSONResponseSerializerWithData.h"

@implementation MBHTTPRequestOperation

@synthesize fallbackToCacheOnFailure=fallbackToCacheOnFailure_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(instancetype)initWithRequest:(NSURLRequest *)urlRequest {
    self = [super initWithRequest:urlRequest];
    if(self != nil) {
        fallbackToCacheOnFailure_ = NO;
        self.responseSerializer = [MBJSONResponseSerializerWithData serializer];
    }
    
    return self;
}

- (void)setCompletionBlockWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [super setCompletionBlockWithSuccess:success
                                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                     if(fallbackToCacheOnFailure_ == YES) {
                                         NSCachedURLResponse *cachedResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:self.request];
                                         
                                         if(cachedResponse != nil && cachedResponse.data.length > 0 ) {
                                             NSError *error = nil;
                                             id object = [self.responseSerializer responseObjectForResponse:cachedResponse.response data:cachedResponse.data error:&error];
                                             
                                             if (error == nil) {
                                                 success(operation, object);
                                             } else {
                                                 failure(operation, error);
                                             }
                                         } else {
                                             failure(operation, error);
                                         }
                                     } else {
                                         failure(operation, error);
                                     }
                                 }];
}

@end
