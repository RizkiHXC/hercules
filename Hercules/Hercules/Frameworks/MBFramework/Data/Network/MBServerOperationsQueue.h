
/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBServerOperationsQueue : NSObject

+(instancetype)sharedInstance;
-(void)cancelAllOperationsForQueueWithName:(NSString*)queueName;
-(void)cancelAllOperations;
-(void)addQueue:(NSString *)queueName withMaxConcurrentOperations:(NSInteger)maxConcurrentOperations;
-(void)setMaxConcurrentOperations:(NSInteger)maxConcurrentOperations forQueueWithName:(NSString *)queueName;
-(void)addOperation:(NSOperation *)request forQueueWithName:(NSString*)queueName;

@end
