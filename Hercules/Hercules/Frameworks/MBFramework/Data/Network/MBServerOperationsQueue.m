
#import "MBServerOperationsQueue.h"
#import "MBServerPortal.h"

static id sharedInstance_ = nil;

@interface MBServerOperationsQueue() {
    NSMutableDictionary *operationQueues_;
}

@end

@implementation MBServerOperationsQueue

static const NSInteger kDefaultConcurrentOperations = 4;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

+ (instancetype)sharedInstance {
    if(!sharedInstance_)
        sharedInstance_ = [[self alloc] init];
    return sharedInstance_;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        operationQueues_ = [[NSMutableDictionary alloc]init];
    }
    return self;
}

- (void)dealloc {
    [operationQueues_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

-(void)cancelAllOperationsForQueueWithName:(NSString*)queueName {
    if([operationQueues_ objectForKey:queueName] == nil) {
        NSOperationQueue* queue = (NSOperationQueue*)[operationQueues_ valueForKey:queueName];
        [queue cancelAllOperations];
    }
}

- (void)cancelAllOperations {
    NSEnumerator *enumerator = [operationQueues_ keyEnumerator];
    NSString* queueName;
    while ((queueName = [enumerator nextObject])) {
        [self cancelAllOperationsForQueueWithName:queueName];
    }
}

- (void)addQueue:(NSString *)queueName withMaxConcurrentOperations:(NSInteger)maxConcurrentOperations{
    if([operationQueues_ objectForKey:queueName] == nil) {
        NSOperationQueue* queue = [[[NSOperationQueue alloc] init]autorelease];
        [queue setMaxConcurrentOperationCount:maxConcurrentOperations];
        [operationQueues_ setValue:queue forKey:queueName];
    }
}

- (void)setMaxConcurrentOperations:(NSInteger)maxConcurrentOperations forQueueWithName:(NSString *)queueName {
    if([operationQueues_ objectForKey:queueName] == nil) {
        NSOperationQueue* queue = (NSOperationQueue*)[operationQueues_ valueForKey:queueName];
        [queue setMaxConcurrentOperationCount:maxConcurrentOperations];
    }
}

- (void)addOperation:(NSOperation *)request forQueueWithName:(NSString*)queueName{
    if([operationQueues_ objectForKey:queueName] == nil) {
        [self addQueue:queueName withMaxConcurrentOperations:kDefaultConcurrentOperations];
        [operationQueues_ setValue:[[[NSOperationQueue alloc] init]autorelease] forKey:queueName];
    }
    
    NSOperationQueue* queue =[operationQueues_ valueForKey:queueName];
    [queue addOperation:request];
}
@end
