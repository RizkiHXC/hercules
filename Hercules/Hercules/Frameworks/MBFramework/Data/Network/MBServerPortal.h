#import "MBNetworkRequest.h"

@protocol MBServerPortalObserver;

DEPRECATED_MSG_ATTRIBUTE("Use MBApiPortal instead")
@interface MBServerPortal : NSObject {
    NSURLRequestCachePolicy     POSTCachePolicy_;
    bool                        POSTFallbackToCacheOnFailure_;
    NSTimeInterval              POSTTimeOutSeconds_;
    NSURLRequestCachePolicy     GETCachePolicy_;
    bool                        GETFallbackToCacheOnFailure_;
    NSTimeInterval              GETTimeOutSeconds_;
    int                         concurrentOperations_;
    NSString*                   defaultApiKey_;
    
}

@property (nonatomic, assign) NSURLRequestCachePolicy   POSTCachePolicy;
@property (nonatomic, assign) bool                      POSTFallbackToCacheOnFailure;
@property (nonatomic, assign) NSTimeInterval            POSTTimeOutSeconds;
@property (nonatomic, assign) NSURLRequestCachePolicy   GETCachePolicy;
@property (nonatomic, assign) bool                      GETFallbackToCacheOnFailure;
@property (nonatomic, assign) NSTimeInterval            GETTimeOutSeconds;
@property (nonatomic, assign) int                       concurrentOperations;
@property (nonatomic, retain) NSString*                 defaultApiKey;


+(instancetype)sharedInstance;
-(void)useCacheWithMemoryCapacity:(NSInteger)memory diskCapacity:(NSInteger)capacity;
-(void)processNetworkRequest:(MBNetworkRequest*)request;

- (void)addObserver:(id<MBServerPortalObserver>)observer;
- (void)removeObserver:(id<MBServerPortalObserver>)observer;

@end

DEPRECATED_MSG_ATTRIBUTE("MBServerPortal is deprecated")
@protocol MBServerPortalObserver <NSObject>
@optional
-(void)serverPortal:(MBServerPortal*)serverPortal didReceiveError:(NSError*)error forRequest:(MBNetworkRequest*)request;
-(void)serverPortal:(MBServerPortal*)serverPortal didReceiveData:(id)data forRequest:(MBNetworkRequest*)request;
-(void)serverPortal:(MBServerPortal*)serverPortal appVersionWasDeprecatedWithMessage:(NSString*)message andUrl:(NSString*)url forRequest:(MBNetworkRequest*)request;
@end