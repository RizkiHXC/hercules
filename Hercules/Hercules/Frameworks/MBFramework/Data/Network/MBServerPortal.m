
#import "MBServerPortal.h"
#import "MBServerOperationsQueue.h"
#import "MBObserverController.h"

#define SERVER_OPERATION_REQUEST       @"requests"
#define DEFAULT_CONCURRENT_OPERATIONS 4
#define CACHE_NAME      @"mbserverportal_cache"

static id sharedInstance_ = nil;

@interface MBServerPortal() {
    NSMutableDictionary* requests_;
    MBObserverController* observerController_;
}
@end

@implementation MBServerPortal

@synthesize GETCachePolicy=GETCachePolicy_;
@synthesize GETTimeOutSeconds=GETTimeOutSeconds_;
@synthesize GETFallbackToCacheOnFailure=GETFallbackToCacheOnFailure_;
@synthesize concurrentOperations=concurrentOperations_;
@synthesize defaultApiKey=defaultApiKey_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init {
    self = [super init];
    if (self) {
        requests_ = [[NSMutableDictionary alloc] init];
        observerController_ = [[MBObserverController alloc]init];
        self.GETTimeOutSeconds                      = 15;
        self.GETCachePolicy                         = NSURLRequestUseProtocolCachePolicy;
        self.GETFallbackToCacheOnFailure            = NO;
        self.concurrentOperations                   = DEFAULT_CONCURRENT_OPERATIONS;
        self.defaultApiKey                          = @"";
    }
    
    return self;
}

- (void)dealloc {
    [observerController_ release];
    
    for(NSString *key in requests_)
        [[requests_ objectForKey:key] cancel];
    [requests_ release];
    [super dealloc];
}

+ (instancetype)sharedInstance {
    if(!sharedInstance_) {
        sharedInstance_ = [[self alloc] init];
    }
    
    return sharedInstance_;
}

-(void)useCacheWithMemoryCapacity:(NSInteger)memory diskCapacity:(NSInteger)capacity {
    NSURLCache *cache = [[[NSURLCache alloc] initWithMemoryCapacity:memory diskCapacity:capacity diskPath:CACHE_NAME]autorelease];
    [NSURLCache setSharedURLCache:cache];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

-(void)MB_startRequest:(MBHTTPRequestOperation *)request {
    if(request.request.URL != nil && request.request.URL.absoluteString.length > 0 ) {
        NSString *key = [request.request.URL absoluteString];
        [[requests_ objectForKey:key] clearDelegatesAndCancel];
        [requests_ setObject:request forKey:key];
        [[MBServerOperationsQueue sharedInstance] addOperation:request forQueueWithName:SERVER_OPERATION_REQUEST];
    }
}

- (void)MB_cleanupRequest:(NSURL*)url {
    [requests_ removeObjectForKey:url.absoluteString];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

-(void)processNetworkRequest:(MBNetworkRequest*)request {
// Ignore deprecated declaration warning because the class itself is already deprecated
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
   
    __block id                  me             = self;
    __block MBNetworkRequest*  currentRequest = request;
    
    [request setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* outError = nil;
        if([currentRequest handleRequestFinished:responseObject outError:&outError] == YES) {
            [observerController_ notifyObserversWithSelector:@selector(serverPortal:didReceiveData:forRequest:) retainArguments:NO, self, responseObject, currentRequest];
        } else {
            NSError* underlyingError = [outError.userInfo valueForKey:NSUnderlyingErrorKey];
            
            if(outError.code == MBRequestErrApiResult && underlyingError != nil && underlyingError.code == MBBaseApiErrAppVersionDeprecated) {
                NSString* message = [underlyingError.userInfo valueForKey:NSLocalizedDescriptionKey];
                NSString* url = nil;
                
                if([responseObject isKindOfClass:[NSDictionary class]]) {
                    if([responseObject valueForKey:@"data"] != nil) {
                        NSDictionary* data = [responseObject valueForKey:@"data"];
                        url = [data valueForKey:@"url"];
                    }
                }
                [observerController_ notifyObserversWithSelector:@selector(serverPortal:appVersionWasDeprecatedWithMessage:andUrl:forRequest:) retainArguments:NO, self, message, url, currentRequest];
            } else {
                [observerController_ notifyObserversWithSelector:@selector(serverPortal:didReceiveError:forRequest:) retainArguments:NO, self, outError, currentRequest];
            }
        }
        [me MB_cleanupRequest:currentRequest.request.URL];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if([currentRequest isCancelled] == NO) {
            NSError* outError = nil;
            if([currentRequest handleRequestFailed:error outError:&outError] == NO){
                [observerController_ notifyObserversWithSelector:@selector(serverPortal:didReceiveError:forRequest:) retainArguments:NO, self, outError, currentRequest];
            }
        }
        [me MB_cleanupRequest:currentRequest.request.URL];
    }];

    [self MB_startRequest:request];
    
#pragma clang diagnostic pop
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Observer functions

- (void)addObserver:(id<MBServerPortalObserver>)observer {
	[observerController_ addObserver:observer];
}

- (void)removeObserver:(id<MBServerPortalObserver>)observer {
	[observerController_ removeObserver:observer];
}

- (BOOL)containsObserver:(id<MBServerPortalObserver>)observer {
	return [observerController_ containsObserver:observer];
}

@end

