//
//  MBApiRequest.h
//  MBFrameworkCatalog
//
//  Created by Marco Jonker on 2/14/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBBaseDataAdapter.h"
#import "MBHTTPRequestOperation.h"

#define MBRequestErrInvalidData     1   //The json data format was not in the correct mediabunker format
#define MBRequestErrJsonParser      2   //The json parser failed to parse the data
#define MBRequestErrHttpStatus      3   //The http status code was invalid
#define MBRequestErrApiResult       5   //The Api returned a status false
#define MBRequestErrPermissionDenied 6   //Permission to access the function is denied
#define MBRequestErrAFNetworking    7   //AFNetworking caused an error

#define MBRequestErrInvalidDataMessage      @"Invalid data format"
#define MBRequestErrJsonParserMessage       @"Json parser error"
#define MBRequestErrHttpStatusMessage       @"Invalid http status code: %d"
#define MBRequestErrAFNetworkingMessage     @"AFNetworking error"
#define MBRequestErrApiResultMessage        @"Api error"
#define MBRequestErrPermissionDeniedMessage @"Permission to access the function is denied"

#define MBBaseApiErrException                       1001
#define MBBaseApiErrInvalidArgumentException        1002
#define MBBaseApiErrNoItemFound                     1003
#define MBBaseApiErrInternalProgramError            1004
#define MBBaseApiErrValidationError                 1005
#define MBBaseApiErrSoapFault                       1006
#define MBBaseApiErrUnauthorized                    1007
#define MBBaseApiErrAppVersionDeprecated            1008

#define MBRequestMethodGET      @"GET"
#define MBRequestMethodPOST     @"POST"
#define MBRequestMethodPUT      @"PUT"
#define MBRequestMethodDELETE   @"DELETE"

#define MB_HTTPHEADER_API_KEY           @"com.mediabunker-api-key"
#define MB_HTTPHEADER_APP_IDENTIFIER    @"com.mediabunker-app-identifier"
#define MB_FORMDATA_KEY                 @"formData"

@protocol MBApiRequestDelegate;

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBApiRequest : MBHTTPRequestOperation {
    MBBaseDataAdapter* dataAdapter_;
    id<MBApiRequestDelegate> delegate_;
    BOOL isAppDeprecated_;
}

@property (nonatomic, retain) MBBaseDataAdapter* dataAdapter;
@property (nonatomic, assign) NSString* apiSecretKey;
@property (nonatomic, assign) id<MBApiRequestDelegate> delegate;
@property (nonatomic, readonly) BOOL isAppDeprecated;

-(instancetype)initPOSTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
-(instancetype)initPOSTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
-(instancetype)initPOSTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;
-(instancetype)initPOSTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;

-(instancetype)initGETWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl;
-(instancetype)initGETWithUrl:(NSURL*)requestUrl;

-(instancetype)initPUTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
-(instancetype)initPUTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
-(instancetype)initPUTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;
-(instancetype)initPUTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;

-(id)handleRequestFinished:(id)responseObject outError:(NSError **)outErrorPtr;
-(BOOL)handleRequestFailed:(NSError*)error outError:(NSError **)outErrorPtr;
-(void)callDidReceiveObjectDelegate:(id)data;
-(void)callDidReceiveErrorDelegate:(NSError*)receivedError withData:(id)data;

-(void)clearDelegatesAndCancel;

+(id)POSTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
+(id)POSTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;
+(id)POSTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
+(id)POSTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;

+(id)GETWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl;
+(id)GETWithUrl:(NSURL*)requestUrl;

+(id)PUTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
+(id)PUTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;
+(id)PUTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
+(id)PUTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;

@end

@protocol MBApiRequestDelegate <NSObject>

@optional
-(void)apiRequest:(MBApiRequest*)request didReceiveData:(id)data;
-(void)apiRequest:(MBApiRequest*)request didReceiveError:(NSError*)error withData:(id)data;

@end

// Block type used to define blocks called MBApiPortal request updates
typedef void (^MBApiRequestCompletionHandler)(MBApiRequest *request, id data, NSError *error);
