//
//  MBApiRequest.m
//  MBFrameworkCatalog
//
//  Created by Marco Jonker on 2/14/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBApiRequest.h"
#import "NSURL+MBFramework.h"
#import "MBError.h"
#import "MBApiPortal.h"
#import "AFURLRequestSerialization.h"
#import "NSBundle+MBProperties.h"
#import "AFSecurityPolicy.h"
#import "MBApiPortal.h"
#import "MBAppDeprecatedDataAdapter.h"

@interface MBApiRequest()
@end

@implementation MBApiRequest

@synthesize dataAdapter=dataAdapter_;
@synthesize delegate=delegate_;
@synthesize isAppDeprecated=isAppDeprecated_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

//POST

-(instancetype)initPOSTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters method:MBRequestMethodPOST serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initPOSTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters method:MBRequestMethodPOST serializer:[AFJSONRequestSerializer serializer]];
}

-(instancetype)initPOSTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:parameters method:MBRequestMethodPOST serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initPOSTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:parameters method:MBRequestMethodPOST serializer:[AFJSONRequestSerializer serializer]];
}

//GET

-(instancetype)initGETWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl {
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:nil method:MBRequestMethodGET serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initGETWithUrl:(NSURL*)requestUrl{
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:nil method:MBRequestMethodGET serializer:[AFHTTPRequestSerializer serializer]];
}

//PUT

-(instancetype)initPUTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters method:MBRequestMethodPUT serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initPUTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters method:MBRequestMethodPUT serializer:[AFJSONRequestSerializer serializer]];
}

-(instancetype)initPUTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:parameters method:MBRequestMethodPUT serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initPUTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:parameters method:MBRequestMethodPUT serializer:[AFJSONRequestSerializer serializer]];
}

//DELETE


//INIT

-(instancetype)initWithUrl:(NSURL*)requestUrl method:(NSString*)method serializer:(AFHTTPRequestSerializer*)serializer {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:nil method:method serializer:serializer];
}

-(instancetype)initWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters method:(NSString*)method serializer:(AFHTTPRequestSerializer*)serializer{
    
    NSMutableURLRequest* request = nil;
    NSError *error = nil;

    id formDataItems = nil;
    
    // Form data can be added to a POST request via an array which is included in parameters
    if ([method isEqualToString:MBRequestMethodPOST]) {
        formDataItems = [parameters objectForKey:MB_FORMDATA_KEY];
    }
    
    if ([formDataItems isKindOfClass:[NSArray class]]) {
        request  = [serializer multipartFormRequestWithMethod:method
                                                    URLString:requestUrl.absoluteString
                                                   parameters:parameters
                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                        for (NSDictionary *data in formDataItems) {
                                            [self MB_appendPart:data toMultipartFormData:formData];
                                        }
                                    }
                                                        error:&error];
        
    } else {
        request = [serializer requestWithMethod:method URLString:requestUrl.absoluteString parameters:parameters error:&error];
    }
    
    if([method isEqualToString:MBRequestMethodGET]) {
        request.timeoutInterval       = [[MBApiPortal sharedInstance]GETTimeOutSeconds];
        request.cachePolicy           = [[MBApiPortal sharedInstance]GETCachePolicy];
        NSString* defaultApiKey = [[MBApiPortal sharedInstance]defaultApiKey];
        
        if(defaultApiKey != nil && defaultApiKey.length > 0) {
            [request setValue:defaultApiKey forHTTPHeaderField:MB_HTTPHEADER_API_KEY];
        }
        
        self.fallbackToCacheOnFailure = [[MBApiPortal sharedInstance]GETFallbackToCacheOnFailure];
    } else if ([method isEqualToString:MBRequestMethodPOST]) {
        [request setTimeoutInterval:[[MBApiPortal sharedInstance] POSTTimeOutSeconds]];
    }
    
    [request setValue:[MBApiRequest MB_appIdentifierString] forHTTPHeaderField:MB_HTTPHEADER_APP_IDENTIFIER];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    self = [super initWithRequest:request];
    
    
    
    if(self != nil) {
        if([method compare:MBRequestMethodGET] == NSOrderedSame) {
            self.fallbackToCacheOnFailure = [[MBApiPortal sharedInstance]GETFallbackToCacheOnFailure];
        }
        self.dataAdapter = dataAdapter;
        
        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        [self setSecurityPolicy:securityPolicy];
    }
    return self;
}

-(void)dealloc {
    [self clearDelegatesAndCancel];
    self.delegate = nil;
    [dataAdapter_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

+(NSString*)MB_appIdentifierString {
    return [NSString stringWithFormat:@"os:%@;appv:%@;osv:%@", @"ios",[[NSBundle mainBundle] version],[[UIDevice currentDevice] systemVersion]];
}

- (void)MB_appendPart:(NSDictionary *)data toMultipartFormData:(id<AFMultipartFormData>)formData
{
    id fileData = [data objectForKey:@"fileData"];
    if (fileData) {
        id name = [data objectForKey:@"name"];
        NSAssert(name, @"Value name not set");
        id fileName = [data objectForKey:@"fileName"];
        NSAssert(fileName, @"Value fileName not set");
        id mimeType = [data objectForKey:@"mimeType"];
        NSAssert(mimeType, @"Value mimeType not set");
        [formData appendPartWithFileData:fileData name:name fileName:fileName mimeType:mimeType];
    }
    
}

-(id)MB_handleRequestData:(NSDictionary*)dictionary {
    id data = [dictionary valueForKey:@"data"];
    
    if(data != nil && dataAdapter_  != nil) {
        if(dataAdapter_.objectField != nil && dataAdapter_.objectField.length > 0) {
            data = [data valueForKeyPath:dataAdapter_.objectField];
        }
        if([data isKindOfClass:[NSDictionary class]]) {
            data = [self.dataAdapter fromDictionary:data];
        } else if([data isKindOfClass:[NSArray class]]){
            data =[self.dataAdapter fromArray:data];
        }
    }
    
    return data;
}

-(bool)MB_validateDataFormat:(NSDictionary*)dictionary {
    return [dictionary objectForKey:@"result"];
}

-(NSError*)MB_errorFromResponse:(NSDictionary*)dictionary {
    NSInteger errorCode     = 0;
    NSString* errorMessage  = @"";
    
    id value = [dictionary valueForKey:@"error_code"];
    if ([value isKindOfClass:[NSNumber class]]) {
        errorCode = [value intValue];
    }
    
    value = [dictionary valueForKey:@"error_message"];
    if ([value isKindOfClass:[NSString class]]) {
        errorMessage = value;
    }
    
    NSError* apiError = [MBError createError:errorMessage withErrorCode:errorCode andError:nil];
    NSError* error = [MBError createError:MBRequestErrApiResultMessage withErrorCode:MBRequestErrApiResult andError:apiError];
    return error;
}

-(void)callDidReceiveObjectDelegate:(id)data {
    if([delegate_ respondsToSelector:@selector(apiRequest:didReceiveData:)]){
        [delegate_ apiRequest:self didReceiveData:data];
    }
}

-(void)callDidReceiveErrorDelegate:(NSError*)receivedError withData:(id)data {
    if([delegate_ respondsToSelector:@selector(apiRequest:didReceiveError:withData:)]){
        [delegate_ apiRequest:self didReceiveError:receivedError withData:data]; // Invalid data
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)clearDelegatesAndCancel {
    self.delegate = nil;
    [super cancel];
}

-(id)handleRequestFinished:(id)responseObject outError:(NSError **)outErrorPtr {
    id result = nil;
    NSError* outError = nil;
    
    if([self MB_validateDataFormat:responseObject] == true){
        if([[responseObject valueForKey:@"result"] boolValue] == true) {
            result = [self MB_handleRequestData:responseObject];
        } else {
            outError = [self MB_errorFromResponse:responseObject];
            isAppDeprecated_ = [MBApiPortal isAppDeprecatedError:outError];
            if(isAppDeprecated_ == YES && [responseObject valueForKey:@"data"] != nil) {
                result = [[MBAppDeprecatedDataAdapter sharedInstance]fromDictionary:[responseObject valueForKey:@"data"]];
            }
        }
    } else {
        outError = [MBError createError:MBRequestErrInvalidDataMessage withErrorCode:MBRequestErrInvalidData andError:nil];
    }
    
    if(outError != nil && outErrorPtr != nil) {
        *outErrorPtr = outError;
    }
    
    return result;
}

- (BOOL)handleRequestFailed:(NSError*)error outError:(NSError **)outErrorPtr{
    NSError* outError = nil;
    
#ifdef DEBUG
    if(self.responseData.bytes != nil) {
        NSLog(@"Error %@", [NSString stringWithCString:self.responseData.bytes encoding:NSUTF8StringEncoding]);
    }
#endif
    
    if([self isCancelled] == NO) {
        if(401 != self.response.statusCode) {
            outError = [MBError createError:MBRequestErrAFNetworkingMessage  withErrorCode:MBRequestErrAFNetworking andError:self.error];
        } else {
            outError = [MBError createError:MBRequestErrPermissionDeniedMessage withErrorCode:MBRequestErrPermissionDenied andError:nil];
        }
    
        if(outErrorPtr != nil) {
            *outErrorPtr = outError;
        }
    }
    return (outError != nil);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - properties

-(void)setApiSecretKey:(NSString *)apiSecretKey {
    [((NSMutableURLRequest*)self.request) setValue:apiSecretKey forHTTPHeaderField:MB_HTTPHEADER_API_KEY];
}

-(NSString *)apiSecretKey {
    return [self.request valueForHTTPHeaderField:MB_HTTPHEADER_API_KEY];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public static functions

+(id)POSTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [[[MBApiRequest alloc]initPOSTWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters]autorelease];
}

+(id)POSTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [[[MBApiRequest alloc]initPOSTWithUrl:requestUrl parameters:parameters]autorelease];
}

+(id)POSTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [[[MBApiRequest alloc]initPOSTJsonWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters]autorelease];
}

+(id)POSTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [[[MBApiRequest alloc]initPOSTJsonWithUrl:requestUrl parameters:parameters]autorelease];
}

+(id)GETWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl{
    return [[[MBApiRequest alloc]initGETWithDataAdapter:dataAdapter andUrl:requestUrl]autorelease];
}

+(id)GETWithUrl:(NSURL*)requestUrl {
    return [[[MBApiRequest alloc]initGETWithUrl:requestUrl]autorelease];
}

+(id)PUTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [[[MBApiRequest alloc]initPUTWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters]autorelease];
}

+(id)PUTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [[[MBApiRequest alloc]initPUTWithUrl:requestUrl parameters:parameters]autorelease];
}

+(id)PUTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [[[MBApiRequest alloc]initPUTJsonWithUrl:requestUrl parameters:parameters]autorelease];
}

+(id)PUTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [[[MBApiRequest alloc]initPUTJsonWithUrl:requestUrl parameters:parameters]autorelease];
}

@end

