//
//  MBRequest.h
//  MBFramework
//
//  Created by Marco Jonker on 3/25/13.
//
//

#import "MBBaseDataAdapter.h"
#import "MBHTTPRequestOperation.h"

#define MBRequestErrInvalidData     1   //The json data format was not in the correct mediabunker format
#define MBRequestErrJsonParser      2   //The json parser failed to parse the data
#define MBRequestErrHttpStatus      3   //The http status code was invalid
#define MBRequestErrApiResult       5   //The Api returned a status false
#define MBRequestErrPermissionDenied 6   //Permission to access the function is denied
#define MBRequestErrAFNetworking    7   //AFNetworking caused an error

#define MBBaseApiErrException                       1001
#define MBBaseApiErrInvalidArgumentException        1002
#define MBBaseApiErrNoItemFound                     1003
#define MBBaseApiErrInternalProgramError            1004
#define MBBaseApiErrValidationError                 1005
#define MBBaseApiErrSoapFault                       1006
#define MBBaseApiErrUnauthorized                    1007
#define MBBaseApiErrAppVersionDeprecated            1008

#define MBRequestMethodGET      @"GET"
#define MBRequestMethodPOST     @"POST"
#define MBRequestMethodPUT      @"PUT"
#define MBRequestMethodDELETE   @"DELETE"

#define MB_HTTPHEADER_API_KEY           @"com.mediabunker-api-key"
#define MB_HTTPHEADER_APP_IDENTIFIER    @"com.mediabunker-app-identifier"

@protocol MBNetworkRequestDelegate;

/**
 Class not yet (fully) documented. Documentation for this class is in progress.
 @discussion **Important:** MBNetworkRequest is deprecated. Use MBApiRequest instead.
 */
DEPRECATED_MSG_ATTRIBUTE("Use MBApiRequest instead")
@interface MBNetworkRequest : MBHTTPRequestOperation {
    MBBaseDataAdapter* dataAdapter_;
    id<MBNetworkRequestDelegate> delegate_;
}

@property (nonatomic, retain) MBBaseDataAdapter* dataAdapter;
@property (nonatomic, assign) NSString* apiSecretKey;
@property (nonatomic, assign) id<MBNetworkRequestDelegate> delegate;

-(instancetype)initPOSTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
-(instancetype)initPOSTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
-(instancetype)initPOSTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;
-(instancetype)initPOSTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;

-(instancetype)initGETWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl;
-(instancetype)initGETWithUrl:(NSURL*)requestUrl;

-(instancetype)initPUTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
-(instancetype)initPUTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
-(instancetype)initPUTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;
-(instancetype)initPUTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;

-(BOOL)handleRequestFinished:(id)responseObject outError:(NSError **)outErrorPtr;
-(BOOL)handleRequestFailed:(NSError*)error outError:(NSError **)outErrorPtr;

-(void)clearDelegatesAndCancel;

+(id)POSTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
+(id)POSTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;
+(id)POSTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
+(id)POSTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;

+(id)GETWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl;
+(id)GETWithUrl:(NSURL*)requestUrl;

+(id)PUTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
+(id)PUTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;
+(id)PUTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters;
+(id)PUTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters ;

@end

DEPRECATED_MSG_ATTRIBUTE("MBNetworkRequest is deprecated. Use MBApiRequestDelegate instead")
@protocol MBNetworkRequestDelegate <NSObject>
@optional
-(void)request:(MBNetworkRequest*)request didReceiveArrayOfObjects:(NSArray*)objects;
-(void)request:(MBNetworkRequest*)request didReceiveObject:(id)object;
-(void)request:(MBNetworkRequest*)request didReceiveError:(NSError*)error;
-(void)request:(MBNetworkRequest*)request appVersionWasDeprecatedWithMessage:(NSString*)message andUrl:(NSString*)url;
@end