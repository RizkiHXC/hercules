//
//  MBRequest.m
//  MBFramework
//
//  Created by Marco Jonker on 3/25/13.
//
//

#import "MBNetworkRequest.h"
#import "NSURL+MBFramework.h"
#import "MBError.h"
#import "MBServerPortal.h"
#import "AFURLRequestSerialization.h"
#import "NSBundle+MBProperties.h"
#import "AFSecurityPolicy.h"

#define MBRequestErrInvalidDataMessage      @"Invalid data format"
#define MBRequestErrJsonParserMessage       @"Json parser error"
#define MBRequestErrHttpStatusMessage       @"Invalid http status code: %d"
#define MBRequestErrAFNetworkingMessage     @"AFNetworking error"
#define MBRequestErrApiResultMessage    @"Api error"
#define MBRequestErrPermissionDeniedMessage @"Permission to access the function is denied"

@interface MBNetworkRequest()
-(void)MB_callDidReceiveObjectDelegate:(NSDictionary*)dictionary;
-(void)MB_callDidReceiveArrayOfObjectsDelegate:(NSArray*)array;
-(void)MB_callDidReceiveErrorDelegate:(NSError*)receivedError responseObject:(id)responseObject;
-(void)MB_handleRequestData:(NSDictionary*)dictionary;
@end

@implementation MBNetworkRequest

@synthesize dataAdapter=dataAdapter_;
@synthesize delegate=delegate_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

//POST

-(instancetype)initPOSTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters method:MBRequestMethodPOST serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initPOSTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters method:MBRequestMethodPOST serializer:[AFJSONRequestSerializer serializer]];
}

-(instancetype)initPOSTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:parameters method:MBRequestMethodPOST serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initPOSTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:parameters method:MBRequestMethodPOST serializer:[AFJSONRequestSerializer serializer]];
}

//GET

-(instancetype)initGETWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl {
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:nil method:MBRequestMethodGET serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initGETWithUrl:(NSURL*)requestUrl{
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:nil method:MBRequestMethodGET serializer:[AFHTTPRequestSerializer serializer]];
}

//PUT

-(instancetype)initPUTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters method:MBRequestMethodPUT serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initPUTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters method:MBRequestMethodPUT serializer:[AFJSONRequestSerializer serializer]];
}

-(instancetype)initPUTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:parameters method:MBRequestMethodPUT serializer:[AFHTTPRequestSerializer serializer]];
}

-(instancetype)initPUTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:parameters method:MBRequestMethodPUT serializer:[AFJSONRequestSerializer serializer]];
}

//DELETE


//INIT

-(instancetype)initWithUrl:(NSURL*)requestUrl method:(NSString*)method serializer:(AFHTTPRequestSerializer*)serializer {
    return [self initWithDataAdapter:nil andUrl:requestUrl parameters:nil method:method serializer:serializer];
}

-(instancetype)initWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters method:(NSString*)method serializer:(AFHTTPRequestSerializer*)serializer{

    NSError *error = nil;
    NSMutableURLRequest* request  = [serializer requestWithMethod:method URLString:requestUrl.absoluteString parameters:parameters error:&error];
    
    if([method compare:MBRequestMethodGET] == NSOrderedSame) {
        request.timeoutInterval       = [[MBServerPortal sharedInstance]GETTimeOutSeconds];
        request.cachePolicy           = [[MBServerPortal sharedInstance]GETCachePolicy];
        NSString* defaultApiKey = [[MBServerPortal sharedInstance]defaultApiKey];
        
        if(defaultApiKey != nil && defaultApiKey.length > 0) {
            [request setValue:defaultApiKey forHTTPHeaderField:MB_HTTPHEADER_API_KEY];
        }
        
        self.fallbackToCacheOnFailure = [[MBServerPortal sharedInstance]GETFallbackToCacheOnFailure];
    }
    
    [request setValue:[MBNetworkRequest MB_appIdentifierString] forHTTPHeaderField:MB_HTTPHEADER_APP_IDENTIFIER];
    
    self = [super initWithRequest:request];

    
    
    if(self != nil) {
        if([method compare:MBRequestMethodGET] == NSOrderedSame) {
            self.fallbackToCacheOnFailure = [[MBServerPortal sharedInstance]GETFallbackToCacheOnFailure];
        }
        self.dataAdapter = dataAdapter;
        
        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        [self setSecurityPolicy:securityPolicy];
    }
    return self;
}

-(void)dealloc {
    self.delegate = nil;
    [dataAdapter_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

+(NSString*)MB_appIdentifierString {
    return [NSString stringWithFormat:@"os:%@;appv:%@;osv:%@", @"ios",[[NSBundle mainBundle] version],[[UIDevice currentDevice] systemVersion]];
}

-(void)MB_handleRequestData:(NSDictionary*)dictionary {
    id data = [dictionary valueForKey:@"data"];
    
    if(data != nil && dataAdapter_  != nil && dataAdapter_.objectField != nil && dataAdapter_.objectField.length > 0) {
        data = [data valueForKeyPath:dataAdapter_.objectField];
    }
    
    if([data isKindOfClass:[NSArray class]]) {
        [self MB_callDidReceiveArrayOfObjectsDelegate:data];
    } else {
        [self MB_callDidReceiveObjectDelegate:data];
    }
}

-(bool)MB_validateDataFormat:(NSDictionary*)dictionary {
    return [dictionary objectForKey:@"result"];
}

-(NSError*)MB_errorFromResponse:(NSDictionary*)dictionary {
    NSInteger errorCode     = 0;
    NSString* errorMessage  = @"";
    
    if([dictionary valueForKey:@"error_code"] != nil && [[dictionary valueForKey:@"error_code"] isKindOfClass:[NSNumber class]] ) {
        errorCode = [[dictionary valueForKey:@"error_code"] intValue];
    }
    
    if([dictionary valueForKey:@"error_message"] && [[dictionary valueForKey:@"error_message"] isKindOfClass:[NSString class]] ) {
        errorMessage = [dictionary valueForKey:@"error_message"];
    }
    
    NSError* apiError = [MBError createError:errorMessage withErrorCode:errorCode andError:nil];
    NSError* error = [MBError createError:MBRequestErrApiResultMessage withErrorCode:MBRequestErrApiResult andError:apiError];
    return error;
}

-(void)MB_callDidReceiveObjectDelegate:(id)object {
    if([delegate_ respondsToSelector:@selector(request:didReceiveObject:)]){
        if(dataAdapter_ != nil) {
            [delegate_ request:self didReceiveObject:[self.dataAdapter fromDictionary:object]];
        } else {
            [delegate_ request:self didReceiveObject:object];
        }
    }
}

-(void)MB_callDidReceiveArrayOfObjectsDelegate:(NSArray*)array {
    if([delegate_ respondsToSelector:@selector(request:didReceiveArrayOfObjects:)]){
        if(dataAdapter_ != nil) {
            [delegate_ request:self didReceiveArrayOfObjects:[self.dataAdapter fromArray:array]];
        } else {
            [delegate_ request:self didReceiveArrayOfObjects:array];
        }
    }
}

-(void)MB_callDidReceiveErrorDelegate:(NSError*)receivedError responseObject:(id)responseObject {
    NSError* underlyingError = [receivedError.userInfo valueForKey:NSUnderlyingErrorKey];
    
    if(receivedError.code == MBRequestErrApiResult && underlyingError != nil && underlyingError.code == MBBaseApiErrAppVersionDeprecated) {
        if([delegate_ respondsToSelector:@selector(request:appVersionWasDeprecatedWithMessage:andUrl:)]) {
            NSString* url = nil;
            
            if([responseObject isKindOfClass:[NSDictionary class]]) {
                NSDictionary* data = [responseObject valueForKey:@"data"];
                url = [data valueForKey:@"url"];
            }
            
            [delegate_ request:self appVersionWasDeprecatedWithMessage:[underlyingError.userInfo valueForKey:NSLocalizedDescriptionKey] andUrl:url];
        }
    } else {
        if([delegate_ respondsToSelector:@selector(request:didReceiveError:)]){
            [delegate_ request:self didReceiveError:receivedError]; // Invalid data
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)clearDelegatesAndCancel {
    self.delegate = nil;
    [super cancel];
}

-(BOOL)handleRequestFinished:(id)responseObject outError:(NSError **)outErrorPtr{
    BOOL result = YES;
    NSError* outError = nil;
    
    if([self MB_validateDataFormat:responseObject] == true){
        if([[responseObject valueForKey:@"result"] boolValue] == true) {
            [self MB_handleRequestData:responseObject];
        } else {
            outError = [self MB_errorFromResponse:responseObject];
            [self MB_callDidReceiveErrorDelegate:outError responseObject:responseObject];
        }
    } else {
        outError = [MBError createError:MBRequestErrInvalidDataMessage withErrorCode:MBRequestErrInvalidData andError:nil];
        [self MB_callDidReceiveErrorDelegate:outError responseObject:nil];
    }
    
    if(outError == nil) {
        result = YES;
    } else {
        if(outErrorPtr != nil) {
            *outErrorPtr = outError;
        }
        result = NO;
    }
    
    return result;
}

-(BOOL)handleRequestFailed:(NSError*)error outError:(NSError **)outErrorPtr{
    BOOL result = YES;
    NSError* outError = nil;
    
#ifdef DEBUG
    if(self.responseData.bytes != nil) {
        NSLog(@"Error %@", [NSString stringWithCString:self.responseData.bytes encoding:NSUTF8StringEncoding]);
    }
#endif
    
    if([self isCancelled] == NO) {
        if(401 != self.response.statusCode) {
            outError = [MBError createError:MBRequestErrAFNetworkingMessage  withErrorCode:MBRequestErrAFNetworking andError:self.error];
            [self MB_callDidReceiveErrorDelegate:outError responseObject:nil];
        } else {
            outError = [MBError createError:MBRequestErrPermissionDeniedMessage withErrorCode:MBRequestErrPermissionDenied andError:nil];
            [self MB_callDidReceiveErrorDelegate:outError responseObject:nil];
        }
    }
    
    if(outError == nil) {
        result = YES;
    } else {
        if(outErrorPtr != nil) {
            *outErrorPtr = outError;
        }
        result = NO;
    }
    
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - properties

-(void)setApiSecretKey:(NSString *)apiSecretKey {
    [((NSMutableURLRequest*)self.request) setValue:apiSecretKey forHTTPHeaderField:MB_HTTPHEADER_API_KEY];
}

-(NSString *)apiSecretKey {
    return [self.request valueForHTTPHeaderField:MB_HTTPHEADER_API_KEY];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public static functions

+(id)POSTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [[[MBNetworkRequest alloc]initPOSTWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters]autorelease];
}

+(id)POSTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [[[MBNetworkRequest alloc]initPOSTWithUrl:requestUrl parameters:parameters]autorelease];
}

+(id)POSTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [[[MBNetworkRequest alloc]initPOSTJsonWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters]autorelease];
}

+(id)POSTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [[[MBNetworkRequest alloc]initPOSTJsonWithUrl:requestUrl parameters:parameters]autorelease];
}

+(id)GETWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl{
    return [[[MBNetworkRequest alloc]initGETWithDataAdapter:dataAdapter andUrl:requestUrl]autorelease];
}

+(id)GETWithUrl:(NSURL*)requestUrl {
    return [[[MBNetworkRequest alloc]initGETWithUrl:requestUrl]autorelease];
}

+(id)PUTWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [[[MBNetworkRequest alloc]initPUTWithDataAdapter:dataAdapter andUrl:requestUrl parameters:parameters]autorelease];
}

+(id)PUTWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters{
    return [[[MBNetworkRequest alloc]initPUTWithUrl:requestUrl parameters:parameters]autorelease];
}

+(id)PUTJsonWithDataAdapter:(MBBaseDataAdapter*)dataAdapter andUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [[[MBNetworkRequest alloc]initPUTJsonWithUrl:requestUrl parameters:parameters]autorelease];
}

+(id)PUTJsonWithUrl:(NSURL*)requestUrl parameters:(NSDictionary*)parameters {
    return [[[MBNetworkRequest alloc]initPUTJsonWithUrl:requestUrl parameters:parameters]autorelease];
}

@end
