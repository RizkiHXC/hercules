//
//  MBJSONResponseSerializerWithData.h
//  MBFramework
//
//  Created by Rob de Rijk on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "AFURLResponseSerialization.h"

// NSError userInfo key that will contain response data
static NSString * const JSONResponseSerializerWithDataKey = @"JSONResponseSerializerWithDataKey";

@interface MBJSONResponseSerializerWithData : AFJSONResponseSerializer

@end
