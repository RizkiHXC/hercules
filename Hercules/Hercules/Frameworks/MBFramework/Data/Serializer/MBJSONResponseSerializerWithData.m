//
//  MBJSONResponseSerializerWithData.m
//  MBFramework
//
//  Created by Rob de Rijk on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBJSONResponseSerializerWithData.h"

@implementation MBJSONResponseSerializerWithData

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
	id JSONObject = [super responseObjectForResponse:response data:data error:error];
    
    // If there is an error we add the reponse string to it.
	if (*error != nil) {
		NSMutableDictionary *userInfo = [(*error).userInfo mutableCopy];
		if (data == nil) {
            userInfo[JSONResponseSerializerWithDataKey] = @"";
		} else {
            id serializedJson = [NSJSONSerialization JSONObjectWithData:data options:self.readingOptions error:nil];
            
            if(serializedJson != nil) {
                userInfo[JSONResponseSerializerWithDataKey] = serializedJson;
            }
		}
        
		NSError *newError = [NSError errorWithDomain:(*error).domain code:(*error).code userInfo:userInfo];
		(*error) = newError;
        
        [userInfo release];
	}
    
	return (JSONObject);
}

@end
