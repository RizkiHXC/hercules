//
//  MBFramework.h
//  MBFramework
//
//  Created by Marco Jonker on 5/29/13.
//  Copyright (c) 2013 Mediabunker. All rights reserved.
//

#define MBFRAMEWORK_NETWORK_DEBUG 0

#import <Foundation/Foundation.h>

//Supporting Files
#import "MBFrameworkDefines.h"

//Core/
#import "MBAppEnvironment.h"
#import "MBHash.h"
#import "MBMutableCollection.h"

//Controllers/
#import "MBRefreshDataController.h"
#import "MBPushNotificationCenter.h"
#import "MBObserverController.h"
#import "MBClassFactory.h"
#import "MBToolBarDisplayController.h"

//Controllers/InteractiveTransition
#import "MBEdgePanInteractiveTransition.h"
#import "MBPinchInteractiveTransition.h"

//Controllers/TransitionAnimation
#import "MBViewControllerCrossFadeTransition.h"
#import "MBViewControllerScaleTransition.h"

//Categories/Core
#import "NSArray+MBFramework.h"
#import "NSBundle+MBProperties.h"
#import "NSDate+MBDateCategory.h"
#import "UIDevice+MBFramework.h"
#import "NSDictionary+MBFramework.h"
#import "NSError+MBFramework.h"
#import "NSInvocation+MBFramework.h"
#import "NSString+MBEncoding.h"
#import "NSURL+MBFramework.h"
#import "NSString+MBFramework.h"

//Categories/Controllers
#import "UINavigationController+MBFramework.h"
#import "UIViewController+MBUserInterfaceIdiom.h"

//Categories/UI/CollectionView
#import "UICollectionViewFlowLayout+MBFramework.h"
#import "UICollectionView+MBFramework.h"

//Categories/UI/Controls
#import "UIBarButtonItem+MBFramework.h"
#import "UIControl+MBFramework.h"
#import "UILabel+MBFramework.h"

//Categories/UI
#import "UIColor+MBColorConvertions.h"
#import "UIImage+MBFramework.h"
#import "UIImage+MBResize.h"
#import "UIView+MBViewGeometry.h"
#import "UIView+MBFramework.h"

//Categories/UI/CoreAnimation
#import "CALayer+MBAnimationCompletion.h"

//Categories/UI/Views
#import "UIAlertView+MBCompletionBlock.h"
#import "UIActionSheet+MBCompletionBlock.h"
#import "UIImageView+MBNetworking.h"

//Data/
#import "MBHTTPRequestOperation.h"
#import "MBNetworkRequest.h"
#import "MBApiRequest.h"
#import "MBServerOperationsQueue.h"
#import "MBServerPortal.h"
#import "MBApiPortal.h"
#import "MBDataMapping.h"
#import "MBDownloadQueue.h"
#import "MBBaseDataObject.h"
#import "MBAppDeprecatedDataObject.h"
#import "MBBaseDataAdapter.h"
#import "MBAppDeprecatedDataAdapter.h"

//Data/Serializer
#import "MBJSONResponseSerializerWithData.h"

//Data/ErrorHandlers
#import "MBNetworkRequestErrorHandlerProtocol.h"
#import "MBStandardNetworkRequestErrorHandler.h"

//Data/Actions

//Data/Adapters
#import "MBBaseDataAdapter.h"
#import "MBDataMapping.h"

//Data/Handlers
#import "MBDownloadQueue.h"

//Data/DataObjects
#import "MBBaseDataObject.h"

//Utils/
#import "MBAppSettings.h"
#import "MBDeviceCapabilities.h"
#import "MBError.h"
#import "MBErrorController.h"
#import "MBGlobalDefines.h"
#import "MBGoogleGeocodingApi.h"
#import "MBGoogleGeocodingLocation.h"
#import "MBGoogleGeocodingSearchParameters.h"
#import "MBKeychainItemCertificate.h"
#import "MBKeychainItemGenericPassword.h"
#import "MBKeychainItemIdentity.h"
#import "MBKeychainItemInternetPassword.h"
#import "MBKeychainItemKey.h"
#import "MBLocationController.h"
#import "MBLog.h"
#import "MBReachabilityManager.h"

//Utils/Layout
#import "MBLayoutGuide.h"

//Utils/Math
#import "MBMath.h"
#import "MBRectFunctions.h"

//Utils/Formatters
#import "MBCurrencyFormatter.h"
#import "MBPhoneNumberFormatter.h"
#import "MBTimeSpanFormatter.h"
#import "MBURLFormatter.h"

//Utils/Time
#import "MBStopwatchTimer.h"

//UI/Controls
#import "MBAccessoryLabel.h"
#import "MBCheckbox.h"
#import "MBHyperlinkLabel.h"
#import "MBPageControl.h"
#import "MBPlaceholderControl.h"
#import "MBRangeSlider.h"
#import "MBRatingBar.h"
#import "MBRefreshControl.h"
#import "MBSegmentedControl.h"
#import "MBSwitch.h"
#import "MBTextField.h"
#import "MBTextView.h"

//UI/Layout
#import "MBCollectionViewGridLayout.h"

//UI/ViewControllers
#import "MBImagesActivityIndicatorView.h"
#import "MBMasterDetailViewController.h"
#import "MBPopoverInputViewController.h"
#import "MBSideMenuController.h"
#import "MBTextInputController.h"

//UI/Validators
#import "MBActivityIndicatorPopoverView.h"
#import "MBBaseValidator.h"
#import "MBCustomValidator.h"
#import "MBEmailAddressValidator.h"
#import "MBInputValidateManager.h"
#import "MBRegularExpressionValidator.h"
#import "MBRequiredFieldValidator.h"

//UI/Views
#import "MBBreadcrumbView.h"
#import "MBClipView.h"
#import "MBGradientView.h"
#import "MBGridPanel.h"
#import "MBGridView.h"
#import "MBInputSheet.h"
#import "MBInputView.h"
#import "MBLaunchImageView.h"
#import "MBPageScrollView.h"
#import "MBParallaxView.h"
#import "MBPickerViewCell.h"
#import "MBPickerViewInputSheet.h"
#import "MBDatePickerInputSheet.h"
#import "MBPopoverView.h"
#import "MBShadowView.h"
#import "MBStepProcessIndicator.h"
#import "MBTabContainerView.h"
#import "MBTableViewCellSeparator.h"
#import "MBTableViewHeaderFooterView.h"
#import "MBTitledToolbar.h"
#import "MBVerticalStackPanel.h"

//UI/Views/CollectionView
#import "MBCollectionViewCell.h"

@interface MBFramework : NSObject
{
    NSDictionary*   plistDictionary_;
    NSBundle*       bundle_;
}

+ (instancetype)sharedInstance;

@property (nonatomic, readonly) NSString* version;
@property (nonatomic, readonly) NSString* build;
@property (nonatomic, readonly) NSDate* buildDate;

@end
