//
//  MBFramework.m
//  MBFramework
//
//  Created by Marco Jonker on 5/29/13.
//  Copyright (c) 2013 Mediabunker. All rights reserved.
//

#import "MBFramework.h"

static id sharedInstance_ = nil;

@implementation MBFramework

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(instancetype)init {
    self = [super init];
    if(self != nil) {
        bundle_                 = [[NSBundle alloc]initWithPath:[[NSBundle mainBundle] pathForResource:@"MBFrameworkResources" ofType:@"bundle"]];
        plistDictionary_        = [[NSDictionary alloc] initWithContentsOfFile:[bundle_ pathForResource:@"MBFramework" ofType:@"plist"]];
    }
    
    return self;
}

-(void)dealloc {
    [bundle_ release];
    [plistDictionary_ release];
    [super dealloc];
}

-(NSString *)version {
    return [plistDictionary_ valueForKey:@"CFBundleShortVersionString"];
}

-(NSString *)build {
    return [plistDictionary_ valueForKey:@"CFBundleVersion"];
}

-(NSString *)buildDate {
    return [plistDictionary_ valueForKey:@"CFBuildDate"];
    
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static public functions

+(instancetype)sharedInstance {
    if(nil == sharedInstance_)
        sharedInstance_ = [[self alloc] init];
    return sharedInstance_;
}

@end




