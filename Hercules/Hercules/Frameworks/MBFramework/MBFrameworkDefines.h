//
//  MBFrameworkDefines.h
//  MBFramework
//
//  Created by Marco Jonker on 5/30/13.
//  Copyright (c) 2013 Mediabunker. All rights reserved.
//

#ifndef MBFramework_MBFrameworkDefines_h
#define MBFramework_MBFrameworkDefines_h

// Deprecated classes or methods
#define MB_WILL_BE_REMOVED_IN_NEXT_MAJOR DEPRECATED_MSG_ATTRIBUTE("Will be remove in next major of the framework")

#endif
