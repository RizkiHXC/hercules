//
//  MBAccessoryLabel.h
//  MBFramework
//
//  Created by Arno Woestenburg on 05/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Constants indicating the direction for the layout. Dit is een test
 */
typedef NS_ENUM(NSInteger, MBAccessoryLabelLayoutDirection) {
    /**
     The label and image layout horizontally. This is default.
     */
    MBAccessoryLabelLayoutDirectionHorizontal,
    /**
     The label and image layout vertically.
     */
    MBAccessoryLabelLayoutDirectionVertical,
};

/**
 The MBAccessoryLabel class implements an image view with a read-only text view.
 */
@interface MBAccessoryLabel : UIView
{
    UILabel *titleLabel_;
    UIImageView *imageView_;
    UIControlContentVerticalAlignment contentVerticalAlignment_;
    UIControlContentHorizontalAlignment contentHorizontalAlignment_;
    UIEdgeInsets contentEdgeInsets_;
    UIEdgeInsets titleEdgeInsets_;
    UIEdgeInsets imageEdgeInsets_;
    MBAccessoryLabelLayoutDirection layoutDirection_;
}

/**
 Sets the image.
 @param image The image to use
 */
- (void)setImage:(UIImage *)image;
/**
 Sets the text displayed by the title label.
 @param title The title text displayed by the title label
 */
- (void)setTitle:(NSString *)title;
/**
 Sets the styled text displayed by the title label.
 @param title The styled title text displayed by the title label
 */
- (void)setAttributedTitle:(NSAttributedString *)title;
/**
 Returns the rectangle in which the receiver draws its entire content.
 @param bounds The bounding rectangle for the receiver.
 @return The rectangle in which the receiver draws its entire content.
 */
- (CGRect)contentRectForBounds:(CGRect)bounds;
/**
 Returns the rectangle in which the receiver draws its title.
 @param contentRect The content rectangle for the receiver.
 @return The rectangle in which the receiver draws its title.
 */
- (CGRect)titleRectForContentRect:(CGRect)contentRect;
/**
 Returns the rectangle in which the receiver draws its image.
 @param contentRect The content rectangle for the receiver.
 @return The rectangle in which the receiver draws its image.
 */
- (CGRect)imageRectForContentRect:(CGRect)contentRect;
/**
 The accessory label's image view. (read-only)
 */
@property(nonatomic,readonly,retain) UIImageView *imageView;
/**
 A view that displays the value of the title property for the label. (read-only)
 */
@property(nonatomic,readonly,retain) UILabel *titleLabel;
/**
 The vertical alignment of content (text and image) within the receiver.
 */
@property(nonatomic) UIControlContentVerticalAlignment contentVerticalAlignment;
/**
 The horizontal alignment of content (text and image) within the receiver.
 */
@property(nonatomic) UIControlContentHorizontalAlignment contentHorizontalAlignment;
/**
 Direction for the layout. Default is MBAccessoryLabelLayoutDirectionHorizontal.
 */
@property(nonatomic) MBAccessoryLabelLayoutDirection layoutDirection;
/**
 The inset or outset margins for the rectangle surrounding all of the accessory label's content.
 */
@property(nonatomic,assign) UIEdgeInsets contentEdgeInsets;
/**
 The inset or outset margins for the rectangle around the accessory label's title text.
 */
@property(nonatomic,assign) UIEdgeInsets titleEdgeInsets;
/**
 The inset or outset margins for the rectangle around the accessory label's image.
 */
@property(nonatomic,assign) UIEdgeInsets imageEdgeInsets;

@end
