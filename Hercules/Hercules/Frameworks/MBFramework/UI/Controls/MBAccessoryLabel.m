//
//  MBAccessoryLabel.m
//  MBFramework
//
//  Created by Arno Woestenburg on 05/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBAccessoryLabel.h"
#import "MBRectFunctions.h"

@implementation MBAccessoryLabel

@synthesize titleLabel = titleLabel_;
@synthesize imageView = imageView_;
@synthesize contentVerticalAlignment = contentVerticalAlignment_;
@synthesize contentHorizontalAlignment = contentHorizontalAlignment_;
@synthesize contentEdgeInsets = contentEdgeInsets_;
@synthesize titleEdgeInsets = titleEdgeInsets_;
@synthesize imageEdgeInsets = imageEdgeInsets_;
@synthesize layoutDirection = layoutDirection_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // By default user events are ignored and removed from the event queue.
        [self setUserInteractionEnabled:NO];
        layoutDirection_ = MBAccessoryLabelLayoutDirectionHorizontal;
        contentEdgeInsets_ = UIEdgeInsetsZero;
        titleEdgeInsets_ = UIEdgeInsetsZero;
        imageEdgeInsets_ = UIEdgeInsetsZero;
        titleLabel_ = [[[UILabel alloc] initWithFrame:self.bounds] autorelease];
        [titleLabel_ setBackgroundColor:[UIColor clearColor]];
        [self addSubview:titleLabel_];
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    CGRect contentRect = [self contentRectForBounds:self.bounds];
    if (imageView_) {
        [imageView_ setFrame:[self imageRectForContentRect:contentRect]];
    }
    [titleLabel_ setFrame:[self titleRectForContentRect:contentRect]];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize result = CGSizeZero;
    
    switch (self.layoutDirection) {
        case MBAccessoryLabelLayoutDirectionHorizontal:
            result = [self MB_sizeThatFitsHorizontal:size];
            break;
        case MBAccessoryLabelLayoutDirectionVertical:
            result = [self MB_sizeThatFitsVertical:size];
            break;
        default:
            NSAssert(FALSE, @"Unhandled case");
            break;
    }
    result.width += self.contentEdgeInsets.left + self.contentEdgeInsets.right;
    result.height += self.contentEdgeInsets.top + self.contentEdgeInsets.bottom;
    
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (MBAlignment)MB_alignmentForContent
{
    MBAlignment alignment = 0;

    switch (self.contentVerticalAlignment) {
        case UIControlContentVerticalAlignmentTop:
            alignment |= MBAlignmentTop;
            break;
        case UIControlContentVerticalAlignmentBottom:
            alignment |= MBAlignmentBottom;
            break;
        default:
            alignment |= MBAlignmentVerticalCenter;
            // Center
            break;
    }
    switch (self.contentHorizontalAlignment) {
        case UIControlContentHorizontalAlignmentLeft:
            alignment |= MBAlignmentLeft;
            break;
        case UIControlContentHorizontalAlignmentRight:
            alignment |= MBAlignmentRight;
            break;
        default:
            alignment |= MBAlignmentHorizontalCenter;
            // Center
            break;
    }
    return alignment;
}

- (CGSize)MB_sizeThatFitsHorizontal:(CGSize)size
{
    CGSize imageSize = imageView_.image.size;
    CGSize sizeConstraint = CGSizeZero;
    if (!CGSizeEqualToSize(size, CGSizeZero)) {
        sizeConstraint = CGSizeMake(size.width - imageSize.width, size.height);
    }
    CGSize labelSize = [titleLabel_ sizeThatFits:sizeConstraint];
    
    return CGSizeMake(imageSize.width + labelSize.width, MAX(imageSize.height, labelSize.height));
}

- (CGSize)MB_sizeThatFitsVertical:(CGSize)size
{
    CGSize imageSize = imageView_.image.size;
    CGSize sizeConstraint = CGSizeZero;
    if (!CGSizeEqualToSize(size, CGSizeZero)) {
        sizeConstraint = CGSizeMake(size.width, size.height - imageSize.height);
    }
    CGSize labelSize = [titleLabel_ sizeThatFits:sizeConstraint];
    return CGSizeMake(MAX(imageSize.width, labelSize.width), imageSize.height + labelSize.height);
}


//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setImage:(UIImage *)image
{
    [self.imageView setImage:image];
    [self setNeedsLayout];
}

- (void)setTitle:(NSString *)title
{
    [titleLabel_ setText:title];
    [self setNeedsLayout];
}

- (void)setAttributedTitle:(NSAttributedString *)title
{
    [titleLabel_ setAttributedText:title];
    [self setNeedsLayout];
}

- (CGRect)contentRectForBounds:(CGRect)bounds
{
    return UIEdgeInsetsInsetRect(bounds, contentEdgeInsets_);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGRect titleRect = CGRectZero;
    
    CGSize imageSize = imageView_.bounds.size;
    CGFloat maxWidth = contentRect.size.width;
    
    if (self.layoutDirection == MBAccessoryLabelLayoutDirectionHorizontal) {
        maxWidth -= imageSize.width;
    }
    titleRect.size = [titleLabel_ sizeThatFits:CGSizeMake(maxWidth, CGFLOAT_MAX)];
    titleRect.size.width = MIN(titleRect.size.width, maxWidth);
    
    CGRect totalRect = CGRectZero;
    MBAlignment titleAlignment = 0;
    MBAlignment mask = 0;
    
    switch (self.layoutDirection) {
        case MBAccessoryLabelLayoutDirectionHorizontal:
            titleAlignment = MBAlignmentRight;
            mask = MBAlignmentMaskVertical;
            totalRect.size = CGSizeMake(imageSize.width + titleRect.size.width, titleRect.size.height);
            break;
        case MBAccessoryLabelLayoutDirectionVertical:
            titleAlignment = MBAlignmentBottom;
            mask = MBAlignmentMaskHorizontal;
            totalRect.size = CGSizeMake(titleRect.size.width, imageSize.height + titleRect.size.height);
            break;
        default:
            NSAssert(FALSE, @"Unhandled case");
            break;
    }
    
    MBAlignment alignment = [self MB_alignmentForContent];
    totalRect = [MBRectFunctions alignRect:totalRect toRect:contentRect alignMode:alignment];
    titleRect = [MBRectFunctions alignRect:titleRect toRect:totalRect alignMode:(alignment & mask) | titleAlignment];

    CGSize maxSize = CGSizeZero;
    
    titleRect.origin.x += self.titleEdgeInsets.left;
    maxSize.width = CGRectGetMaxX(contentRect) - titleRect.origin.x - self.titleEdgeInsets.right;
    titleRect.size.width = MIN(titleRect.size.width, maxSize.width);
    
    titleRect.origin.y += self.titleEdgeInsets.top;
    maxSize.height = CGRectGetMaxY(contentRect) - titleRect.origin.y - self.titleEdgeInsets.bottom;
    titleRect.size.height = MIN(titleRect.size.height, maxSize.height);
    
    return titleRect;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGRect imageRect = CGRectZero;
    
    if (imageView_.image) {
        CGSize imageSize = imageView_.image.size;
        imageRect = CGRectMake(0, 0, imageSize.width, imageSize.height);
        
        CGRect titleRect = CGRectZero;
        CGFloat maxWidth = contentRect.size.width;
        if (self.layoutDirection == MBAccessoryLabelLayoutDirectionHorizontal) {
            maxWidth -= imageSize.width;
        }
        titleRect.size = [titleLabel_ sizeThatFits:CGSizeMake(maxWidth, CGFLOAT_MAX)];
        titleRect.size.width = MIN(titleRect.size.width, maxWidth);
        
        CGRect totalRect = CGRectZero;
        MBAlignment imageAlignment = 0;
        MBAlignment mask = 0;

        switch (self.layoutDirection) {
            case MBAccessoryLabelLayoutDirectionHorizontal:
                imageAlignment = MBAlignmentLeft;
                mask = MBAlignmentMaskVertical;
                totalRect.size = CGSizeMake(imageRect.size.width + titleRect.size.width, imageRect.size.height);
                break;
            case MBAccessoryLabelLayoutDirectionVertical:
                imageAlignment = MBAlignmentTop;
                mask = MBAlignmentMaskHorizontal;
                totalRect.size = CGSizeMake(imageRect.size.width, imageRect.size.height + titleRect.size.height);
                break;
            default:
                NSAssert(FALSE, @"Unhandled case");
                break;
        }
        
        MBAlignment alignment = [self MB_alignmentForContent];
        totalRect = [MBRectFunctions alignRect:totalRect toRect:contentRect alignMode:alignment];
        imageRect = [MBRectFunctions alignRect:imageRect toRect:totalRect alignMode:(alignment & mask) | imageAlignment];
        imageRect.origin.x += (self.imageEdgeInsets.left - self.imageEdgeInsets.right);
        imageRect.origin.y += (self.imageEdgeInsets.top - self.imageEdgeInsets.bottom);
    }
    return imageRect;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UIImageView *)imageView
{
    // Create on demand
    if (!imageView_) {
        imageView_ = [[[UIImageView alloc] initWithImage:nil] autorelease];
        [self addSubview:imageView_];
    }
    return imageView_;
}

- (void)setContentHorizontalAlignment:(UIControlContentHorizontalAlignment)contentHorizontalAlignment
{
    if (contentHorizontalAlignment_ != contentHorizontalAlignment) {
        contentHorizontalAlignment_ = contentHorizontalAlignment;
        [self setNeedsLayout];
    }
}

@end
