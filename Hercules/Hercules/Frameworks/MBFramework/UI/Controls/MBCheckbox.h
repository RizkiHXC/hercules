//
//  MBCheckbox.h
//  MBFramework
//
//  Created by Arno Woestenburg on 28/10/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 An instance of the MBCheckbox class implements a checkbox on the touch screen. A checkbox intercepts touch events and toggles checked state and sends an action message to a target object when tapped. Methods for setting the target and action are inherited from UIControl. This class provides methods for setting the title, image, and other appearance properties of a checkbox. By using these accessors, you can specify a different appearance for each checkbox state.
 */
@interface MBCheckbox : UIButton

/**
 A Boolean value that determines the receiver’s checked state.
 @discussion Checked state is equal to selected state (title, image, background). Default is NO. Animated is NO.
 */
@property(nonatomic,getter=isChecked) BOOL checked;

@end
