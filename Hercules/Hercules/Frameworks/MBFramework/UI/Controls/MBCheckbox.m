//
//  MBCheckbox.m
//  MBFramework
//
//  Created by Arno Woestenburg on 28/10/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBCheckbox.h"
#import "MBRectFunctions.h"

@implementation MBCheckbox

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIButton

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGRect imageRect = [super imageRectForContentRect:contentRect];
    return [MBRectFunctions alignRect:imageRect toRect:contentRect alignMode:MBAlignmentRightCenter];
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    contentRect.size.width -= self.imageView.bounds.size.width;
    return [super titleRectForContentRect:contentRect];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (BOOL)isChecked
{
    // Checked state is equal to selected state
    return self.selected;
}

- (void)setChecked:(BOOL)checked
{
    // Checked state is equal to selected state
    [self setSelected:checked];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIControl

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.touchInside) {
        // Toggle checked state.
        [self setChecked:!self.checked];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
    [super touchesEnded:touches withEvent:event];
}


@end
