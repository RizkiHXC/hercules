//
//  MBHyperlinkLabel.h
//  MBFramework
//
//  Created by Arno Woestenburg on 06/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 An instance of the MBHyperlinkLabel class implements a tappable hyperlink label on the touch screen. A hyperlink intercepts touch events and sends an action message to a target object when tapped. Methods for setting the target and action are inherited from UIControl. This class provides methods for setting the title, and other appearance properties of a hyperlink label. By using these accessors, you can specify a different appearance for each link state.
 */
@interface MBHyperlinkLabel : UIControl
{
    UILabel *textLabel_;
    NSURL *url_;
}

/**
 Sets the color of the title to use for the specified state.
 @param color The color of the title to use for the specified state.
 @param state The state that uses the specified color. The possible values are described in UIControlState.
 */
- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state;
/**
 Returns the title color used for a state.
 @param state The state that uses the title color. The possible values are described in UIControlState.
 @return The color of the title for the specified state.
 */
- (UIColor *)titleColorForState:(UIControlState)state;

/**
 A view that displays the value of the currentTitle property for the hyperlink. (read-only)
 */
@property(nonatomic,retain,readonly) UILabel *textLabel;
/**
 The hyperlink label URL.
 */
@property(nonatomic,retain) NSURL *url;


@end
