//
//  MBHyperlinkLabel.m
//  MBFramework
//
//  Created by Arno Woestenburg on 06/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBHyperlinkLabel.h"

@interface MBHyperlinkLabel ()
{
    NSMutableDictionary *titleColors_;
    UIControlState controlState_;
}
@end

@implementation MBHyperlinkLabel

@synthesize textLabel = textLabel_;
@synthesize url = url_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        textLabel_ = [[[UILabel alloc] initWithFrame:self.bounds] autorelease];
        [textLabel_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [textLabel_ setBackgroundColor:[UIColor clearColor]];
        [self addSubview:textLabel_];
        
        titleColors_ = [[NSMutableDictionary alloc] init];
        
    }
    return self;
}

- (void)dealloc
{
    [titleColors_ release];
    self.url = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_updateViewForState
{
    UIControlState state = UIControlStateNormal;
    
    if (self.enabled) {
        if (self.highlighted) {
            state |= UIControlStateHighlighted;
        }
        
        if (self.selected) {
            state |= UIControlStateSelected;
        }
    } else {
        state = UIControlStateDisabled;
    }
    if (state != controlState_) {
        controlState_ = state;
        [self.textLabel setTextColor:[self titleColorForState:state]];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (CGSize)sizeThatFits:(CGSize)size
{
    return [textLabel_ sizeThatFits:size];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIControl

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self MB_updateViewForState];
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self MB_updateViewForState];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state
{
    [titleColors_ setObject:color forKey:[NSNumber numberWithUnsignedInt:state]];
    if (state == controlState_) {
        // Update color when state is current state.
        [self.textLabel setTextColor:[self titleColorForState:state]];
    }
}

- (UIColor *)titleColorForState:(UIControlState)state
{
    UIColor *titleColor = [titleColors_ objectForKey:[NSNumber numberWithUnsignedInt:state]];
    
    if (!titleColor) {
        // Default to normal color when color has not been set for the specified state.
        titleColor = [titleColors_ objectForKey:[NSNumber numberWithUnsignedInt:state]];
    }
    return titleColor;
}


@end
