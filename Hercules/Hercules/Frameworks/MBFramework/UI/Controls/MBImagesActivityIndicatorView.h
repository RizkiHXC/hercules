//
//  MBImagesActivityIndicatorView.h
//  MBFramework
//
//  Created by Marco Jonker on 11/26/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBImagesActivityIndicatorView : UIView

@property(nonatomic, assign) BOOL hidesWhenStopped;
@property(nonatomic, assign) BOOL showWhenStarted;
@property(nonatomic, assign) NSArray* images;
@property(nonatomic, assign) NSArray* highlightedImages;
@property(nonatomic, assign) NSTimeInterval animationDurationInSeconds;

/**
 Initializes and returns a newly allocated ImagesActivityIndicatorView object with the specified frame rectangle.
 @param frame The frame rectangle for the view, measured in points. The origin of the frame is relative to the superview in which you plan to add it. This method uses the frame rectangle to set the center and bounds properties accordingly.
 @param images A set of images for animation.
 @return An initialized ImagesActivityIndicatorView object or nil if the object couldn't be created.
 */
- (instancetype)initWithFrame:(CGRect)frame andImages:(NSArray*)images;
/**
 Initializes and returns a newly allocated ImagesActivityIndicatorView object with the specified frame rectangle.
 @param frame The frame rectangle for the view, measured in points. The origin of the frame is relative to the superview in which you plan to add it. This method uses the frame rectangle to set the center and bounds properties accordingly.
 @param imagePrefix Prefix string for the image load function.
 @param startIndex Start index for animation.
 @param numberOfImages Number of images for animation.
 @return An initialized ImagesActivityIndicatorView object or nil if the object couldn't be created.
 */
- (instancetype)initWithFrame:(CGRect)frame imagePrefix:(NSString*)imagePrefix startIndex:(NSInteger)startIndex numberOfImages:(NSInteger)numberOfImages;
- (void)startAnimating;
- (void)stopAnimating;
- (BOOL)isAnimating;

- (void)setColor:(UIColor*)color;
- (void)setImagesWithImagePrefix:(NSString*)imagePrefix  startIndex:(NSInteger)startIndex numberOfImages:(NSInteger)numberOfImages;

@end
