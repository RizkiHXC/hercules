//
//  MBImagesActivityIndicatorView.m
//  MBFramework
//
//  Created by Marco Jonker on 11/26/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBImagesActivityIndicatorView.h"

@interface MBImagesActivityIndicatorView() {
    UIImageView* imageView_;
}
@end

@implementation MBImagesActivityIndicatorView

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imageView_ = [[UIImageView alloc]initWithFrame:self.bounds];
        imageView_.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:imageView_];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andImages:(NSArray*)images {
    self = [self initWithFrame:frame];
    if(self != nil) {
        [self setImages:images];
    }
    
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame imagePrefix:(NSString*)imagePrefix startIndex:(NSInteger)startIndex numberOfImages:(NSInteger)numberOfImages  {
    self = [self initWithFrame:frame];
    if(self != nil) {
        [self setImagesWithImagePrefix:imagePrefix startIndex:startIndex numberOfImages:numberOfImages];
    }
    
    return self;
}

- (void)dealloc
{
    [imageView_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

-(void)setAnimationDurationInSeconds:(NSTimeInterval)seconds {
    imageView_.animationDuration = seconds;
}

-(NSTimeInterval)animationDurationInSeconds {
    return imageView_.animationDuration;
}

-(void)sizeToFit {
    if(imageView_.animationImages.count > 0) {
        UIImage* image = [imageView_.animationImages objectAtIndex:0];
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, image.size.width, image.size.height);
    } else {
        self.frame = CGRectZero;
    }
    
    imageView_.frame = self.bounds;
}

-(void)setImagesWithImagePrefix:(NSString*)imagePrefix startIndex:(NSInteger)startIndex numberOfImages:(NSInteger)numberOfImages  {
    NSMutableArray* images = [[[NSMutableArray alloc]init]autorelease];
 
    for(NSInteger i = startIndex; i < numberOfImages + startIndex; i++) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@%@", imagePrefix, [NSNumber numberWithInteger:i]]]];
    }
    
    [self setImages:images];
}

-(void)setImages:(NSArray *)images {
    imageView_.animationImages = images;
    [self sizeToFit];
}

-(NSArray *)images {
    return imageView_.animationImages;
}

-(void)setHighlightedImages:(NSArray *)highlightedImages {
    [imageView_ setHighlightedAnimationImages:highlightedImages];
}

-(NSArray *)highlightedImages {
    return imageView_.highlightedAnimationImages;
}

- (void)startAnimating {
    [imageView_ startAnimating];
    if(self.showWhenStarted == YES) {
        self.hidden = NO;
    }
}

- (void)stopAnimating {
    [imageView_ stopAnimating];
    if(self.hidesWhenStopped == YES) {
        self.hidden = YES;
    }
}

- (BOOL)isAnimating {
    return [imageView_ isAnimating];
}

-(void)setColor:(UIColor*)color {
    self.backgroundColor = color;
}

@end
