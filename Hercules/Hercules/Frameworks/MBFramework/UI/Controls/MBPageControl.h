//
//  MBPageControl.h
//  MBFramework
//
//  Created by Arno Woestenburg on 21/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 MBPageControl displays a horizontal series of dots, each of which corresponds to a page in the application’s document (or other data-model entity). The currently viewed page is indicated by a white dot.
 
 MBPageControl is based upon, and is intended to behave like a UIPageControl but offers more flexibility and is able to display a larger amount of dots.
 */
@interface MBPageControl : UIControl
{
    NSMutableArray  *indicators_;
    NSInteger       currentPage_;
    NSInteger       displayedPage_;
    CGFloat         pageMargin_;
    BOOL            hidesForSinglePage_;
    BOOL            defersCurrentPageDisplay_;
    UIImage         *currentPageImage_;
    UIImage         *pageImage_;
    UIEdgeInsets    contentEdgeInsets_;
}

/**
 Update page display to match the currentPage. 
 @discussion Ignored if defersCurrentPageDisplay is NO. setting the page value directly will update immediately
 */
- (void)updateCurrentPageDisplay;
/**
 Returns minimum size required to display dots for given page count. Can be used to size control if page count could change.
 @param pageCount The number of pages to fit in the receiver’s bounds.
 @return The minimum size required to display dots for the page count.
 */
- (CGSize)sizeForNumberOfPages:(NSInteger)pageCount;
/**
 Returns minimum size required to display dots for given page count. Tries to fit inside the given size value
 @param pageCount The number of pages to fit in the receiver’s bounds.
 @param size The maximum acceptable size for the reciever.
 @return The minimum size required to display dots for the page count.
 */
- (CGSize)sizeForNumberOfPages:(NSInteger)pageCount constrainedToSize:(CGSize)size;

/**
 The number of pages the receiver shows (as dots).
 */
@property(nonatomic) NSInteger numberOfPages;
/**
 The current page, shown by the receiver as a white dot.
 */
@property(nonatomic) NSInteger currentPage;
/**
 The inset or outset margins for the rectangle surrounding all of the page control's content.
 */
@property(nonatomic,assign) UIEdgeInsets contentEdgeInsets;
/**
 Hide the the indicator if there is only one page. default is NO
 */
@property(nonatomic) BOOL hidesForSinglePage;
/**
 If set, clicking to a new page won't update the currently displayed page until -updateCurrentPageDisplay is called. default is NO
 */
@property(nonatomic) BOOL defersCurrentPageDisplay;
/**
 The tint color to be used for the page indicator.
 */
@property(nonatomic,retain) UIColor *pageIndicatorTintColor;
/**
 The tint color to be used for the current page indicator.
 */
@property(nonatomic,retain) UIColor *currentPageIndicatorTintColor;
/**
 The image to be used for the page indicator.
 */
@property(nonatomic,retain) UIImage *currentPageImage;
/**
 The image to be used for the current page indicator.
 */
@property(nonatomic,retain) UIImage *pageImage;

@end
