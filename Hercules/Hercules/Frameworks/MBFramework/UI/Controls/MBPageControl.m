//
//  MBPageControl.m
//  MBFramework
//
//  Created by Arno Woestenburg on 21/02/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBPageControl.h"

@interface MBPageControl ()
{
    NSInteger  indicatorPage_;
}
@end

@implementation MBPageControl

@synthesize contentEdgeInsets = contentEdgeInsets_;
@synthesize currentPage = currentPage_;
@synthesize currentPageImage = currentPageImage_;
@synthesize currentPageIndicatorTintColor = currentPageIndicatorTintColor_;
@synthesize defersCurrentPageDisplay = defersCurrentPageDisplay_;
@synthesize hidesForSinglePage = hidesForSinglePage_;
@synthesize numberOfPages = numberOfPages_;
@synthesize pageImage = pageImage_;
@synthesize pageIndicatorTintColor = pageIndicatorTintColor_;

const CGFloat kMinimumPageMargin = 2.0;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        currentPage_ = 0;
        indicatorPage_ = 0;
        displayedPage_ = 0;
        hidesForSinglePage_ = NO;
        defersCurrentPageDisplay_ = NO;
        currentPageImage_ = nil;
        pageImage_ = nil;
        pageMargin_ = 9;
        contentEdgeInsets_ = UIEdgeInsetsMake(4, 0, 4, 0);
        self.pageIndicatorTintColor = [UIColor colorWithWhite:1.0 alpha:0.2];
        self.currentPageIndicatorTintColor = [UIColor colorWithWhite:1.0 alpha:1.0];

        [self setBackgroundColor:[UIColor clearColor]];
        [self MB_createDefaultImages];
        
        [self addTarget:self action:@selector(MB_touchUpInside:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
}

- (void)dealloc
{
    self.pageImage = nil;
    self.currentPageImage = nil;
    self.pageIndicatorTintColor = nil;
    self.currentPageIndicatorTintColor = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect
{
    CGFloat margin = pageMargin_;
    CGRect imageRect = CGRectZero;
    CGRect drawArea = CGRectZero;

    imageRect.size = pageImage_.size;

    drawArea = UIEdgeInsetsInsetRect(self.bounds, contentEdgeInsets_);
    
    [self MB_fitSizeForNumberOfPages:self.numberOfPages constrainedToSize:drawArea.size imageSize:&imageRect.size margin:&margin];
    
    drawArea.origin.x = roundf((self.bounds.size.width - drawArea.size.width) * 0.5);
    drawArea.origin.y = roundf((self.bounds.size.height - drawArea.size.height) * 0.5);
    
    imageRect.origin.x = drawArea.origin.x;
    imageRect.origin.y = roundf(CGRectGetMidY(drawArea) - (imageRect.size.height * 0.5));
    
    for (NSInteger page = 0; page < self.numberOfPages; page++) {
        if (page == indicatorPage_) {
            [self MB_drawImage:currentPageImage_ inRect:imageRect fillColor:currentPageIndicatorTintColor_];
        } else {
            [self MB_drawImage:pageImage_ inRect:imageRect fillColor:pageIndicatorTintColor_];
        }
        imageRect.origin.x += imageRect.size.width + margin;
    }
}

- (CGSize)sizeThatFits:(CGSize)size
{
    return [self sizeForNumberOfPages:self.numberOfPages constrainedToSize:size];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_createDefaultImages
{
    CGFloat scale = [[UIScreen mainScreen] scale];
    CGRect ellipseRect = CGRectMake(0, 0, 7, 7);
    
    // Page image
    UIGraphicsBeginImageContextWithOptions(ellipseRect.size, NO, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetRGBFillColor(context, 0.1f, 0.1f, 0.1f, 1.0f);
    CGContextFillEllipseInRect(context, ellipseRect);
    self.pageImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Current page image
    UIGraphicsBeginImageContextWithOptions(ellipseRect.size, NO, scale);
    context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.1f, 0.1f, 0.1f, 1.0f);
    CGContextFillEllipseInRect(context, ellipseRect);
    self.currentPageImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (void)MB_drawImage:(UIImage *)image inRect:(CGRect)drawRect fillColor:(UIColor *)fillColor
{
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
    if (image.renderingMode != UIImageRenderingModeAlwaysOriginal) {
#else
        if ([image respondsToSelector:@selector(renderingMode)] && image.renderingMode != UIImageRenderingModeAlwaysOriginal) {
#endif // __IPHONE_OS_VERSION_MIN_REQUIRED
        image = [self tintedImage:image withColor:fillColor];
    }
    [image drawInRect:drawRect];
}

- (UIImage *)tintedImage:(UIImage *)image withColor:(UIColor *)tintColor
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);
    [tintColor setFill];
    CGRect bounds = CGRectMake(0, 0, image.size.width, image.size.height);
    UIRectFill(bounds);
    [image drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1.0];
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}

- (void)MB_fitSizeForNumberOfPages:(NSInteger)pageCount constrainedToSize:(CGSize)size imageSize:(CGSize *)imageSize margin:(CGFloat *)margin
{
    if (pageCount > 1 && imageSize->width > 0) {
        CGSize viewSize = CGSizeMake((pageCount * imageSize->height) + ((pageCount - 1) * *margin), imageSize->height);
        if (viewSize.width > size.width) {
            CGFloat fitMargin = (size.width - (pageCount * imageSize->height)) / (pageCount - 1);
            CGFloat fitImageScale = 1.0;
            
            if (fitMargin < kMinimumPageMargin) {
                fitMargin = kMinimumPageMargin;
                
                fitImageScale = ((size.width - ((pageCount - 1) * fitMargin)) / pageCount) / imageSize->width;
                imageSize->width *= fitImageScale;
                imageSize->height *= fitImageScale;
            }
            *margin = fitMargin;
        }
    }
}

- (void)MB_updateCurrentPage:(NSInteger)currentPage defersPageDisplay:(BOOL)defersPageDisplay
{
    // Values outside the possible range are pinned to either 0 or numberOfPages minus 1.
    currentPage = MAX(MIN(currentPage, (self.numberOfPages - 1)), 0);
    
    if (currentPage != currentPage_) {
        currentPage_ = currentPage;
        if (defersPageDisplay == NO) {
            indicatorPage_ = currentPage;
        }
        [self setNeedsDisplay];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)updateCurrentPageDisplay
{
    // This method updates the page indicator so that the current page (the white dot) matches the value returned from currentPage.
    if (self.defersCurrentPageDisplay) {
        if (indicatorPage_ != self.currentPage) {
            indicatorPage_ = self.currentPage;
            [self setNeedsDisplay];
        }
    }
}

- (CGSize)sizeForNumberOfPages:(NSInteger)pageCount
{
    pageCount = MAX(pageCount, 1);
    CGSize result = CGSizeMake((pageCount * pageImage_.size.width) + ((pageCount - 1) * pageMargin_), pageImage_.size.height);
    result.width += contentEdgeInsets_.left + contentEdgeInsets_.right;
    result.height += contentEdgeInsets_.top + contentEdgeInsets_.bottom;
    return result;
}

- (CGSize)sizeForNumberOfPages:(NSInteger)pageCount constrainedToSize:(CGSize)size
{
    if (size.width == 0) {
        size.width = CGFLOAT_MAX;
    }
    if (size.height == 0) {
        size.height = CGFLOAT_MAX;
    }
    
    pageCount = MAX(pageCount, 1);
    size.width -= (contentEdgeInsets_.left + contentEdgeInsets_.right);
    
    CGSize imageSize = pageImage_.size;
    CGFloat pageMargin = pageMargin_;
    
    [self MB_fitSizeForNumberOfPages:pageCount constrainedToSize:size imageSize:&imageSize margin:&pageMargin];
    
    CGSize result = CGSizeMake((pageCount * imageSize.width) + ((pageCount - 1) * pageMargin), imageSize.height);

    result.width += contentEdgeInsets_.left + contentEdgeInsets_.right;
    result.height += contentEdgeInsets_.top + contentEdgeInsets_.bottom;
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setNumberOfPages:(NSInteger)numberOfPages
{
    if (numberOfPages_ != numberOfPages) {
        numberOfPages_ = numberOfPages;
        if (self.hidesForSinglePage) {
            [self setHidden:numberOfPages_ <= 1 ? YES : NO];
        }
        [self setNeedsDisplay];
    }
}

- (void)setCurrentPage:(NSInteger)currentPage
{
    [self MB_updateCurrentPage:currentPage defersPageDisplay:NO];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponder

- (void)MB_touchUpInside:(MBPageControl *)sender withEvent:(UIEvent *)event
{
    UITouch *touch = [[event touchesForView:sender] anyObject];
    CGPoint location = [touch locationInView:sender];
    CGRect bounds = self.bounds;
    
    if (CGRectContainsPoint(bounds, location)) {
        // Left half or right half of the view
        if (location.x < (CGRectGetMidX(bounds))) {
            // Left half of the view
            [self MB_updateCurrentPage:(self.currentPage - 1) defersPageDisplay:self.defersCurrentPageDisplay];
        } else {
            // Right half of the view
            [self MB_updateCurrentPage:(self.currentPage + 1) defersPageDisplay:self.defersCurrentPageDisplay];
        }
    }
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

@end
