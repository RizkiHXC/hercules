//
//  MBPlaceholderControl.h
//  MBFramework
//
//  Created by Arno Woestenburg on 13/06/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBAccessoryLabel.h"

/**
 The placeholder control can be used i.e. for empty TableViews etc. The control can be tapped to initiate a retry event e.g. when the connection is lost.
 */
@interface MBPlaceholderControl : UIControl

/**
 Returns the accessory label used for the main textual content of the placeholder control. (read-only)
 */
@property(nonatomic,readonly) MBAccessoryLabel *accessoryLabel;
/**
 The background view of the placeholder view.
 @discussion A placeholder view’s background view is automatically resized to match the size of the view. This view is placed as a subview of the placeholder view behind all content.
 */
@property(nonatomic,retain) UIView *backgroundView;
/**
 The inset or outset margins for the rectangle surrounding all of the placeholder view's content.
 */
@property(nonatomic,assign) UIEdgeInsets contentEdgeInsets;

@end
