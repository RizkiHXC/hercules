//
//  MBPlaceholderControl.m
//  MBFramework
//
//  Created by Arno Woestenburg on 13/06/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//
//  Note: This control can be used i.e. for empty TableViews etc.
//  the control can be tapped to initiate a retry event e.g. when the connection is lost.

#import "MBPlaceholderControl.h"
#import "MBRectFunctions.h"

@implementation MBPlaceholderControl

@synthesize accessoryLabel = accessoryLabel_;
@synthesize backgroundView = backgroundView_;
@synthesize contentEdgeInsets = contentEdgeInsets_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        contentEdgeInsets_ = UIEdgeInsetsZero;
        // Accessory label
        accessoryLabel_ = [[[MBAccessoryLabel alloc] init] autorelease];
        [accessoryLabel_ setUserInteractionEnabled:NO];
        [self addSubview:accessoryLabel_];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [backgroundView_ setFrame:self.bounds];
    
    CGRect contentRect = UIEdgeInsetsInsetRect(self.bounds, contentEdgeInsets_);
    
    CGRect viewRect = CGRectZero;
    viewRect.size = [accessoryLabel_ sizeThatFits:contentRect.size];
    [accessoryLabel_ setFrame:[MBRectFunctions alignRect:viewRect toRect:contentRect alignMode:MBAlignmentCenter]];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setBackgroundView:(UIView *)backgroundView
{
    [backgroundView_ release];
    [backgroundView_ removeFromSuperview];
    backgroundView_ = [backgroundView retain];
    if (backgroundView_) {
        // Insert at the back of the Z-order
        [self insertSubview:backgroundView_ atIndex:0];
        [self setNeedsLayout];
    }
}

- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets
{
    if (!UIEdgeInsetsEqualToEdgeInsets(contentEdgeInsets_, contentEdgeInsets)) {
        contentEdgeInsets_ = contentEdgeInsets;
        [self setNeedsLayout];
    }
}

@end
