//
//  MBRangeSlider.h
//  MBFramework
//
//  Created by Arno Woestenburg on 29/08/14.
//  Copyright (c) 2014 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 A MBRangeSlider object is a visual control used to select a value range from a continuous range of values. Sliders are always displayed as horizontal bars. An indicator, or thumb, notes the current value of the slider and can be moved by the user to change the setting.
  @discussion MBRangeSlider is based upon, and is intended to behave like a UIKit's UISlider control.
 */
@interface MBRangeSlider : UIControl

/**
 Contains a Boolean value indicating whether changes in the sliders value generate continuous update events.
 */
@property(nonatomic, getter=isContinuous) BOOL continuous;

/**
 Contains the receiver’s current low range value.
 */
@property(nonatomic) CGFloat lowValue;

/**
 Contains the receiver’s current high range value.
 */
@property(nonatomic) CGFloat highValue;

/**
 Contains the minimum value of the receiver.
 */
@property(nonatomic) CGFloat minimumValue;

/**
 Contains the minimum value of the receiver.
 */
@property(nonatomic) CGFloat maximumValue;

/**
 Contains the minimum range value of the receiver.
 */
@property(nonatomic) CGFloat minimumRange;

/**
 Contains the maximum range value of the receiver.
 */
@property(nonatomic) CGFloat maximumRange;

/**
 An image to use for the range portion of the slider bar that is filled.
 */
@property(nonatomic, retain) UIImage *rangeImage;

/**
 An image to use for the portion of the track that is not filled.
 */
@property(nonatomic, retain) UIImage *trackImage;

/**
 Insets for the thumbs. Only left and right are used, top and bottom are ignored.
 */
@property(nonatomic,assign) UIEdgeInsets thumbInsets;

/**
 Assigns a low range value thumb image to the specified control states.
 *
 @param image The thumb image to associate with the specified states.
 @param state The control state with which to associate the image.
 */
- (void)setLowValueThumbImage:(UIImage *)image forState:(UIControlState)state;

/**
 Returns the low range value thumb image associated with the specified control state.
 @param state The control state whose thumb image you want.
 @return The low range value thumb image associated with the specified state, or nil if an appropriate image could not be retrieved.
 */
- (UIImage *)lowValueThumbImageForState:(UIControlState)state;

/**
 Assigns a high range value thumb image to the specified control states.
 @param image The thumb image to associate with the specified states.
 @param state The control state with which to associate the image.
 */
- (void)setHighValueThumbImage:(UIImage *)image forState:(UIControlState)state;

/**
 Returns the high range value thumb image associated with the specified control state.
 @param state The control state whose thumb image you want.
 @return The high range value thumb image associated with the specified state, or nil if an appropriate image could not be retrieved.
 */
- (UIImage *)highValueThumbImageForState:(UIControlState)state;

/**
 Returns the drawing rectangle for the slider’s track.
 @param bounds The bounding rectangle of the receiver.
 @return The computed drawing rectangle for the track. This rectangle corresponds to the entire length of the track between the minimum and maximum value images.
 */
- (CGRect)trackRectForBounds:(CGRect)bounds;

/**
 Returns the drawing rectangle for the slider’s high value thumb image.
 @param bounds The bounding rectangle of the receiver.
 @param rect   The drawing rectangle for the receiver’s track, as returned by the trackRectForBounds: method.
 @param value  The current high value value of the range slider.
 @return The computed drawing rectangle for the high value thumb image.
 */
- (CGRect)highValueThumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(CGFloat)value;

/**
 Returns the drawing rectangle for the slider’s low value thumb image.
 @param bounds The bounding rectangle of the receiver.
 @param rect   The drawing rectangle for the receiver’s track, as returned by the trackRectForBounds: method.
 @param value  The current low value value of the range slider.
 @return The computed drawing rectangle for the low value thumb image.
 */
- (CGRect)lowValueThumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(CGFloat)value;


@end
