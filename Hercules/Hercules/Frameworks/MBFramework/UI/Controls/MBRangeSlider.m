//
//  MBRangeSlider.m
//  MBFramework
//
//  Created by Arno Woestenburg on 29/08/14.
//  Copyright (c) 2014 mediaBunker B.V. All rights reserved.
//

#import "MBRangeSlider.h"
#import "MBFramework.h"

@interface MBRangeSlider ()
{
    UIImageView *trackImageView_;
    UIImageView *rangeImageView_;
    UIImageView *lowValueThumb_;
    UIImageView *highValueThumb_;
    UIImageView *currentTrackingThumb_;
}
@end

@implementation MBRangeSlider

static const CGSize kDefaultSize = { 30, 36 };

@synthesize continuous = continuous_;
@synthesize highValue = highValue_;
@synthesize lowValue = lowValue_;
@synthesize maximumRange = maximumRange_;
@synthesize minimumRange = minimumRange_;
@synthesize maximumValue = maximumValue_;
@synthesize minimumValue = minimumValue_;
@synthesize rangeImage = rangeImage_;
@synthesize thumbInsets = thumbInsets_;
@synthesize trackImage = trackImage_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Default values
        continuous_ = YES;
		minimumRange_ = 0.0;
		maximumRange_ = CGFLOAT_MAX;
        maximumValue_ = 1.0f;
        minimumValue_ = 0.0f;
		lowValue_ = minimumValue_;
		highValue_ = maximumValue_;

        // Track image view (background image)
		trackImageView_ = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
		[self addSubview:trackImageView_];
		
		// Range image view
        rangeImageView_ = [[[UIImageView alloc] init] autorelease];
        [self addSubview:rangeImageView_];
        [self setRangeImage:[UIImage imageWithColor:[UIColor redColor] size:CGSizeMake(10.0f, 10.0f)]];
        [self setTrackImage:[UIImage imageWithColor:[UIColor lightGrayColor] size:CGSizeMake(20.0f, 20.0f)]];

		[self MB_initializeSliders];
		[self MB_updateThumbViews];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_withThumbView:(UIImageView *)thumbView setThumbImage:(UIImage *)image forState:(UIControlState)state
{
    switch (state) {
        case UIControlStateNormal:
            [thumbView setImage:image];
            break;
        case UIControlStateHighlighted:
            [thumbView setHighlightedImage:image];
        default:
            break;
    }
}

- (UIImage *)MB_withThumbView:(UIImageView *)thumbView thumbImageForState:(UIControlState)state
{
    UIImage *thumbImage = nil;
    switch (state) {
        case UIControlStateNormal:
            thumbImage = thumbView.image;
            break;
        case UIControlStateHighlighted:
            thumbImage = thumbView.highlightedImage;
        default:
            break;
    }
    return thumbImage;
}

- (void)MB_updateValuesWithTouch:(UITouch *)touch sendUpdate:(BOOL)sendUpdate
{
    CGPoint location = [touch locationInView:self];
    CGFloat lowValue = lowValue_;
    CGFloat highValue = highValue_;
    CGFloat coordinate = location.x - (currentTrackingThumb_.width * 0.5);
    CGFloat newValue = [self MB_factorFromCoordinate:coordinate];

    if (minimumRange_ > maximumRange_) {
        [NSException raise:NSInternalInconsistencyException format:@"%@ Minimum range must be smaller or equal to maximum range", NSStringFromSelector(_cmd)];
    }

    if (currentTrackingThumb_ == lowValueThumb_) {
        // New low value
        lowValue = MIN((maximumValue_ - minimumRange_), MAX(minimumValue_, newValue));
        // Update high value if needed
        highValue = MIN(maximumValue_, MAX(minimumValue_, MIN(MAX(highValue_, lowValue + minimumRange_), (lowValue + maximumRange_))));
    } else if (currentTrackingThumb_ == highValueThumb_) {
        // New high value
        highValue = MIN(maximumValue_, MAX((minimumValue_ + minimumRange_), newValue));
        // Update low value if needed
        lowValue = MIN(maximumValue_, MAX(minimumValue_, MAX(MIN(lowValue_, highValue - minimumRange_), highValue - maximumRange_)));
    }

    // Update values
	if (lowValue != lowValue_ || highValue != highValue_ || (sendUpdate && !continuous_)) {
		lowValue_ = lowValue;
		highValue_ = highValue;
        if (sendUpdate) {
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
	}
}

- (CGFloat)MB_valueFromPoint:(CGPoint)point
{
    point.x -= thumbInsets_.left;
    return [self MB_factorFromCoordinate:point.x];
}

- (CGPoint)MB_pointFromValue:(CGFloat)value
{
    CGPoint point = CGPointZero;
    point.x = [self MB_coordinateFromFactor:value];
    point.x += thumbInsets_.left;
    point.y = roundf(CGRectGetMidY(self.bounds));
    return point;
}

- (UIView *)MB_thumbHitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *hitView = [self hitTest:point withEvent:event];
    
    if (hitView == self) {
        for (UIView *subview in @[highValueThumb_, lowValueThumb_]) {
            if ([subview pointInside:[self convertPoint:point toView:subview] withEvent:event]) {
                hitView = subview;
                break;
            }
        }
    }
    return hitView;
}

- (CGFloat)MB_factorFromCoordinate:(CGFloat)coordinate
{
    CGRect thumbBounds = UIEdgeInsetsInsetRect(self.bounds, thumbInsets_);
    CGFloat percentage = coordinate / thumbBounds.size.width;
    
    CGFloat range = maximumValue_ - minimumValue_;
    CGFloat factor = minimumValue_ + (range * percentage);
    
    return factor;
}

- (CGFloat)MB_coordinateFromFactor:(CGFloat)factor
{
    CGRect thumbBounds = UIEdgeInsetsInsetRect(self.bounds, thumbInsets_);
    CGFloat range = maximumValue_ - minimumValue_;
    CGFloat percentage = (factor - minimumValue_) / range;
    
    CGFloat coordinate = (percentage * thumbBounds.size.width);
    
    return coordinate;
}

- (void)MB_initializeSliders
{
    // Low value thumb view
	lowValueThumb_ = [[[UIImageView alloc] init] autorelease];
	[self addSubview:lowValueThumb_];
    // High value thumb view
    highValueThumb_ = [[[UIImageView alloc] init] autorelease];
	[highValueThumb_ setFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
	[self addSubview:highValueThumb_];

    UIImage *defaultThumbImage = [self MB_defaultThumbImage];
    [self setLowValueThumbImage:defaultThumbImage forState:UIControlStateNormal];
    [self setHighValueThumbImage:defaultThumbImage forState:UIControlStateNormal];
}

- (UIImage *)MB_defaultThumbImage
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(20.f, 20.f), NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGRect rect = CGRectMake(0, 0, 20, 20);
    CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextFillEllipseInRect(ctx, rect);
    
    CGContextRestoreGState(ctx);
    UIImage *thumbImage  = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return thumbImage;
}

- (void)MB_setCurrentTrackingThumb:(UIImageView *)thumb
{
    [currentTrackingThumb_ setHighlighted:NO];
    currentTrackingThumb_ = thumb;
    [currentTrackingThumb_ setHighlighted:YES];
}

- (void)MB_updateThumbViews
{
    CGRect bounds = self.bounds;
    CGRect trackRect = [self trackRectForBounds:bounds];
    CGRect viewRect = CGRectZero;

    // Set high and low value thumbs
    [highValueThumb_ setFrame:[self highValueThumbRectForBounds:bounds trackRect:trackRect value:highValue_]];
    [lowValueThumb_ setFrame:[self highValueThumbRectForBounds:bounds trackRect:trackRect value:lowValue_]];
    
    // Range image view.
    viewRect.size = CGSizeMake(highValueThumb_.center.x - lowValueThumb_.center.x, rangeImageView_.image.size.height);
    viewRect.origin = CGPointMake(CGRectGetMidX(lowValueThumb_.frame), roundf(CGRectGetMidY(bounds) - (viewRect.size.height * 0.5)));
    [rangeImageView_ setFrame:viewRect];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // Track image view.
    CGRect trackRect = [self trackRectForBounds:self.bounds];
    [trackImageView_ setFrame:trackRect];

    [self MB_updateThumbViews];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    size.width = MAX(size.width, kDefaultSize.width);
    size.height = 0.0f;
    size.height = MAX(size.height, trackImageView_.height);
    size.height = MAX(size.height, rangeImageView_.height);
    size.height = MAX(size.height, [lowValueThumb_ sizeThatFits:CGSizeZero].height);
    size.height = MAX(size.height, [highValueThumb_ sizeThatFits:CGSizeZero].height);
    return size;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setHighValueThumbImage:(UIImage *)image forState:(UIControlState)state
{
    [self MB_withThumbView:highValueThumb_ setThumbImage:image forState:state];
}

- (UIImage *)highValueThumbImageForState:(UIControlState)state
{
    return [self MB_withThumbView:highValueThumb_ thumbImageForState:state];
}

- (void)setLowValueThumbImage:(UIImage *)image forState:(UIControlState)state
{
    [self MB_withThumbView:lowValueThumb_ setThumbImage:image forState:state];
}

- (UIImage *)lowValueThumbImageForState:(UIControlState)state
{
    return [self MB_withThumbView:lowValueThumb_ thumbImageForState:state];
}

- (CGRect)trackRectForBounds:(CGRect)bounds
{
    CGRect trackRect = CGRectZero;
    trackRect.size = CGSizeMake(bounds.size.width, trackImageView_.image.size.height);
    trackRect = [MBRectFunctions alignRect:trackRect toRect:bounds alignMode:MBAlignmentCenter];
    return trackRect;
}

- (CGRect)highValueThumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(CGFloat)value
{
    CGRect thumbRect = CGRectZero;
    CGPoint center = CGPointZero;
    
    // High value thumb
    UIImage *thumbImage = [self highValueThumbImageForState:UIControlStateNormal];
    thumbRect.size = thumbImage.size;
    center = [self MB_pointFromValue:value];
    thumbRect.origin = CGPointMake(roundf(center.x - (thumbRect.size.width * 0.5f)), roundf(center.y - (thumbRect.size.height * 0.5f)));
    return thumbRect;
}

- (CGRect)lowValueThumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(CGFloat)value
{
    CGRect thumbRect = CGRectZero;
    CGPoint center = CGPointZero;
    
    // Low value thumb
    UIImage *thumbImage = [self lowValueThumbImageForState:UIControlStateNormal];
    thumbRect.size = thumbImage.size;
    center = [self MB_pointFromValue:value];
    thumbRect.origin = CGPointMake(roundf(center.x - (thumbRect.size.width * 0.5f)), roundf(center.y - (thumbRect.size.height * 0.5f)));
    return thumbRect;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setRangeImage:(UIImage *)rangeImage
{
	[rangeImageView_ setImage:rangeImage];
	[self setNeedsLayout];
}

- (void)setTrackImage:(UIImage *)image
{
	[trackImageView_ setImage:image];
	[self setNeedsLayout];
}

- (void)setLowValue:(CGFloat)lowValue
{
    // Check range
	lowValue_ = MIN(maximumValue_, MAX(minimumValue_, lowValue));
	[self MB_updateThumbViews];
}

- (void)setHighValue:(CGFloat)highValue
{
    // Check range
	highValue_ = MIN(maximumValue_, MAX(minimumValue_, highValue));
	[self MB_updateThumbViews];
}

- (void)setMinimumRange:(CGFloat)minimumRange
{
	minimumRange_ = MAX(MIN(minimumRange, maximumRange_), 0.0);
    if (minimumRange != minimumRange_) {
        minimumRange_ = minimumRange;
        [self MB_updateThumbViews];
    }
}

- (void)setMaximumRange:(CGFloat)maximumRange
{
	maximumRange = MAX(maximumRange_, minimumRange_);
    if (maximumRange != maximumRange_) {
        maximumRange_ = maximumRange;
        [self MB_updateThumbViews];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    // Track the touch
    if ([self beginTrackingWithTouch:touch withEvent:event]) {
        [super touchesBegan:touches withEvent:event];
        UIView *hitView = [self MB_thumbHitTest:[touch locationInView:self] withEvent:event];
        
        if (hitView == lowValueThumb_ || hitView == highValueThumb_) {
            [self MB_setCurrentTrackingThumb:(UIImageView *)hitView];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    // Track the touch
    if ([self continueTrackingWithTouch:touch withEvent:event]) {
        [super touchesMoved:touches withEvent:event];
        [self MB_updateValuesWithTouch:touch sendUpdate:continuous_];
        [self MB_updateThumbViews];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self MB_updateValuesWithTouch:touch sendUpdate:YES];
	[super touchesEnded:touches withEvent:event];
    [self endTrackingWithTouch:touch withEvent:event];
	[self MB_setCurrentTrackingThumb:nil];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [super touchesCancelled:touches withEvent:event];
    [self cancelTrackingWithEvent:event];
    [self MB_updateValuesWithTouch:touch sendUpdate:YES];
	[self MB_setCurrentTrackingThumb:nil];
}

@end
