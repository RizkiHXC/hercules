//
//  MBRatingBar.h
//  MBFramework
//
//  Created by Arno Woestenburg on 10/11/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 A rating bar is an extension of UIControl that can be used to display a rating value. If user interaction is enabled, the user can touch and drag to set the rating value.
 
 User interaction is turned off by default, to enable user interaction set the userInteractionEnabled property to YES.
 */
@interface MBRatingBar : UIControl

/**
 Sets the rate image to use for the specified state.
 @param rateImage The rate image to use for the specified state.
 @param state     The state that uses the specified image. The values are described in UIControlState.
 */
- (void)setRateImage:(UIImage *)rateImage forState:(UIControlState)state;
/**
 Returns the rate image used for a state.
 @param state The state that uses the rate image. Possible values are described in UIControlState.
 @return The image used for the specified state.
 */
- (UIImage *)rateImageForState:(UIControlState)state;
/**
 Sets the receiver’s current value, allowing you to animate the change visually.
 @param value    The new value to assign to the value property
 @param animated Specify YES to animate the change in value when the receiver is redrawn; otherwise, specify NO to draw the receiver with the new value only.
 */
- (void)setValue:(double)value animated:(BOOL)animated;
/**
 Contains the receiver’s current value.
 @discussion If you try to set a value that is below the minimum or above the maximum value, the minimum or maximum value is set instead. The default value of this property is 0.0.
 */
@property(nonatomic,assign) double value;
/**
 Contains a Boolean value indicating whether changes in the rating bar value generate continuous update events.
 @discussion If YES, the rating bar sends update events continuously to the associated target’s action method. If NO, the rating bar only sends an action event when the user releases the slider’s thumb control to set the final value.
 
 The default value of this property is YES.
 */
@property(nonatomic, getter=isContinuous) BOOL continuous;
/**
 Contains the minimum value of the receiver.
 @discussion The amount of rate images drawn by the rating bar is determined by the the value range (maximumValue - minimumValue) rounded to a integer value.
 Default value for minimumValue is 0.0.
 */
@property(nonatomic) double minimumValue;
/**
 Contains the minimum value of the receiver.
 @discussion The amount of rate images drawn by the rating bar is determined by the the value range (maximumValue - minimumValue) rounded to a integer value.
 Default value for maximumValue is 5.0.
 */
@property(nonatomic) double maximumValue;
/**
 The step, or increment, value for the rating bar.
 @discussion If stepValue is greater than 0, the value of the rating bar is rounded to a multiple of the value specified in stepValue.
 @discussion Must be numerically greater than 0. If you attempt to set this property’s value to 0 or to a negative number, the system raises an NSInvalidArgumentException exception.
 The default value for this property is 1.
 */
@property(nonatomic,assign) double stepValue;
/**
 The inset or outset margins for the rectangle surrounding all of the page control's content.
 */
@property(nonatomic,assign) UIEdgeInsets contentEdgeInsets;
/**
 The inset or outset margins for the rectangle around the rate image's.
 */
@property(nonatomic,assign) UIEdgeInsets imageEdgeInsets;

@end
