//
//  MBRatingBar.m
//  MBFramework
//
//  Created by Arno Woestenburg on 10/11/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBRatingBar.h"
#import "UIImage+MBFramework.h"
#import "MBMath.h"

@interface MBRatingBar ()
{
    NSDictionary *rateImages_;
    UIView *contentView_;
    UIImageView *highlightedRatingView_;
    UIImageView *trackRatingView_;
}
@end

@implementation MBRatingBar

@synthesize contentEdgeInsets = contentEdgeInsets_;
@synthesize continuous = continuous_;
@synthesize imageEdgeInsets = imageEdgeInsets_;
@synthesize maximumValue = maximumValue_;
@synthesize minimumValue = minimumValue_;
@synthesize stepValue = stepValue_;
@synthesize value = value_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setBackgroundColor:[UIColor clearColor]];
        contentEdgeInsets_ = UIEdgeInsetsZero;
        imageEdgeInsets_ = UIEdgeInsetsZero;
        minimumValue_ = 0.0f;
        maximumValue_ = 5.0f;
        stepValue_ = 1.0f;
        continuous_ = YES;

        // Content view
        contentView_ = [[[UIView alloc] initWithFrame:UIEdgeInsetsInsetRect(self.bounds, contentEdgeInsets_)] autorelease];
        [contentView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [contentView_ setUserInteractionEnabled:NO];
        [self addSubview:contentView_];
        
        // Image layers
        trackRatingView_ = [[[UIImageView alloc] initWithFrame:contentView_.bounds] autorelease];
        [trackRatingView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [trackRatingView_.layer setMask:[CALayer layer]];
        [trackRatingView_.layer.mask setBackgroundColor:[UIColor whiteColor].CGColor];
        [contentView_ addSubview:trackRatingView_];

        highlightedRatingView_ = [[[UIImageView alloc] initWithFrame:contentView_.bounds] autorelease];
        [highlightedRatingView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        CALayer *maskLayer = [CALayer layer];
        [highlightedRatingView_.layer setMask:maskLayer];
        [maskLayer setBackgroundColor:[UIColor blackColor].CGColor];
        [contentView_ addSubview:highlightedRatingView_];
        
        // Default style
        [self setRateImage:[self MB_defaultRateImage] forState:UIControlStateHighlighted];
        [self MB_updateValue:0 animated:NO];
        
        // User interaction is turned off by default, to enable user interaction set this property to YES from outside the class.
        [self setUserInteractionEnabled:NO];
    }
    return self;
}

- (void)dealloc
{
    [rateImages_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (UIImage *)MB_defaultRateImage
{
    CGRect rect = CGRectMake(0.0, 0.0, 35.0f, 35.0f);
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGFloat radius = rect.size.width * 0.5;
    CGFloat theta = 2 * M_PI * (2.0 / 5.0);
    CGFloat flip = -1.0f; // Flip vertically (default star representation)
    CGFloat center = radius;
    
    CGColorRef fillColor = [UIColor colorWithRed:1 green:0.843 blue:0 alpha:1].CGColor;
    
    CGContextSetFillColorWithColor(context, fillColor);
    CGContextSetStrokeColorWithColor(context, [UIColor lightGrayColor].CGColor);
    
    CGContextMoveToPoint(context, center, radius * flip + radius);
    
    for (NSUInteger k=1; k<5; k++) {
        CGFloat x = radius * sin(k * theta);
        CGFloat y = radius * cos(k * theta);
        CGContextAddLineToPoint(context, x + center, y * flip + radius);
    }
    
    CGContextClosePath(context);
    CGContextFillPath(context);
    CGContextStrokePath(context);

    UIImage *rateImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return rateImage;
}

- (void)MB_updateImageLayers
{
    // Highlighted
    UIImage *rateImageHighlighted = [self rateImageForState:UIControlStateHighlighted];
    [highlightedRatingView_ setImage:[self MB_createImageLayerWithImage:rateImageHighlighted]];
    
    // Normal
    UIImage *rateImageNormal = [self rateImageForState:UIControlStateNormal];
    if (!rateImageNormal) {
        rateImageNormal = [rateImageHighlighted imageWithAlpha:0.5f];
    }
    [trackRatingView_ setImage:[self MB_createImageLayerWithImage:rateImageNormal]];
}

- (void)MB_updateMaskLayers
{
    CGRect maskRect = contentView_.bounds;
    CGFloat percentage = (value_ / (maximumValue_ - minimumValue_));
    maskRect.size.width = roundf(percentage * contentView_.bounds.size.width);
    
    [highlightedRatingView_.layer.mask setFrame:maskRect];
    
    maskRect = UIEdgeInsetsInsetRect(contentView_.bounds, UIEdgeInsetsMake(0.0f, maskRect.size.width, 0.0f, 0.0f));
    
    [trackRatingView_.layer.mask setFrame:maskRect];
}

- (UIImage *)MB_createImageLayerWithImage:(UIImage *)image
{
    NSInteger numberOfImages = [self MB_numberOfRateImages];
    CGRect itemRect = CGRectMake(0.0f, 0.0f, image.size.width, image.size.height);
    itemRect.size.height += (imageEdgeInsets_.top + imageEdgeInsets_.bottom);
    itemRect.size.width += (imageEdgeInsets_.left + imageEdgeInsets_.right);

    CGRect imageRect = CGRectMake(0.0, 0.0, (itemRect.size.width * numberOfImages), itemRect.size.height);
    
    UIGraphicsBeginImageContextWithOptions(imageRect.size, NO, 0.0f);

    for (NSUInteger index = 0; index < numberOfImages; index++) {
        CGRect drawRect = CGRectOffset(itemRect, imageEdgeInsets_.left, imageEdgeInsets_.top);
        [image drawAtPoint:drawRect.origin];
        itemRect.origin.x += itemRect.size.width;
    }
    
    UIImage *rateImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return rateImage;
}

- (void)MB_updateValue:(double)value animated:(BOOL)animated
{
    value_ = (stepValue_ > 0.0f) ? (roundf(value / stepValue_) * stepValue_) : value;
    [CATransaction begin];
    [CATransaction setValue:(animated ? (id)kCFBooleanFalse : (id)kCFBooleanTrue) forKey: kCATransactionDisableActions];
    [self MB_updateMaskLayers];
    [CATransaction commit];
}

- (void)MB_updateValuesWithTouch:(UITouch *)touch sendUpdate:(BOOL)sendUpdate
{
    CGPoint location = [touch locationInView:contentView_];
    CGFloat coordinate = location.x;
    double newValue = [self MB_factorFromCoordinate:coordinate];
    
    newValue = MIN(maximumValue_, MAX(minimumValue_, newValue));
    
    // Update value
    if (newValue != value_ || (sendUpdate && !continuous_)) {
        [self MB_updateValue:newValue animated:NO];
        if (sendUpdate) {
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
    }
}

- (double)MB_factorFromCoordinate:(CGFloat)coordinate
{
    CGRect bounds = contentView_.bounds;
    CGFloat percentage = coordinate / bounds.size.width;
    double range = maximumValue_ - minimumValue_;
    double factor = minimumValue_ + (range * percentage);
    
    return factor;
}

- (CGFloat)MB_coordinateFromFactor:(CGFloat)factor
{
    CGRect bounds = contentView_.bounds;
    CGFloat range = maximumValue_ - minimumValue_;
    CGFloat percentage = (factor - minimumValue_) / range;
    
    CGFloat coordinate = (percentage * bounds.size.width);
    
    return coordinate;
}

- (NSInteger)MB_numberOfRateImages
{
    return ceil(maximumValue_ - minimumValue_);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    [contentView_ setFrame:UIEdgeInsetsInsetRect(self.bounds, contentEdgeInsets_)];
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    [self MB_updateMaskLayers];
    [CATransaction commit];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    UIImage *rateImage = [self rateImageForState:UIControlStateHighlighted];
    if (rateImage) {
        CGRect rateImageRect = CGRectZero;
        rateImageRect.size = rateImage.size;
        rateImageRect.size.height += (imageEdgeInsets_.top + imageEdgeInsets_.bottom);
        rateImageRect.size.width += (imageEdgeInsets_.left + imageEdgeInsets_.right);
        size.width = rateImageRect.size.width * [self MB_numberOfRateImages];
        size.width += contentEdgeInsets_.left + contentEdgeInsets_.right;
        size.height = rateImageRect.size.height;
        size.height += contentEdgeInsets_.top + contentEdgeInsets_.bottom;
    }
    return size;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    // Track the touch
    if ([self beginTrackingWithTouch:touch withEvent:event]) {
        [super touchesBegan:touches withEvent:event];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    // Track the touch
    if ([self continueTrackingWithTouch:touch withEvent:event]) {
        [super touchesMoved:touches withEvent:event];
        [self MB_updateValuesWithTouch:touch sendUpdate:continuous_];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self MB_updateValuesWithTouch:touch sendUpdate:YES];
    [super touchesEnded:touches withEvent:event];
    [self endTrackingWithTouch:touch withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [super touchesCancelled:touches withEvent:event];
    [self cancelTrackingWithEvent:event];
    [self MB_updateValuesWithTouch:touch sendUpdate:YES];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setRateImage:(UIImage *)rateImage forState:(UIControlState)state
{
    NSMutableDictionary *rateImages = [[rateImages_ mutableCopy] autorelease];
    if (!rateImages) {
        rateImages = [NSMutableDictionary dictionary];
    }
    if (rateImage) {
        [rateImages setObject:rateImage forKey:@(state)];
    } else {
        [rateImages removeObjectForKey:@(state)];
    }
    [rateImages_ release];
    rateImages_ = [[NSDictionary alloc] initWithDictionary:rateImages];
    [self MB_updateImageLayers];
}

- (UIImage *)rateImageForState:(UIControlState)state
{
    return [rateImages_ objectForKey:@(state)];
}

- (void)setValue:(double)value animated:(BOOL)animated
{
    if (value != value_) {
        [self MB_updateValue:value animated:animated];
    }
}


//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setValue:(double)value
{
    [self setValue:value animated:YES];
}

- (void)setMinimumValue:(double)minimumValue
{
    minimumValue_ = minimumValue;
    if (self.value < minimumValue_) {
        [self setValue:minimumValue_ animated:NO];
    }
}

- (void)setMaximumValue:(double)maximumValue
{
    maximumValue_ = maximumValue;
    if (self.value > maximumValue_) {
        [self setValue:maximumValue_ animated:NO];
    }
}

- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets
{
    if (UIEdgeInsetsEqualToEdgeInsets(contentEdgeInsets_, contentEdgeInsets) == NO) {
        contentEdgeInsets_ = contentEdgeInsets;
        [self setNeedsLayout];
    }
}

- (void)setImageEdgeInsets:(UIEdgeInsets)imageEdgeInsets
{
    if (UIEdgeInsetsEqualToEdgeInsets(imageEdgeInsets_, imageEdgeInsets) == NO) {
        imageEdgeInsets_ = imageEdgeInsets;
        [self MB_updateImageLayers];
        [self setNeedsLayout];
    }
}

@end
