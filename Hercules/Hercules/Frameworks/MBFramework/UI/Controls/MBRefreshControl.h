//
//  MBRefreshControl.h
//  MBFramework
//
//  Created by Arno Woestenburg on 4/15/13.
//
//

#import <UIKit/UIKit.h>

/**
 A MBRefreshControl object provides a control that can be used to initiate the refreshing of a table view’s or collection view's contents.
 */
@interface MBRefreshControl : UIControl
{
    UIColor *tintColor_;
    UILabel *titleLabel_;
    UILabel *detailTextLabel_;
    UIActivityIndicatorView *activityIndicatorView_;
    BOOL refreshing_;
    UIEdgeInsets contentInset_;
    CGFloat spacing_;
}

/**
 Tells the control that a refresh operation was started programmatically.
 */
- (void)beginRefreshing;
/**
 Tells the control that a refresh operation has ended.
 */
- (void)endRefreshing;
/**
 Sets the title for the refresh control.
 @param title The title.
 */
- (void)setTitle:(NSString *)title;
/**
 Sets the secondary title text for the refresh control.
 @param detailText The secondary title text.
 */
- (void)setDetailText:(NSString *)detailText;

/**
 The tint color for the refresh control.
 */
@property(nonatomic,retain) UIColor *tintColor;
/**
 Returns the label used for the main textual content of the refresh control. (read-only)
 */
@property(nonatomic,readonly) UILabel *titleLabel;
/**
 Returns the secondary label of the refresh control if one exists. (read-only)
 */
@property(nonatomic,readonly) UILabel *detailTextLabel;
/**
 Returns the activity indicator used to indicate the refresh operation. (read-only)
 */
@property(nonatomic,readonly) UIActivityIndicatorView *activityIndicatorView;
/**
 A Boolean value indicating whether a refresh operation has been triggered and is in progress. (read-only).
 */
@property(nonatomic,readonly, getter=isRefreshing) BOOL refreshing;

@end
