//
//  MBRefreshControl.m
//  MBFramework
//
//  Created by Arno Woestenburg on 4/15/13.
//
//

#import "MBRefreshControl.h"

@implementation MBRefreshControl

@synthesize tintColor = tintColor_;
@synthesize titleLabel = titleLabel_;
@synthesize detailTextLabel = detailTextLabel_;
@synthesize activityIndicatorView = activityIndicatorView_;
@synthesize refreshing = refreshing_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init {
    return [self initWithFrame:CGRectMake(0, 0, 200, 50)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        refreshing_ = NO;
        tintColor_ = [UIColor grayColor];
        titleLabel_ = [[[UILabel alloc] init] autorelease];
        [titleLabel_ setBackgroundColor:[UIColor clearColor]];
        [self addSubview:titleLabel_];
        activityIndicatorView_ = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] autorelease];
        [self addSubview:activityIndicatorView_];
        
        contentInset_ = UIEdgeInsetsMake(10, 0, 10, 0);
        spacing_ = 10;
    }
    return self;
}

- (void)dealloc {
    self.tintColor = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (CGRect)MB_contentRectForBounds:(CGRect)bounds {
    CGRect result = CGRectZero;
    CGSize titleLabelSize = [titleLabel_ sizeThatFits:CGSizeZero];
    CGSize detailTextLabelSize = [detailTextLabel_ sizeThatFits:CGSizeZero];
    
    CGRect textRect = CGRectZero;
    
    textRect.size.width = MAX(titleLabelSize.width, detailTextLabelSize.width);
    if (detailTextLabelSize.height > 0) {
        textRect.size.height = (titleLabelSize.height + spacing_ + detailTextLabelSize.height);
    } else {
        textRect.size.height = titleLabelSize.height;
    }
    
    result.size.width = activityIndicatorView_.bounds.size.width + spacing_ + textRect.size.width;
    result.size.height = MAX(activityIndicatorView_.bounds.size.height, textRect.size.height);

    textRect.origin.x = round((bounds.size.width - textRect.size.width) * 0.5);
    
    result.origin.x = textRect.origin.x - (activityIndicatorView_.bounds.size.width + spacing_);
    result.origin.y = (bounds.size.height - result.size.height - contentInset_.bottom);
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)beginRefreshing {
    refreshing_ = YES;
    [activityIndicatorView_ startAnimating];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)endRefreshing {
    refreshing_ = NO;
    [activityIndicatorView_ stopAnimating];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)setTitle:(NSString *)title {
    if (![title isEqualToString:self.titleLabel.text]) {
        [self.titleLabel setText:title];
        [self setNeedsLayout];
    }
}

- (void)setDetailText:(NSString *)detailText {
    if (![detailText isEqualToString:self.detailTextLabel.text]) {
        [self.detailTextLabel setText:detailText];
        [self setNeedsLayout];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setTintColor:(UIColor *)tintColor {
    if (tintColor_ != tintColor) {
        [tintColor_ release];
        tintColor_ = [tintColor retain];
        [activityIndicatorView_ setColor:tintColor_];
        [titleLabel_ setTextColor:tintColor_];
        [detailTextLabel_ setTextColor:tintColor_];
    }
}

- (UILabel *)detailTextLabel {
    // Create on demand
    if (!detailTextLabel_) {
        detailTextLabel_ = [[[UILabel alloc] init] autorelease];
        [detailTextLabel_ setBackgroundColor:[UIColor clearColor]];
        [detailTextLabel_ setTextColor:tintColor_];
        [self addSubview:detailTextLabel_];
        [self setNeedsLayout];
    }
    return detailTextLabel_;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews {
    
    CGSize titleLabelSize = [titleLabel_ sizeThatFits:CGSizeZero];
    CGSize detailTextLabelSize = [detailTextLabel_ sizeThatFits:CGSizeZero];
    
    CGRect contentRect = [self MB_contentRectForBounds:self.bounds];

    CGRect controlRect = CGRectZero;

    controlRect.origin.x = contentRect.origin.x;
    controlRect.origin.y = roundf(contentRect.origin.y + (contentRect.size.height - activityIndicatorView_.bounds.size.height) * 0.5);
    controlRect.size = activityIndicatorView_.bounds.size;

    [activityIndicatorView_ setFrame:controlRect];
    
    controlRect.origin.x += controlRect.size.width + spacing_;
    controlRect.size.width = contentRect.size.width - controlRect.origin.x + contentRect.origin.x;
    controlRect.origin.y = contentRect.origin.y;
    controlRect.size.height = titleLabelSize.height;

    [titleLabel_ setFrame:controlRect];

    if (detailTextLabel_) {
        controlRect.origin.y = (contentRect.origin.y + contentRect.size.height - detailTextLabelSize.height);
        controlRect.size.height = detailTextLabelSize.height;
        [detailTextLabel_ setFrame:controlRect];
    }
    
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGRect result = [self MB_contentRectForBounds:self.bounds];
    result.size.height += contentInset_.top + contentInset_.bottom;
    result.size.width += contentInset_.left + contentInset_.right;
    return result.size;
}

@end
