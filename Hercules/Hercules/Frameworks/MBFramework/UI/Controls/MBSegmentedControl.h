//
//  MBSegmentedControl.h
//  MBFramework
//
//  Created by Arno Woestenburg on 7/15/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 A MBSegmentedControl object is a horizontal control made of multiple segments, each segment functioning as a discrete button. The MBSegmentedControl is based upon, and is intended to behave like the UIKit's UISegmentedControl class but offers more flexibility to customize the appearance of the control.
 */
@interface MBSegmentedControl : UIControl

/**
 Initializes and returns a segmented control with segments having the given titles.
 @param items An array of NSString objects for segment titles.
 @return A MBSegmentedControl object or nil if there was a problem in initializing the object.
 */
- (instancetype)initWithItems:(NSArray *)items;
/**
 Returns the label used for the segment title.
 @param segment An index number identifying a segment in the control. It must be a number between 0 and the number of segments (numberOfSegments) minus 1; values exceeding this upper range are pinned to it.
 @return The segment label or nil if it can't be found.
 */
- (UILabel *)labelForSegmentAtIndex:(NSUInteger)segment;
/**
 Returns the background image for a given state and bar metrics.
 @param state      A control state.
 @param barMetrics Bar metrics.
 @return The background image for state and barMetrics.
 */
- (UIImage *)backgroundImageForState:(UIControlState)state barMetrics:(UIBarMetrics)barMetrics;
/**
 Sets the background image for a given state and bar metrics.
 @param backgroundImage The background image to use for state and barMetrics.
 @param state           A control state.
 @param barMetrics      Bar metrics.
 */
- (void)setBackgroundImage:(UIImage *)backgroundImage forState:(UIControlState)state barMetrics:(UIBarMetrics)barMetrics;

/**
 Returns the divider image used for a given combination of left and right segment states and bar metrics.
 @param leftState  The state of the left segment.
 @param rightState The state of the right segment.
 @param barMetrics Bar metrics.
 @return The divider image used for the given combination of left and right segment states and bar metrics.
 */
- (UIImage *)dividerImageForLeftSegmentState:(UIControlState)leftState rightSegmentState:(UIControlState)rightState barMetrics:(UIBarMetrics)barMetrics;
/**
 Sets the divider image used for a given combination of left and right segment states and bar metrics.
 @param dividerImage The divider image to use.
 @param leftState  The state of the left segment.
 @param rightState The state of the right segment.
 @param barMetrics Bar metrics.
 */
- (void)setDividerImage:(UIImage *)dividerImage forLeftSegmentState:(UIControlState)leftState rightSegmentState:(UIControlState)rightState barMetrics:(UIBarMetrics)barMetrics;
/**
 Returns the text attributes of the title for a given control state.
 @param state A control state.
 @return The text attributes of the title for state.
 */
- (NSDictionary *)titleTextAttributesForState:(UIControlState)state;

/**
 Sets the text attributes of the title for a given control state.
 @param attributes The text attributes of the title for state.
 @param state      A control state.
 */
- (void)setTitleTextAttributes:(NSDictionary *)attributes forState:(UIControlState)state;
/**
 Sets the title of a segment.
 @param title   A string to display in the segment as its title.
 @param segment An index number identifying a segment in the control. It must be a number between 0 and the number of segments (numberOfSegments) minus 1; values exceeding this upper range are pinned to it.
 */
- (void)setTitle:(NSString *)title forSegmentAtIndex:(NSUInteger)segment;
/**
 Returns the title of the specified segment.
 @param segment An index number identifying a segment in the control. It must be a number between 0 and the number of segments (numberOfSegments) minus 1; values exceeding this upper range are pinned to it.
 @return Returns the string (title) assigned to the receiver as content. If no title has been set, it returns nil.
 */
- (NSString *)titleForSegmentAtIndex:(NSUInteger)segment;
/**
 Returns the number of segments the receiver has. (read-only)
 */
@property(nonatomic,readonly) NSUInteger numberOfSegments;
/**
 The index number identifying the selected segment (that is, the last segment touched).
 */
@property(nonatomic) NSInteger selectedSegmentIndex;
/**
 The inset or outset margins for the rectangle around the segment's item title text.
 */
@property(nonatomic,assign) UIEdgeInsets titleEdgeInsets;

@end
