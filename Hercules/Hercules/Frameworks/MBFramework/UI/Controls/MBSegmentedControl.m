//
//  MBSegmentedControl.m
//  MBFramework
//
//  Created by Arno Woestenburg on 7/15/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBSegmentedControl.h"

@interface MBSegmentedControl ()
{
    NSMutableDictionary *backgroundImages_;
    NSMutableDictionary *dividerImages_;
    NSArray *segments_;
    NSMutableDictionary *titleTextAttributes_;
}
@end

@implementation MBSegmentedControl

@synthesize selectedSegmentIndex = selectedSegmentIndex_;
@synthesize titleEdgeInsets = titleEdgeInsets_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithItems:(NSArray *)items
{
    // The returned segmented control is automatically sized to fit its content within the width of its superview.
    self = [super initWithFrame:CGRectMake(0, 0, 200, 46)];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        NSMutableArray *labels = [NSMutableArray array];
        for (NSString *title in items) {
            UILabel *segmentLabel = [[[UILabel alloc] init] autorelease];
            [segmentLabel setText:title];
            [labels addObject:segmentLabel];
            [segmentLabel setTextAlignment:NSTextAlignmentCenter];
            [segmentLabel setBackgroundColor:[UIColor clearColor]];
            [self addSubview:segmentLabel];
        }
        segments_ = [[NSArray arrayWithArray:labels] retain];
        selectedSegmentIndex_ = NSIntegerMin;
        titleEdgeInsets_ = UIEdgeInsetsZero;
        [self MB_initialize];
    }
    return self;
}

- (void)dealloc
{
    [segments_ release];
    [backgroundImages_ release];
    [dividerImages_ release];
    [titleTextAttributes_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect
{
    NSInteger index = 0;
    NSInteger count = segments_.count;
    
    CGRect itemRect = CGRectMake(0, 0, self.bounds.size.width / count, self.bounds.size.height);
    CGRect drawRect = itemRect;

    UIImage *backgroundImage = nil;
    
    UIImage *backgroundImageNormal = [self backgroundImageForState:UIControlStateNormal barMetrics:UIBarMetricsDefault];;
    UIImage *backgroundImageSelected = [self backgroundImageForState:UIControlStateSelected barMetrics:UIBarMetricsDefault];

    UIImage *middleNoneSelected = [self dividerImageForLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    UIImage *middleLeftSelected = [self dividerImageForLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    UIImage *middleRightSelected = [self dividerImageForLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    for (index = 0; index < count; index++) {
        
        backgroundImage = selectedSegmentIndex_ == index ? backgroundImageSelected : backgroundImageNormal;
        
        CGSize size = backgroundImage.size;
        CGPoint drawPoint = CGPointZero;
        
        UIImage *dividerImage = middleNoneSelected;

        if (selectedSegmentIndex_ != NSIntegerMin) {
            if (selectedSegmentIndex_ == index) {
                dividerImage = middleLeftSelected;
            } else if (selectedSegmentIndex_ == (index - 1)) {
                dividerImage = middleNoneSelected;
            } else if (selectedSegmentIndex_ == (index + 1)) {
                dividerImage = middleRightSelected;
            } 
        }
        
        if (index == 0) {
            size.width -= backgroundImage.capInsets.right;
            drawRect.size.width = itemRect.size.width - roundf(dividerImage.size.width * 0.5);
            drawRect.origin = CGPointMake(0, itemRect.origin.y);
        } else if (index == (count - 1) ) {
            size.width -= backgroundImage.capInsets.left;
            drawPoint.x = -backgroundImage.capInsets.left;
            
            drawRect.origin = CGPointMake(itemRect.origin.x + floorf(dividerImage.size.width * 0.5), itemRect.origin.y);
            drawRect.size.width = itemRect.size.width - floorf(dividerImage.size.width * 0.5);

            dividerImage = nil; // No divider image
        } else {
            size.width -= (backgroundImage.capInsets.left + backgroundImage.capInsets.right);
            drawPoint.x = -backgroundImage.capInsets.left;
            drawRect.origin = CGPointMake(itemRect.origin.x + floorf(dividerImage.size.width * 0.5), itemRect.origin.y);
            drawRect.size.width = itemRect.size.width - roundf(dividerImage.size.width * 0.5) - floorf(dividerImage.size.width * 0.5);
        }
        UIGraphicsBeginImageContextWithOptions(size, NO, 0);
        [backgroundImage drawAtPoint:drawPoint];
        UIImage* clippedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [[clippedImage resizableImageWithCapInsets:backgroundImage.capInsets] drawInRect:drawRect];
        
        if (dividerImage) {
            drawRect.size = CGSizeMake(dividerImage.size.width, itemRect.size.height);
            drawRect.origin.x = CGRectGetMaxX(itemRect) - roundf(dividerImage.size.width * 0.5);
            [dividerImage drawInRect:drawRect];
        }
        itemRect.origin.x += itemRect.size.width;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect segmentArea = CGRectMake(0, 0, self.bounds.size.width / segments_.count, self.bounds.size.height);
    CGRect labelArea = CGRectZero;
    CGRect labelRect = CGRectZero;
    
    for (UILabel *segmentLabel in segments_) {
        labelArea = UIEdgeInsetsInsetRect(segmentArea, titleEdgeInsets_);
        
        labelRect.size = [segmentLabel sizeThatFits:labelArea.size];
        
        labelRect.origin.x = roundf(CGRectGetMidX(labelArea) - (labelRect.size.width * 0.5));
        labelRect.origin.y = roundf(CGRectGetMidY(labelArea) - (labelRect.size.height * 0.5));
        [segmentLabel setFrame:labelRect];
        segmentArea.origin.x += segmentArea.size.width;
    }
}

- (CGSize)sizeThatFits:(CGSize)size
{
    UIImage *backgroundImage = [self backgroundImageForState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    if (backgroundImage) {
        size.height = backgroundImage.size.height;
    } else {
        size.height = 44;
    }
    
    CGSize labelSize = CGSizeZero;
    UIEdgeInsets titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 6);
    
    for (UILabel *label in segments_) {
        labelSize.width = MAX([label sizeThatFits:CGSizeZero].width, labelSize.width);
    }
    labelSize.width += (titleEdgeInsets.right + titleEdgeInsets.left);
    size.width = segments_.count * labelSize.width;
    
    return size;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_initialize
{
    backgroundImages_ = [[NSMutableDictionary alloc] init];
    dividerImages_ = [[NSMutableDictionary alloc] init];
}

- (NSInteger)MB_hitTest:(CGPoint)point
{
    NSInteger item = NSIntegerMin;
    NSInteger index = 0;
    NSInteger count = segments_.count;
    
    CGRect labelArea = CGRectMake(0, 0, self.bounds.size.width / segments_.count, self.bounds.size.height);
    
    while (index < count && item == NSIntegerMin) {
        if (CGRectContainsPoint(labelArea, point)) {
            item = index;
        }
        labelArea.origin.x += labelArea.size.width;
        index++;
    }
    
    return item;
}

- (void)MB_applyStateTextAttributes
{
    UILabel *label = nil;
    UIControlState labelState = UIControlStateNormal;
    
    for (NSUInteger index = 0; index < segments_.count; index++) {
    
        label = [segments_ objectAtIndex:index];
        labelState = (index == self.selectedSegmentIndex ? UIControlStateSelected : UIControlStateNormal);
        [self MB_updateTextAttributesForLabel:label withState:labelState];
    }
}

- (void)MB_updateTextAttributesForLabel:(UILabel *)label withState:(UIControlState)state
{
    NSDictionary *textAttributes = [self titleTextAttributesForState:state];
    
    UIFont *font = [textAttributes objectForKey:NSFontAttributeName];
    UIColor *foregroundColor = [textAttributes objectForKey:NSForegroundColorAttributeName];
    NSShadow *shadow = [textAttributes objectForKey:NSShadowAttributeName];
    UIColor *textShadowColor = nil;
    NSValue *textShadowOffset = nil;
    
    if (shadow) {
        textShadowColor = (UIColor *)shadow.shadowColor;
        textShadowOffset = [NSValue valueWithCGSize:shadow.shadowOffset];
    }
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    // Provided for compatibility with old attributes
    if (!font) {
        font = [textAttributes objectForKey:UITextAttributeFont];
    }
    if (!foregroundColor) {
        foregroundColor = [textAttributes objectForKey:UITextAttributeTextColor];
    }
    if (!textShadowColor) {
        textShadowColor = [textAttributes objectForKey:UITextAttributeTextShadowColor];
    }
    if (!textShadowOffset) {
        textShadowOffset = [textAttributes objectForKey:UITextAttributeTextShadowOffset];
    }
#endif //__IPHONE_OS_VERSION_MIN_REQUIRED
    
    if (font) {
        [label setFont:font];
    }
    
    if (foregroundColor) {
        [label setTextColor:foregroundColor];
    }
    
    if (textShadowColor) {
        [label setShadowColor:textShadowColor];
    }
    
    if (textShadowOffset) {
        [label setShadowOffset:[textShadowOffset CGSizeValue]];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (UILabel *)labelForSegmentAtIndex:(NSUInteger)segment
{
    return segments_[segment];
}

- (UIImage *)backgroundImageForState:(UIControlState)state barMetrics:(UIBarMetrics)barMetrics
{
    return [backgroundImages_ objectForKey:@(state)];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage forState:(UIControlState)state barMetrics:(UIBarMetrics)barMetrics
{
    [backgroundImages_ setObject:backgroundImage forKey:@(state)];
}

- (UIImage *)dividerImageForLeftSegmentState:(UIControlState)leftState rightSegmentState:(UIControlState)rightState barMetrics:(UIBarMetrics)barMetrics
{
    NSUInteger combinedState = leftState | (rightState << 16);
    return [dividerImages_ objectForKey:@(combinedState)];
}

- (void)setDividerImage:(UIImage *)dividerImage forLeftSegmentState:(UIControlState)leftState rightSegmentState:(UIControlState)rightState barMetrics:(UIBarMetrics)barMetrics
{
    NSUInteger combinedState = leftState | (rightState << 16);
    [dividerImages_ setObject:dividerImage forKey:@(combinedState)];
}

- (NSDictionary *)titleTextAttributesForState:(UIControlState)state
{
    NSDictionary *attributes = nil;
    if (titleTextAttributes_) {
        attributes = [titleTextAttributes_ objectForKey:@(state)];
        if (!attributes) {
            // Default to normal state attributes
            attributes = [titleTextAttributes_ objectForKey:@(UIControlStateNormal)];
        }
    }
    return [NSDictionary dictionaryWithDictionary:attributes];
}

- (void)setTitleTextAttributes:(NSDictionary *)attributes forState:(UIControlState)state
{
    if (!titleTextAttributes_) {
        titleTextAttributes_ = [[NSMutableDictionary alloc] init];
    }
    [titleTextAttributes_ setObject:[NSDictionary dictionaryWithDictionary:attributes] forKey:@(state)];
    [self MB_applyStateTextAttributes];
}

- (void)setTitle:(NSString *)title forSegmentAtIndex:(NSUInteger)segment
{
    UILabel *label = segments_[segment];
    [label setText:title];
}

- (NSString *)titleForSegmentAtIndex:(NSUInteger)segment
{
    UILabel *label = segments_[segment];
    return label.text;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponder

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    NSUInteger hitItem = [self MB_hitTest:[touch locationInView:self]];
    if (hitItem != selectedSegmentIndex_) {
        self.selectedSegmentIndex = hitItem;
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
    return [super beginTrackingWithTouch:touch withEvent:event];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSUInteger)numberOfSegments
{
    return segments_.count;
}

- (void)setSelectedSegmentIndex:(NSInteger)selectedSegmentIndex
{
    if (selectedSegmentIndex_ != selectedSegmentIndex) {
        selectedSegmentIndex_ = selectedSegmentIndex;
        [self MB_applyStateTextAttributes];
        [self setNeedsDisplay];
    }
}

- (void)setTitleEdgeInsets:(UIEdgeInsets)titleEdgeInsets
{
    if (!UIEdgeInsetsEqualToEdgeInsets(titleEdgeInsets, titleEdgeInsets_)) {
        titleEdgeInsets_ = titleEdgeInsets;
        [self setNeedsLayout];
    }
}

@end
