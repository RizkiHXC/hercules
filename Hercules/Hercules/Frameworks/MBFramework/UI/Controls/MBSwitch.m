//
//  MBSwitch.m
//  MBFramework
//
//  Created by Arno Woestenburg on 3/13/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//


#import "MBSwitch.h"
#import "MBRectFunctions.h"
#import <QuartzCore/QuartzCore.h>

@interface MBSwitch ()
{
    UIImageView *onImageView_;
    UIImageView *offImageView_;
    UIImageView *thumbImageView_;
    UIView *stateImageContainer_;
    CGFloat thumbStartPosition_;
    CGFloat thumbPosition_;
    CGFloat trackOffset_;
    BOOL mustFlip_;
}
@end

@implementation MBSwitch

@synthesize on = on_;
@synthesize onImage = onImage_;
@synthesize offImage = offImage_;
@synthesize thumbImage = thumbImage_;
@synthesize bezelImage = bezelImage_;
@synthesize onLabel = onLabel_;
@synthesize offLabel = offLabel_;
@synthesize cornerRadius = cornerRadius_;

static const CGSize kSwitchDefaultSizeiOS6 = {79, 27};
static const CGSize kSwitchDefaultSizeiOS7 = {51, 31};

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    // The size components of the frame rectangle are ignored (as with the default UISwitch control).
    frame.size = [self sizeThatFits:CGSizeZero];
	self = [super initWithFrame:frame];
    if (self) {
        cornerRadius_ = -1;
        [self MB_initialize];
	}
	return self;
}

- (void)dealloc
{
    self.onImage = nil;
    self.offImage = nil;
    self.thumbImage = nil;
    self.bezelImage = nil;
	[super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_generateDefaultImages
{
    CGSize viewSize = [self sizeThatFits:CGSizeZero];

    // Default thumb image
    CGSize imageSize = CGSizeMake(viewSize.height, viewSize.height);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    
    self.thumbImage = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 20, 13)];
    UIGraphicsEndImageContext();
   
    // Default on image
    imageSize = CGSizeMake(viewSize.width, viewSize.height);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
    context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:0.294 green:0.847 blue:0.388 alpha:1.0].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    self.onImage = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 20, 13)];
    UIGraphicsEndImageContext();
    
    // Default off image
    imageSize = CGSizeMake(viewSize.width, viewSize.height);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
    context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor colorWithWhite:0.95 alpha:1.0].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    self.offImage = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 20, 13)];
    UIGraphicsEndImageContext();
    
}

- (void)MB_initialize
{
	self.contentMode = UIViewContentModeRedraw;
    
    stateImageContainer_ = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width*2, self.bounds.size.height)] autorelease];
    [self addSubview:stateImageContainer_];
    
    onImageView_ = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
    [stateImageContainer_ addSubview:onImageView_];
    
    offImageView_ = [[[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width, 0, self.bounds.size.width, self.bounds.size.height)] autorelease];
    [stateImageContainer_ addSubview:offImageView_];
    [stateImageContainer_ setUserInteractionEnabled:NO];
    
    thumbImageView_ = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.height, self.bounds.size.height)] autorelease];

    [self addSubview:thumbImageView_];
    
    onLabel_ = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    [onImageView_ addSubview:onLabel_];
    
    offLabel_ = [[[UILabel alloc] init] autorelease];
    [offImageView_ addSubview:offLabel_];
    
    [self MB_generateDefaultImages];
    
    [self setClipsToBounds:YES];
    [self.layer setMasksToBounds:YES];
    thumbPosition_ = 0.0;
    [self MB_setThumbPosition:thumbPosition_ withAnimation:NO duration:0];
}

- (void)MB_performSwitchToPosition:(CGFloat)position animated:(BOOL)animated duration:(NSTimeInterval)animationDuration
{
	thumbPosition_ = position;
    [self MB_setThumbPosition:thumbPosition_ withAnimation:animated duration:animationDuration];
	[self sendActionsForControlEvents:UIControlEventValueChanged];
	[self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void)MB_setThumbPosition:(CGFloat)position withAnimation:(BOOL)animated duration:(NSTimeInterval)animationDuration {
    CGFloat thumbWidth = thumbImageView_.bounds.size.width;
    CGFloat center = roundf(thumbWidth * 0.5 + (thumbPosition_ * (self.bounds.size.width - thumbWidth)));
    [UIView animateWithDuration:animated ? animationDuration : 0.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [stateImageContainer_ setFrame:CGRectMake(roundf(center - (stateImageContainer_.bounds.size.width *0.5)), 0, stateImageContainer_.bounds.size.width, stateImageContainer_.bounds.size.height)];
                         
                         [thumbImageView_ setFrame:CGRectMake(roundf(center - (thumbImageView_.bounds.size.width *0.5)), 0, thumbImageView_.bounds.size.width, thumbImageView_.bounds.size.height)];
                     } completion:^(BOOL finished) { }];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat thumbWidth = thumbImageView_.bounds.size.width;
    CGFloat center = roundf(thumbWidth * 0.5 + (thumbPosition_ * (self.bounds.size.width - thumbWidth)));
    
    CGRect controlRect = CGRectMake(0, 0, self.bounds.size.width*2, self.bounds.size.height);
    controlRect.origin.x = roundf(center - (controlRect.size.width *0.5));
    [stateImageContainer_ setFrame:controlRect];
    
    [thumbImageView_ setFrame:CGRectMake(roundf(center - (thumbImageView_.bounds.size.width *0.5)), 0, thumbImageView_.bounds.size.width, thumbImageView_.bounds.size.height)];
    
    CGRect bounds = stateImageContainer_.bounds;
    [onImageView_ setFrame:CGRectMake(0, 0, roundf(bounds.size.width * 0.5), bounds.size.height)];
    [offImageView_ setFrame:CGRectMake(roundf(bounds.size.width * 0.5), 0, roundf(bounds.size.width * 0.5), bounds.size.height)];

    CGRect alignToRect = onImageView_.bounds;
    alignToRect.size.width -= thumbImageView_.frame.size.width;
    alignToRect.origin.x = thumbImageView_.frame.size.width / 2;
    
    CGRect onLabelRect = [onLabel_ textRectForBounds:alignToRect limitedToNumberOfLines:0];
    [onLabel_ setFrame:[MBRectFunctions alignRect:onLabelRect toRect:alignToRect alignMode:MBAlignmentCenter]];
    
    CGRect offLabelRect = [offLabel_ textRectForBounds:alignToRect limitedToNumberOfLines:0];
    [offLabel_ setFrame:[MBRectFunctions alignRect:offLabelRect toRect:alignToRect alignMode:MBAlignmentCenter]];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    // iOS version
    return (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) ? kSwitchDefaultSizeiOS7 : kSwitchDefaultSizeiOS6);
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    if (cornerRadius_ < 0) {
        [self.layer setCornerRadius:self.bounds.size.height*0.5];
    }
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if (cornerRadius_ < 0) {
        [self.layer setCornerRadius:self.bounds.size.height*0.5];
    }
}


//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint point = [touch locationInView:self];
    trackOffset_ = point.x - (thumbImageView_.frame.origin.x + (thumbImageView_.bounds.size.width * 0.5));
	[self setHighlighted:YES];
	thumbStartPosition_ = thumbPosition_;
	mustFlip_ = YES;
	[self setNeedsDisplay];
	[self sendActionsForControlEvents:UIControlEventTouchDown];
	return YES;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint point = [touch locationInView:self];
    point.x -= trackOffset_;
	thumbPosition_ = (point.x - thumbImageView_.bounds.size.width * 0.5) / (self.bounds.size.width - thumbImageView_.bounds.size.width);
    
    thumbPosition_ = MAX(thumbPosition_, 0.0);
    thumbPosition_ = MIN(thumbPosition_, 1.0);
    
    [self MB_setThumbPosition:thumbPosition_ withAnimation:NO duration:0];
	[self sendActionsForControlEvents:UIControlEventTouchDragInside];
	return YES;
}

- (void)finishEvent
{
	[self setHighlighted:NO];
	CGFloat toPosition = 0.0;
    CGFloat animationDuration = 0.0;
    
    if (thumbStartPosition_ == thumbPosition_) {
        toPosition = roundf(1.0 - thumbStartPosition_);
        animationDuration = 0.2;
    } else {
        animationDuration = 0.05;
        toPosition = thumbPosition_ >= 0.5 ? 1.0 : 0.0;
    }
	[self MB_performSwitchToPosition:toPosition animated:YES duration:animationDuration];
}

- (void)cancelTrackingWithEvent:(UIEvent *)event
{
	[self finishEvent];
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	[self finishEvent];
}

- (void)setOn:(BOOL)on animated:(BOOL)animated
{
    if (on != self.on) {
        CGFloat position = on ? 1.0 : 0.0;
        [self MB_performSwitchToPosition:position animated:animated duration:0.2];
    }
}

- (void)setOnImage:(UIImage *)onImage
{
    [onImageView_ setImage:onImage];
}

- (void)setOffImage:(UIImage *)offImage
{
    [offImageView_ setImage:offImage];
}

- (void)setThumbImage:(UIImage *)thumbImage
{
    [thumbImageView_ setImage:thumbImage];
    [thumbImageView_ sizeToFit];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setCornerRadius:(CGFloat)cornerRadius
{
    cornerRadius_ = cornerRadius;
    if (cornerRadius_ >= 0) {
        [self.layer setCornerRadius:cornerRadius_];
    } else {
        [self.layer setCornerRadius:self.bounds.size.height*0.5];
    }
}

- (BOOL)isOn
{
	return (thumbPosition_ >= 0.5);
}

- (void)setOn:(BOOL)on
{
	[self setOn:on animated:NO];
}

@end
