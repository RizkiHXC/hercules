//
//  MBTextField.h
//  MBFramework
//
//  Created by Marco Jonker on 10/28/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MBControlStateError     (1 << 16)
#define MBControlStateEditing   (1 << 17)

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBTextField : UITextField
{
    UIControlState controlState_;
    NSMutableDictionary *backgroundImages_;
    NSMutableDictionary *images_;
    NSMutableDictionary *textColors_;
    UIImageView *backgroundView_;
    UIImageView *imageView_;
    UIEdgeInsets contentEdgeInsets_;
    UIEdgeInsets textEdgeInsets_;
    UIEdgeInsets leftViewEdgeInsets_;
    UIEdgeInsets rightViewEdgeInsets_;
    UIView *accessoryView_;
    BOOL editingEnabled_;
}

- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state;
- (UIImage *)backgroundImageForState:(UIControlState)state;
- (void)setImage:(UIImage *)image forState:(UIControlState)state;
- (UIImage *)imageForState:(UIControlState)state;
- (void)setTextColor:(UIColor *)textColor forState:(UIControlState)state;
- (UIColor *)textColorForState:(UIControlState)state;

@property(nonatomic) BOOL containsError;
@property(nonatomic,getter=isEditingEnabled) BOOL editingEnabled;

@end
