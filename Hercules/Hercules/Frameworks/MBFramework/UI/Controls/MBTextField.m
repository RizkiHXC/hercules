//
//  MBTextField.m
//  MBFramework
//
//  Created by Arno Woestenburg on 10/28/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBTextField.h"

@implementation MBTextField

@synthesize editingEnabled = editingEnabled_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        editingEnabled_ = YES;
        
        controlState_ = UIControlStateNormal;
        
        backgroundView_ = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
        [backgroundView_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
        [self addSubview: backgroundView_];
        
        backgroundImages_ = [[NSMutableDictionary alloc] init];
        images_ = [[NSMutableDictionary alloc] init];
        textColors_ = [[NSMutableDictionary alloc] init];
        
        [self MB_subscribeForTextFieldNotifications];
    }
    return self;
}

- (void)dealloc {
    [self MB_unsubscribeForNotifications];
    [backgroundImages_ release];
    [images_ release];
    [textColors_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_updateStyleForState {
    
    UIControlState filteredState = UIControlStateNormal;

    // Filter state in case states are combined
    if ((controlState_ & MBControlStateError) == MBControlStateError) {
        filteredState = MBControlStateError;
    } else if (self.isEditing) {
        filteredState = MBControlStateEditing;
    } else if ((controlState_ & UIControlStateDisabled) == UIControlStateDisabled) {
        filteredState = UIControlStateDisabled;
    } else if (self.isHighlighted && self.isSelected) {
        filteredState = (UIControlStateHighlighted | UIControlStateSelected);
    } else if (self.isHighlighted) {
        filteredState = UIControlStateHighlighted;
    } else if (self.isSelected) {
        filteredState = UIControlStateSelected;
    } 

    UIImage *image = [self backgroundImageForState:filteredState];
    if (nil == image && filteredState != UIControlStateNormal) {
        // Default to image for normal state if available.
        image = [self backgroundImageForState:UIControlStateNormal];
    }
    [backgroundView_ setImage:image];
    
    image = [self imageForState:filteredState];
    if (nil == image && filteredState != UIControlStateNormal) {
        // Default to image for normal state if available.
        image = [self imageForState:UIControlStateNormal];
    }
    if (image) {
        if (!imageView_) {
            imageView_ = [[[UIImageView alloc] init] autorelease];
            [self setLeftView:imageView_];
            [self setLeftViewMode:UITextFieldViewModeAlways];
        }
        [imageView_ setImage:image];
        [imageView_ sizeToFit];
    }
    
    UIColor *color = [self textColorForState:filteredState];
    if (nil == color && filteredState != UIControlStateNormal) {
        // Default to color for normal state if available.
        color = [self textColorForState:UIControlStateNormal];
    }
    if (color) {
        [self setTextColor:color];
    }
}

- (void)MB_subscribeForTextFieldNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidBeginEditing:) name:UITextFieldTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidEndEditing:) name:UITextFieldTextDidEndEditingNotification object:nil];
}

- (void)MB_unsubscribeForNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    bounds = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets_);
    if (self.leftView) {
        CGFloat offset = self.leftView.frame.size.width;
        bounds.origin.x += offset;
        bounds.size.width -= offset;
    }
    if (self.rightView) {
        bounds.size.width -= self.rightView.frame.size.width;
    }
    return UIEdgeInsetsInsetRect(bounds, textEdgeInsets_);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    bounds = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets_);
    CGSize viewSize = self.leftView.bounds.size;
    bounds = UIEdgeInsetsInsetRect(bounds, leftViewEdgeInsets_);
    return CGRectMake(bounds.origin.x, bounds.origin.y + roundf((bounds.size.height - viewSize.height) * 0.5), viewSize.width, viewSize.height);
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds {
    bounds = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets_);
    CGSize viewSize = self.rightView.bounds.size;
    bounds = UIEdgeInsetsInsetRect(bounds, rightViewEdgeInsets_);
    return CGRectMake(bounds.origin.x + bounds.size.width - viewSize.width, bounds.origin.y + roundf((bounds.size.height - viewSize.height) * 0.5), viewSize.width, viewSize.height);
}

- (CGRect)caretRectForPosition:(UITextPosition *)position {
    return (self.editingEnabled ? [super caretRectForPosition:position] : CGRectZero);
}

-(void)setPlaceholderTextColor:(UIColor*)color {
    id placeHolder =  [self valueForKey:@"_placeholderLabel"];
    if(placeHolder != nil && [placeHolder isKindOfClass:[UILabel class]]) {
        [(UILabel*)placeHolder setTextColor:color];
    }
}

// Available >= iOS 6.0
//- (BOOL)shouldChangeTextInRange:(UITextRange *)range replacementText:(NSString *)text {
//    return self.editingEnabled ? NO : YES;
//}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state {
    NSNumber *key = [NSNumber numberWithUnsignedInteger:state];
    if (image) {
        [backgroundImages_ setObject:image forKey:key];
    } else {
        [backgroundImages_ removeObjectForKey:key];
    }
    if (controlState_ == state) {
        [self MB_updateStyleForState];
    }
}

- (UIImage *)backgroundImageForState:(UIControlState)state {
    return [backgroundImages_ objectForKey:[NSNumber numberWithUnsignedInteger:state]];
}

- (void)setImage:(UIImage *)image forState:(UIControlState)state {
    [images_ setObject:image forKey:[NSNumber numberWithUnsignedInteger:state]];
    if (controlState_ == state) {
        [self MB_updateStyleForState];
    }
}

- (UIImage *)imageForState:(UIControlState)state {
    return [images_ objectForKey:[NSNumber numberWithUnsignedInteger:state]];
}

- (void)setTextColor:(UIColor *)textColor forState:(UIControlState)state {
    [textColors_ setObject:textColor forKey:[NSNumber numberWithUnsignedInteger:state]];
    if (controlState_ == state) {
        [self MB_updateStyleForState];
    }
}

- (UIColor *)textColorForState:(UIControlState)state {
    return [textColors_ objectForKey:[NSNumber numberWithUnsignedInteger:state]];
}

- (void)setContainsError:(BOOL)containsError {
    UIControlState oldState = controlState_;
    
    if (containsError && (controlState_ & MBControlStateError) != MBControlStateError) {
        controlState_ |= MBControlStateError;
    } else if (!containsError && (controlState_ & MBControlStateError) == MBControlStateError) {
        controlState_ &= ~MBControlStateError;
    }
    
    if (oldState != controlState_) {
        [self MB_updateStyleForState];
    }
}


- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    BOOL result = [super canPerformAction:action withSender:sender];
    if (self.editingEnabled == NO) {
        if (action == @selector(cut:) || action == @selector(paste:)) {
            result = NO;
        }
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextFieldNotification

- (void)textFieldTextDidBeginEditing:(NSNotification *)notification {
    if (notification.object == self) {
        [self MB_updateStyleForState];
    }
}

- (void)textFieldTextDidEndEditing:(NSNotification *)notification {
    if (notification.object == self) {
        [self MB_updateStyleForState];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (BOOL)containsError {
    return ((controlState_ & MBControlStateError) == MBControlStateError);
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    [self MB_updateStyleForState];
}

@end
