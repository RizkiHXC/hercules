//
//  MBTextView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 3/20/13.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Expands functionality for the default UITextview
 */
@interface MBTextView : UITextView
{
    NSString *placeholder_;
}

/**
 Returns the drawing rectangle for the text view's text.
 @param bounds The bounding rectangle of the receiver.
 @return The computed drawing rectangle for the label’s text.
 */
- (CGRect)textRectForBounds:(CGRect)bounds;
/**
 Returns the rectangle in which editable text can be displayed.
 @param bounds The bounding rectangle of the receiver.
 @return The computed editing rectangle for the text.
 */
- (CGRect)editingRectForBounds:(CGRect)bounds;
/**
 Returns the drawing rectangle for the text view's placeholder text.
 @param bounds The bounding rectangle of the receiver.
 @return The computed drawing rectangle for the placeholder text.
 */
- (CGRect)placeholderRectForBounds:(CGRect)bounds;

/**
 The string that is displayed when there is no other text in the text field.
 */
@property(nonatomic,copy) NSString *placeholder;

@end
