//
//  MBTextView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 3/20/13.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import "MBTextView.h"
#import "MBGlobalDefines.h"

@implementation MBTextView

@synthesize placeholder = placeholder_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MB_textViewTextDidChange:) name:UITextViewTextDidChangeNotification object:nil];
        [self setContentMode:UIViewContentModeRedraw];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    [placeholder_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGRect textRect = bounds;
    if ([self respondsToSelector:@selector(textContainerInset)]) {
        textRect = UIEdgeInsetsInsetRect(textRect, self.textContainerInset);
    }
    return textRect;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
//    CGRect placeholderRect = [self textRectForBounds:bounds];
//    UIFont *font = self.font ? self.font : self.typingAttributes[NSFontAttributeName];
//    placeholderRect.size = [placeholder_ sizeWithFont:font forWidth:bounds.size.width lineBreakMode:NSLineBreakByWordWrapping];
//    
//    
//    if (MB_IOS_VERSION_EQUALHIGHER(7)) {
//        // typingAttributes crashes in iOS 6
//        if (self.typingAttributes) {
//            NSParagraphStyle *style = self.typingAttributes[NSParagraphStyleAttributeName];
//            if (style) {
//                placeholderRect = CGRectOffset(placeholderRect, style.headIndent, style.firstLineHeadIndent);
//            }
//        }
//    }
//    return placeholderRect;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_textViewTextDidChange:(NSNotification *)notification
{
    if (notification.object == self) {
        [self setNeedsDisplay];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setPlaceholder:(NSString *)placeholder
{
    if (![placeholder_ isEqualToString:placeholder]) {
        [placeholder_ release];
        placeholder_ = [placeholder copy];
        [self setNeedsDisplay];
    }
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    [self setNeedsDisplay];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (self.text.length == 0 && self.placeholder) {
        rect = [self placeholderRectForBounds:self.bounds];
        
        UIFont *font = self.font ? self.font : self.typingAttributes[NSFontAttributeName];
        
        // Draw the text
        if ([self respondsToSelector:@selector(textContainer)]) {
            // >= iOS 7
            [[UIColor colorWithWhite:0.0f alpha:0.22] set];
            CGFloat padding = self.textContainer.lineFragmentPadding;
            rect = UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(0, padding, 0, padding));
        } else {
            [[UIColor colorWithWhite:0.8 alpha:1.0] set];
            rect = UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(0, 8, 0, 0));
        } 
        [self.placeholder drawInRect:rect withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:self.textAlignment];
    }
}

@end
