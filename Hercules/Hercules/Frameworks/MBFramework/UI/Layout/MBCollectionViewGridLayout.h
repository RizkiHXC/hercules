//
//  MBCollectionViewGridLayout.h
//  MBFramework
//
//  Created by Arno Woestenburg on 10/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The MBCollectionViewGridLayout is a convenience class for quickly creating a grid layout for a UICollectionView class.
 */
@interface MBCollectionViewGridLayout : UICollectionViewFlowLayout

/**
 Number of columns for the grid layout. Default is 0.
 @discussion If numberOfColumns is not 0, itemSize width is automatically set to create the desired number of columns.
 */
@property(nonatomic,assign) NSUInteger numberOfColumns;

///**
// *  Number of rows for the grid layout. Default is 0.
// *  @discussion If numberOfRows is not 0, itemSize heigth is automatically set to create the desired number of rows.
// */
//@property(nonatomic,assign) NSUInteger numberOfRows;

@end
