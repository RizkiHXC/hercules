//
//  MBCollectionViewGridLayout.m
//  MBFramework
//
//  Created by Arno Woestenburg on 10/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBCollectionViewGridLayout.h"

@implementation MBCollectionViewGridLayout

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.numberOfColumns = 0;
//        self.numberOfRows = 0;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UICollectionViewLayout

- (void)prepareLayout
{
    if (self.numberOfColumns > 0) {
        CGRect sectionRect = UIEdgeInsetsInsetRect(self.collectionView.bounds, self.sectionInset);
        CGFloat itemSize = (sectionRect.size.width - ((self.numberOfColumns - 1) * self.minimumInteritemSpacing)) / self.numberOfColumns;
        [self setItemSize:CGSizeMake(itemSize, itemSize)];
    }
    [super prepareLayout];
}

@end
