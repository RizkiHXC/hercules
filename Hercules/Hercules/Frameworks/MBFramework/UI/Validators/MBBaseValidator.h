//
//  MBBaseValidator.h
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 MBBaseValidator is a generic class that implements all the basic behavior required of a validator object. Validator objects are used for validation on user-entered data values in a view controller. 
 It is not possible to use instances of direct subclasses of MBBaseValidator (or any other class not inheriting from MBBaseValidator) with an input validate manager.
 */
@interface MBBaseValidator : NSObject

/**
 Creates and returns a newly allocated validator object with the specified responder.
 @param responder Responder based class used for validation.
 @return An initialized validator object or nil if the object couldn't be created.
 */
+ (instancetype)validatorWithResponder:(UIResponder *)responder;

/**
 Initializes and returns a newly allocated BaseValidator object with the specified responder.
 @param responder Responder based class used for validation.
 @return An initialized BaseValidator object or nil if the object couldn't be created.
 */
- (instancetype)initWithResponder:(UIResponder *)responder;
/**
 Returns the text displayed by the responder. The responder needs to respond to the text selector for this function to.
 @return The content text from the responder if available.
 */
- (NSString *)inputTextFromResponder;
/**
 Triggers validation of the associated responder.
 @return YES if validation succeeded, NO if not.
 */
- (BOOL)validate;
/**
 Override to provide your own conditions for validation.
 @return NO to skip validation, YES to proceed validation.
 @important Be sure to include [super mustValidate].
 */
- (BOOL)mustValidate;

/**
 A UIResponder based object which content or state is validated. (read-only)
 */
@property(nonatomic,readonly) UIResponder *responder;
/**
 Set to YES to validate if the responder is hidden, NO to skip validation if the responder is hidden. Default is NO.
 */
@property(nonatomic,assign) BOOL validateIfResponderIsHidden;
/**
 Specifies the error message to be displayed in case the validator fails.
 */
@property(nonatomic,copy) NSString *errorMessage;

@end
