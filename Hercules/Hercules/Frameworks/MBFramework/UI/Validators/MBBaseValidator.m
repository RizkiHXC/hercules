//
//  MBBaseValidator.m
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBBaseValidator.h"

@implementation MBBaseValidator

@synthesize responder = responder_;
@synthesize validateIfResponderIsHidden = validateIfResponderIsHidden_;
@synthesize errorMessage = errorMessage_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (instancetype)validatorWithResponder:(UIResponder *)responder
{
    return [[[[self class] alloc] initWithResponder:responder] autorelease];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithResponder:(UIResponder *)responder
{
    self = [self init];
    if (self) {
        responder_ = responder;
        validateIfResponderIsHidden_ = NO;
    }
    return self;
}

- (void)dealloc
{
    [errorMessage_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSString *)inputTextFromResponder {
    NSString *inputText = nil;
    if ([responder_ respondsToSelector:@selector(text)]) {
        inputText = [[[responder_ performSelector:@selector(text)] mutableCopy] autorelease];
    }
    return inputText;
}

- (BOOL)validate {
    // This function must be overriden by a subclass.
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (BOOL)mustValidate
{
    // Override to provide your own conditions for validation. Be sure to include [super mustValidate].
    BOOL result = YES;
    
    if ([self.responder respondsToSelector:@selector(isHidden)]) {
        // Validate if responder (view) is hidden.
        result = (self.validateIfResponderIsHidden || [self.responder performSelector:@selector(isHidden)] == NO);
    }
    return result;
}

@end
