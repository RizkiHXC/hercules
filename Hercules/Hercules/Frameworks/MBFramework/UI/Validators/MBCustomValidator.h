//
//  MBCustomValidator.h
//  MBFramework
//
//  Created by Arno Woestenburg on 2/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBBaseValidator.h"

@class MBCustomValidator;

/**
 This protocol defines delegate methods for MBCustomValidator objects.
 */
@protocol MBCustomValidatorDelegate <NSObject>

/**
 Validation callback method.
 @param validator The validator requesting the validation.
 @return YES if validation succeeded, NO if not.
 */
- (BOOL)validate:(MBCustomValidator *)validator;

@end

/**
 MBCustomValidator performs user-defined validation on an input control.
 */
@interface MBCustomValidator : MBBaseValidator
{
@private
    id<MBCustomValidatorDelegate> delegate_;
    id target_;
    SEL selector_;
    id userInfo_;
    NSPredicate *predicate_;
    id validateObject_;
}

/**
 Creates and returns a newly allocated BaseValidator object with the specified responder.
 @param responder Responder based class used for validation.
 @param delegate The receiver’s delegate or nil if it doesn’t have a delegate.
 @return An initialized BaseValidator object or nil if the object couldn't be created.
 */
+ (instancetype)validatorWithResponder:(UIResponder *)responder andDelegate:(id<MBCustomValidatorDelegate>)delegate;
/**
 Creates and returns a newly allocated BaseValidator object with the specified responder.
 @param responder Responder based class used for validation.
 @param target The target object—that is, the object to which the action message is sent.
 @param selector A selector identifying an action message. It cannot be NULL.
 @return An initialized BaseValidator object or nil if the object couldn't be created.
 */
+ (instancetype)validatorWithResponder:(UIResponder *)responder target:(id)target selector:(SEL)selector;
/**
 Creates and returns a newly allocated BaseValidator object with the specified responder.
 @param responder      Responder based class used for validation.
 @param predicate      The predicate against which to evaluate the validateObject.
 @param validateObject The object against which to validate the predicate. The object is retained by the reciever. Can be nil.
 @discussion If validateObject is nil, the custom validator uses the responder object to validate it's predicate against.
 @return An initialized BaseValidator object or nil if the object couldn't be created.
 */
+ (instancetype)validatorWithResponder:(UIResponder *)responder predicate:(NSPredicate *)predicate validateObject:(id)validateObject;

/**
 Initializes and returns a newly allocated BaseValidator object with the specified responder.
 @param responder Responder based class used for validation.
 @param delegate The receiver’s delegate or nil if it doesn’t have a delegate.
 @return An initialized BaseValidator object or nil if the object couldn't be created.
 */
- (instancetype)initWithResponder:(UIResponder *)responder andDelegate:(id<MBCustomValidatorDelegate>)delegate;
/**
 Initializes and returns a newly allocated BaseValidator object with the specified responder.
 @param responder Responder based class used for validation.
 @param target The target object—that is, the object to which the action message is sent.
 @param selector A selector identifying an action message. It cannot be NULL.
 @return An initialized BaseValidator object or nil if the object couldn't be created.
 */
- (instancetype)initWithResponder:(UIResponder *)responder target:(id)target selector:(SEL)selector;
/**
 Initializes and returns a newly allocated BaseValidator object with the specified responder.
 @param responder      Responder based class used for validation.
 @param predicate      The predicate against which to evaluate the validateObject.
 @param validateObject The object against which to validate the predicate. The object is retained by the reciever. Can be nil.
 @discussion If validateObject is nil, the custom validator uses the responder object to validate it's predicate against.
 @return An initialized BaseValidator object or nil if the object couldn't be created.
 */
- (instancetype)initWithResponder:(UIResponder *)responder predicate:(NSPredicate *)predicate validateObject:(id)validateObject;

/**
 The delegate of the validator object.
 @discussion The delegate must adopt the MBCustomValidatorDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBCustomValidatorDelegate> delegate;

/**
 Returns the user object associated with the receiver.
 */
@property(nonatomic,retain) id userInfo;

@end
