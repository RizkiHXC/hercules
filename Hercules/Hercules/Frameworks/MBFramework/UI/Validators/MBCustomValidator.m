//
//  MBCustomValidator.m
//  MBFramework
//
//  Created by Arno Woestenburg on 2/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBCustomValidator.h"

@implementation MBCustomValidator

@synthesize delegate = delegate_;
@synthesize userInfo = userInfo_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (instancetype)validatorWithResponder:(UIResponder *)responder andDelegate:(id<MBCustomValidatorDelegate>)delegate
{
    return [[[[self class] alloc] initWithResponder:responder andDelegate:delegate] autorelease];
}

+ (instancetype)validatorWithResponder:(UIResponder *)responder target:(id)target selector:(SEL)selector
{
    return [[[[self class] alloc] initWithResponder:responder target:target selector:selector] autorelease];
}

+ (instancetype)validatorWithResponder:(UIResponder *)responder predicate:(NSPredicate *)predicate validateObject:(id)validateObject
{
    return [[[[self class] alloc] initWithResponder:responder predicate:predicate validateObject:validateObject] autorelease];
}


//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithResponder:(UIResponder *)responder andDelegate:(id<MBCustomValidatorDelegate>) delegate
{
    self = [self initWithResponder:responder];
    if (self) {
        delegate_ = delegate;
    }
    return self;
}

- (instancetype)initWithResponder:(UIResponder *)responder target:(id)target selector:(SEL)selector
{
    self = [self initWithResponder:responder];
    if (self) {
        target_ = target;
        selector_ = selector;
    }
    return self;
}

- (instancetype)initWithResponder:(UIResponder *)responder predicate:(NSPredicate *)predicate validateObject:(id)validateObject
{
    self = [self initWithResponder:responder];
    if (self) {
        predicate_ = [predicate retain];
        validateObject_ = [validateObject retain];
    }
    return self;
}

- (void)dealloc
{
    [userInfo_ release];
    [predicate_ release];
    [validateObject_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MBBaseValidator

- (BOOL)validate
{
    BOOL result = NO;
    
    if ([delegate_ respondsToSelector:@selector(validate:)]) {
        // Validate via delegate.
        result = [delegate_ validate:self];
    } else if (target_ != nil) {
        // Validate via target and selector.
        result = (BOOL)[target_ performSelector:selector_ withObject:self];
    } else if (predicate_) {
        // Validate via predicate. (Default to responder if validateObject is nil)
        result = [predicate_ evaluateWithObject:(validateObject_ ? validateObject_ : self.responder)];
    }
    return result;
}

@end
