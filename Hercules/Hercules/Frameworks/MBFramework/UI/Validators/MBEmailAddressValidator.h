//
//  MBEmailAddressValidator.h
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//
//

#import "MBRegularExpressionValidator.h"

/**
 MBEmailAddressValidator verifies that the user input is a valid email address. The validator uses a regular expression to check if the input is in a valid email address format.
 
 The validator uses a regular expression to check if the input is in a valid email address format.
 The regular expression used for this check is : @"^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"
 
 **Important** if the input being validated is empty, MBEmailAddressValidator will not do validation. Use a MBRequiredFieldValidator to ensure the value is not empty.
 */
@interface MBEmailAddressValidator : MBRegularExpressionValidator

@end
