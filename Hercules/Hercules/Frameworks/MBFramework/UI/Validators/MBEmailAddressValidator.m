//
//  MBEmailAddressValidator.m
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//
//

#import "MBEmailAddressValidator.h"

#define EMAIL_REGEXP @"^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"

@implementation MBEmailAddressValidator

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithResponder:(UIResponder *)responder
{
    return [self initWithPattern:EMAIL_REGEXP options:NSRegularExpressionCaseInsensitive responder:responder];
}

@end
