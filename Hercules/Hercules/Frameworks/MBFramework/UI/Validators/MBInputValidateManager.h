//
//  MBInputValidateManager.h
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//
//

#import <Foundation/Foundation.h>
#import "MBBaseValidator.h"

/**
 This protocol defines delegate methods for MBInputValidateManager objects.
 */
@protocol MBInputValidateManagerDelegate <NSObject>
@optional
/**
 Tells the delegate that validation has failed for the responder contained in the validator object.
 @param validator The validator object containing the responder object.
 */
- (void)validationErrorForValidator:(MBBaseValidator *)validator;
/**
 Tells the delegate that validation has succeeded for the responder contained in the validator object.
 @param validator The validator object containing the responder object.
 */
- (void)validationSuccessForValidator:(MBBaseValidator *)validator;
/**
 Tells the delegate to reset validation state for the responder contained in the validator object.
 @param validator The validator object containing the responder object.
 */
- (void)validationResetForValidator:(MBBaseValidator *)validator;
/**
 Asks the delegate whether validation needs to be performed for the responder contained in the validator object.
 @param validator The validator object containing the responder object.
 @return NO to skip validation, YES to proceed validation.
 */
- (BOOL)validatorShouldValidate:(MBBaseValidator *)validator;
/**
 Asks the delegate whether the responder object should be made first responder if validation fails.
 @param responder The responder object.
 @return YES is the responder must be made first responder.
 */
- (BOOL)shouldSetToFirstResponder:(UIResponder *)responder;

@end

/**
 The input validate manager provides a mechanism to perform validation on user-entered data values in a view controller before they are sent to the server.
 */
@interface MBInputValidateManager : NSObject

/**
 Initializes and returns a newly allocated MBInputValidateManager object with the specified properties.
 @param delegate The receiver’s delegate or nil if it doesn’t have a delegate.
 @return An initialized MBInputValidateManager object or nil if the object couldn't be created.
 */
- (instancetype)initWithDelegate:(id<MBInputValidateManagerDelegate>) delegate;
/**
 Adds a validator object to the validate manager.
 @param validator A validator object based on the MBBaseValidator class.
 */
- (void)addValidator:(MBBaseValidator *)validator;
/**
 Remove a validator object to the validate manager.
 @param validator A validator object based on the MBBaseValidator class.
 */
- (void)removeValidator:(MBBaseValidator *)validator;
/**
 Removes all validators associated with the specified responder.
 @param responder The responder object.
 */
- (void)removeValidatorsForResponder:(UIResponder *)responder;
/**
 Triggers the validation mechanism.
 @return YES if validation succeeded, NO if not.
 */
- (BOOL)validate;
/**
 Triggers the validation mechanism for the specified responder.
 @param responder The responder object to validate.
 @return YES if validation succeeded, NO if not.
 */
- (BOOL)validateResponder:(UIResponder *)responder;
/**
 Returns a new array containing the all the validator objects.
 @return A new array containing all the validator objects, or an empty array if the aren't any.
 */
- (NSArray *)validators;
/**
 Returns a new array containing the all the validator objects associated with the specified responder.
 @param responder The responder object.
 @return A new array containing all the validator objects associated with the specified responder, or an empty array if the aren't any.
 */
- (NSArray *)validatorsForResponder:(UIResponder *)responder;
/**
 Returns a new array containing the all the validator that are currenty in error state.
 @return A new array containing all the validator objects in error state, or an empty array if the aren't any.
 @discussion After a call to the validate method, this array is no longer valid and needs to be reloaded.
 */
- (NSArray *)validatorsWithError;
/**
 Asks the validator manager if the specified responder currenty contains any validation error.
 @param responder The responder object.
 @return YES if the responder currently has any associated validator object in error state, NO if not.
 */
- (BOOL)responderHasError:(UIResponder *)responder;
/**
 Tells all the validator objects to reset it's validation state.
 */
- (void)reset;
/**
 Returns the first responder that currently has any associated validator object in error state, or nil if there is none.
 @return The first responder that currently has error state.
 */
- (UIResponder *)firstInvalidResponder;
/**
 Returns the first validator that currently is in error state, or nil if there is none.
 @return The first validator that currently has error state.
 */
- (MBBaseValidator *)firstInvalidValidator;

/**
 The object that acts as the delegate of the receiving input validate manager.
 @discussion The delegate must adopt the MBInputValidateManagerDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBInputValidateManagerDelegate> delegate;
/**
 If YES, the validator manager automatically makes the first validator that has error state after validation the first responder. Default is NO.
 */
@property(nonatomic,assign) BOOL automaticallySetFirstInvalidResponder;

@end
