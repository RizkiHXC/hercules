//
//  MBInputValidateManager.m
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//
//

#import "MBInputValidateManager.h"

@interface MBInputValidateManager ()
{
    NSMutableArray *validators_;
    NSMutableArray *validatorsWithError_;
    id<MBInputValidateManagerDelegate> delegate_;
}
@end

@implementation MBInputValidateManager

@synthesize automaticallySetFirstInvalidResponder = automaticallySetFirstInvalidResponder_;
@synthesize delegate = delegate_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        validators_ = [[NSMutableArray alloc] init];
        self.automaticallySetFirstInvalidResponder = NO;
    }
    return self;
}

- (instancetype)initWithDelegate:(id<MBInputValidateManagerDelegate>) delegate
{
    self = [self init];
    if (self) {
        [self setDelegate:delegate];
    }
    return self;
}

- (void)dealloc
{
    [validators_ release];
    [validatorsWithError_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_notifyValidationErrorForValidator:(MBBaseValidator *)validator
{
    if ([delegate_ respondsToSelector:@selector(validationErrorForValidator:)]) {
        [delegate_ validationErrorForValidator:validator];
    }
}

- (void)MB_notifyValidationSuccessForValidator:(MBBaseValidator *)validator
{
    if ([delegate_ respondsToSelector:@selector(validationSuccessForValidator:)]) {
        [delegate_ validationSuccessForValidator:validator];
    }
}

- (void)MB_notifyValidationResetForValidator:(MBBaseValidator *)validator
{
    if ([delegate_ respondsToSelector:@selector(validationResetForValidator:)]) {
        [delegate_ validationResetForValidator:validator];
    }
}

- (BOOL)MB_notifyValidatorShouldValidate:(MBBaseValidator *)validator
{
    BOOL result = YES;
    if ([delegate_ respondsToSelector:@selector(validatorShouldValidate:)]) {
        result = [delegate_ validatorShouldValidate:validator];
    }
    return result;
}

- (NSMutableArray *)MB_mutableValidatorsForResponder:(UIResponder *)responder
{
    NSUInteger index = [validators_ indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return ([((MBBaseValidator *)[(NSMutableArray *)obj firstObject]) responder] == responder);
    }];
    return index != NSNotFound ? [validators_ objectAtIndex:index] : nil;
}

- (void)MB_setFirstInvalidResponder
{
    UIResponder *responder = [self firstInvalidResponder];
    
    if (![self.delegate respondsToSelector:@selector(shouldSetToFirstResponder:)] ||
        [self.delegate shouldSetToFirstResponder:responder] == YES) {
        [responder becomeFirstResponder];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)addValidator:(MBBaseValidator *)validator
{
    NSMutableArray* validatorsForResponder = [self MB_mutableValidatorsForResponder:validator.responder];

    if (validatorsForResponder == nil) {
        validatorsForResponder = [[[NSMutableArray alloc] init] autorelease];
        [validators_ addObject:validatorsForResponder];
    }
    [validatorsForResponder addObject:validator];
}

- (void)removeValidator:(MBBaseValidator *)validator
{
    NSMutableArray* validatorsForResponder = [self MB_mutableValidatorsForResponder:validator.responder];
    [validatorsForResponder removeObject:validator];
}

- (void)removeValidatorsForResponder:(UIResponder *)responder
{
    NSMutableArray* validatorsForResponder = [self MB_mutableValidatorsForResponder:responder];
    [validators_ removeObject:validatorsForResponder];
}

- (BOOL)validate
{
    BOOL result = YES;
    BOOL valid = YES;
    BOOL didValidate = NO;
    NSUInteger index = 0;
    NSUInteger count = 0;
    MBBaseValidator *validator = nil;
        
    [self reset];
    validatorsWithError_ = [[NSMutableArray alloc] init];
    
    for (NSMutableArray *validatorsForResponder in validators_) {
        
        valid = YES;
        didValidate = NO;
        index = 0;
        count = validatorsForResponder.count;
       
        while (index < count && valid == YES) {
            validator = [validatorsForResponder objectAtIndex:index];

            if ([validator mustValidate] && [self MB_notifyValidatorShouldValidate:validator]) {
                // A valid==NO result will end the loop.
                didValidate = YES;
                valid = [validator validate];
            }
            index++;
        }
        
        if (didValidate) {
            if (valid) {
                [self MB_notifyValidationSuccessForValidator:validator];
            } else {
                [self MB_notifyValidationErrorForValidator:validator];
                [validatorsWithError_ addObject:validator];
                result = NO; // End result is NO if one if any validator of any responder is invalid.
            }
        }
    }
    
    if (result == NO && self.automaticallySetFirstInvalidResponder == YES) {
        [self MB_setFirstInvalidResponder];
    }
    return result;
}

- (BOOL)validateResponder:(UIResponder *)responder
{
    BOOL result = YES;
    BOOL valid = YES;
    NSUInteger index = 0;
    NSUInteger count = 0;
    MBBaseValidator *validator = nil;
    
    NSArray *validatorsForResponder = [self MB_mutableValidatorsForResponder:responder];
    
    count = validatorsForResponder.count;
    
    while (index < count && valid == YES) {
        validator = [validatorsForResponder objectAtIndex:index];
        // A valid==NO result will end the loop.
        valid = [validator validate];
        index++;
    }
    
    if (valid) {
        [self MB_notifyValidationSuccessForValidator:validator];
    } else {
        [self MB_notifyValidationErrorForValidator:validator];
        result = NO; // End result is NO if one if any validator of the reponder is invalid.
    }
    return result;
}

- (BOOL)responderHasError:(UIResponder *)responder
{
    BOOL result = NO;
    MBBaseValidator *validator = nil;
    NSUInteger index = 0;
    NSUInteger count = validatorsWithError_.count;
    
    while (result == NO && index < count) {
        validator = [validatorsWithError_ objectAtIndex:index];
        if (validator.responder == responder) {
            result = YES;
        }
        index++;
    }
    return result;
}

- (void)reset
{
    // Reset all responders.
    [validatorsWithError_ release];
    validatorsWithError_ = nil;
    
    for (NSArray *validatorsForResponder in validators_) {

        if (validatorsForResponder.count > 0) {
            MBBaseValidator *validator = [validatorsForResponder firstObject];
            [self MB_notifyValidationResetForValidator:validator];
        }
    }
}

- (NSArray *)validators
{
    NSMutableArray *validators = [[[NSMutableArray alloc] init] autorelease];
    for (NSArray *validatorsForResponder in validators_) {
        [validators addObjectsFromArray:validatorsForResponder];
    }
    return [NSArray arrayWithArray:validators];
}

- (NSArray *)validatorsForResponder:(UIResponder *)responder
{
    return [NSArray arrayWithArray:[self MB_mutableValidatorsForResponder:responder]];
}

- (NSArray *)validatorsWithError
{
    return [NSArray arrayWithArray:validatorsWithError_];
}

- (UIResponder *)firstInvalidResponder
{
    MBBaseValidator *validator = nil;
    if (validatorsWithError_.count > 0) {
        validator = [validatorsWithError_ firstObject];
    }
    return [validator responder];
}

- (MBBaseValidator *)firstInvalidValidator
{
    MBBaseValidator *validator = nil;
    if (validatorsWithError_.count > 0) {
        validator = [validatorsWithError_ firstObject];
    }
    return validator;
}

@end
