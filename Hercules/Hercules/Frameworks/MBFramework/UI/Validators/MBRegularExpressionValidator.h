//
//  MBRegularExpressionValidator.h
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//
//

#import "MBBaseValidator.h"

#define kTelephoneCharactersPattern  @"^[0-9\\s-\\+\\*\\(\\)]*$" // Allowed characters for a telefone number

/**
 MBRegularExpressionValidator verifies the user input against a regular pattern using the NSRegularExpression class. The validation fails if the input does not match the pattern. The regular expression can be specified by the regularExpressionPattern property.
 
 **Important** MBRegularExpressionValidator only checks for nonempty user input. Use a MBRequiredFieldValidator to ensure the user input is not empty.
 */
@interface MBRegularExpressionValidator : MBBaseValidator

/**
 Initializes and returns a newly allocated RegularExpressionValidator object.
 @param regularExpression A regular expression type for validating.
 @param responder Responder based class used for validation.
 @return An initialized RegularExpressionValidator object or nil if the object couldn't be created.
 */
- (instancetype)initWithRegularExpression:(NSRegularExpression *)regularExpression responder:(UIResponder *)responder;
/**
 Initializes and returns a newly allocated RegularExpressionValidator object.
 @param pattern The regular expression pattern to compile.
 @param options The regular expression options that are applied to the expression during matching. See “NSRegularExpressionOptions” for possible values.
 @param responder Responder based class used for validation.
 @return An initialized RegularExpressionValidator object or nil if the object couldn't be created.
 */
- (instancetype)initWithPattern:(NSString *)pattern options:(NSRegularExpressionOptions)options responder:(UIResponder *)responder;

/**
 The NSRegularExpression object used for validation. (read-only)
 */
@property(nonatomic,retain,readonly) NSRegularExpression *regularExpression;

@end
