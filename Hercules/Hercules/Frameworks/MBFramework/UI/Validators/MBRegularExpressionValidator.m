//
//  MBRegularExpressionValidator.m
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//
//

#import "MBRegularExpressionValidator.h"

@implementation MBRegularExpressionValidator

@synthesize regularExpression = regularExpression_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithRegularExpression:(NSRegularExpression *)regularExpression responder:(UIResponder *)responder
{
    self = [super initWithResponder:responder];
    if (self) {
        regularExpression_ = [regularExpression retain];
    }
    return self;
}

- (instancetype)initWithPattern:(NSString *)pattern options:(NSRegularExpressionOptions)options responder:(UIResponder *)responder
{
    self = [super initWithResponder:responder];
    if (self) {
        regularExpression_ = [[NSRegularExpression alloc] initWithPattern:pattern options:options error:nil];
    }
    return self;
}

- (void)dealloc
{
    [regularExpression_ release];
    regularExpression_ = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MBBaseValidator

- (BOOL)validate
{
    NSAssert(regularExpression_ != nil, @"No regular expression set");
    
    NSString* inputText = [self inputTextFromResponder];
    
    // Validation succeeds if the associated input control contains empty input. Use a MBRequiredFieldValidator to ensure the input is not empty.
    return (inputText == nil || [regularExpression_ numberOfMatchesInString:inputText options:0 range:NSMakeRange(0, [inputText length])] > 0);
}


@end
