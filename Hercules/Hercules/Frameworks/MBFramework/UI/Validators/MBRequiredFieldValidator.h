//
//  MBRequiredFieldValidator.h
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//
//

#import "MBBaseValidator.h"

/**
 MBRequiredFieldValidator ensures that the user enters some data in the specified input field. By default, MBRequiredFieldValidator will check if the user input is empty or not. The validation fails if the input is empty.
 */
@interface MBRequiredFieldValidator : MBBaseValidator

/**
 Determines if the reciever should trim whitespaces before validation. Default is YES.
 */
@property(nonatomic,assign) BOOL trimTextBeforeValidate;

@end
