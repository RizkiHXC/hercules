//
//  MBRequiredFieldValidator.m
//  MBFramework
//
//  Created by Arno Woestenburg on 2/8/13.
//
//

#import "MBRequiredFieldValidator.h"

@implementation MBRequiredFieldValidator

@synthesize trimTextBeforeValidate = trimTextBeforeValidate_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        trimTextBeforeValidate_ = YES;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MBBaseValidator

- (BOOL)validate {
    NSString *inputText = [self inputTextFromResponder];

    if (inputText.length > 0 && trimTextBeforeValidate_) {
        inputText = [inputText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    return ([inputText length] > 0);
}

@end
