//
//  MBMasterDetailViewController.h
//  MBFramework
//
//  Created by Arno Woestenburg on 19/05/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The delegate of a MBMasterDetailViewController object must adopt the MBMasterDetailViewControllerDelegate protocol.
 */
@protocol MBMasterDetailViewControllerDelegate <NSObject>

@optional
/**
 Tells the delegate that the specified row is now selected in the master view controller.
 @param indexPath An index path locating the new selected row.
 */
- (void)masterViewDidSelectRowAtIndexPath:(NSIndexPath *)indexPath;
/**
 Tells the delegate that the specified object is now selected in the master view controller.
 *
 @param object A reference to the selected object.
 */
- (void)masterViewDidSelectObject:(id)object;
/**
 Tells the delegate that the specified row is now deselected in the master view controller.
 *
 @param indexPath An index path locating the deselected row.
 */
- (void)masterViewDidDeselectRowAtIndexPath:(NSIndexPath *)indexPath;
/**
 Tells the delegate that the specified object is now deselected in the master view controller.
 *
 @param object A reference to the deselected object.
 */
- (void)masterViewDidDeselectObject:(id)object;
/**
 Tells the delegate that the specified row is about to be deleted in the master view controller.
 *
 @param indexPath An index path locating the row that's about to be deleted.
 */
- (void)masterViewWillDeleteRowAtIndexPath:(NSIndexPath *)indexPath;
/**
 Tells the delegate that the specified object is about to be deleted in the master view controller.
 *
 @param object A reference to the object that's about to be deleted.
 */
- (void)masterViewWillDeleteObject:(id)object;
@end

/**
 The MBMasterDetailViewController class is a container view controller that manages the presentation of two side-by-side view controllers. (Class acts like a UISplitViewController but offers more flexiblity) You use this class to implement a master-detail interface, in which for example the left-side view controller presents a list of items and the right-side presents details of the selected item. Master/detail view controllers are for use exclusively on iPad devices. Attempting to create one on other devices results in an exception.
 */
@interface MBMasterDetailViewController : UIViewController

/**
 The view controller used for the master view.
 */
@property(nonatomic,retain) UIViewController *masterViewController;
/**
 The view controller used for the detail view.
 */
@property(nonatomic,retain) UIViewController *detailViewController;
/**
 The divider view between the master and detail view.
 */
@property(nonatomic,retain) UIView *dividerView;
/**
 The tint color for the divider view.
 */
@property(nonatomic,retain) UIColor *dividerTintColor;
/**
 The width for the master view column.
 @discussion When masterColumnWidth is 0, a default ratio is used
 */
@property(nonatomic,assign) CGFloat masterColumnWidth;

@end
