//
//  MBMasterDetailViewController.m
//  MBFramework
//
//  Created by Arno Woestenburg on 19/05/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBMasterDetailViewController.h"

@interface MBMasterDetailViewController ()
{
    UIView *masterViewContainer_;
    UIView *detailViewContainer_;
}
@end

@implementation MBMasterDetailViewController

@synthesize masterViewController = masterViewController_;
@synthesize detailViewController = detailViewController_;
@synthesize dividerTintColor = dividerTintColor_;
@synthesize dividerView = dividerView_;
@synthesize masterColumnWidth = masterColumnWidth_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.dividerTintColor = [UIColor darkGrayColor];
        self.masterColumnWidth = 0.0f; 
    }
    return self;
}

- (void)dealloc
{
    self.masterViewController = nil;
    self.detailViewController = nil;
    [dividerTintColor_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)loadView
{
    // This class can only be used on iPad
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        [NSException raise:NSInternalInconsistencyException format:@"This class can only be used on iPad"];
    }

	self.view = [[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]] autorelease];

    CGFloat masterColumnWidth = [self MB_widthForMasterColumn];
    CGRect viewRect = self.view.bounds;
    viewRect.size.width = masterColumnWidth;
    
    masterViewContainer_ = [[[UIView alloc] initWithFrame:viewRect] autorelease];
    [self.view addSubview:masterViewContainer_];

    viewRect = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(0, masterColumnWidth, 0, 0));
    detailViewContainer_ = [[[UIView alloc] initWithFrame:viewRect] autorelease];
    [self.view addSubview:detailViewContainer_];
    
    // Divider view between master and detail view
    [self.dividerView setBackgroundColor:self.dividerTintColor];
    [self.view setAutoresizesSubviews:NO];
    [self MB_layoutSubviews];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (masterViewController_) {
        [self MB_setMasterView:masterViewController_.view];
    }
    
    if (detailViewController_) {
        [self MB_setDetailsView:detailViewController_.view];
    }
}

- (void)viewWillLayoutSubviews
{
    [self MB_layoutSubviews];
    [super viewWillLayoutSubviews];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_setMasterView:(UIView *)masterView
{
    [masterViewContainer_ addSubview:masterView];
    [masterView setFrame:masterViewContainer_.bounds];
    [masterView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
}

- (void)MB_setDetailsView:(UIView *)detailsView
{
    [detailViewContainer_ addSubview:detailsView];
    [detailsView setFrame:detailViewContainer_.bounds];
    [detailsView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
}

- (void)MB_layoutSubviews
{
    CGFloat masterColumnWidth = [self MB_widthForMasterColumn];
    CGRect viewRect = self.view.bounds;
    viewRect.size.width = masterColumnWidth;
    
    [masterViewContainer_ setFrame:viewRect];
    
    CGFloat detailViewWidth = self.view.bounds.size.width - masterColumnWidth;
    
    if (dividerView_) {
        viewRect.origin.x = CGRectGetMaxX(masterViewContainer_.frame);
        viewRect.size.width = dividerView_.bounds.size.width;
        [dividerView_ setFrame:viewRect];
        
        detailViewWidth -= dividerView_.bounds.size.width;
    }
    
    viewRect.size.width = detailViewWidth;
    viewRect.origin.x = CGRectGetMaxX(self.view.bounds) - detailViewWidth;
    [detailViewContainer_ setFrame:viewRect];
}

- (CGFloat)MB_widthForMasterColumn
{
    CGFloat widthForMasterColumn = masterColumnWidth_;
    if (widthForMasterColumn <= 0.0f) {
        // Use default ratio 3.2
        widthForMasterColumn = roundf(self.view.bounds.size.width / 3.2);
    }
    return widthForMasterColumn;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setMasterViewController:(UIViewController *)masterViewController
{
    if (masterViewController_ != masterViewController) {
        if (masterViewController_) {
            [masterViewController_.view removeFromSuperview];
            [masterViewController_ removeFromParentViewController];
        }
        masterViewController_ = masterViewController;
        if (masterViewController_) {
            [self addChildViewController:masterViewController_];
            
            if ([self isViewLoaded]) {
                [self MB_setMasterView:masterViewController_.view];
            }
        }
    }
}

- (void)setMasterColumnWidth:(CGFloat)masterColumnWidth
{
    masterColumnWidth_ = masterColumnWidth;
    if ([self isViewLoaded]) {
        [self MB_layoutSubviews];
    }
}

- (void)setDetailViewController:(UIViewController *)detailViewController
{
    if (detailViewController_ != detailViewController) {
        if (detailViewController_) {
            [detailViewController_.view removeFromSuperview];
            [detailViewController_ removeFromParentViewController];
        }
        detailViewController_ = detailViewController;
        if (detailViewController_) {
            [self addChildViewController:detailViewController_];
            if ([self isViewLoaded]) {
                [self MB_setDetailsView:detailViewController_.view];
            }
        }
    }
}

- (void)setDividerTintColor:(UIColor *)dividerTintColor
{
    [dividerTintColor_ release];
    dividerTintColor_ = [dividerTintColor retain];
    if ([self isViewLoaded]) {
        [self.dividerView setBackgroundColor:dividerTintColor_];
    }
}

- (UIView *)dividerView
{
    if (!dividerView_) {
        // Create if needed. Call setDividerView to provide a custom divider.
        [self setDividerView:[[[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, self.view.bounds.size.height)] autorelease]];
    }
    return dividerView_;
}

- (void)setDividerView:(UIView *)dividerView
{
    [dividerView_ removeFromSuperview];
    dividerView_ = dividerView;
    [dividerView_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
    [self.view addSubview:dividerView_];
    [self MB_layoutSubviews];
}


@end
