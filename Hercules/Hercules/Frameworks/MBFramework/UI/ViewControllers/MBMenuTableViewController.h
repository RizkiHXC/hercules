//
//  MBMenuTableViewController.h
//  MBFramework
//
//  Created by Marco Jonker on 1/20/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MBMenuTableViewControllerDelegate;

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBMenuTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    id<MBMenuTableViewControllerDelegate> delegate_;
    UIViewController* defaultViewController_;
}

- (instancetype)initWithStyle:(UITableViewStyle)style;
- (void)setViewControllers:(NSArray *)viewControllers forSection:(NSInteger)section;
- (NSArray *)viewControllersForSection:(NSInteger)section;
- (UIViewController*)viewControllerForIndexPath:(NSIndexPath*)indexPath;
- (void)setSelectedViewController:(UIViewController *)selectedViewController animated:(BOOL)animated;
- (void)selectDefaultViewController;
- (void)selectDefaultViewControllerAnimated:(BOOL)animated;

@property (nonatomic,retain) UITableView *tableView;
/**
 *  The object that acts as the delegate of the receiving menu tableview controller.
 *
 *  @discussion The delegate must adopt the MBMenuTableViewControllerDelegate protocol. The delegate is not retained.
 */
@property (nonatomic, assign) id<MBMenuTableViewControllerDelegate> delegate;
@property (nonatomic, assign) NSIndexPath* selectedIndexPath;
@property (nonatomic, assign) UIViewController* selectedViewController;
@property (nonatomic, retain) UIViewController* defaultViewController;

@end

@protocol MBMenuTableViewControllerDelegate <NSObject>
@optional
- (UITableViewCell *)tableView:(UITableView *)tableView menuCellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableViewCell:(UITableViewCell *)menuCell menuCellStyleForRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)shouldSelectViewController:(UIViewController *)viewController;
- (void)willSelectViewController:(UIViewController *)viewController;
- (void)didSelectViewController:(UIViewController *)viewController;
@end
