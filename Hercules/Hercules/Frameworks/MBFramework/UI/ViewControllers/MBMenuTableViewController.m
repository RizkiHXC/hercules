//
//  MBMenuTableViewController.m
//  MBFramework
//
//  Created by Marco Jonker on 1/20/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//
 
#import "MBMenuTableViewController.h"
#import "MBMutableCollection.h"

@interface MBMenuTableViewController ()
{
    UITableViewStyle tableViewStyle_;
    NSMapTable *menuItemCells_;
    MBMutableCollection *viewControllers_;
}
@end

@implementation MBMenuTableViewController

@synthesize delegate=delegate_;
@synthesize defaultViewController=defaultViewController_;
@synthesize selectedViewController=selectedViewController_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithStyle:(UITableViewStyle)style
{
	tableViewStyle_ = style;
    return [super init];
}

- (instancetype)init
{
    return [self initWithStyle:UITableViewStylePlain];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        menuItemCells_ = [[NSMapTable alloc] init];
        viewControllers_ = [[MBMutableCollection alloc] init];
    }
    return self;
}

-(void)dealloc{
    [viewControllers_ release];
    [menuItemCells_ release];
    [defaultViewController_ release];
    [selectedViewController_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)loadView
{
    self.view = [[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]] autorelease];
    // TableView
    self.tableView = [[[UITableView alloc] initWithFrame:self.view.bounds style:tableViewStyle_] autorelease];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:self.tableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setShowsVerticalScrollIndicator:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(defaultViewController_ == nil && viewControllers_.allObjects.count > 0) {
        self.defaultViewController = [viewControllers_ objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        // >= iOS 7
        CGFloat inset = self.topLayoutGuide.length;
        [self.tableView setContentInset:UIEdgeInsetsMake(inset, 0, 0, 0)];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_setSelectedViewController:(UIViewController *)selectedViewController
{
    [selectedViewController_ release];
    selectedViewController_ = [selectedViewController retain];
    [self didSelectViewController:selectedViewController];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setSelectedIndexPath:(NSIndexPath *)indexPath
{
    if(![self.tableView.indexPathForSelectedRow isEqual:indexPath]) {
        UIViewController* viewController = [viewControllers_ objectAtIndexPath:indexPath];
        if (viewController) {
            [self setSelectedViewController:viewController];
        }
    }
}

-(void)selectDefaultViewController {
    [self setSelectedViewController:defaultViewController_ animated:NO];
}

-(void)selectDefaultViewControllerAnimated:(BOOL)animated {
    [self setSelectedViewController:defaultViewController_ animated:animated];
}

-(NSIndexPath*)selectedIndexPath {
    return self.tableView.indexPathForSelectedRow;
}

- (void)setSelectedViewController:(UIViewController *)selectedViewController animated:(BOOL)animated {
    // The specified view controller must be in the viewControllers array.
    if (![viewControllers_ containsObject:selectedViewController]) {
        [NSException raise:NSInvalidArgumentException format:@"The specified view controller must be in the viewControllers collection."];
    }
    if (selectedViewController != selectedViewController_) {
        [self MB_setSelectedViewController:selectedViewController];
    } else {
        // Pop to root when selecting a viewcontroller which is already selected.
        if ([selectedViewController isKindOfClass:[UINavigationController class]]) {
            [((UINavigationController *)selectedViewController) popToRootViewControllerAnimated:NO];
        }
        [self didSelectViewController:selectedViewController_];
    }

    [self.tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionNone animated:animated];
}

- (void)setSelectedViewController:(UIViewController *)selectedViewController {
    [self setSelectedViewController:selectedViewController animated:NO];
}

//-(UIViewController*)selectedViewController {
//    UIViewController* viewController = nil;
//    NSIndexPath* indexPath = self.selectedIndexPath;
//    
//    if(indexPath != nil) {
//        viewController = [viewControllers_ objectAtIndexPath:self.selectedIndexPath];
//    }
//    
//    return viewController;
//}

- (void)setViewControllers:(NSArray *)viewControllers forSection:(NSInteger)section
{
    UIViewController *newSelectedViewController = nil;
    NSArray *oldViewControllers = nil;
    
    [menuItemCells_ removeAllObjects];
    if ([viewControllers_ numberOfSections] > section) {
        
        oldViewControllers = [[viewControllers_ arrayAtSection:section]retain];

        // Release selected view controller when it belongs to the old section viewcontrollers
        if ([oldViewControllers containsObject:self.selectedViewController]) {
            newSelectedViewController = [viewControllers objectAtIndex:0];
        }
        [viewControllers_ replaceArrayAtSection:section withArray:viewControllers];
    } else {
        [viewControllers_ insertArray:viewControllers atSection:section];
    }
    
    //FIXME: [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView reloadData];
    if (newSelectedViewController) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForItem:[viewControllers indexOfObject:newSelectedViewController] inSection:section];
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        [self setSelectedViewController:newSelectedViewController];
        [self didSelectViewController:newSelectedViewController];
    }
    
    [oldViewControllers release];
}

- (NSArray *)viewControllersForSection:(NSInteger)section {
    return [viewControllers_ arrayAtSection:section];
}

- (UIViewController*)viewControllerForIndexPath:(NSIndexPath*)indexPath {
    return [viewControllers_ objectAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView menuCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([delegate_ respondsToSelector:@selector(tableView:menuCellForRowAtIndexPath:)]) {
        return [delegate_ tableView:tableView menuCellForRowAtIndexPath:indexPath];
    } else {
        // Override to use a different UITableViewCell class.
        return [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
    }
}

- (void)tableViewCell:(UITableViewCell *)menuCell menuCellStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([delegate_ respondsToSelector:@selector(tableViewCell:menuCellStyleForRowAtIndexPath:)]) {
        return [delegate_ tableViewCell:menuCell menuCellStyleForRowAtIndexPath:indexPath];
    }
}

- (BOOL)shouldSelectViewController:(UIViewController *)viewController {
    if([delegate_ respondsToSelector:@selector(shouldSelectViewController:)]) {
        return [delegate_ shouldSelectViewController:viewController];
    } else {
        // Override for custom behaviour.
        return YES;
    }
}

- (void)willSelectViewController:(UIViewController *)viewController {
    if([delegate_ respondsToSelector:@selector(willSelectViewController:)]) {
        return [delegate_ willSelectViewController:viewController];
    }
}

- (void)didSelectViewController:(UIViewController *)viewController {
    if([delegate_ respondsToSelector:@selector(didSelectViewController:)]) {
        return [delegate_ didSelectViewController:viewController];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self shouldSelectViewController:[viewControllers_ objectAtIndexPath:indexPath]] ? indexPath : nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController = [viewControllers_ objectAtIndexPath:indexPath];
    [self setSelectedViewController:viewController];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController* viewController = [viewControllers_ objectAtIndexPath:indexPath];
    UITableViewCell *cell = [menuItemCells_ objectForKey:viewController];
    
    // Reusable cells are not used, so there must always be a cell for a view controller. If not, create one
    // and add to the map table.
    if (cell == nil) {
        cell = [self tableView:tableView menuCellForRowAtIndexPath:indexPath];
        if (cell.textLabel.text == nil) {
            // Default text when no title has been set
            [cell.textLabel setText:viewController.title];
        }
        if (cell.imageView.image == nil) {
            // Try to load the image from the root viewController
            if ([viewController respondsToSelector:@selector(imageForMenuItem)]) {
                [cell.imageView setImage:(UIImage *)[viewController performSelector:@selector(imageForMenuItem)]];
            }
            // Try to load the image from the root viewController
            if ([viewController respondsToSelector:@selector(highlightedImageForMenuItem)]) {
                [cell.imageView setHighlightedImage:(UIImage *)[viewController performSelector:@selector(highlightedImageForMenuItem)]];
            }
            
        }
        [menuItemCells_ setObject:cell forKey:viewController];
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [viewControllers_ numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [viewControllers_ numberOfObjectsInSection:section];
}

@end
