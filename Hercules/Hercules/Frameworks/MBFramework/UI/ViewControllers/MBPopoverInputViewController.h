//
//  MBPopoverInputViewController.h
//  MBFramework
//
//  Created by Arno Woestenburg on 10/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBPopoverInputViewController : NSObject
{
    
}

/**
 Initializes and returns a newly allocated PopoverInputViewController object with the specified responder view.
 @param responderView UIView class.
 @return An initialized PopoverInputViewController object or nil if the object couldn't be created.
 */
- (instancetype)initWithResponderView:(UIView *)responderView;

@property(nonatomic,retain) UIView *inputAccessoryView;
@property(nonatomic,retain) UIView *inputView;
@property(nonatomic,retain) UIView *responderView;
@property(nonatomic,retain,readonly) UIView *inputPlaceholderView;
@property(nonatomic,assign) CGSize preferredContentSize;
@property(nonatomic,assign) UIPopoverArrowDirection permittedPopoverArrowDirections;

@end

@protocol MBInputPlaceholderViewDelegate <NSObject>

- (void)inputViewDidMoveToSuperview;
- (void)inputViewDidRemoveFromSuperview;

@end
