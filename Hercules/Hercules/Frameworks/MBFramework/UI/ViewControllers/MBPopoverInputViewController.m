//
//  MBPopoverInputViewController.m
//  MBFramework
//
//  Created by Arno Woestenburg on 10/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBPopoverInputViewController.h"

// Private class
@interface _MBInputPlaceholderView : UIView
@property(nonatomic,assign) id<MBInputPlaceholderViewDelegate> delegate;
@end


@interface MBPopoverInputViewController () <MBInputPlaceholderViewDelegate, UIPopoverControllerDelegate>

@property(nonatomic,retain) UIView *inputPlaceholderView;
@property(nonatomic,retain) UIPopoverController *popoverController;

@end

@implementation MBPopoverInputViewController

@synthesize inputAccessoryView = inputAccessoryView_;
@synthesize inputView = inputView_;
@synthesize inputPlaceholderView = inputPlaceholderView_;
@synthesize responderView = responderView_;
@synthesize popoverController = popoverController_;
@synthesize preferredContentSize = preferredContentSize_;
@synthesize permittedPopoverArrowDirections = permittedPopoverArrowDirections_;


//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithResponderView:(UIView *)responderView
{
    self = [super init];
    self.responderView = responderView;
    if (self) {
        [self MB_initialize];
    }
    return self;
}

- (void)dealloc
{
    [(_MBInputPlaceholderView *)self.inputPlaceholderView setDelegate:nil];
    self.inputAccessoryView = nil;
    self.inputView = nil;
    self.inputPlaceholderView = nil;
    self.popoverController = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_initialize
{
    self.preferredContentSize = CGSizeMake(320, CGFLOAT_MAX);
    self.permittedPopoverArrowDirections = UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight;
    // Height must be at least 1.
    self.inputPlaceholderView = [[[_MBInputPlaceholderView alloc] initWithFrame:CGRectMake(0, 0, 100, 1)] autorelease];
    [(_MBInputPlaceholderView *)self.inputPlaceholderView setDelegate:self];
}

- (UIPopoverController *)MB_createPopoverController
{
    // UIPopoverController view controller
    UIViewController *contentViewController = [[[UIViewController alloc] init] autorelease];
    
    CGSize preferredContentSize = [self MB_preferredContentSize];
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    if ([contentViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
        [contentViewController setPreferredContentSize:preferredContentSize];
    } else {
        [contentViewController setContentSizeForViewInPopover:preferredContentSize];
    }
#else 
    [contentViewController setPreferredContentSize:preferredContentSize];
#endif

    [contentViewController.view setBounds:CGRectMake(0, 0, preferredContentSize.width, preferredContentSize.height)];
    
    if (self.inputAccessoryView) {
        [contentViewController.view addSubview:self.inputAccessoryView];
        [self.inputAccessoryView setFrame:CGRectMake(0, 0, contentViewController.view.bounds.size.width, self.inputAccessoryView.bounds.size.height)];
        [self.inputAccessoryView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    }
    
    [self.inputView setFrame:UIEdgeInsetsInsetRect(contentViewController.view.bounds, UIEdgeInsetsMake(self.inputAccessoryView.bounds.size.height, 0, 0, 0))];
    [self.inputView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [contentViewController.view addSubview:self.inputView];
    
    UIPopoverController *popoverController = [[[UIPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    [popoverController setDelegate:self];
    return popoverController;
}

- (CGSize)MB_preferredContentSize
{
    CGSize preferredContentSize = self.preferredContentSize;
    
    if (preferredContentSize.height == CGFLOAT_MAX) {
        // Automatically determine content height
        preferredContentSize.height = self.inputAccessoryView.bounds.size.height + self.inputView.bounds.size.height;
    }
    return preferredContentSize;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MBInputPlaceholderViewDelegate

- (void)inputViewDidMoveToSuperview
{
    self.popoverController = [self MB_createPopoverController];
    
    [self.popoverController presentPopoverFromRect:self.responderView.bounds
                                            inView:self.responderView
                          permittedArrowDirections:self.permittedPopoverArrowDirections
                                          animated:YES];
}

- (void)inputViewDidRemoveFromSuperview
{
    [self.popoverController dismissPopoverAnimated:YES];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIPopoverControllerDelegate

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    BOOL result = YES;
    if ([self.responderView isFirstResponder]) {
        [self.responderView endEditing:YES];
    }
    return result;
}

@end

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - _MBInputPlaceholderView private class

@implementation _MBInputPlaceholderView

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUserInteractionEnabled:NO];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    // Whenever a view receives removeFromSuperview, and the view's superview was not already nil, the view will do [self willMoveToSuperview:nil].
    if (self.superview) {
        [self.delegate inputViewDidMoveToSuperview];
    } else {
        [self.delegate inputViewDidRemoveFromSuperview];
    }
}

@end
