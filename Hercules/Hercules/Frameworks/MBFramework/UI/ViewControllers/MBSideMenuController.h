//
//  MBSideMenuController.h
//  MBFramework
//
//  Created by Arno Woestenburg on 11/23/12.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBFrameworkDefines.h"

@class MBSideMenuController;

extern NSString *const MBSideMenuWillShowNotification;
extern NSString *const MBSideMenuDidShowNotification;
extern NSString *const MBSideMenuWillHideNotification;
extern NSString *const MBSideMenuDidHideNotification;
extern NSString *const MBSideMenuCancelledNotification;

extern NSString *const kSideMenuSideIdentifier;

/**
 Identifies the view of the Side menu controller.
 */
typedef NS_ENUM(NSInteger, MBSideMenuSideIdentifier) {
    /**
     The left view of the side menu controller.
     */
    MBSideMenuSideIdentifierLeft,
    /**
     The right view of the side menu controller.
     */
    MBSideMenuSideIdentifierRight,
    /**
     The center view of the side menu controller.
     */
    MBSideMenuSideIdentifierCenter,
};

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBSideMenuController : UIViewController<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
{
	CGFloat menuWidth_;
	UITableView *tableView_;
	UIView *headerView_;
	UIView *leftContainer_;
	UIView *rightContainer_;
	UIView *viewContainer_;
    UIView *shadingView_;
	UIViewController *selectedViewController_;
    CGFloat backShadingValue_;
    UIPanGestureRecognizer *panGestureRecognizer_;
    UIViewController *rightViewController_;
}

- (void)close;
- (void)closeWithAnimation:(BOOL)animated;

/**
 Sets the view controllers of the side menu controller.
 @param viewControllers The array of custom view controllers to display in the side menu interface. The order of the view controllers in this array corresponds to the display order in the side menu, with the controller at index 0 representing the top-most menu item, the controller at index 1 the next menu, and so on.
 @param section         An index number that identifies a section of the menu.
 @discussion When you assign a new set of view controllers at runtime, the side menu controller removes all of the old view controllers before installing the new ones. When changing the view controllers, the side menu controller remembers the view controller object that was previously selected and attempts to reselect it. If the selected view controller is no longer present, it attempts to select the view controller at the same index in the array as the previous selection. If that index is invalid, it selects the view controller at index 0.
 */
- (void)setViewControllers:(NSArray *)viewControllers forSection:(NSInteger)section;
- (NSArray *)viewControllersForSection:(NSInteger)section;
- (UIViewController *)viewControllerForItemAtIndexPath:(NSIndexPath *)indexPath;
- (UITableViewCell *)tableView:(UITableView *)tableView menuCellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)setSelectedIndexPath:(NSIndexPath *)indexPath;
- (void)revealMenuAnimated:(BOOL)animated;
- (void)revealLeftViewAnimated:(BOOL)animated;
- (void)revealRightViewAnimated:(BOOL)animated;

@property(nonatomic,assign)              UIViewController *selectedViewController;
@property(nonatomic,readonly)            UITableView *tableView;
@property(nonatomic,getter=isMenuHidden) BOOL menuHidden;
@property(nonatomic,retain)              UIView* headerView;
@property(nonatomic,readonly)            UIView* viewContainer;
@property(nonatomic,readonly)            NSIndexPath* selectedIndexPath;
@property(nonatomic)                     CGFloat menuWidth;
@property(nonatomic,retain)              UIViewController *rightViewController;
@property(nonatomic,readonly)            UIPanGestureRecognizer *panGestureRecognizer;
@property(nonatomic,readonly)            UIView *shadingView;
@property(nonatomic,assign)              CGFloat menuParallaxValue;
@property(nonatomic,copy)                NSShadow *viewContainerShadow;

// Deprecated:
- (void)setViewControllers:(NSArray *)viewControllers       DEPRECATED_MSG_ATTRIBUTE("Deprecated: use setViewControllers:forSection: instead");
- (NSUInteger)selectedIndex                                 DEPRECATED_MSG_ATTRIBUTE("Deprecated: use selectedIndexPath instead");
- (void)setSelectedIndex:(NSUInteger)selectedIndex          DEPRECATED_MSG_ATTRIBUTE("Deprecated: use selectedIndexPath instead");
- (void)setMenuHidden:(BOOL)hidden animated:(BOOL)animated  DEPRECATED_MSG_ATTRIBUTE("Deprecated: use revealMenuAnimated: instead");
- (void)tableViewCell:(UITableViewCell *)menuCell menuCellStyleForRowAtIndexPath:(NSIndexPath *)indexPath   DEPRECATED_MSG_ATTRIBUTE("Deprecated: use menuCellForRowAtIndexPath: instead");


@end
