//
//  MBSideMenuController.m
//  MBFramework
//
//  Created by Arno Woestenburg on 11/23/12.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import "MBSideMenuController.h"
#import "MBMutableCollection.h"
#import <QuartzCore/QuartzCore.h>

NSString *const MBSideMenuWillShowNotification  = @"MBSideMenuWillShowNotification";
NSString *const MBSideMenuDidShowNotification   = @"MBSideMenuDidShowNotification";
NSString *const MBSideMenuWillHideNotification  = @"MBSideMenuWillHideNotification";
NSString *const MBSideMenuDidHideNotification   = @"MBSideMenuDidHideNotification";
NSString *const MBSideMenuCancelledNotification = @"MBSideMenuCancelledNotification";

NSString *const kSideMenuSideIdentifier          = @"kSideMenuSideIdentifier";

@interface MBSideMenuController ()
{
    UIView *overlayView_;
    NSMapTable *menuItemCells_;
    CGPoint panStartLocation_;
    MBMutableCollection *viewControllers_;
    MBSideMenuSideIdentifier currentSide_;
    MBSideMenuSideIdentifier upcomingSide_;
    BOOL needAdjustScrollViewInsets_;
}
@end

@implementation MBSideMenuController

@synthesize headerView = headerView_;
@synthesize menuParallaxValue = menuParallaxValue_;
@synthesize menuWidth = menuWidth_;
@synthesize panGestureRecognizer = panGestureRecognizer_;
@synthesize rightViewController = rightViewController_;
@synthesize selectedIndexPath = selectedIndexPath_;
@synthesize selectedViewController = selectedViewController_;
@synthesize tableView = tableView_;
@synthesize viewContainer = viewContainer_;
@synthesize shadingView = shadingView_;
@synthesize viewContainerShadow = viewContainerShadow_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_adjustScrollViewInsetsIfNeeded
{
    if (needAdjustScrollViewInsets_) {
        needAdjustScrollViewInsets_ = NO;
        // Adjust table view insets.
        UIEdgeInsets contentInsets = tableView_.contentInset;
        contentInsets.top += self.topLayoutGuide.length;
        contentInsets.bottom += self.bottomLayoutGuide.length;
        [tableView_ setContentInset:contentInsets];
        [tableView_ setContentOffset:CGPointMake(0.0f, -contentInsets.top)];
        contentInsets = tableView_.scrollIndicatorInsets;
        contentInsets.top += self.topLayoutGuide.length;
        contentInsets.bottom += self.bottomLayoutGuide.length;
        [tableView_ setScrollIndicatorInsets:contentInsets];
    }
}

- (void)MB_layoutSubviews
{
    CGRect tableFrame = tableView_.frame;
    
    if (headerView_ != nil) {
        tableFrame.origin.y = headerView_.bounds.size.height;
        tableFrame.size.height = leftContainer_.bounds.size.height - headerView_.bounds.size.height;
        [headerView_ setFrame:CGRectMake(0.0, 0.0, leftContainer_.bounds.size.width, headerView_.bounds.size.height)];
    } else {
        tableFrame.origin.y = 0;
        tableFrame.size.height = leftContainer_.bounds.size.height;
    }
    
    [tableView_ setFrame:tableFrame];
}

- (void)MB_layoutViewControllers
{
    [selectedViewController_.view setFrame:viewContainer_.bounds];
}

- (void)MB_setShadowForViewContainer
{
    NSAssert(viewContainerShadow_ != nil, @"Shouldn't be here");
    
    [viewContainer_.layer setShadowOffset:viewContainerShadow_.shadowOffset];
    [viewContainer_.layer setShadowColor:[(UIColor *)viewContainerShadow_.shadowColor CGColor]];
    [viewContainer_.layer setShadowRadius:viewContainerShadow_.shadowBlurRadius];
    [viewContainer_.layer setShadowOpacity:1.0f];
    CGFloat margin = 20;
    [viewContainer_.layer setShadowPath:[[UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(viewContainer_.bounds, UIEdgeInsetsMake(-margin, 0, -2*margin, 0))] CGPath]];
}

- (void)MB_moveViewContainerToPosition:(CGPoint)position animated:(BOOL)animated
{
    // Set limits.
    position.x = MAX(-rightContainer_.bounds.size.width, MIN(leftContainer_.bounds.size.width, position.x));
    CGAffineTransform mainTranslation;
    CGAffineTransform leftTranslation = CGAffineTransformIdentity;
    CGAffineTransform rightTranslation = CGAffineTransformIdentity;
    
    CGFloat shadingAlpha = 1.0;
    
    if (0 != position.x) {
        mainTranslation = CGAffineTransformMakeTranslation(position.x, 0);
        if (overlayView_) {
            [overlayView_ setHidden:NO];
            [viewContainer_ bringSubviewToFront:overlayView_];
        } else {
            [self MB_addViewOverlay];
        }
        
        CGFloat visibleRatio = 0.0;
        
        if (position.x > 0) {   // Showing left view
            
            visibleRatio = position.x / leftContainer_.bounds.size.width;
            if (leftContainer_.isHidden) {
                [leftContainer_ setHidden:NO];
                [self MB_sendNotification:MBSideMenuWillShowNotification sideIdentifier:MBSideMenuSideIdentifierLeft];
            }
        } else {   // Showing right view
            visibleRatio = ABS(position.x) / rightContainer_.bounds.size.width;
            if (rightContainer_ != nil && rightContainer_.isHidden) {
                [rightContainer_ setHidden:NO];
                [self MB_sendNotification:MBSideMenuWillShowNotification sideIdentifier:MBSideMenuSideIdentifierRight];
            }
        }
        shadingAlpha = (1 - visibleRatio);
        
    } else {
        if(animated == YES) {
            if(leftContainer_.isHidden == NO){
                [self MB_sendNotification:MBSideMenuWillHideNotification sideIdentifier:MBSideMenuSideIdentifierLeft];
            }
            
            if(rightContainer_.isHidden == NO) {
                [self MB_sendNotification:MBSideMenuWillHideNotification sideIdentifier:MBSideMenuSideIdentifierRight];
            }
        }
        // position.x is 0
        mainTranslation = CGAffineTransformIdentity;
        [overlayView_ setHidden:YES];
    }
    
    // Side translation
    if (menuParallaxValue_ > 0) {
        leftTranslation = CGAffineTransformMakeTranslation(((-leftContainer_.bounds.size.width + position.x) * menuParallaxValue_), 0);
        rightTranslation = CGAffineTransformMakeTranslation(((rightContainer_.bounds.size.width + position.x) * menuParallaxValue_), 0);
    }
    
    if (animated) {
        [UIView animateWithDuration:0.2
                         animations:^{
                             [viewContainer_ setTransform:mainTranslation];
                             [leftContainer_ setTransform:leftTranslation];
                             [rightContainer_ setTransform:rightTranslation];
                             [shadingView_ setAlpha:shadingAlpha];
                         }
                         completion:^(BOOL finished) {
                             if (viewContainer_.transform.tx <= 0 && leftContainer_.isHidden == NO) {
                                 [leftContainer_ setHidden:YES];
                                 [self MB_sendNotification:MBSideMenuDidHideNotification sideIdentifier:MBSideMenuSideIdentifierLeft];
                             } else if (leftContainer_.isHidden == NO) {
                                 [self MB_sendNotification:MBSideMenuDidShowNotification sideIdentifier:MBSideMenuSideIdentifierLeft];
                             }
                             if (viewContainer_.transform.tx >= 0 && rightContainer_.isHidden == NO) {
                                 [rightContainer_ setHidden:YES];
                                 [self MB_sendNotification:MBSideMenuDidHideNotification sideIdentifier:MBSideMenuSideIdentifierRight];
                             } else if(rightContainer_.isHidden == NO) {
                                 [self MB_sendNotification:MBSideMenuDidShowNotification sideIdentifier:MBSideMenuSideIdentifierRight];
                             }
                             
                             if(rightContainer_.isHidden == YES && leftContainer_.isHidden == YES) {
                                 currentSide_ = MBSideMenuSideIdentifierCenter;
                                 upcomingSide_ = currentSide_;
                             }
                         }];
    } else {
        [viewContainer_ setTransform:mainTranslation];
        [leftContainer_ setTransform:leftTranslation];
        [rightContainer_ setTransform:rightTranslation];
        [shadingView_ setAlpha:shadingAlpha];
        
        if (viewContainer_.transform.tx <= 0 && leftContainer_.isHidden == NO) {
            [leftContainer_ setHidden:YES];
            [self MB_sendNotification:MBSideMenuDidHideNotification sideIdentifier:MBSideMenuSideIdentifierLeft];
        }
        
        if (viewContainer_.transform.tx >= 0 && rightContainer_.isHidden == NO) {
            [rightContainer_ setHidden:YES];
            [self MB_sendNotification:MBSideMenuDidHideNotification sideIdentifier:MBSideMenuSideIdentifierRight];
        }
    }
    
    if(rightContainer_.isHidden == YES && leftContainer_.isHidden == YES) {
        currentSide_ = MBSideMenuSideIdentifierCenter;
        upcomingSide_ = currentSide_;
    }
}

- (void)MB_addViewOverlay
{
    overlayView_ = [[[UIView alloc] initWithFrame:viewContainer_.bounds] autorelease];
    [overlayView_ addGestureRecognizer:[[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MB_overlayTapped:)] autorelease]];
    [viewContainer_ addSubview:overlayView_];
}

- (void)MB_sendNotification:(NSString *)name sideIdentifier:(MBSideMenuSideIdentifier)sideIdentifier
{
    NSDictionary* userInfo = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:sideIdentifier] forKey:kSideMenuSideIdentifier];
    bool needPost = YES;
    if([name isEqualToString:MBSideMenuWillShowNotification]) {
        if(upcomingSide_ != sideIdentifier) {
            upcomingSide_ = sideIdentifier;
        } else {
            needPost = NO;
        }
    } else if([name isEqualToString:MBSideMenuDidShowNotification]) {
        if(currentSide_ != sideIdentifier) {
            currentSide_  = sideIdentifier;
        } else {
            name = MBSideMenuCancelledNotification;
        }
        upcomingSide_ = currentSide_;
    } else if([name isEqualToString:MBSideMenuWillHideNotification]) {
        if(upcomingSide_ != MBSideMenuSideIdentifierCenter && currentSide_ != MBSideMenuSideIdentifierCenter) {
            upcomingSide_ = MBSideMenuSideIdentifierCenter;
        } else {
            needPost = NO;
        }
    } else if([name isEqualToString:MBSideMenuDidHideNotification]) {
        if(currentSide_ != MBSideMenuSideIdentifierCenter) {
            currentSide_ = sideIdentifier;
        } else {
            name = MBSideMenuCancelledNotification;
        }
        upcomingSide_ = currentSide_;
    } else if([name isEqualToString:MBSideMenuCancelledNotification]) {
        upcomingSide_ = currentSide_;
    }
    
    if(needPost == YES) {
        //MBLogDebug(@"MBSideMenuNotification: %@, SideIdentifier %d", name, sideIdentifier);
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:userInfo];
    }
}

- (void)MB_updateSelectedViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.isViewLoaded) {
        // Remove the current view controller
        [selectedViewController_ removeFromParentViewController];
        [selectedViewController_ autorelease];
        UIView *oldView = selectedViewController_.view;
        UIView *newView = viewController.view;
        
        // Set new view controller
        selectedViewController_ = [viewController retain];
        if (selectedViewController_) {
            [selectedIndexPath_ release];
            selectedIndexPath_ = [[viewControllers_ indexPathOfObject:viewController] retain];
            NSIndexPath *selectedRow = [tableView_ indexPathForSelectedRow];
            if (selectedRow == nil || ![selectedRow isEqual:selectedIndexPath_]) {
                [tableView_ selectRowAtIndexPath:selectedIndexPath_ animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            [viewController.view setFrame:viewContainer_.bounds];
            [self addChildViewController:viewController];
            [viewContainer_ addSubview:newView];
        }
        if (animated) {
            [UIView transitionFromView:oldView
                                toView:newView
                              duration:0.2
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            completion:^(BOOL finished) {
                                [oldView removeFromSuperview];
                            }];
            
        } else {
            [oldView removeFromSuperview];
        }
    } else {
        [selectedViewController_ release];
        selectedViewController_ = [viewController retain];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init {
    self = [super init];
    if (self) {
        menuWidth_          = 200.0;
        menuItemCells_      = [[NSMapTable alloc] init];
        viewControllers_    = [[MBMutableCollection alloc] init];
        menuParallaxValue_  = 0.3;     // Amount of parallax effect of the side views.
        currentSide_        = MBSideMenuSideIdentifierCenter;
        upcomingSide_       = MBSideMenuSideIdentifierCenter;
        needAdjustScrollViewInsets_ = NO;
        // Shadow behind main view
        viewContainerShadow_ = [[NSShadow alloc] init];
        [viewContainerShadow_ setShadowOffset:CGSizeZero];
        [viewContainerShadow_ setShadowBlurRadius:10.0];
        [viewContainerShadow_ setShadowColor:[UIColor colorWithWhite:0.0 alpha:1.0]];
    }
    return self;
}

- (void)dealloc {
    [headerView_ release];
    [viewControllers_ release];
    [menuItemCells_ release];
    [selectedIndexPath_ release];
    [selectedViewController_ release];
    [viewContainerShadow_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Left container to hold the menu
    leftContainer_ = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, menuWidth_, self.view.bounds.size.height)] autorelease];
    [leftContainer_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
    leftContainer_.hidden = YES;
    [self.view addSubview:leftContainer_];
    
    // Table view for navigation
    tableView_ = [[[UITableView alloc] initWithFrame:leftContainer_.bounds style:UITableViewStylePlain] autorelease];
    [tableView_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [tableView_ setDelegate:self];
    [tableView_ setDataSource:self];
    [tableView_ setShowsVerticalScrollIndicator:NO];
    [leftContainer_ addSubview:tableView_];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    // Main view container
    viewContainer_ = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
    [viewContainer_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [viewContainer_ setBackgroundColor:[UIColor clearColor]];
    
    // Shading view
    shadingView_ = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
    [shadingView_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [shadingView_ setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.5]];
    [self.view addSubview:shadingView_];
    
    // Pan gesture
    panGestureRecognizer_ = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(MB_viewContainerPan:)] autorelease];
    //http://stackoverflow.com/questions/9953706/unable-to-move-rows-in-a-uitableview-even-when-grabber-are-displaying
    [panGestureRecognizer_ setCancelsTouchesInView:YES];
    [panGestureRecognizer_ setDelegate:self];
    [viewContainer_ addGestureRecognizer:panGestureRecognizer_];
    
    [self.view addSubview:viewContainer_];
    [self.view bringSubviewToFront:viewContainer_];
    
    if (viewControllers_.allObjects.count > 0) {
        if (selectedViewController_ != nil) {
            [self MB_updateSelectedViewController:selectedViewController_ animated:NO];
        } else {
            [self MB_updateSelectedViewController:[viewControllers_ objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] animated:NO];
        }
    }
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    // >= iOS 7
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        // Default is YES.
        needAdjustScrollViewInsets_ = [self automaticallyAdjustsScrollViewInsets];
    }
#else 
    // Default is YES.
    needAdjustScrollViewInsets_ = [self automaticallyAdjustsScrollViewInsets];
#endif

}

- (void)viewWillAppear:(BOOL)animated
{
    [self MB_layoutViewControllers];
    [super viewWillAppear:animated];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if (viewContainerShadow_) {
        [self MB_setShadowForViewContainer];
    }
    [self MB_adjustScrollViewInsetsIfNeeded];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if ([selectedViewController_ isKindOfClass:[UINavigationController class]]) {
        [((UINavigationController *)selectedViewController_).navigationBar performSelector:@selector(sizeToFit) withObject:nil afterDelay:(0.5f * duration)];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)close
{
    [self closeWithAnimation:YES];
}

- (void)closeWithAnimation:(BOOL)animated
{
    [self MB_moveViewContainerToPosition:CGPointZero animated:animated];
}

- (void)setHeaderView:(UIView *)headerView {
    if (headerView_ != headerView) {
        [headerView_ removeFromSuperview];
        [headerView_ release];
        
        headerView_ = [headerView retain];
        if (headerView_) {
            [headerView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
            [leftContainer_ addSubview:headerView_];
            [self MB_layoutSubviews];
        }
    }
}

- (void)setMenuHidden:(BOOL)hidden animated:(BOOL)animated
{
    // Deprecated: use revealMenuAnimated: instead
    if (hidden) {
        [self close];
    } else {
        [self revealMenuAnimated:animated];
    }
}

- (void)revealMenuAnimated:(BOOL)animated
{
    [self revealLeftViewAnimated:animated];
}

- (void)revealLeftViewAnimated:(BOOL)animated
{
    [self MB_moveViewContainerToPosition:CGPointMake(menuWidth_, 0) animated:animated];
}

- (void)revealRightViewAnimated:(BOOL)animated
{
    [self MB_moveViewContainerToPosition:CGPointMake(-rightContainer_.bounds.size.width, 0) animated:animated];
}

- (void)setMenuWidth:(CGFloat)menuWidth {
    if (menuWidth_ != menuWidth) {
        menuWidth_ = menuWidth;
        if (leftContainer_ != nil) {
            CGRect frame = leftContainer_.frame;
            frame.size.width = menuWidth;
            [leftContainer_ setFrame:frame];
        }
    }
}

- (void)setSelectedIndexPath:(NSIndexPath *)indexPath
{
    if (![selectedIndexPath_ isEqual:indexPath]) {
        [selectedIndexPath_ release];
        selectedIndexPath_ = [indexPath copy];
        UIViewController* viewController = [viewControllers_ objectAtIndexPath:selectedIndexPath_];
        if (viewController) {
            [self setSelectedViewController:viewController];
        }
    }
}

- (void)setSelectedViewController:(UIViewController *)selectedViewController {
    // The specified view controller must be in the viewControllers array.
    if (![viewControllers_ containsObject:selectedViewController]) {
        [NSException raise:NSInvalidArgumentException format:@"The specified view controller must be in the viewControllers array."];
    }
   
    if (selectedViewController_ != selectedViewController) {
        [self MB_updateSelectedViewController:selectedViewController animated:NO];
    } else {
        // Pop to root when selecting a viewcontroller which is already selected.
        if ([selectedViewController_ isKindOfClass:[UINavigationController class]]) {
            [((UINavigationController *)selectedViewController_) popToRootViewControllerAnimated:YES];
        }
    }
    [tableView_ scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionNone animated:YES];
    [self closeWithAnimation:YES];
}

- (void)setViewControllers:(NSArray *)viewControllers {
    [self setViewControllers:viewControllers forSection:0];
}

- (NSArray *)viewControllers {
    // Deprecated: use viewControllers:forSection: instead
    return [self viewControllersForSection:0];
}

- (void)setViewControllers:(NSArray *)viewControllers forSection:(NSInteger)section
{
    UIViewController *newSelectedViewController = selectedViewController_;
    
    if (!selectedIndexPath_ || selectedIndexPath_.section == section) {
        // Attempt to keep the current selected view controller selected. If it exists in the new array.
        if ([viewControllers containsObject:selectedViewController_]) {
            newSelectedViewController = selectedViewController_;
        } else {
            NSInteger index = NSNotFound;
            if ([viewControllers_ numberOfSections] > section) {
                NSArray *oldViewControllers = [viewControllers_ arrayAtSection:section];
                // Try to select the view controller with the same index
                index = [oldViewControllers indexOfObject:selectedViewController_];
                
            }
            if (index != NSNotFound && index < viewControllers.count) {
                newSelectedViewController = [viewControllers objectAtIndex:index];
            } else {
                newSelectedViewController = [viewControllers firstObject];
            }
        }
    }
    
    [menuItemCells_ removeAllObjects];
    if ([viewControllers_ numberOfSections] > section) {
        [viewControllers_ replaceArrayAtSection:section withArray:viewControllers];
    } else {
        [viewControllers_ insertArray:viewControllers atSection:section];
    }
    
    [self.tableView reloadData];
    if (newSelectedViewController != selectedViewController_) {
        [self MB_updateSelectedViewController:newSelectedViewController animated:YES];
    } else if (selectedIndexPath_) {
        // Restore selection (after reloadData)
        [tableView_ selectRowAtIndexPath:selectedIndexPath_ animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

- (NSArray *)viewControllersForSection:(NSInteger)section {
    return [viewControllers_ arrayAtSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView menuCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Override to use a different UITableViewCell class.
    return [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
}

- (UIViewController *)viewControllerForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [viewControllers_ objectAtIndexPath:indexPath];
}

- (void)tableViewCell:(UITableViewCell *)menuCell menuCellStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Override to set styles for the table menu cell.
}

- (BOOL)shouldSelectViewController:(UIViewController *)viewController {
    // Override for custom behaviour.
    return YES;
}

- (void)didSelectViewController:(UIViewController *)viewController {
    // Override for custom behaviour.
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSUInteger)selectedIndex
{
    // Deprecated: use selectedIndexPath instead.
    return selectedIndexPath_.row;
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    // Deprecated: use selectedIndexPath instead.
    [self setSelectedIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0]];
}

- (void)setRightViewController:(UIViewController *)rightViewController {
    if (rightViewController_ != rightViewController) {
        // Remove old view controller
        [rightViewController_ release];
        [rightViewController_ removeFromParentViewController];
        [rightViewController_.view removeFromSuperview];
        
        rightViewController_ = [rightViewController retain];
        
        if (!rightContainer_) {
            rightContainer_ = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
            [rightContainer_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin];
            rightContainer_.hidden = YES;
            [self.view insertSubview:rightContainer_ belowSubview:shadingView_];
        }
        [rightContainer_ setFrame:CGRectMake(self.view.bounds.size.width - rightViewController_.view.bounds.size.width, 0, rightViewController_.view.bounds.size.width, self.view.bounds.size.height)];
        [rightContainer_ addSubview:rightViewController_.view];
        [self addChildViewController:rightViewController_];
    }
}

- (BOOL)isMenuHidden
{
    return [leftContainer_ isHidden];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self shouldSelectViewController:[viewControllers_ objectAtIndexPath:indexPath]] ? indexPath : nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController = [viewControllers_ objectAtIndexPath:indexPath];
    [self setSelectedViewController:viewController];
    [self didSelectViewController:viewController];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController* viewController = [viewControllers_ objectAtIndexPath:indexPath];
    UITableViewCell *cell = [menuItemCells_ objectForKey:viewController];
    
    // Reusable cells are not used, so there must always be a cell for a view controller. If not, create one
    // and add to the map table.
    if (cell == nil) {
        cell = [self tableView:tableView menuCellForRowAtIndexPath:indexPath];
        if (cell.textLabel.text == nil) {
            // Default text when no title has been set
            [cell.textLabel setText:viewController.title];
        }
        [menuItemCells_ setObject:cell forKey:viewController];
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [viewControllers_ numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [viewControllers_ numberOfObjectsInSection:section];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponse

- (void)MB_overlayTapped:(UITapGestureRecognizer *) sender {
    [self close];
}

- (void)MB_viewContainerPan:(UIPanGestureRecognizer *)sender {
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            panStartLocation_ = CGPointMake(viewContainer_.transform.tx, 0);
            
            if (currentSide_ == MBSideMenuSideIdentifierLeft) {
                [self MB_sendNotification:MBSideMenuWillHideNotification sideIdentifier:MBSideMenuSideIdentifierLeft];
            } else if (currentSide_ == MBSideMenuSideIdentifierRight) {
                [self MB_sendNotification:MBSideMenuWillHideNotification sideIdentifier:MBSideMenuSideIdentifierRight];
            }
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [sender translationInView:sender.view.superview];
            CGPoint location = viewContainer_.frame.origin;
            location.x = panStartLocation_.x + translation.x;
            [self MB_moveViewContainerToPosition:location animated:NO];
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            CGPoint translation = [sender translationInView:sender.view.superview];
            CGPoint location = viewContainer_.frame.origin;
            CGPoint velocity = [sender velocityInView:self.view];
            
            location.x = panStartLocation_.x + translation.x + (0.2 * velocity.x);
            
            CGFloat currentPosition = viewContainer_.transform.tx;
            
            if ((velocity.x > 0)) { // Direction towards right
                // Prevent shooting through to the other side
                if (currentPosition < 0) {
                    location.x = MIN(location.x, 0);
                }
            } else if ((velocity.x < 0)) {  // Direction towards left
                // Prevent shooting through to the other side
                if (currentPosition > 0) {
                    location.x = MAX(location.x, 0);
                }
            }
            // Snap to left, right or middle
            if (location.x > (leftContainer_.bounds.size.width * 0.5)) {
                location.x = leftContainer_.bounds.size.width;
            } else if (location.x < (-rightContainer_.bounds.size.width * 0.5)) {
                location.x = -rightContainer_.bounds.size.width;
            } else {
                location.x = 0;
            }
            
            [self MB_moveViewContainerToPosition:CGPointMake(location.x, viewContainer_.frame.origin.y) animated:YES];
        }
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    UIView *view = [gestureRecognizer view];
    CGPoint translation = [gestureRecognizer translationInView:[view superview]];
    
#ifdef ENABLE_SIDEMENU_LOGS
    NSLog(@"gestureRecognizerShouldBegin: %@", ((fabsf(translation.x) > fabsf(translation.y)) ? @"YES" : @"NO"));
#endif
    // Check for horizontal gesture
    return (fabsf(translation.x) > fabsf(translation.y));
}

@end
