//
//  MBSidePanelController.h
//  MBFramework
//
//  Created by Marco Jonker on 01/21/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifdef DEBUG
//    #define ENABLE_SidePanel_LOGS
#endif

@class MBSidePanelViewController;

extern NSString *const MBSidePanelWillShowNotification;
extern NSString *const MBSidePanelDidShowNotification;
extern NSString *const MBSidePanelWillHideNotification;
extern NSString *const MBSidePanelDidHideNotification;
extern NSString *const MBSidePanelCancelledNotification;

extern NSString *const kSidePanelSideIdentifier;

/**
 Identifies the view of the Side panel controller.
 */
typedef NS_ENUM(NSInteger, MBSidePanelSideIdentifier) {
    /**
     The left view of the side panel controller.
     */
    MBSidePanelSideIdentifierLeft,
    /**
     The right view of the side panel controller.
     */
    MBSidePanelSideIdentifierRight,
    /**
     The center view of the side panel controller.
     */
    MBSidePanelSideIdentifierCenter,
};

@class MBSidePanelViewController;

@protocol MBSidePanelViewControllerDelegate <NSObject>

@optional
- (BOOL)sidePanelViewController:(MBSidePanelViewController *)controller shouldBeginSlideGesture:(UIPanGestureRecognizer *)gestureRecognizer;

@end

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBSidePanelViewController : UIViewController<UIGestureRecognizerDelegate>
{
    UIView *shadingView_;
    CGFloat backShadingValue_;
    CGFloat sideSlideFactor_;
    UIPanGestureRecognizer *panGestureRecognizer_;

    UIViewController *centerViewController_;
    UIViewController *leftViewController_;
    UIViewController *rightViewController_;
}

- (void)close;
- (void)closeWithAnimation:(BOOL)animated;
- (void)revealLeftViewAnimated:(BOOL)animated;
- (void)revealRightViewAnimated:(BOOL)animated;
- (void)setCenterViewController:(UIViewController *)centerViewController animated:(BOOL)animated;
- (void)setLeftViewController:(UIViewController *)leftViewController withWidth:(CGFloat)width;
- (void)setRightViewController:(UIViewController *)rightViewController withWidth:(CGFloat)width;

@property(nonatomic,readonly) UIViewController *centerViewController;
@property(nonatomic,readonly) UIViewController *rightViewController;
@property(nonatomic,readonly) UIViewController *leftViewController;
@property(nonatomic,readonly) UIPanGestureRecognizer *panGestureRecognizer;
@property(nonatomic,readonly) UIView *shadingView;
@property(nonatomic,readonly) UIView* leftContainer;
@property(nonatomic,readonly) UIView* rightContainer;
@property(nonatomic,readonly) UIView* centerContainer;
/**
 *  The object that acts as the delegate of the receiving sidepanel view controller.
 *
 *  @discussion The delegate must adopt the MBSidePanelViewControllerDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBSidePanelViewControllerDelegate> delegate;


@end
