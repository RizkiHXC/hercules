//
//  MBSidePanelController.m
//  MBFramework
//
//  Created by Marco Jonker on 01/21/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBSidePanelViewController.h"
#import "MBMutableCollection.h"
#import <QuartzCore/QuartzCore.h>

NSString *const MBSidePanelWillShowNotification  = @"MBSidePanelWillShowNotification";
NSString *const MBSidePanelDidShowNotification   = @"MBSidePanelDidShowNotification";
NSString *const MBSidePanelWillHideNotification  = @"MBSidePanelWillHideNotification";
NSString *const MBSidePanelDidHideNotification   = @"MBSidePanelDidHideNotification";
NSString *const MBSidePanelCancelledNotification = @"MBSidePanelCancelledNotification";

NSString *const kSidePanelSideIdentifier          = @"kSidePanelSideIdentifier";

@interface MBSidePanelViewController ()
{
    UIView *overlayView_;
    CGPoint panStartLocation_;
    MBSidePanelSideIdentifier currentSide_;
    MBSidePanelSideIdentifier upcomingSide_;
    
    UIView *leftContainer_;
	UIView *rightContainer_;
    UIView *centerContainer_;
}
@end

@implementation MBSidePanelViewController

@synthesize delegate = delegate_;
@synthesize panGestureRecognizer = panGestureRecognizer_;
@synthesize rightViewController = rightViewController_;
@synthesize leftViewController = leftViewController_;
@synthesize centerViewController = centerViewController_;
@synthesize shadingView = shadingView_;
@synthesize leftContainer = leftContainer_;
@synthesize rightContainer = rightContainer_;
@synthesize centerContainer = centerContainer_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_setShadowForViewContainer
{
    [centerContainer_.layer setShadowOffset:CGSizeZero];
    [centerContainer_.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [centerContainer_.layer setShadowRadius:10.0];
    [centerContainer_.layer setShadowOpacity:1.0];
    CGFloat margin = 20;
    [centerContainer_.layer setShadowPath:[[UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(centerContainer_.bounds, UIEdgeInsetsMake(-margin, 0, -2*margin, 0))] CGPath]];
}

- (void)MB_moveViewContainerToPosition:(CGPoint)position withAnimation:(BOOL)animated
{
    // Set limits.
    position.x = MAX(-rightContainer_.bounds.size.width, MIN(leftContainer_.bounds.size.width, position.x));
    CGAffineTransform mainTranslation;
    CGAffineTransform leftTranslation = CGAffineTransformIdentity;
    CGAffineTransform rightTranslation = CGAffineTransformIdentity;
    
    CGFloat shadingAlpha = 1.0;
    
    if (0 != position.x) {
        mainTranslation = CGAffineTransformMakeTranslation(position.x, 0);
        if (overlayView_) {
            [overlayView_ setHidden:NO];
            [centerContainer_ bringSubviewToFront:overlayView_];
        } else {
            [self MB_addViewOverlay];
        }
        
        CGFloat visibleRatio = 0.0;
        
        if (position.x > 0) {   // Showing left view
            
            visibleRatio = position.x / leftContainer_.bounds.size.width;
            if (leftContainer_.isHidden) {
                [leftContainer_ setHidden:NO];
                [self MB_sendNotification:MBSidePanelWillShowNotification sideIdentifier:MBSidePanelSideIdentifierLeft];
            }
        } else {   // Showing right view
            visibleRatio = ABS(position.x) / rightContainer_.bounds.size.width;
            if (rightContainer_ != nil && rightContainer_.isHidden) {
                [rightContainer_ setHidden:NO];
                [self MB_sendNotification:MBSidePanelWillShowNotification sideIdentifier:MBSidePanelSideIdentifierRight];
            }
        }
        shadingAlpha = (1 - visibleRatio);
        
    } else {
        if(animated == YES) {
            if(leftContainer_.isHidden == NO){
                [self MB_sendNotification:MBSidePanelWillHideNotification sideIdentifier:MBSidePanelSideIdentifierLeft];
            }
            
            if(rightContainer_.isHidden == NO) {
                [self MB_sendNotification:MBSidePanelWillHideNotification sideIdentifier:MBSidePanelSideIdentifierRight];
            }
        }
        // position.x is 0
        mainTranslation = CGAffineTransformIdentity;
        [overlayView_ setHidden:YES];
    }
    
    // Side translation
    if (sideSlideFactor_ > 0) {
        leftTranslation = CGAffineTransformMakeTranslation(((-leftContainer_.bounds.size.width + position.x) * sideSlideFactor_), 0);
        rightTranslation = CGAffineTransformMakeTranslation(((rightContainer_.bounds.size.width + position.x) * sideSlideFactor_), 0);
    }
    
    if (animated) {
        [UIView animateWithDuration:0.2
                         animations:^{
                             [centerContainer_ setTransform:mainTranslation];
                             [leftContainer_ setTransform:leftTranslation];
                             [rightContainer_ setTransform:rightTranslation];
                             [shadingView_ setAlpha:shadingAlpha];
                         }
                         completion:^(BOOL finished) {
                             if (centerContainer_.transform.tx <= 0 && leftContainer_.isHidden == NO) {
                                 [leftContainer_ setHidden:YES];
                                 [self MB_sendNotification:MBSidePanelDidHideNotification sideIdentifier:MBSidePanelSideIdentifierLeft];
                             } else if (leftContainer_.isHidden == NO) {
                                 [self MB_sendNotification:MBSidePanelDidShowNotification sideIdentifier:MBSidePanelSideIdentifierLeft];
                             }
                             if (centerContainer_.transform.tx >= 0 && rightContainer_.isHidden == NO) {
                                 [rightContainer_ setHidden:YES];
                                 [self MB_sendNotification:MBSidePanelDidHideNotification sideIdentifier:MBSidePanelSideIdentifierRight];
                             } else if(rightContainer_.isHidden == NO) {
                                 [self MB_sendNotification:MBSidePanelDidShowNotification sideIdentifier:MBSidePanelSideIdentifierRight];
                             }
                             
                             if(rightContainer_.isHidden == YES && leftContainer_.isHidden == YES) {
                                 currentSide_ = MBSidePanelSideIdentifierCenter;
                                 upcomingSide_ = currentSide_;
                             }
                         }];
    } else {
        [centerContainer_ setTransform:mainTranslation];
        [leftContainer_ setTransform:leftTranslation];
        [rightContainer_ setTransform:rightTranslation];
        [shadingView_ setAlpha:shadingAlpha];
        
        if (centerContainer_.transform.tx <= 0 && leftContainer_.isHidden == NO) {
            [leftContainer_ setHidden:YES];
            [self MB_sendNotification:MBSidePanelDidHideNotification sideIdentifier:MBSidePanelSideIdentifierLeft];
        }
        
        if (centerContainer_.transform.tx >= 0 && rightContainer_.isHidden == NO) {
            [rightContainer_ setHidden:YES];
            [self MB_sendNotification:MBSidePanelDidHideNotification sideIdentifier:MBSidePanelSideIdentifierRight];
        }
    }
    
    if(rightContainer_.isHidden == YES && leftContainer_.isHidden == YES) {
        currentSide_ = MBSidePanelSideIdentifierCenter;
        upcomingSide_ = currentSide_;
    }
}

- (void)MB_addViewOverlay
{
    overlayView_ = [[[UIView alloc] initWithFrame:centerContainer_.bounds] autorelease];
    [overlayView_ addGestureRecognizer:[[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MB_overlayTapped:)] autorelease]];
    [centerContainer_ addSubview:overlayView_];
}

- (void)MB_sendNotification:(NSString *)name sideIdentifier:(MBSidePanelSideIdentifier)sideIdentifier
{
    NSDictionary* userInfo = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:sideIdentifier] forKey:kSidePanelSideIdentifier];
    bool needPost = YES;
    if([name isEqualToString:MBSidePanelWillShowNotification]) {
        if(upcomingSide_ != sideIdentifier) {
            upcomingSide_ = sideIdentifier;
        } else {
            needPost = NO;
        }
    } else if([name isEqualToString:MBSidePanelDidShowNotification]) {
        if(currentSide_ != sideIdentifier) {
            currentSide_  = sideIdentifier;
        } else {
            name = MBSidePanelCancelledNotification;
        }
        upcomingSide_ = currentSide_;
    } else if([name isEqualToString:MBSidePanelWillHideNotification]) {
        if(upcomingSide_ != MBSidePanelSideIdentifierCenter && currentSide_ != MBSidePanelSideIdentifierCenter) {
            upcomingSide_ = MBSidePanelSideIdentifierCenter;
        } else {
            needPost = NO;
        }
    } else if([name isEqualToString:MBSidePanelDidHideNotification]) {
        if(currentSide_ != MBSidePanelSideIdentifierCenter) {
            currentSide_ = sideIdentifier;
        } else {
            name = MBSidePanelCancelledNotification;
        }
        upcomingSide_ = currentSide_;
    } else if([name isEqualToString:MBSidePanelCancelledNotification]) {
        upcomingSide_ = currentSide_;
    }
    
    if(needPost == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:userInfo];
    }
}

- (void)MB_updateSelectedViewController:(UIViewController *)viewController animated:(BOOL)animated
{
 }

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        sideSlideFactor_    = 0.3;     // Amount of parallax effect of the side views.
        currentSide_        = MBSidePanelSideIdentifierCenter;
        upcomingSide_       = MBSidePanelSideIdentifierCenter;
    }
    return self;
}

- (void)dealloc {
    [leftViewController_ release];
    [rightViewController_ release];
    [centerViewController_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    // Main view container
    centerContainer_ = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
    [centerContainer_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [centerContainer_ setBackgroundColor:[UIColor clearColor]];
    
    // Shading view
    shadingView_ = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
    [shadingView_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [shadingView_ setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.5]];
    [self.view addSubview:shadingView_];
    
    // Pan gesture
    panGestureRecognizer_ = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(MB_viewContainerPan:)] autorelease];
    //http://stackoverflow.com/questions/9953706/unable-to-move-rows-in-a-uitableview-even-when-grabber-are-displaying
    [panGestureRecognizer_ setCancelsTouchesInView:YES];
    [panGestureRecognizer_ setDelegate:self];
    [centerContainer_ addGestureRecognizer:panGestureRecognizer_];
    
    [self.view addSubview:centerContainer_];
    [self.view bringSubviewToFront:centerContainer_];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self MB_setShadowForViewContainer];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if ([centerViewController_ isKindOfClass:[UINavigationController class]]) {
        [((UINavigationController *)centerViewController_).navigationBar performSelector:@selector(sizeToFit) withObject:nil afterDelay:(0.5f * duration)];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)close
{
    [self closeWithAnimation:YES];
}

- (void)closeWithAnimation:(BOOL)animated
{
    [self MB_moveViewContainerToPosition:CGPointZero withAnimation:animated];
}

- (void)revealLeftViewAnimated:(BOOL)animated
{
    [self MB_moveViewContainerToPosition:CGPointMake(leftContainer_.bounds.size.width, 0) withAnimation:animated];
}

- (void)revealRightViewAnimated:(BOOL)animated
{
    [self MB_moveViewContainerToPosition:CGPointMake(-rightContainer_.bounds.size.width, 0) withAnimation:animated];
}

- (BOOL)slideGestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer
{
    BOOL result = YES;
    if ([self.delegate respondsToSelector:@selector(sidePanelViewController:shouldBeginSlideGesture:)]) {
        result = [self.delegate sidePanelViewController:self shouldBeginSlideGesture:gestureRecognizer];
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

-(void)setCenterViewController:(UIViewController *)centerViewController animated:(BOOL)animated{
  
    if (self.isViewLoaded) {
        if(centerViewController_ != centerViewController) {
            // Remove the current view controller
            [centerViewController_ removeFromParentViewController];
            [centerViewController_ autorelease];
            UIView *oldView = centerViewController_.view;
            UIView *newView = centerViewController.view;
            
            // Set new view controller
            centerViewController_ = [centerViewController retain];
            if (centerViewController_) {
                [centerViewController_.view setFrame:centerContainer_.bounds];
                [self addChildViewController:centerViewController_];
                [centerContainer_ addSubview:newView];
            }
            if (animated) {
                [UIView transitionFromView:oldView
                                    toView:newView
                                  duration:0.2
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                completion:^(BOOL finished) {
                                    [oldView removeFromSuperview];
                                }];
                
            } else {
                [oldView removeFromSuperview];
            }
        }
    } else {
        [centerViewController_ release];
        centerViewController_ = [centerViewController retain];
    }
}

-(void)setLeftViewController:(UIViewController *)leftViewController withWidth:(CGFloat)width{
    if (leftViewController_ != leftViewController) {
        // Remove old view controller
        [leftViewController_ release];
        [leftViewController_ removeFromParentViewController];
        [leftViewController_.view removeFromSuperview];
        
        leftViewController_ = [leftViewController retain];
        
        if (!leftContainer_) {
            leftContainer_ = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
            [leftContainer_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
            leftContainer_.hidden = YES;
            [self.view insertSubview:leftContainer_ belowSubview:shadingView_];
        }
        
        [leftContainer_ setFrame:CGRectMake(0, 0, width, self.view.bounds.size.height)];
        leftViewController_.view.frame = leftContainer_.bounds;
        [leftContainer_ addSubview:leftViewController_.view];
        [leftViewController_.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self addChildViewController:leftViewController_];
    }
}

- (void)setRightViewController:(UIViewController *)rightViewController withWidth:(CGFloat)width {
    if (rightViewController_ != rightViewController) {
        // Remove old view controller
        [rightViewController_ release];
        [rightViewController_ removeFromParentViewController];
        [rightViewController_.view removeFromSuperview];
        
        rightViewController_ = [rightViewController retain];
        
        if (!rightContainer_) {
            rightContainer_ = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
            [rightContainer_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin];
            rightContainer_.hidden = YES;
            [self.view insertSubview:rightContainer_ belowSubview:shadingView_];
        }
        [rightContainer_ setFrame:CGRectMake(self.view.bounds.size.width - width, 0, width, self.view.bounds.size.height)];
        rightViewController_.view.frame = rightContainer_.bounds;
        [rightContainer_ addSubview:rightViewController_.view];
        [self addChildViewController:rightViewController_];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponse

- (void)MB_overlayTapped:(UITapGestureRecognizer *) sender {
    [self close];
}

- (void)MB_viewContainerPan:(UIPanGestureRecognizer *)sender {
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            panStartLocation_ = CGPointMake(centerContainer_.transform.tx, 0);
            
            if(currentSide_ == MBSidePanelSideIdentifierLeft) {
                [self MB_sendNotification:MBSidePanelWillHideNotification sideIdentifier:MBSidePanelSideIdentifierLeft];
            }
            
            if(currentSide_ == MBSidePanelSideIdentifierRight) {
                [self MB_sendNotification:MBSidePanelWillHideNotification sideIdentifier:MBSidePanelSideIdentifierRight];
            }
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [sender translationInView:sender.view.superview];
            CGPoint location = centerContainer_.frame.origin;
            location.x = panStartLocation_.x + translation.x;
            [self MB_moveViewContainerToPosition:location withAnimation:NO];
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            CGPoint translation = [sender translationInView:sender.view.superview];
            CGPoint location = centerContainer_.frame.origin;
            CGPoint velocity = [sender velocityInView:self.view];
            
            location.x = panStartLocation_.x + translation.x + (0.2 * velocity.x);
            
            CGFloat currentPosition = centerContainer_.transform.tx;
            
            if ((velocity.x > 0)) { // Direction towards right
                // Prevent shooting though to the other side
                if (currentPosition < 0) {
                    location.x = MIN(location.x, 0);
                }
            } else if ((velocity.x < 0)) {  // Direction towards left
                // Prevent shooting though to the other side
                if (currentPosition > 0) {
                    location.x = MAX(location.x, 0);
                }
            }
            // Snap to left, right or middle
            if (location.x > (leftContainer_.bounds.size.width * 0.5)) {
                location.x = leftContainer_.bounds.size.width;
            } else if (location.x < (-rightContainer_.bounds.size.width * 0.5)) {
                location.x = -rightContainer_.bounds.size.width;
            } else {
                location.x = 0;
            }
            
            [self MB_moveViewContainerToPosition:CGPointMake(location.x, centerContainer_.frame.origin.y) withAnimation:YES];
        }
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    BOOL result = YES;
    
    if (gestureRecognizer == panGestureRecognizer_) {
        result = NO;
        
        UIView *view = [panGestureRecognizer_ view];
        CGPoint translation = [panGestureRecognizer_ translationInView:[view superview]];
        
        // Check for horizontal gesture
        if (fabsf(translation.x) > fabsf(translation.y)) {
            CGFloat currentPosition = centerContainer_.transform.tx;
            // To Left direction
            if (translation.x < 0) {
                result = (currentPosition > 0 || self.rightViewController != nil);
            } else if (translation.x > 0) { // To right direction
                result = (currentPosition < 0 || self.leftViewController != nil);
            }
        }
        if (result == YES) {
            result = [self slideGestureRecognizerShouldBegin:panGestureRecognizer_];
        }
#ifdef ENABLE_SidePanel_LOGS
        NSLog(@"gestureRecognizerShouldBegin: %@", result ? @"YES" : @"NO");
#endif
    }
    return result;
}

@end
