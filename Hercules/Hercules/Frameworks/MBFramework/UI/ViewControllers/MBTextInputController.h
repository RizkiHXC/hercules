//
//  MBTextInputController.h
//  MBFramework
//
//  Created by Arno Woestenburg on 8/21/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The position in the controlling scroll view (top, middle, bottom) to which the active text input view row is scrolled.
 */
typedef NS_ENUM(NSInteger, MBTextInputControllerScrollPosition)
{
    /**
     The text input controller scrolls the active text input view of interest to be fully visible with a minimum of movement. If the active text input view is already fully visible, no scrolling occurs. For example, if the row is above the visible area, the behavior is identical to that specified by UITableViewScrollPositionTop. This is the default.
     */
    MBTextInputControllerScrollPositionNone,
    /**
     The text input controller scrolls the active text input view of interest to the top of the scroll view.
     */
    MBTextInputControllerScrollPositionTop,
    /**
     The text input controller scrolls the active text input view of interest to the middle of the scroll view.
     */
    MBTextInputControllerScrollPositionMiddle,
    /**
     The text input controller scrolls the active text input view of interest to the bottom of the scroll view.
     */
    MBTextInputControllerScrollPositionBottom
};

@class MBTextInputController;

/**
 The MBTextInputControllerDelegate protocol defines the messages sent to a text input controller delegate. All of the methods of this protocol are optional.
 */
@protocol MBTextInputControllerDelegate <UITextFieldDelegate>
@optional
/**
 Tells the delegate that all editing has stopped.
 *
 @param textFieldController The text input controller for which the receiver is the delegate.
 */
- (void)didFinishEditingTextFields:(MBTextInputController *)textFieldController;
/**
 Tells the delegate that the text field indicated by the parameter textField has taken the active responder state from the parameter indicated by previous.
 *
 @param textField The new active responder
 @param previous  The previous avtive responder
 */
- (void)textField:(UIResponder *)textField takingFirstResponderFrom:(UIResponder *)previous;
- (void)textInputControllerDoneBarButtonTouched:(MBTextInputController *)inputController;

@end

/**
 A text input controller manages the display of text input views (UITextField or UITextView based controls), along with a view controller and scroll view that contain the text input views.
 
 You initialize a search display controller with an array of text input controls, a view controller and a scroll view. When the user starts a text edit, the text input controller scrolls to make sure the active text input is located inside the visible area of the screen and optionally shows a Close bar button to provide a way to end the current edit state.
 */
@interface MBTextInputController : NSObject<UITextFieldDelegate> {
@protected
    NSArray *textFields_;
    UIResponder *activeResponder_;
    UIBarButtonItem *doneBarButton_;
    id<MBTextInputControllerDelegate> delegate_;
    id<UITextFieldDelegate> textFieldDelegate_;
    UIViewController *controlViewController_;
    UIScrollView *scrollView_;
}

/**
 Initializes and returns a newly allocated TextInputController object with the specified parameters.
 *
 @param textFields An array of UITextField objects which are managed by this class.
 *
 @return An initialized TextInputController object or nil if the object couldn't be created.
 */
- (instancetype)initWithTextFields:(NSArray *)textFields;
/**
 Initializes and returns a newly allocated TextInputController object with the specified parameters.
 *
 @param textFields An array of UITextField objects which are managed by this class.
 *
 @param delegate The receiver’s delegate or nil if it doesn’t have a delegate.
 *
 @return An initialized TextInputController object or nil if the object couldn't be created.
 */
- (instancetype)initWithTextFields:(NSArray *)textFields delegate:(id<MBTextInputControllerDelegate>)delegate;
/**
 Ends the current editing mode.
 */
- (void)cancelActiveEdits;
/**
 Scrolls the scroll view until a subview is at a particular location on the screen.
 *
 @param subview        The subview of interest.
 @param scrollPosition A constant that identifies a relative position in the receiving scroll view (top, middle, bottom). 
 @param animated       YES if you want to animate the change in position, NO if it should be immediate.
 @see MBTextInputControllerScrollPosition
 */
- (void)scrollToSubview:(UIView *)subview atScrollPosition:(MBTextInputControllerScrollPosition)scrollPosition animated:(BOOL)animated;
/**
 Sets the controlling view controller and scrollview.
 *
 @param viewController The view controller that manages display of the text input fields.
 @param scrollView     The scroll view that is in the parent view hierarchy of the text input views.
 */
- (void)setControlViewController:(UIViewController *)viewController withScrollView:(UIScrollView *)scrollView;
/**
 Register to receive keyboard notifications.
 @warning Calling this method is mandatory for the text input controller to function properly. If a text editing operation is started without registering for keyboard notification once the text input controller will throw an exception.
 @discussion This method is typically called in the viewWillAppear method of the view controller.
 */
- (void)registerForKeyboardNotifications;
/**
 Unregister from keyboard notifications.
 */
- (void)unregisterForKeyboardNotifications;

/**
 Done bar button which is displayed when the text edit mode is active.
 @discussion Any target and action are overwritten by the text input controller.
 */
@property(nonatomic,retain) UIBarButtonItem *doneBarButton;
/**
 Text fields (UITextField or UITextView based controls) that are controlled by the text input controller.
 */
@property(nonatomic,retain) NSArray *textFields;
/**
 A Boolean value indicating whether text editing mode is currently active.
 */
@property(nonatomic,readonly,getter=isEditing) BOOL editing;

/**
 Returns the current active responder, or nil if it has none.
 */
@property(nonatomic,retain) UIResponder *activeResponder;
/**
 The object that acts as the delegate of the receiving text input controller.
 *
 @discussion The delegate must adopt the MBTextInputControllerDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBTextInputControllerDelegate> delegate;
/**
 The object that acts as the delegate of the active text input of a UITextField based class.
 *
 @discussion The delegate must adopt the UITextFieldDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<UITextFieldDelegate> textFieldDelegate;
/**
 The object that acts as the delegate of the active text input of a UITextView based class.
 *
 @discussion The delegate must adopt the UITextViewDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<UITextViewDelegate> textViewDelegate;
/**
 The view controller that manages the text input controls. (read-only)
 */
@property(nonatomic,retain,readonly) UIViewController *controlViewController;
/**
 The scroll view that is in the parent view hierarchy of the text input views. (read-only)
 */
@property(nonatomic,retain,readonly) UIScrollView *scrollView;

@end
