//
//  MBTextInputController.h
//  MBFramework
//
//  Created by Arno Woestenburg on 8/21/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBTextInputController.h"
#import "NSArray+MBFramework.h"

@interface MBTextInputController ()
{
    BOOL keyboardVisible_;
    BOOL registeredForNotifications_;
    BOOL restoreBackButton_;
    BOOL restoreNavigationBar_;
    BOOL restoreRightBarButton_;
    CGSize keyboardSize_;
    UIBarButtonItem *swappedBarButton_;
    UIResponder *nextTextInput_;
    UIResponder *previousTextInput_;
    CGFloat contentInsetForKeyboard_;
    CGFloat scrollIndicatorInsetForKeyboard_;
}
@end

@implementation MBTextInputController

@synthesize activeResponder = activeResponder_;
@synthesize controlViewController = controlViewController_;
@synthesize delegate = delegate_;
@synthesize doneBarButton = doneBarButton_;
@synthesize scrollView = scrollView_;
@synthesize textFieldDelegate = textFieldDelegate_;
@synthesize textFields = textFields_;
@synthesize textViewDelegate = textViewDelegate_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_inputViewPrepareBeginEditing:(UIView *)inputView
{
    // First thing called when new field is selected using touch, second when moving with enter from another field (then textFieldShouldEnter is called.)
    nextTextInput_ = inputView;
    if (!activeResponder_) { // Indicating that this was the first.
        [self MB_prepareNavigationBarForEditing];
    }
}

- (void)MB_inputViewDidBeginEditing:(UIView *)inputView
{
    // Call registerForKeyboardNotifications in viewWillAppear/viewDidAppear of your
    // controlling view controller. Call unregisterForKeyboardNotifications in viewWillDisappear/viewDidDisappear
    NSAssert(registeredForNotifications_, @"Not registered for keyboard notifications");
    
    // Last thing called when a new textfield was selected.
    if (controlViewController_ && keyboardVisible_) {
        [self scrollToSubview:inputView atScrollPosition:MBTextInputControllerScrollPositionMiddle animated:YES];
    }
    
    [self setActiveResponder:inputView];
    
    nextTextInput_     = nil;
    previousTextInput_ = nil;
}

- (void)MB_inputViewPrepareEndEditing:(UIView *)inputView
{
    previousTextInput_ = inputView;
    if (!nextTextInput_) {// textFieldShouldBeginEditing wasn't called, indicating that we're done editing.
        if (controlViewController_) {
            [self MB_restoreNavigationBarFromEditing];
        }
    }
}

- (void)MB_inputViewDidEndEditing:(UIView *)inputView
{
    [self setActiveResponder:nil];
}

- (void)MB_showDoneBarButton {
    if (controlViewController_ != nil && doneBarButton_ != nil && doneBarButton_ != controlViewController_.navigationItem.rightBarButtonItem) {
        swappedBarButton_ = [controlViewController_.navigationItem.rightBarButtonItem retain];
        [controlViewController_.navigationItem setRightBarButtonItem:doneBarButton_ animated:NO];
        restoreRightBarButton_ = YES;
    }
}

- (void)MB_restoreSwappedBarButton {
    if (controlViewController_ != nil && restoreRightBarButton_ == YES) {
        [controlViewController_.navigationItem setRightBarButtonItem:[swappedBarButton_ autorelease] animated:NO];
        restoreRightBarButton_ = NO;
    }
}

- (void)MB_prepareNavigationBarForEditing
{
    if (doneBarButton_) {
        UINavigationController *navController = controlViewController_.navigationController;
        if (0 < navController.viewControllers.count) {
            
            if (![controlViewController_.navigationItem hidesBackButton]) {
                [controlViewController_.navigationItem setHidesBackButton:YES];
                restoreBackButton_ = YES;
            }
            
            if (navController.isNavigationBarHidden) {
                [navController setNavigationBarHidden:NO animated:YES];
                restoreNavigationBar_ = YES;
            }
        }
    }
}

- (void)MB_restoreNavigationBarFromEditing
{
    if (doneBarButton_) {
        UINavigationController *navController = controlViewController_.navigationController;
        if (0 < navController.viewControllers.count) {
            if (restoreBackButton_) {
                [controlViewController_.navigationItem setHidesBackButton:NO];
                restoreBackButton_ = NO;
            }
            if (restoreNavigationBar_) {
                [navController setNavigationBarHidden:YES animated:YES];
                restoreNavigationBar_ = NO;
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithTextFields:(NSArray *)textFields
{
    self = [super init];
    if (self) {
        [self setTextFields:textFields];
        keyboardVisible_ = NO;
        restoreRightBarButton_ = NO;
    }
    return self;
}

- (instancetype)initWithTextFields:(NSArray *)textFields delegate:(id<MBTextInputControllerDelegate>)delegate
{
    self = [self initWithTextFields:textFields];
    if (self) {
        delegate_ = delegate;
    }
    return self;
}

- (void)dealloc {
    [textFields_ makeObjectsPerformSelector:@selector(setDelegate:) withObject:nil];
    [textFields_ release];
    [doneBarButton_ release];
    [controlViewController_ release];
    [scrollView_ release];
    [activeResponder_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties
- (BOOL)isEditing {
    return (nil != activeResponder_);
}

- (void)setTextFields:(NSArray *)textFields {
    [textFields_ makeObjectsPerformSelector:@selector(setDelegate:) withObject:nil];
    [textFields_ autorelease];
    textFields_ = [textFields retain];
    [textFields_ makeObjectsPerformSelector:@selector(setDelegate:) withObject:self];
}

- (void)setActiveResponder:(UIResponder *)activeResponder {

    if (activeResponder != activeResponder_) {
        if (!activeResponder_ && activeResponder) {
            [self MB_showDoneBarButton];
        } else if (!activeResponder && activeResponder_) {
            [self MB_restoreSwappedBarButton];
        }
        [activeResponder_ release];
        activeResponder_ = [activeResponder retain];
    }
}

- (void)setDoneBarButton:(UIBarButtonItem *)doneBarButton {
    if (doneBarButton_ != doneBarButton) {
        [doneBarButton_ release];
        doneBarButton_ = [doneBarButton retain];
        [doneBarButton_ setTarget:self];
        [doneBarButton setAction:@selector(MB_btnDoneTouched:)];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public
- (void)cancelActiveEdits {
    if (activeResponder_) {
        if ([delegate_ respondsToSelector:@selector(didFinishEditingTextFields:)]) {
            [delegate_ didFinishEditingTextFields:self];
        }

        [activeResponder_ resignFirstResponder];
        [self setActiveResponder:nil];
    }
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];

    registeredForNotifications_ = YES;
}

- (void)unregisterForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    registeredForNotifications_ = NO;
}

- (void)setControlViewController:(UIViewController *)viewController withScrollView:(UIScrollView *)scrollView {
    [controlViewController_ release];
    controlViewController_ = [viewController retain];
    [scrollView_ release];
    scrollView_ = [scrollView retain];
}

- (void)scrollToSubview:(UIView *)subview atScrollPosition:(MBTextInputControllerScrollPosition)scrollPosition animated:(BOOL)animated {
    CGRect subviewFrame = [scrollView_ convertRect:subview.frame fromView:subview.superview];

    CGRect visibleRect = scrollView_.bounds;
    visibleRect.origin = CGPointMake(scrollView_.contentOffset.x, scrollView_.contentOffset.y);
    visibleRect.size.height -= keyboardSize_.height;

    CGPoint scrollPoint = CGPointZero;
    BOOL doScroll = NO;
    
    switch (scrollPosition) {
        case MBTextInputControllerScrollPositionNone:
            [scrollView_ scrollRectToVisible:subviewFrame animated:animated];
            NSAssert(FALSE, @"Untested, remove this assert when tested"); // Untested
            break;
        case MBTextInputControllerScrollPositionTop:
            scrollPoint = CGPointMake(0.0, subviewFrame.origin.y); 
            doScroll = YES;
            NSAssert(FALSE, @"Untested, remove this assert when tested"); // Untested
            break;
        case MBTextInputControllerScrollPositionMiddle:
            scrollPoint = CGPointMake(0.0, roundf(subviewFrame.origin.y - (visibleRect.size.height * 0.5)));
            doScroll = YES;
            break;
        case MBTextInputControllerScrollPositionBottom:
            scrollPoint.y = (visibleRect.size.height - subviewFrame.origin.y);
            doScroll = YES;
            NSAssert(FALSE, @"Untested, remove this assert when tested"); // Untested
            break;
        default:
            NSAssert(FALSE, @"Unhandled case");
    }
    
    
    if (doScroll) {
        scrollPoint.y = MAX(scrollPoint.y, 0);
        [scrollView_ setContentOffset:scrollPoint animated:animated];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Method forwarding

- (BOOL)respondsToSelector:(SEL)aSelector
{
    // This class, or one of the delegates handle the selector.
    return ([super respondsToSelector:aSelector] || [textFieldDelegate_ respondsToSelector:aSelector] || [textViewDelegate_ respondsToSelector:aSelector]);
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector
{
    NSMethodSignature *signature = [super methodSignatureForSelector:selector];
    if (!signature && [textFieldDelegate_ respondsToSelector:selector]) {
        signature = [(NSObject *)textFieldDelegate_ methodSignatureForSelector:selector];
    }
    if (!signature && [textViewDelegate_ respondsToSelector:selector]) {
        signature = [(NSObject *)textViewDelegate_ methodSignatureForSelector:selector];
    }
    return signature;
}

- (void)forwardInvocation:(NSInvocation*)invocation
{
    // Forward to delegates if implemented
    if ([textFieldDelegate_ respondsToSelector:[invocation selector]]) {
        [invocation invokeWithTarget:textFieldDelegate_];
    } else if ([textViewDelegate_ respondsToSelector:[invocation selector]]) {
        [invocation invokeWithTarget:textViewDelegate_];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL result = YES;
    if ([textFieldDelegate_ respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
        result = [textFieldDelegate_ textFieldShouldBeginEditing:textField];
    }
    if (result) {
        [self MB_inputViewPrepareBeginEditing:textField];
    }
    return result;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textFieldDelegate_ respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [textFieldDelegate_ textFieldDidBeginEditing:textField];
    }
    [self MB_inputViewDidBeginEditing:textField];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
    BOOL result = YES;
    if ([textFieldDelegate_ respondsToSelector:@selector(textFieldShouldEndEditing:)]) {
        result = [textFieldDelegate_ textFieldShouldEndEditing:textField];
    }
    if (result) {
        [self MB_inputViewPrepareEndEditing:textField];
    }
    return result;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textFieldDelegate_ respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [textFieldDelegate_ textFieldDidEndEditing:textField];
    }
    [self MB_inputViewDidEndEditing:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BOOL result = NO;
    if ([textFieldDelegate_ respondsToSelector:@selector(textFieldShouldReturn:)]) {
        result = [textFieldDelegate_ textFieldShouldReturn:textField];
    }
    if (result == NO) {
        // First thing called when pressing enter in a field.
        if (![textFields_ objectFollowing:textField]) { // It was the last one,
            [textField resignFirstResponder];
        } else { // There is at least one more textField
            [[textFields_ objectFollowing:textField] becomeFirstResponder];
        }
        if (!activeResponder_ && [delegate_ respondsToSelector:@selector(didFinishEditingTextFields:)]) {
            [delegate_ didFinishEditingTextFields:self];
        }
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    BOOL result = YES;
    if ([textViewDelegate_ respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
        result = [textViewDelegate_ textViewShouldBeginEditing:textView];
    }
    if (result) {
        [self MB_inputViewPrepareBeginEditing:textView];
    }
    return result;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
    BOOL result = YES;
    if ([textViewDelegate_ respondsToSelector:@selector(textViewShouldEndEditing:)]) {
        result = [textViewDelegate_ textViewShouldEndEditing:textView];
    }
    if (result) {
        [self MB_inputViewPrepareEndEditing:textView];
    }
    return result;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textViewDelegate_ respondsToSelector:@selector(textViewDidBeginEditing:)]) {
        [textViewDelegate_ textViewDidBeginEditing:textView];
    }
    [self MB_inputViewDidBeginEditing:textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textViewDelegate_ respondsToSelector:@selector(textViewDidEndEditing:)]) {
        [textViewDelegate_ textViewDidEndEditing:textView];
    }
    [self MB_inputViewDidEndEditing:textView];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponders

- (void)MB_btnDoneTouched:(UIBarButtonItem *)sender
{
    if ([delegate_ respondsToSelector:@selector(textInputControllerDoneBarButtonTouched:)]) {
        [delegate_ textInputControllerDoneBarButtonTouched:self];
    }
    [self cancelActiveEdits];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    
    CGRect keyboardRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    keyboardRect = [scrollView_ convertRect:keyboardRect fromView:nil];
    keyboardSize_ = keyboardRect.size;
    
    // Adjust content bottom insets for keyboard
    UIEdgeInsets insets = scrollView_.contentInset;
    insets.bottom -= contentInsetForKeyboard_;
    
    contentInsetForKeyboard_ = MAX(0, (keyboardRect.size.height - insets.bottom));
    insets.bottom += contentInsetForKeyboard_;
    [scrollView_ setContentInset:insets];
    
    // Adjust ScrollIndicator bottom insets for keyboard
    insets = scrollView_.scrollIndicatorInsets;
    insets.bottom -= scrollIndicatorInsetForKeyboard_;
    
    scrollIndicatorInsetForKeyboard_ = MAX(0, (keyboardRect.size.height - insets.bottom));
    insets.bottom += scrollIndicatorInsetForKeyboard_;
    [scrollView_ setScrollIndicatorInsets:insets];
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    if (scrollView_ && activeResponder_ != nil) {
        [self scrollToSubview:(UIView *)activeResponder_ atScrollPosition:MBTextInputControllerScrollPositionMiddle animated:YES];
    }
    keyboardVisible_ = YES;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    // Reset scroll and scrollindicayro insets.
    UIEdgeInsets insets = scrollView_.contentInset;
    insets.bottom -= contentInsetForKeyboard_;
    [scrollView_ setContentInset:insets];
    
    insets = scrollView_.scrollIndicatorInsets;
    insets.bottom -= scrollIndicatorInsetForKeyboard_;
    [scrollView_ setScrollIndicatorInsets:insets];
    
    contentInsetForKeyboard_ = 0.0f;
    scrollIndicatorInsetForKeyboard_ = 0.0f;
    
    keyboardVisible_ = NO;
}



@end
