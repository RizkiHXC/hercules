//
//  MBCollectionViewCell.h
//  MBFramework
//
//  Created by Arno Woestenburg on 26/09/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The MBCollectionViewCell class provides a basic cell content and layout for the cells that appear in UICollectionView objects.
 */
@interface MBCollectionViewCell : UICollectionViewCell

/**
 Returns the label used for the main textual content of the collection view cell. (read-only)
 */
@property(nonatomic, readonly, retain) UILabel *textLabel;
/**
 Returns the secondary label of the collection view cell if one exists. (read-only)
 */
@property(nonatomic, readonly, retain) UILabel *detailTextLabel;
/**
 Returns the image view of the collection view cell. (read-only)
 */
@property(nonatomic, readonly, retain) UIImageView *imageView;
/**
 The inset or outset margins for the rectangle surrounding all of the collection view cell’s content.
 */
@property(nonatomic) UIEdgeInsets contentEdgeInsets;

@end
