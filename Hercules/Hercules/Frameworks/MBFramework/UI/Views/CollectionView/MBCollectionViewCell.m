//
//  MBCollectionViewCell.m
//  MBFramework
//
//  Created by Arno Woestenburg on 26/09/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBCollectionViewCell.h"

@implementation MBCollectionViewCell

@synthesize textLabel = textLabel_;
@synthesize detailTextLabel = detailTextLabel_;
@synthesize imageView = imageView_;
@synthesize contentEdgeInsets = contentEdgeInsets_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        contentEdgeInsets_ = UIEdgeInsetsZero;
    }
    return self;
}

- (void)dealloc
{
    [textLabel_ release];
    [detailTextLabel_ release];
    [imageView_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UILabel *)textLabel
{
    if (!textLabel_) {
        textLabel_ = [[UILabel alloc] init];
        [self.contentView addSubview:textLabel_];
    }
    return textLabel_;
}

- (UILabel *)detailTextLabel
{
    if (!detailTextLabel_) {
        detailTextLabel_ = [[UILabel alloc] init];
        [self.contentView addSubview:detailTextLabel_];
    }
    return detailTextLabel_;
}

- (UIImageView *)imageView
{
    if (!imageView_) {
        imageView_ = [[UIImageView alloc] init];
        [self.contentView addSubview:imageView_];
    }
    return imageView_;
}

- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets
{
    if (UIEdgeInsetsEqualToEdgeInsets(contentEdgeInsets_, contentEdgeInsets) == NO) {
        contentEdgeInsets_ = contentEdgeInsets;
        [self setNeedsLayout];
    }
}

@end
