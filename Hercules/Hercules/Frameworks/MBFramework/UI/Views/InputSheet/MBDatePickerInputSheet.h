//
//  MBDatePickerViewInputSheet.h
//  MBFramework
//
//  Created by Rob de Rijk on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBInputSheet.h"

@interface MBDatePickerInputSheet : MBInputSheet
{
    
}

@property(nonatomic,readonly) UIDatePicker *datePicker;

@end
