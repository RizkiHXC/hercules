//
//  MBDatePickerInputSheet.m
//  MBFramework
//
//  Created by Rob de Rijk on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBDatePickerInputSheet.h"

@implementation MBDatePickerInputSheet

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.inputView = [[[UIDatePicker alloc] initWithFrame:frame] autorelease];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UIDatePicker *)datePicker {
    return (UIDatePicker *)self.inputView;
}

@end
