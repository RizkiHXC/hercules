//
//  MBInputSheet.h
//  MBFramework
//
//  Created by Arno Woestenburg on 7/26/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBInputSheet;

@protocol MBInputSheetDelegate <NSObject>
@optional

- (void)willPresentInputSheet:(MBInputSheet *)inputSheet;
- (void)didPresentInputSheet:(MBInputSheet *)inputSheet;
- (void)inputSheetWillDismiss:(MBInputSheet *)inputSheet;
- (void)inputSheetDidDismiss:(MBInputSheet *)inputSheet;
- (void)inputSheetCancel:(MBInputSheet *)inputSheet;

@end

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBInputSheet : UIView<UIPopoverControllerDelegate>
{
    id<MBInputSheetDelegate> delegate_;
    BOOL showInPopoverOnPad_;
    BOOL allowShowInPopover_;
}

- (BOOL)canBecomeFirstResponder;
- (void)dismiss;
// ** Implement when needed
//- (void)showFromTabBar:(UITabBar *)view;      // **
//- (void)showFromToolbar:(UIToolbar *)view;    // **
- (void)showInView:(UIView *)view;
//- (void)showFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated;    // **
//- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated;  // **

/**
 *  The object that acts as the delegate of the receiving input sheet.
 *
 *  @discussion The delegate must adopt the MBInputSheetDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBInputSheetDelegate> delegate;
@property(nonatomic,readonly, getter=isVisible) BOOL visible;
@property(nonatomic,retain) UIView *inputView;
@property(nonatomic,retain) UIView *inputAccessoryView;
@property(nonatomic,readonly) UIView *dimmingView;
@property(nonatomic,assign) BOOL tapDimmingViewToDismissEnabled;
@property(nonatomic,assign) BOOL allowShowInPopover;
@property(nonatomic,readonly,getter=isSheetInPopover) BOOL sheetInPopover;
@property(nonatomic,assign) BOOL showDimmingView;

@end
