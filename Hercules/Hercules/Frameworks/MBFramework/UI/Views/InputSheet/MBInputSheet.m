//
//  MBInputSheet.m
//  MBFramework
//
//  Created by Arno Woestenburg on 7/26/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBInputSheet.h"
#import "MBFramework.h"

@interface MBInputSheet ()
{
    BOOL sheetInPopover_;
    UIPopoverController *popoverController_;
    UIWindow *containerWindow_;
}

@property(nonatomic,retain) UIView *dimmingView;

@end

@implementation MBInputSheet

@synthesize allowShowInPopover = allowShowInPopover_;
@synthesize delegate = delegate_;
@synthesize dimmingView = dimmingView_;
@synthesize showDimmingView = showDimmingView_;
@synthesize tapDimmingViewToDismissEnabled = tapDimmingViewToDismissEnabled_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        allowShowInPopover_ = YES;
        tapDimmingViewToDismissEnabled_ = YES;
        [self setShowDimmingView:YES];
        [self MB_subscribeForNotifications];
    }
    return self;
}

- (void)dealloc
{
    [self MB_unsubscribeForNotifications];
    self.inputView = nil;
    self.inputAccessoryView = nil;
    self.dimmingView = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponder

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)MB_dimmingViewTapped:(UITapGestureRecognizer *)sender
{
    if (self.tapDimmingViewToDismissEnabled) {
        [self dismiss];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_createContainerWindow
{
    NSAssert(containerWindow_ == nil, @"containerWindow_ must be nil at this point");
    
    [self retain]; // Self is retained because containerWindow_ doesn't have an owner.
    containerWindow_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    if ([[UIDevice currentDevice] systemVersionEqualHigher:@"7"]) {
        [containerWindow_ setWindowLevel:UIWindowLevelAlert];
    } else {
        [containerWindow_ setWindowLevel:UIWindowLevelNormal];
    }
#else
    [containerWindow_ setWindowLevel:UIWindowLevelAlert];
#endif
    UIViewController *rootViewController = [[[UIViewController alloc] init] autorelease];
    [containerWindow_ setRootViewController:rootViewController];
    [containerWindow_ setUserInteractionEnabled:showDimmingView_ ? YES : NO];
    [containerWindow_ makeKeyAndVisible];
}

- (void)MB_destroyContainerWindow
{
    if (containerWindow_) {
        [containerWindow_ release];
        containerWindow_ = nil;
        // Self was retained because containerWindow_ doesn't have an owner.
        [self release];
    }
}

- (void)MB_showPopoverInView:(UIView *)view
{
    // Show in popover
    [popoverController_ release];
    UIViewController *contentView = [[[UIViewController alloc] init] autorelease];
    [contentView.view addSubview:self];
    
    CGSize popoverSize = CGSizeMake(self.inputView.bounds.size.width, self.inputAccessoryView.bounds.size.height + self.inputView.bounds.size.height);
    [contentView.view setBounds:CGRectMake(0, 0, popoverSize.width, popoverSize.height)];
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    if ([contentView respondsToSelector:@selector(setPreferredContentSize:)]) {
        // >= iOS7
        [contentView setPreferredContentSize:popoverSize];
    } else {
        [contentView setContentSizeForViewInPopover:popoverSize];
    }
#else
    // >= iOS7
    [contentView setPreferredContentSize:popoverSize];
#endif // __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
    
    if (self.inputAccessoryView) {
        [contentView.view addSubview:self.inputAccessoryView];
        [self.inputAccessoryView setFrame:CGRectMake(0, 0, popoverSize.width, self.inputAccessoryView.bounds.size.height)];
        [self.inputAccessoryView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    }
    [contentView.view addSubview:self.inputView];
    [self.inputView setFrame:CGRectMake(0, self.inputAccessoryView.bounds.size.height, popoverSize.width, self.inputView.bounds.size.height)];
    
    popoverController_ = [[UIPopoverController alloc] initWithContentViewController:contentView];
    [popoverController_ setDelegate:self];
    
    if ([delegate_ respondsToSelector:@selector(willPresentInputSheet:)]) {
        [delegate_ willPresentInputSheet:self];
    }
    [popoverController_ presentPopoverFromRect:view.bounds inView:view permittedArrowDirections:0 animated:YES];
    if ([delegate_ respondsToSelector:@selector(willPresentInputSheet:)]) {
        [delegate_ didPresentInputSheet:self];
    }
}

- (void)MB_subscribeForNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
}

- (void)MB_unsubscribeForNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)MB_showDimmingView:(BOOL)show animated:(BOOL)animated completion:(void (^)(BOOL finished))completion
{
    [UIView animateWithDuration:animated ? 0.2 : 0.0
                     animations:^{
                         [dimmingView_ setAlpha:show ? 1.0 : 0.0];
                     }
                     completion:^(BOOL finished) {
                         if (completion) {
                             completion(finished);
                         }
                     }];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)dismiss
{
    if ([delegate_ respondsToSelector:@selector(inputSheetWillDismiss:)]) {
        [delegate_ inputSheetWillDismiss:self];
    }
    
    if (sheetInPopover_) {
        [popoverController_ dismissPopoverAnimated:YES];
        [popoverController_ release];
        popoverController_ = nil;
        sheetInPopover_ = NO;
    }
    // Make sure there's the class is retained until after removeFromSuperview
    [[self retain] autorelease];
    [self removeFromSuperview];

    if ([delegate_ respondsToSelector:@selector(inputSheetDidDismiss:)]) {
        [delegate_ inputSheetDidDismiss:self];
    }
    
    if (dimmingView_) {
        [self MB_showDimmingView:NO animated:YES completion:^(BOOL finished) {
            [self MB_destroyContainerWindow];
        }];
    } else {
        [self MB_destroyContainerWindow];
    }
    
}

//- (void)showFromTabBar:(UITabBar *)view {
//    // Implement when needed
//}

//- (void)showFromToolbar:(UIToolbar *)view {
//    // Implement when needed
//}

- (void)showInView:(UIView *)view
{
    sheetInPopover_ = allowShowInPopover_ && (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
    
    if (sheetInPopover_) {
        [self MB_showPopoverInView:view];
    } else {
        [self MB_createContainerWindow];
        view = containerWindow_.rootViewController.view;
        
        //Show as responder view
        [view addSubview:self];
        if ([delegate_ respondsToSelector:@selector(willPresentInputSheet:)]) {
            [delegate_ willPresentInputSheet:self];
        }
        [self becomeFirstResponder];
        
        if (showDimmingView_) {
            if (!self.dimmingView) {
                self.dimmingView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
                [dimmingView_ setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.4]];
                // Detect taps on the dimming view
                [dimmingView_ addGestureRecognizer:[[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MB_dimmingViewTapped:)] autorelease]];
            }
            [view addSubview:dimmingView_];
            [dimmingView_ setFrame:view.bounds];
            [dimmingView_ setAlpha:0.0f];
            
            [self MB_showDimmingView:YES animated:YES completion:nil];
        }
        
        if ([delegate_ respondsToSelector:@selector(didPresentInputSheet:)]) {
            [delegate_ didPresentInputSheet:self];
        }
    }
}

//- (void)showFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated {
//    // Implement when needed
//}

//- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated {
//    // Implement when needed
//}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    // Called on the delegate when the user has taken action to dismiss the popover. This is not called when -dismissPopoverAnimated: is called directly.
    if ([delegate_ respondsToSelector:@selector(inputSheetCancel:)]) {
        [delegate_ inputSheetCancel:self];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (BOOL)isSheetInPopover
{
    return sheetInPopover_;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSNotificationCenter

- (void)applicationWillResignActive:(UIApplication *)application
{
    [self dismiss];
}


@end
