//
//  MBPickerViewInputSheet.h
//  MBFramework
//
//  Created by Arno Woestenburg on 4/4/13.
//
//

#import <UIKit/UIKit.h>
#import "MBInputSheet.h"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBPickerViewInputSheet : MBInputSheet
{

}

@property(nonatomic,retain) UIPickerView *pickerView;
@property(nonatomic,retain) UIToolbar *toolbar;

@end
