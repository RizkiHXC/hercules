//
//  MBPickerViewInputSheet.m
//  MBFramework
//
//  Created by Arno Woestenburg on 4/4/13.
//
//

#import "MBPickerViewInputSheet.h"

@implementation MBPickerViewInputSheet

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.inputView = [[[UIPickerView alloc] initWithFrame:frame] autorelease];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UIToolbar *)toolbar
{
    if (!self.inputAccessoryView) {
        // The toolbar is created when the getter is called and the toolbar hasn't been set yet.
        self.inputAccessoryView = [[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 44)] autorelease];
        [self.inputAccessoryView sizeToFit];
    }
    return (UIToolbar *)self.inputAccessoryView;
}

- (void)setToolbar:(UIToolbar *)toolbar
{
    [self setInputAccessoryView:toolbar];
}

- (UIPickerView *)pickerView
{
    return (UIPickerView *)self.inputView;
}

- (void)setPickerView:(UIPickerView *)pickerView
{
    [self setInputView:pickerView];
}

@end
