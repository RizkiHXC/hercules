//
//  MBActivityIndicatorPopoverView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 3/7/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MBPopoverView.h"
#import "MBImagesActivityIndicatorView.h"

/**
 MBPopoverView based class with an activityIndicator. This class is commonly used for showing activity for a task that is in progress while temporarily blocking the user interface.
 */
@interface MBActivityIndicatorPopoverView : MBPopoverView
{

}

/**
 Initializes and returns a newly allocated ActivityIndicatorPopoverView object with the specified properties.
 @param indicator A MBImagesActivityIndicatorView object that replaces the default UIActivityIndicator object.
 @param message The string that appears in the receiver’s title.
 @return An initialized ActivityIndicatorPopoverView object or nil if the object couldn't be created.
 */
- (instancetype)initWithIndicator:(MBImagesActivityIndicatorView *)indicator andMessage:(NSString *)message;
/**
 Initializes and returns a newly allocated ActivityIndicatorPopoverView object with the specified properties.
 @param style A constant that specifies the style of the UIActivityIndicator object to be created.
 @param message The string that appears in the receiver’s title.
 @return An initialized ActivityIndicatorPopoverView object or nil if the object couldn't be created.
 */
- (instancetype)initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyle)style andMessage:(NSString *)message;

/**
 An activity indicator which appears as a “gear” that is spinning.
 */
@property(nonatomic,readonly) UIActivityIndicatorView *activityIndicator;


@end
