//
//  MBActivityIndicatorPopoverView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 3/7/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBActivityIndicatorPopoverView.h"
#import "MBImagesActivityIndicatorView.h"

@implementation MBActivityIndicatorPopoverView

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyle)style andMessage:(NSString *)message
{
    self = [super initWithMessage:message];
    if (self) {
        
        UIActivityIndicatorView *activityIndicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style] autorelease];
        [self setAccessoryView:activityIndicator];
    }
    return self;
}

- (instancetype)initWithIndicator:(MBImagesActivityIndicatorView*)indicator andMessage:(NSString *)message
{
    self = [super initWithMessage:message];
    if (self) {
        [self setAccessoryView:indicator];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)showOnView:(UIView *)view
{
    [super showOnView:view];
    [self.activityIndicator startAnimating];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UIActivityIndicatorView *)activityIndicator
{
    return (UIActivityIndicatorView *)self.accessoryView;
}

- (void)setColor:(UIColor *)color
{
    [super setColor:color];
    [self.activityIndicator setColor:self.color];
}

@end
