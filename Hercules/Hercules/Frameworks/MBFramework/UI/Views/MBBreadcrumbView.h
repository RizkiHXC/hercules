//
//  MBBreadcrumbView.h
//  MBFramework
//
//  Created by Marco Jonker on 5/30/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBBreadcrumbView : UIView
{
}

/**
 *  Initializes and returns a newly allocated BreadcrumbView object with the specified frame rectangle.
 *
 *  @param items Initializes the BreadcrumbView with the specified items.
 *
 *  @return An initialized BreadcrumbView object or nil if the object couldn't be created.
 */
- (instancetype)initWithItems:(NSArray *)items;

@property(nonatomic,assign) UIEdgeInsets contentEdgeInsets;
@property(nonatomic,assign) UIEdgeInsets textEdgeInsets;
@property(nonatomic,retain) NSArray *breadcrumbItems;
@property(nonatomic,retain) UIImage *levelImage;
@property(nonatomic,retain) UIImage *backgroundImage;
@property(nonatomic,readonly) UILabel *textLabel;
@property(nonatomic,readonly) UILabel *highlightedTextLabel;
@property(nonatomic) CGFloat indentationWidth;           // width for each level. Default is 20.0


@end
