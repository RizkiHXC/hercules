//
//  MBBreadcrumbView.m
//  MBFramework
//
//  Created by Marco Jonker on 5/30/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBBreadcrumbView.h"

@implementation MBBreadcrumbView

@synthesize backgroundImage = backgroundImage_;
@synthesize breadcrumbItems = breadcrumbItems_;
@synthesize contentEdgeInsets = contentEdgeInsets_;
@synthesize highlightedTextLabel = highlightedTextLabel_;
@synthesize indentationWidth = indentationWidth_;
@synthesize levelImage = levelImage_;
@synthesize textEdgeInsets = textEdgeInsets_;
@synthesize textLabel = textLabel_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithItems:(NSArray *)items
{
    self = [super init];
    if (self) {
        breadcrumbItems_ = [[NSArray alloc] initWithArray:items];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self != nil) {
        self.indentationWidth = 10;

        // Redraw on bounds change (calls -setNeedsDisplay)
        [self setContentMode:UIViewContentModeRedraw];
        
        [self MB_initializeBreadcrumbView];
        
        indentationWidth_ = 20;
        contentEdgeInsets_ = UIEdgeInsetsZero;
        textEdgeInsets_ = UIEdgeInsetsZero;
    }
    return self;
}

- (void)dealloc
{
    self.levelImage = nil;
    self.breadcrumbItems = nil;
    self.levelImage = nil;
    self.backgroundImage = nil;
    [textLabel_ release];
    [highlightedTextLabel_ release];

    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

- (void)MB_initializeBreadcrumbView
{
    self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];

    self.levelImage = [self MB_defaultLevelIndicator];

    // Text label
    textLabel_ = [[UILabel alloc] init];
    [textLabel_ setFont:[UIFont systemFontOfSize:[UIFont smallSystemFontSize]]];
    [textLabel_ setTextColor:[UIColor darkGrayColor]];
    [textLabel_ setNumberOfLines:1];
    
    // Highlighted text label
    highlightedTextLabel_ = [[UILabel alloc] init];
    [highlightedTextLabel_  setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
    [highlightedTextLabel_ setTextColor:[UIColor blackColor]];
    [highlightedTextLabel_ setNumberOfLines:2];
}

- (UIImage *)MB_defaultLevelIndicator
{
    UIImage *levelImage = nil;
    CGSize imageSize = CGSizeMake(34, 26);
    
    // Default level indicator image
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor darkGrayColor].CGColor);
    
    CGContextSetLineWidth(context, 1.0);
    
    CGContextMoveToPoint(context, roundf(imageSize.width * 0.5), 0);
    CGContextAddLineToPoint(context, roundf(imageSize.width * 0.5), roundf(imageSize.height * 0.5));
    CGContextAddLineToPoint(context, imageSize.width, roundf(imageSize.height * 0.5));
    
    CGContextStrokePath(context);

    levelImage = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 20, 13)];

    UIGraphicsEndImageContext();

    return levelImage;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (backgroundImage_) {
        [backgroundImage_ drawInRect:self.bounds];
    }
    
    NSInteger   index  = 0;
    CGRect contentRect = UIEdgeInsetsInsetRect(self.bounds, contentEdgeInsets_);
    UILabel *drawLabel = nil;
    CGRect labelArea = contentRect;
    CGRect imageRect = CGRectZero;
    CGRect labelRect = CGRectZero;
    
    CGFloat maxLabelWidth = 0;
    
    for (NSString *breadcrumbItem in breadcrumbItems_) {

        // Different label for the last (highlighted) item.
        drawLabel = (index != (breadcrumbItems_.count - 1)) ? textLabel_ : highlightedTextLabel_;

        [drawLabel setText:(breadcrumbItem.length > 0) ? breadcrumbItem : @" "];
        
        labelRect = UIEdgeInsetsInsetRect(labelArea, self.textEdgeInsets);
        maxLabelWidth = labelRect.size.width;
        
        labelRect.size = [drawLabel sizeThatFits:CGSizeMake(maxLabelWidth, CGFLOAT_MAX)];
        labelRect.size.width = MIN(labelRect.size.width, maxLabelWidth);
        labelArea.size.height = labelRect.size.height + (self.textEdgeInsets.top + self.textEdgeInsets.bottom);
        
        [drawLabel drawTextInRect:labelRect];

        // Draw level image if not the first breadcrumb
        if (index > 0) {
            imageRect.size.width = self.levelImage.size.width;
            imageRect.size.height = roundf((labelArea.size.height * 0.5) + (self.levelImage.size.height * 0.5));
            imageRect.origin.x = labelArea.origin.x - imageRect.size.width - 2;
            imageRect.origin.y = labelArea.origin.y;
            [self.levelImage drawInRect:imageRect];
        }
        
        // Offset label area
        labelArea = UIEdgeInsetsInsetRect(labelArea, UIEdgeInsetsMake(0, self.indentationWidth, 0, 0));
        labelArea = CGRectOffset(labelArea, 0, labelArea.size.height);
        index++;
    }
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGRect bounds = CGRectMake(0, 0, size.width, size.height);
    NSInteger   index  = 0;
    CGRect contentRect = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets_);
    UILabel *drawLabel = nil;
    CGRect labelArea = contentRect;
    CGRect labelRect = CGRectZero;
    CGFloat maxLabelWidth = 0;
    
    size.height = 0;
    
    for (NSString *breadcrumbItem in breadcrumbItems_) {
        // Different label for the last (highlighted) item.
        drawLabel = (index != (breadcrumbItems_.count - 1)) ? textLabel_ : highlightedTextLabel_;
        
        [drawLabel setText:(breadcrumbItem.length > 0) ? breadcrumbItem : @" "];
        
        labelRect = UIEdgeInsetsInsetRect(labelArea, self.textEdgeInsets);
        maxLabelWidth = labelRect.size.width;
        
        labelRect.size = [drawLabel sizeThatFits:CGSizeMake(maxLabelWidth, CGFLOAT_MAX)];
        labelRect.size.width = MIN(labelRect.size.width, maxLabelWidth);
        labelArea.size.height = labelRect.size.height + (self.textEdgeInsets.top + self.textEdgeInsets.bottom);
        
        // Offset label area
        labelArea = UIEdgeInsetsInsetRect(labelArea, UIEdgeInsetsMake(0, self.indentationWidth, 0, 0));
        labelArea = CGRectOffset(labelArea, 0, labelArea.size.height);
        index++;
        
        size.height += labelArea.size.height;
    }
    
    size.height += (self.contentEdgeInsets.top + self.contentEdgeInsets.bottom);
    
    return size;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    [backgroundImage_ release];
    backgroundImage_ = [backgroundImage retain];
    [self setNeedsDisplay];
}

- (void)setBreadcrumbItems:(NSArray *)breadcrumbItems
{
    if (breadcrumbItems != breadcrumbItems_) {
        [breadcrumbItems_ release];
        breadcrumbItems_ = [breadcrumbItems retain];
        [self setNeedsDisplay];
    }
}

- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets
{
    contentEdgeInsets_ = contentEdgeInsets;
    [self setNeedsDisplay];
}

- (void)setTextEdgeInsets:(UIEdgeInsets)textEdgeInsets
{
    textEdgeInsets_ = textEdgeInsets;
    [self setNeedsDisplay];
}

@end
