//
//  MBClipView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 3/29/13.
//
//

#import <UIKit/UIKit.h>

/**
 The clip view is used to allow user interaction on the scrollview outside the bounds of the scrollview.
 */
@interface MBClipView : UIView

@property(nonatomic,retain) UIScrollView *scrollView;

@end
