//
//  MBClipView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 3/29/13.
//
//

#import "MBClipView.h"

@implementation MBClipView

@synthesize scrollView = scrollView_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    self.scrollView = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView* child = [super hitTest:point withEvent:event];
    return (child == self ? self.scrollView : child);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setScrollView:(UIScrollView *)scrollView
{
    if (scrollView != scrollView_) {
        [scrollView_ release];
        scrollView_ = [scrollView retain];
        if (scrollView_) {
            [self addSubview:scrollView_];
            [scrollView_ setClipsToBounds:NO];
            [scrollView_ setFrame:CGRectMake(roundf((self.bounds.size.width - scrollView_.bounds.size.width) * 0.5), roundf((self.bounds.size.height - scrollView_.bounds.size.height) * 0.5), scrollView_.bounds.size.width, scrollView_.bounds.size.height)];
        }
    }
}

@end
