//
//  MBGradientView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 13/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Defines the start and end points of the gradient when drawn into the view's coordinate space.
 */
typedef NS_ENUM(NSInteger, MBGradientViewDirection)
{
    /**
     Draws the gradient in a verical direction. This is the default.
     */
    MBGradientViewDirectionVertical,
    /**
     Draws the gradient in a horizontal direction.
     */
    MBGradientViewDirectionHorizontal,
};

/**
 The MBGradientView class draws a color gradient over its background color, filling the shape of the view.
 */
@interface MBGradientView : UIView

/**
 Sets the start and end points of the gradient when drawn into the view's coordinate space.
 @param gradientDirection Defines the start and end points of the gradient.
 */
- (void)setGradientDirection:(MBGradientViewDirection)gradientDirection;

/**
 The view’s Core Animation layer used for rendering. (read-only) 
 */
@property(nonatomic,readonly,retain) CAGradientLayer *gradientLayer;
/** 
 An array of UIColor objects defining the color of each gradient stop.
 */
@property(copy) NSArray *colors;
/** 
 The end point of the gradient when drawn in the view's coordinate space. 
 */
@property CGPoint endPoint;
/** 
 An optional array of NSNumber objects defining the location of each gradient stop. 
 */
@property(copy) NSArray *locations;
/** 
 The start point of the gradient when drawn in the view’s coordinate space. 
 */
@property CGPoint startPoint;
/**
 A Boolean that indicates whether the gradient is rendered as a bitmap before compositing. Default is YES.
 @discussion When the value of this property is YES, the layer is rendered as a bitmap in its local coordinate space and then composited to the destination with any other content.
 Set this value to YES to speed up rendering when the size of the view never or rarely changes.
 */
@property(nonatomic) BOOL shouldRasterize;

@end
