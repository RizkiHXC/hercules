//
//  MBGradientView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 13/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBGradientView.h"

@implementation MBGradientView

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setShouldRasterize:YES];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (Class)layerClass
{
    return [CAGradientLayer class];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setGradientDirection:(MBGradientViewDirection)gradientDirection;
{
    switch (gradientDirection) {
        case MBGradientViewDirectionHorizontal:
            self.startPoint = CGPointMake(0, 0.5);
            self.endPoint   = CGPointMake(1, 0.5);
            break;
        case MBGradientViewDirectionVertical:
            self.startPoint = CGPointMake(0.5, 0);
            self.endPoint   = CGPointMake(0.5, 1);
            break;
        default:
            NSAssert(NO, @"Undefined case");
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (CAGradientLayer *)gradientLayer
{
    return (CAGradientLayer *)self.layer;
}

- (void)setColors:(NSArray *)colors
{
    // CAGradientLayer wants array of CGColorRef pointers
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:colors.count];
    for (UIColor *color in colors) {
        [array addObject:(id)color.CGColor];
    }
    [self.gradientLayer setColors:[NSArray arrayWithArray:array]];
}

- (NSArray *)colors
{
    return self.gradientLayer.colors;
}

- (void)setEndPoint:(CGPoint)endPoint
{
    [self.gradientLayer setEndPoint:endPoint];
}

- (CGPoint)endPoint
{
    return self.gradientLayer.endPoint;
}

- (void)setLocations:(NSArray *)locations
{
    [self.gradientLayer setLocations:locations];
}

- (NSArray *)locations
{
    return self.gradientLayer.locations;
}

- (void)setStartPoint:(CGPoint)startPoint
{
    [self.gradientLayer setStartPoint:startPoint];
}

- (CGPoint)startPoint
{
    return self.gradientLayer.startPoint;
}

- (void)setType:(NSString *)type
{
    [self.gradientLayer setType:type];
}

- (NSString *)type
{
    return self.gradientLayer.type;
}

- (void)setShouldRasterize:(BOOL)shouldRasterize
{
    if (shouldRasterize) {
        [self.gradientLayer setRasterizationScale:[[UIScreen mainScreen] scale]];
    }
    [self.gradientLayer setShouldRasterize:shouldRasterize];
}

- (BOOL)shouldRasterize
{
    return self.gradientLayer.shouldRasterize;
}

@end
