//
//  MBGridPanel.h
//  MBFramework
//
//  Created by Marco Jonker on 5/27/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBGridPanel : UIView {
    UIView*         backgroundView_;
    NSMutableArray* controls_;
    NSInteger       rows_;
    NSInteger       cols_;
    UIEdgeInsets    contentEdgeInsets_;
}

/**
 The background view of the grid panel view.
 @discussion A grid panel view’s background view is automatically resized to match the size of the view. This view is placed as a subview of the grid panel view behind all content.
 */
@property (nonatomic, retain) UIView* backgroundView;
/**
 The inset or outset margins for the rectangle surrounding all of the grid panel's content.
 */
@property (nonatomic) UIEdgeInsets contentEdgeInsets;

/**
 *  Initializes and returns a newly allocated GridPanel object with the specified properties.
 *
 *  @param frame The frame rectangle for the view, measured in points. The origin of the frame is relative to the superview in which you plan to add it. This method uses the frame rectangle to set the center and bounds properties accordingly.
 *  @param rows Number of rows.
 *  @param cols NUmber of columns.
 *
 *  @return An initialized GridPanel object or nil if the object couldn't be created.
 */
-(instancetype)initWithFrame:(CGRect)frame numberOfRows:(NSInteger)rows numberOfCols:(NSInteger)cols;
-(void)setView:(UIView*)view atIndexPath:(NSIndexPath*)indexPath;
-(void)setView:(UIView*)view forRow:(NSInteger)row andColumn:(NSInteger)col;
-(void)removeViewAtIndexPath:(NSIndexPath*)indexPath;
-(void)removeViewForRow:(NSInteger)row andColumn:(NSInteger)col;
-(UIView*)viewForIndexPath:(NSIndexPath*)indexPath;
-(UIView*)viewForRow:(NSInteger)row andColumn:(NSInteger)col;

@end
