//
//  MBGridPanel.m
//  MBFramework
//
//  Created by Marco Jonker on 5/27/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBGridPanel.h"

@implementation MBGridPanel

@synthesize backgroundView=backgroundView_;
@synthesize contentEdgeInsets = contentEdgeInsets_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

-(instancetype)initWithFrame:(CGRect)frame numberOfRows:(NSInteger)rows numberOfCols:(NSInteger)cols {
    self = [super initWithFrame:frame];
    if(self != nil) {
        contentEdgeInsets_ = UIEdgeInsetsZero;
        rows_ = rows;
        cols_ = cols;
        
        controls_ = [[NSMutableArray alloc]init];
        NSInteger numberOfControls = rows * cols;
        for(int i = 0; i < numberOfControls; i++) {
            [controls_ addObject:[NSNull null]];
        }
    }
    return self;
}

-(void)dealloc {
    [controls_ release];
    self.backgroundView = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

- (CGFloat)round:(CGFloat)value {
    return roundf(value + .5);
}

-(CGRect)MB_getFrameForViewAtRow:(NSInteger)row andColumn:(NSInteger)col {
    
    CGRect contentRect = UIEdgeInsetsInsetRect(self.bounds, contentEdgeInsets_);
    
    CGFloat height  = contentRect.size.height / rows_;
    CGFloat width   = contentRect.size.width  / cols_;
    CGFloat xPos    = contentRect.origin.x + (width  * col);
    CGFloat yPos    = contentRect.origin.y + (height * row);
    
    if(xPos - (NSInteger)xPos > 0.5) {
        xPos    = ceil(xPos);
        width   = floor(width);
    } else {
        xPos    = floor(xPos);
        width   = ceil(width);
    }
    
    if(yPos - (NSInteger)yPos > 0.5) {
        yPos    = ceil(yPos);
        height   = floor(height);
    } else {
        yPos    = floor(yPos);
        height   = ceil(height);
    }
    
    return CGRectMake(xPos, yPos, width, height);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

//Remove the view from a specific index
-(void)removeViewAtIndexPath:(NSIndexPath*)indexPath {
    [self removeViewForRow:indexPath.row andColumn:indexPath.section];
}

-(void)removeViewForRow:(NSInteger)row andColumn:(NSInteger)col {
    NSInteger index = row * cols_ + col;
    
    if(index < [controls_ count]) {
        if(![[controls_ objectAtIndex:index] isKindOfClass:[NSNull class]]) {
            UIView* oldView = (UIView*)[controls_ objectAtIndex:index];
            if([oldView respondsToSelector:@selector(removeFromSuperview)]) {
                [oldView removeFromSuperview];
            }
        }
        
        [controls_ replaceObjectAtIndex:index withObject:[NSNull null]];
    }
}

//Set the current view for a specific index
-(void)setView:(UIView*)view atIndexPath:(NSIndexPath*)indexPath {
    [self setView:view forRow:indexPath.row andColumn:indexPath.section];
}

-(void)setView:(UIView*)view forRow:(NSInteger)row andColumn:(NSInteger)col {
    NSInteger index = row * cols_ + col;
    
    if(index < [controls_ count]) {
        if(![[controls_ objectAtIndex:index] isKindOfClass:[NSNull class]]) {
            UIView* oldView = (UIView*)[controls_ objectAtIndex:index];
            if([oldView respondsToSelector:@selector(removeFromSuperview)]) {
                [oldView removeFromSuperview];
            }
        }
        
        if([view respondsToSelector:@selector(setFrame:)]) {
            [view setFrame:[self MB_getFrameForViewAtRow:row andColumn:col]];
            [self addSubview:view];
            [controls_ replaceObjectAtIndex:index withObject:view];
        }
    }
}

-(UIView*)viewForIndexPath:(NSIndexPath*)indexPath {
    return [self viewForRow:indexPath.row andColumn:indexPath.section];
}

-(UIView*)viewForRow:(NSInteger)row andColumn:(NSInteger)col {
    NSInteger   index = row * cols_ + col;
    UIView*     view = nil;
    
    if(index < [controls_ count]) {
        if(![[controls_ objectAtIndex:index] isKindOfClass:[NSNull class]]) {
            view = [controls_ objectAtIndex:index];
        }
    }
    
    return view;
}

-(void)setBackgroundView:(UIView *)backgroundView {
    if(backgroundView_ != nil) {
        [backgroundView_ removeFromSuperview];
        [backgroundView_ release];
    }
    
    backgroundView_                     = [backgroundView retain];
    backgroundView_.autoresizingMask    = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    backgroundView_.frame               = self.bounds;
    [self addSubview:backgroundView_];
    [self sendSubviewToBack:backgroundView_];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

-(void)layoutSubviews {
    [super layoutSubviews];
    
    NSInteger index = 0;
    
    for(int row = 0; row < rows_; row++) {
        for(int col = 0; col < cols_; col++) {
            if(![[controls_ objectAtIndex:index] isKindOfClass:[NSNull class]]) {
                UIView* view = (UIView*)[controls_ objectAtIndex:index];
                if([view respondsToSelector:@selector(setFrame:)]) {
                    [view setFrame:[self MB_getFrameForViewAtRow:row andColumn:col]];
                }
            }
            index++;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets {
    if (!UIEdgeInsetsEqualToEdgeInsets(contentEdgeInsets, contentEdgeInsets_)) {
        contentEdgeInsets_ = contentEdgeInsets;
        [self setNeedsLayout];
    }
}

@end
