//
//  MBGridView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/21/12.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBGridView;
@protocol MBGridViewDataSource;

DEPRECATED_MSG_ATTRIBUTE("MBGridView is deprecated. Use a UICollectionView with a UICollectionViewFlowLayout instead")
@protocol MBGridViewDelegate<NSObject, UIScrollViewDelegate>
- (void)gridView:(MBGridView*)gridView didSelectCellAtIndex:(NSUInteger)index;
@end

/**
 An instance of MBGridView (or simply, a grid view) is a means for displaying and editing hierarchical lists of information in a grid style layout.
 **Important:** MBGridView is deprecated. To create a grid view, new projects (iOS 6+) should use a UICollectionView with a MBCollectionViewGridLayout (or create their own layout based on a UICollectionViewFlowLayout).
 */
DEPRECATED_MSG_ATTRIBUTE("Use a UICollectionView with a UICollectionViewFlowLayout instead")
@interface MBGridView : UIScrollView<UIGestureRecognizerDelegate>
{
	BOOL allowsMultipleSelection_;
	BOOL allowsSelection_;

	NSUInteger numberOfColumns_;
	NSUInteger numberOfRows_;
	
	id<MBGridViewDataSource> dataSource_;
	id<MBGridViewDelegate> delegate_;
	
	UIView* cellContainerView_;

	NSMutableDictionary* reusableCells_;
	
	CGRect previousVisibleBounds_;
	
	CGFloat	horizontalPadding_;
	CGFloat	verticalPadding_;
	CGFloat rowHeight_;
	
	NSMutableArray* selectedCells_;
}

- (id)dequeueReusableCellWithIdentifier:(NSString *)identifier;
- (instancetype)initWithFrame:(CGRect)frame;
- (void)reloadCellAtIndex:(NSUInteger)index;
- (void)reloadData;
- (UITableViewCell *)cellForIndex:(NSUInteger)index;

- (NSInteger)indexForSelectedCell;
- (NSArray *)indexesForSelectedCells;
- (void)selectCellAtIndex:(NSInteger)index animated:(BOOL)animated scrollPosition:(UITableViewScrollPosition)scrollPosition;
- (void)deselectCellAtIndex:(NSInteger)index animated:(BOOL)animated;

@property (nonatomic, assign) id<MBGridViewDataSource> dataSource;
@property (nonatomic, assign) id<MBGridViewDelegate> delegate;
@property(nonatomic) BOOL allowsMultipleSelection;
@property(nonatomic) BOOL allowsSelection;
@property (nonatomic) NSUInteger numberOfColumns;
@property (nonatomic) NSUInteger numberOfRows;
@property NSUInteger topOffset;
@property CGFloat horizontalPadding;
@property CGFloat verticalPadding;
@property (nonatomic) CGFloat rowHeight;
@property (readonly) CGFloat columnWidth;

@end

DEPRECATED_MSG_ATTRIBUTE("MBGridView is deprecated. Use a UICollectionView with a UICollectionViewFlowLayout instead")
@protocol MBGridViewDataSource<NSObject>
- (NSInteger)gridViewNumberOfItems:(MBGridView *)gridView;
- (UITableViewCell*)gridView:(MBGridView*)gridView cellForItemAtIndex:(NSUInteger)index;
@end


