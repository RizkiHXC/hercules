//
//  MBGridView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/21/12.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import "MBGridView.h"

@interface MBGridView()

- (void)MB_cellTouched:(id)sender;
- (BOOL)MB_isIndexSelected:(NSInteger)index;
- (void)MB_recycleCell:(UITableViewCell*)cell;
- (void)MB_removeSelectedIndex:(NSInteger)index;
- (UITableViewCell *)MB_createNewCellForIndex:(NSUInteger)index withFrame:(CGRect)frame;

@end

@implementation MBGridView

@synthesize numberOfColumns = numberOfColumns_;
@synthesize numberOfRows = numberOfRows_;
@synthesize horizontalPadding = horizontalPadding_;
@synthesize verticalPadding = verticalPadding_;
@synthesize dataSource = dataSource_;
@synthesize delegate = delegate_;
@synthesize rowHeight = rowHeight_;
@synthesize allowsMultipleSelection = allowsMultipleSelection_;
@synthesize allowsSelection = allowsSelection_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self)
	{
		// Default values.
		numberOfColumns_ = 3;
		numberOfRows_ = 3;
		rowHeight_ = 150.0;
		horizontalPadding_ = verticalPadding_ = 0.0;
		previousVisibleBounds_ = CGRectZero;
		
		allowsSelection_ = YES;
		allowsMultipleSelection_ = NO;
        
        self.alwaysBounceVertical = YES;


		reusableCells_ = [[NSMutableDictionary alloc] init];
		selectedCells_ = [[NSMutableArray alloc] init];
		
		CGRect contentRect = self.bounds;
 		contentRect.size.width -= (self.contentInset.left + self.contentInset.right);
		contentRect.size.height -= (self.contentInset.top + self.contentInset.bottom);

		if(cellContainerView_ == nil) {
			cellContainerView_ = [[UIView alloc] initWithFrame:contentRect];
		}
		cellContainerView_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
		[self addSubview:cellContainerView_];
    }
    return self;
}

- (void)dealloc {
	for (NSObject* object in reusableCells_) {
		[object release];
	}
	[reusableCells_ release];
	[selectedCells_ release];
	[cellContainerView_ release];
	[super dealloc];
}

#pragma mark - Private

- (void)MB_cellTouched:(UIGestureRecognizer *)sender {
	assert(sender.view != nil);
	
	if (sender.state == UIGestureRecognizerStateEnded) {
		
		NSInteger index = sender.view.tag;
		
		if (allowsSelection_ == YES) {
			if ([self MB_isIndexSelected:index]) {
				[self deselectCellAtIndex:index animated:NO];
			} else {
				[self selectCellAtIndex:index animated:NO scrollPosition:UITableViewScrollPositionNone];
			}
		}
		
		if(delegate_ != nil) {
			[delegate_ gridView:self didSelectCellAtIndex:index];
		}
	}
}

- (BOOL)MB_isIndexSelected:(NSInteger)index {
	BOOL result = NO;
	NSInteger item = 0;
	NSInteger value = 0;
	
	while (item < selectedCells_.count && result == NO) {
		
		value = [[selectedCells_ objectAtIndex:item] integerValue];
		
		if (value == index) {
			result = YES;
		}
		item++;
	}
	return result;
}

- (void)MB_removeSelectedIndex:(NSInteger)index {
	BOOL done = NO;
	NSInteger item = 0;
	NSInteger value = 0;
	
	while (item < selectedCells_.count && done == NO) {
		
		value = [[selectedCells_ objectAtIndex:item] integerValue];
		
		if (value == index) {
			[selectedCells_ removeObjectAtIndex:item];
			done = YES;
		}
		item++;
	}
}


- (UITableViewCell *)MB_createNewCellForIndex:(NSUInteger)index withFrame:(CGRect)frame {
	UITableViewCell* newCell = [dataSource_ gridView:self cellForItemAtIndex:index];
	if(newCell) {
		newCell.frame = frame;
		newCell.tag = index;
		[cellContainerView_ addSubview:newCell];
		[newCell setNeedsLayout];
		
		if ([self MB_isIndexSelected:index]) {
			[newCell setSelected:YES];
		}
        
        // Remove all gesture recognizers.
        while (newCell.gestureRecognizers.count) {
            [newCell removeGestureRecognizer:[newCell.gestureRecognizers objectAtIndex:0]];
        }
		
		// Add a tab gesture to the view.
		UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MB_cellTouched:)];
		tap.numberOfTapsRequired = 1;
		tap.numberOfTouchesRequired = 1;
		tap.delegate = self;
		[newCell addGestureRecognizer:tap];
		[tap release];
	}
	return newCell;
}


- (void)MB_recycleCell:(UITableViewCell*)cell {
	NSString* reuseIdentifier = (cell.reuseIdentifier != nil ? cell.reuseIdentifier : @"");
	
	NSMutableArray* viewSet = [reusableCells_ objectForKey:reuseIdentifier];
	
	if(viewSet == nil) {
		viewSet = [[[NSMutableArray alloc] init] autorelease];
		[reusableCells_ setObject:viewSet forKey:reuseIdentifier];
	}
    
	[viewSet addObject:cell];
	[cell removeFromSuperview];
}

#pragma mark Public

- (void)layoutSubviews {
	CGRect				visibleBounds	= [self bounds];
	NSUInteger			column			= 0;
	NSUInteger			row				= 0;
	NSUInteger			item			= 0;
	NSUInteger			itemCount		= 0;
	CGFloat				columnWidth		= self.columnWidth;
	CGSize				contentSize		= CGSizeZero;
	CGRect				targetFrame		= CGRectZero;

	[super layoutSubviews];
    
	assert(numberOfColumns_ > 0); // Must be bigger than 0;
	
	if(dataSource_ == nil)
		return;
	
	// Recycle all cells that are no longer visible
	for (UITableViewCell* cell in [cellContainerView_ subviews]) {
		
		if (! CGRectIntersectsRect(cell.frame, visibleBounds)) {
			[self MB_recycleCell:cell];
		}
	}
	
	itemCount = [dataSource_ gridViewNumberOfItems:self];
	
	// Size for one item.
	targetFrame.size.width = roundf(columnWidth - 2*horizontalPadding_);
	targetFrame.size.height = roundf(rowHeight_- 2*verticalPadding_);
		 
	contentSize.width = self.bounds.size.width - (self.contentInset.left + self.contentInset.right);
	contentSize.height = 0;
	
	// Iterate through cells, adding any cells that are missing
	while (item < itemCount)
	{
		targetFrame.origin.x = 0.0;
		targetFrame.origin.y = roundf((rowHeight_ * row) + verticalPadding_);
		 
		for(column = 0; column < numberOfColumns_ && item < itemCount; column++)
		{
			// Positionize the grid cell in the target column.
			targetFrame.origin.x = roundf(column * (columnWidth + 2*horizontalPadding_));
		 
			if (CGRectIntersectsRect(targetFrame, visibleBounds)) {
				if( ! CGRectIntersectsRect(targetFrame, previousVisibleBounds_) || CGRectEqualToRect(previousVisibleBounds_, CGRectZero))  {
					[self MB_createNewCellForIndex:item withFrame:targetFrame];
				}
			}
		 
			// Next item, next column
			item++;
			targetFrame.origin.x += (columnWidth + 2*horizontalPadding_);
		}
		contentSize.height += rowHeight_;
		
		row++;
	}

	// set the content size so it can be scrollable
	[self setContentSize:contentSize];
	cellContainerView_.frame = CGRectMake(0.0, 0.0, contentSize.width, contentSize.height);
	
	previousVisibleBounds_ = visibleBounds;
}

- (id)dequeueReusableCellWithIdentifier:(NSString *)identifier {
	UITableViewCell* cell = nil;
	
	NSString* reuseIdentifier = (identifier != nil ? identifier : @"");
	NSMutableArray* viewSet = [reusableCells_ objectForKey:reuseIdentifier];
	
	if(viewSet) {
		cell = [viewSet lastObject];
		if (cell) {
			// The only object retaining the page is our reusablePages set, so we have to retain/autorelease it
			// before returning it so that it's not immediately deallocated when we remove it from the set
			[[cell retain] autorelease];
			[viewSet removeLastObject];
			[cell prepareForReuse];
		}
	}
	return cell;
}

- (UITableViewCell *)cellForIndex:(NSUInteger)index {
	UITableViewCell* result = nil;
	
	NSUInteger item = 0;
	UITableViewCell* cell = nil;
	
	while (item < cellContainerView_.subviews.count && result == nil) {
		cell = [cellContainerView_.subviews objectAtIndex:item];
		if(cell.tag == index) {
			result = cell;
		}
		item++;
	}
	return result;
}

- (CGFloat)columnWidth {
	return (((self.bounds.size.width - self.contentInset.left - self.contentInset.right - (2*horizontalPadding_ * ((CGFloat)numberOfColumns_ - 1))) / numberOfColumns_));
}

- (NSInteger)indexForSelectedCell {
	NSInteger result = -1;
	
	if (selectedCells_.count > 0) {
		// If there are multiple selections, this method returns the first object in the array of cell selections; this object has the lowest index value.
		result = [[selectedCells_ objectAtIndex:0] integerValue];
	}
	return result;
}

- (NSArray *)indexesForSelectedCells {
	NSArray* result = nil;
	
	if (selectedCells_.count > 0) {
		result = [[[NSArray alloc] initWithArray:selectedCells_] autorelease];
 	}
	return result;
}

- (void)selectCellAtIndex:(NSInteger)index animated:(BOOL)animated scrollPosition:(UITableViewScrollPosition)scrollPosition {
	if ([self MB_isIndexSelected:index] == NO) {
		
		if (allowsMultipleSelection_ == NO) {
			NSInteger selectedCell = [self indexForSelectedCell];
			if (selectedCell > -1) {
				[self deselectCellAtIndex:selectedCell animated:animated];
			}
		}
		UITableViewCell* cell = [self cellForIndex:index];
		if (cell) {
			[cell setSelected:YES animated:animated];
		}
		[selectedCells_ addObject:[NSNumber numberWithInteger:index]];
	}
}

- (void)deselectCellAtIndex:(NSInteger)index animated:(BOOL)animated {
	if ([self MB_isIndexSelected:index] == YES) {
		[self MB_removeSelectedIndex:index];
		UITableViewCell* cell = [self cellForIndex:index];
		if (cell) {
			[cell setSelected:NO animated:animated];
		}
	}
}

- (void)setDataSource:(id<MBGridViewDataSource>)dataSource {
	dataSource_ = dataSource;
	[self reloadData];
}

- (void)setNumberOfColumns:(NSUInteger)numberOfColumns {
	numberOfColumns_ = numberOfColumns;
	[self setNeedsLayout];
}

- (void)reloadCellAtIndex:(NSUInteger)index {
	UITableViewCell* cell = [self cellForIndex:index];
	if(cell) {
		CGRect frame = cell.frame;
		[self MB_recycleCell:cell];
		[self MB_createNewCellForIndex:index withFrame:frame];
	}
}

- (void)reloadData {
    for (UITableViewCell* cell in [cellContainerView_ subviews]) {
		[self MB_recycleCell:cell];
	}

	[reusableCells_ removeAllObjects];
	[selectedCells_ removeAllObjects];
	previousVisibleBounds_ = CGRectZero;
	[self setNeedsLayout];
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	if ([touch.view isKindOfClass:[UIButton class]]) {
		// we touched a button, slider, or other UIControl, ignore the touch.
		return NO;
	}
    return YES;
}

@end
