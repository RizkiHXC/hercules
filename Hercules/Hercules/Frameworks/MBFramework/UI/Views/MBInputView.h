//
//  MBInputView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/26/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBInputView;

//@protocol MBInputViewDelegate <NSObject, UIAlertViewDelegate>
//@optional
//- (BOOL)inputView:(MBInputView *)inputView validateInput:(NSString *)inputText;
//@end

/**
 MBInputView is a subclass of UIAlertView which is a convenience class to allow user input in a standard Alert view.
 */
@interface MBInputView : UIAlertView

/**
 Allows the user to enter text.
 */
@property(nonatomic,readonly) UITextField *textField;
/**
 Allows the user to enter a login identifier.
 */
@property(nonatomic,readonly) UITextField *loginTextField;
/**
 Allows the user to enter a password.
 */
@property(nonatomic,readonly) UITextField *passwordTextField;
/**
 When the returnKey is tapped, the AlertView is dismissed with this button index. Default is firstOtherButtonIndex. Use -1 to prevent auto-dismiss.
 */
@property(nonatomic) NSInteger returnKeyButtonIndex;


@end
