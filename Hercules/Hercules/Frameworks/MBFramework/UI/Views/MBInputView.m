//
//  MBInputView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/26/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBInputView.h"

@interface MBInputView () <UITextFieldDelegate>

@end

@implementation MBInputView

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Default style
        [self setAlertViewStyle:UIAlertViewStylePlainTextInput];
        
        [self.textField setDelegate:self];
        // Default is firstOtherButtonIndex.
        self.returnKeyButtonIndex = NSNotFound;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UITextField *)textField
{
    return [self textFieldAtIndex:0];
}

- (UITextField *)loginTextField
{
    return [self textFieldAtIndex:0];
}

- (UITextField *)passwordTextField
{
    return [self textFieldAtIndex:1];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // Called when 'return' key pressed. return NO to ignore.
    if (self.returnKeyButtonIndex != -1) {
        NSInteger buttonIndex = self.returnKeyButtonIndex != NSNotFound ? self.returnKeyButtonIndex : self.firstOtherButtonIndex;
        
        if ([self.delegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)]) {
            [self.delegate alertView:self clickedButtonAtIndex:buttonIndex];
        }
        [self dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
    return YES;
}

@end
