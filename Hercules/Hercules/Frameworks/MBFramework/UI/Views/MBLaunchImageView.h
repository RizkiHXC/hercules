//
//  MBLaunchImageView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 20/03/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBLaunchImageView : UIWindow

/**
 *  Initializes and returns a newly allocated LaunchImageView object with the specified activityIndicator style.
 *
 *  @param activityIndicatorStyle Style for the ActivityIndicator view.
 *
 *  @return An initialized LaunchImageView object or nil if the object couldn't be created.
 */
- (instancetype)initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityIndicatorStyle;
- (void)show;
- (void)dismissAnimated:(BOOL)animated;

@property(nonatomic,assign) UIEdgeInsets indicatorEdgeInsets;
@property(nonatomic,readonly) UIActivityIndicatorView *activityIndicatorView;

@end
