//
//  MBLaunchImageView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 20/03/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBLaunchImageView.h"
#import "MBFramework.h"

@interface MBLaunchImageView ()
{
    UIImageView *backgroundImageView_;
}
@end

@implementation MBLaunchImageView

@synthesize indicatorEdgeInsets = indicatorEdgeInsets_;
@synthesize activityIndicatorView = activityIndicatorView_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityIndicatorStyle
{
    self = [self initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self) {
        
        activityIndicatorView_ = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:activityIndicatorStyle] autorelease];
        [self.rootViewController.view addSubview:activityIndicatorView_];
        [activityIndicatorView_ setFrame:[MBRectFunctions alignRect:activityIndicatorView_.bounds toRect:self.rootViewController.view.bounds alignMode:MBAlignmentCenter]];
        [activityIndicatorView_ setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin];
        [activityIndicatorView_ startAnimating];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIViewController *rootViewController = [[[UIViewController alloc] init] autorelease];
        [self setRootViewController:rootViewController];
        
        backgroundImageView_ = [[[UIImageView alloc] initWithFrame:rootViewController.view.bounds] autorelease];
        [backgroundImageView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [backgroundImageView_ setBackgroundColor:[UIColor blueColor]];
        [rootViewController.view addSubview:backgroundImageView_];
        
        [rootViewController.view setBackgroundColor:[UIColor greenColor]];
        
        [self setWindowLevel:UIWindowLevelNormal + 1];
        
        [self MB_initializeLaunchImage];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_initializeLaunchImage
{
    self.contentMode = UIViewContentModeScaleAspectFit;
    NSString *imageName = @"";
    
    NSString *osVersionTag = MB_IOS_VERSION_EQUALHIGHER(7) ? @"-700" : @"";
    NSString *orientationTag = @"";
    NSString *scaleTag = ([[UIScreen mainScreen] scale] == 2.0) ? @"@2x" : @"";
    NSString *idomTag = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"~ipad" : @"";
    NSString *screenSizeTag = @"";

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // iPhone
        screenSizeTag = ([UIScreen mainScreen].bounds.size.height == 568.0f) ? @"-568h" : @"";
    } else {
        // iPad
        orientationTag = UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? @"-Portrait" : @"-Landscape";
    }
    
    // Possible values are
    // iPhone, not Retina           : @"LaunchImage"
    // iPhone, Retina 3,5-Inch      : @"LaunchImage@2x"
    // iPhone, Retina 5-Inch        : @"LaunchImage-568h@2x"
    
    // iPad, not Retina, portrait   : @"LaunchImage-Portrait~ipad"
    // iPad, not Retina, landscape  : @"LaunchImage-Landscape~ipad"
    // iPad, Retina, portrait       : @"LaunchImage-Portrait@2x~ipad"
    // iPad, Retina, landscape      : @"LaunchImage-Landscape@2x~ipad"
    
    // iPad (>=iOS 7), not Retina, portrait   : @"LaunchImage-700-Portrait~ipad"
    // iPad (>=iOS 7), not Retina, landscape  : @"LaunchImage-700-Landscape~ipad"
    // iPad (>=iOS 7), Retina, portrait       : @"LaunchImage-700-Portrait@2x~ipad"
    // iPad (>=iOS 7), Retina, landscape      : @"LaunchImage-700-Landscape@2x~ipad"

    imageName = [NSString stringWithFormat:@"LaunchImage%@%@%@%@%@", osVersionTag, screenSizeTag, orientationTag, scaleTag, idomTag];
    
    UIImage *launchImage = [UIImage imageNamed:imageName];
    if (launchImage) {
        [backgroundImageView_ setImage:launchImage];
    } else {
        [self setBackgroundColor:[UIColor whiteColor]];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)show
{
    if (!self.isHidden) {
        [NSException raise:NSInternalInconsistencyException format:@"%@ Must be hidden", NSStringFromSelector(_cmd)];
    }
    [self setHidden:NO];
    [self retain];
}

- (void)dismissAnimated:(BOOL)animated
{
    [UIView transitionWithView:self
                      duration:animated ? 0.2 : 0.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self setHidden:YES];
                    }
                    completion:^(BOOL finished) {
                        [self release];
                    }];
}

- (void)setIndicatorEdgeInsets:(UIEdgeInsets)indicatorEdgeInsets
{
    indicatorEdgeInsets_ = indicatorEdgeInsets;
    CGRect viewArea = UIEdgeInsetsInsetRect(self.bounds, indicatorEdgeInsets_);
    [activityIndicatorView_ setFrame:[MBRectFunctions alignRect:activityIndicatorView_.bounds toRect:viewArea alignMode:MBAlignmentCenter]];
}

@end
