//
//  MBPageScrollView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 10/12/12.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBPageScrollView;
@protocol MBPageScrollViewDataSource;

@protocol MBPageScrollViewDelegate <NSObject, UIScrollViewDelegate>
@optional
- (void)pageScrollViewWillBeginDragging:(MBPageScrollView *)scrollView;
- (void)pageScrollView:(MBPageScrollView *)pageScrollView pageHasChanged:(NSUInteger)index;
- (void)pageScrollView:(MBPageScrollView *)pageScrollView pageDidDisappear:(UIView *)page;
- (void)pageScrollView:(MBPageScrollView *)pageScrollView didTouchItemAtIndex:(NSUInteger)index;

@end


/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBPageScrollView : UIScrollView <UIScrollViewDelegate>
{
    id<MBPageScrollViewDataSource>	dataSource_;
	id<MBPageScrollViewDelegate>	delegate_;
	UIEdgeInsets					pageInset_;
    BOOL                            infiniteScrollEnabled_;
    NSUInteger                      numberOfPages_;
    UIView                          *pageContainerView_;
    CGFloat                         preloadMargin_;
    NSTimeInterval                  animationInterval_;
    NSInteger                       animationRepeatCount_;
}

/**
 *  The object that acts as the data source of the receiving page scroll view.
 *
 *  @discussion The delegate must adopt the MBPageScrollViewDelegate protocol. The data source is not retained.
 */
@property(nonatomic,assign) id <MBPageScrollViewDataSource> dataSource;
/**
 *  The object that acts as the delegate of the receiving page scroll view.
 *
 *  @discussion The delegate must adopt the MBPageScrollViewDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id <MBPageScrollViewDelegate> delegate;
@property(nonatomic,readonly) UIView* pageContainerView;
@property(nonatomic,assign) NSUInteger		currentPage;
@property(nonatomic,assign) UIEdgeInsets	pageInset;
@property(nonatomic,assign) BOOL infiniteScrollEnabled;
@property(nonatomic,assign) CGFloat preloadMargin;
@property(nonatomic,assign) BOOL tapToScrollEnabled;

// Animation
@property(nonatomic) NSTimeInterval animationInterval;

- (id)dequeueReusablePage;
- (id)dequeueReusablePageWithIdentifier:(NSString *)identifier;
- (id)pageForIndex:(NSUInteger)index;
- (void)reloadData;
- (void)reloadPageAtIndex:(NSUInteger)index;
- (void)setCurrentPage:(NSUInteger)currentPage animated:(BOOL)animated;
- (NSUInteger)numberOfPages;

// Animation
- (BOOL)isAnimating;
- (void)startAnimating;
- (void)stopAnimating;

@end


@protocol MBPageScrollViewDataSource <NSObject>

- (NSInteger)pageScrollViewNumberOfPages:(MBPageScrollView *)pageScrollView;
- (UIView *)pageScrollView:(MBPageScrollView *)pageScrollView pageForIndex:(NSInteger)index;

@end