//
//  MBPageScrollView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 10/12/12.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MBPageScrollView.h"
#import <objc/runtime.h>

@interface MBPageScrollView ()
{
    NSMutableDictionary*    reusableViews_;
    CGSize					tileSize_;
    NSInteger				firstVisiblePage_;
    NSInteger				lastVisiblePage_;
	NSInteger				previousPage_;
    NSTimer                 *animationTimer_;
    BOOL                    timerSuspended_;
}

@end

@implementation MBPageScrollView

@synthesize pageContainerView = pageContainerView_;
@synthesize dataSource = dataSource_;
@synthesize delegate = delegate_;
@synthesize pageInset = pageInset_;
@synthesize infiniteScrollEnabled = infiniteScrollEnabled_;
@synthesize preloadMargin = preloadMargin_;
@synthesize tapToScrollEnabled = tapToScrollEnabled_;
@synthesize animationInterval = animationInterval_;

static id MBPageScrollView_indexAssociationKey;

const NSInteger kReuseStackLimit = 15; // Maximum number of reusable views for a reusableIdentifier

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        previousPage_           = -1;
        firstVisiblePage_       = NSIntegerMax;
        lastVisiblePage_        = NSIntegerMin;
		pageInset_              = UIEdgeInsetsZero;
        infiniteScrollEnabled_  = NO;
        preloadMargin_          = 25;
        tapToScrollEnabled_     = NO;
        
        // Animation
        animationInterval_      = 5; // Measured in seconds.
        
		[self setCanCancelContentTouches:NO];
        [self setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
		[self setPagingEnabled:YES];
        [self setShowsHorizontalScrollIndicator:NO];
        [self setAlwaysBounceHorizontal:YES];
        
        reusableViews_      = [[NSMutableDictionary alloc] init];
        pageContainerView_  = [[[UIView alloc] initWithFrame:self.bounds] autorelease];
        [pageContainerView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        [self addGestureRecognizer:[[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MB_touchUpInside:)] autorelease]];
        
        [self addSubview:pageContainerView_];
        // the PagedScrollView is its own UIScrollViewDelegate, so we can handle our own zooming.
        // We need to return our pageContainerView as the view for zooming, and we also need to receive
        // the scrollViewDidEndZooming: delegate callback so we can update our resolution.
        [super setDelegate:self];
		[self MB_checkPageChanged:self.contentOffset];
    }
    return self;
}

- (void)dealloc
{
    [self stopAnimating];
    [reusableViews_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_animationTimerIntervalElapsed:(NSTimer *)sender
{
    if (!self.isDragging) {
        [self MB_selectNextPageAnimated:YES];
    }
}

- (void)MB_checkPageChanged:(CGPoint)contentOffset
{
    if (numberOfPages_ > 0) {
        CGFloat pageWidth = self.bounds.size.width;
        CGFloat fractionalPage = contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage) % numberOfPages_;
        
        if (previousPage_ != page) {
            if (delegate_ != nil && [delegate_ respondsToSelector:@selector(pageScrollView:pageHasChanged:)]) {
                [delegate_ pageScrollView:self pageHasChanged:page];
            }
            previousPage_ = page;
        }
    }
}

- (UIView *)MB_createPageForIndex:(NSUInteger)index
{
	UIView *newPage = [dataSource_ pageScrollView:self pageForIndex:index];
    
	if(newPage) {
        [self MB_setIndex:index forPage:newPage];
		[pageContainerView_ addSubview:newPage];
		[newPage setNeedsLayout];
	}
	return newPage;
}

- (void)MB_recycleView:(UIView *)view
{
	NSString *reuseIdentifier = nil;
    
    if ([view respondsToSelector:@selector(reuseIdentifier)]) {
        reuseIdentifier = [view performSelector:@selector(reuseIdentifier)];
    }
    
    if (!reuseIdentifier) {
        reuseIdentifier = @"";
    }
	
	NSMutableSet* viewSet = [reusableViews_ objectForKey:reuseIdentifier];
	
	if(viewSet == nil) {
		viewSet = [[[NSMutableSet alloc] init] autorelease];
		[reusableViews_ setObject:viewSet forKey:reuseIdentifier];
	}
    // Check maximum number of reusable views for a set
    if (viewSet.count < kReuseStackLimit) {
        [viewSet addObject:view];
    }
	[view removeFromSuperview];
}

- (void)MB_recenterIfNecessary
{
    CGFloat width = numberOfPages_ * self.bounds.size.width;
    CGFloat offset = 0;
    
    if (self.contentOffset.x < width) {
        offset = width;
    } else if (self.contentOffset.x > width*2) {
        offset = -width;
    }
    
    if (offset != 0) {
        self.contentOffset = CGPointMake(self.contentOffset.x + offset, self.contentOffset.y);
        // Move content by the same amount so it appears to stay still
        for (UIView *page in self.pageContainerView.subviews) {
            CGRect frame = [self.pageContainerView convertRect:page.frame toView:self];
            frame.origin.x += offset;
            [page setFrame:[self convertRect:frame toView:self.pageContainerView]];
        }
    }
}

- (void)MB_setIndex:(NSInteger)index forPage:(UIView *)page
{
    // Set runtime association of object
    // param -  source object for association, association key, association value, policy of association
    objc_setAssociatedObject(page, &MBPageScrollView_indexAssociationKey, [NSNumber numberWithInteger:index], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSInteger)MB_pageIndex:(UIView *)page
{
    // Get runtime association of object
    return [objc_getAssociatedObject(page, &MBPageScrollView_indexAssociationKey) integerValue];
}

- (void)MB_setCurrentPage:(NSUInteger)currentPage animated:(BOOL)animated
{
    if (currentPage < numberOfPages_) {
        CGFloat pageWidth = self.bounds.size.width;
        CGFloat newOffset = currentPage * pageWidth;
        
        if (self.infiniteScrollEnabled) {
            CGFloat totalWidth = numberOfPages_ * pageWidth;
            CGFloat offset = NSIntegerMax;
            CGFloat distance = 0;
            
            // Find smallest distance to target point
            for (NSInteger index = 0; index < 3; index++) {
                distance = (newOffset + (totalWidth * index)) - self.contentOffset.x;
                if (ABS(distance) < ABS(offset)) {
                    offset = distance;
                }
            }
            newOffset = self.contentOffset.x + offset;
        }
        CGPoint contentOffset = CGPointMake(newOffset, self.contentOffset.y);
        [self setContentOffset:contentOffset animated:animated];
        [self MB_checkPageChanged:contentOffset];
    }
}

- (void)MB_selectPreviousPageAnimated:(BOOL)animated
{
    NSUInteger currentPage = self.currentPage;
    NSUInteger newPage = currentPage > 0 ? (currentPage - 1) : (self.numberOfPages - 1);
    if (newPage != currentPage) {
        [self MB_setCurrentPage:newPage animated:animated];
    }
}

- (void)MB_selectNextPageAnimated:(BOOL)animated
{
    NSUInteger currentPage = self.currentPage;
    NSUInteger newPage = currentPage < (self.numberOfPages - 1) ? (currentPage + 1) : 0;
    if (newPage != currentPage) {
        [self MB_setCurrentPage:newPage animated:animated];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (id)dequeueReusablePageWithIdentifier:(NSString *)identifier
{
    UIView *page = nil;
    
    if (!identifier) {
        [NSException raise:NSInvalidArgumentException format:@"identifier must not be nil."];
    }
    
    NSMutableSet* viewSet = [reusableViews_ objectForKey:identifier];
    
    if (viewSet) {
        page = [viewSet anyObject];
        if (page) {
            // The only object retaining the page is our reusablePages set, so we have to retain/autorelease it
            // before returning it so that it's not immediately deallocated when we remove it from the set
            [[page retain] autorelease];
            [viewSet removeObject:page];
            if ([page respondsToSelector:@selector(prepareForReuse)]) {
                [page performSelector:@selector(prepareForReuse)];
            }
        }
    }
	return page;
}

- (id)dequeueReusablePage
{
    return [self dequeueReusablePageWithIdentifier:@""];
}

- (id)pageForIndex:(NSUInteger)index
{
	id result = nil;
	
	NSUInteger item = 0;
	UIView* page = nil;
	
	while (item < pageContainerView_.subviews.count && result == nil) {
		page = [pageContainerView_.subviews objectAtIndex:item];
        NSInteger pageIndex = [self MB_pageIndex:page];
		if(pageIndex == index) {
			result = page;
		}
		item++;
	}
	return result;
}

- (void)reloadData
{
    for (UIView *view in pageContainerView_.subviews) {
        [self MB_recycleView:view];
    }
    firstVisiblePage_ = NSIntegerMax;
    lastVisiblePage_  = NSIntegerMin;
    numberOfPages_ = [dataSource_ pageScrollViewNumberOfPages:self];
    [self setNeedsLayout];
	[self MB_checkPageChanged:self.contentOffset];
}

- (void)reloadPageAtIndex:(NSUInteger)index
{
	UIView *page = [self pageForIndex:index];
	if(page) {
        CGRect frame = page.frame;
		[self MB_recycleView:page];
		page = [self MB_createPageForIndex:index];
        [page setFrame:frame];
	}
}

- (NSUInteger)numberOfPages
{
    return numberOfPages_;
}

- (BOOL)isAnimating
{
    // If the timer exists, it means that the class is animating.
    return (animationTimer_ != nil);
}

- (void)startAnimating
{
    [self stopAnimating];
    timerSuspended_ = NO;
    animationTimer_ = [NSTimer scheduledTimerWithTimeInterval:self.animationInterval target:self selector:@selector(MB_animationTimerIntervalElapsed:) userInfo:nil repeats:YES];
}

- (void)stopAnimating
{
    if (animationTimer_) {
        // Remove timer from NSRunLoop
        [animationTimer_ invalidate];
        animationTimer_ = nil;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setDataSource:(id<MBPageScrollViewDataSource>)dataSource
{
    if (dataSource_ != dataSource) {
        dataSource_ = dataSource;
        [self reloadData];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize pageSize  = self.bounds.size;
    NSInteger pageIndex = 0;
    
    BOOL infiniteScroll = (self.infiniteScrollEnabled && numberOfPages_ > 1);
    
    if (infiniteScroll) {
        NSAssert(self.showsHorizontalScrollIndicator == FALSE, @"Can't show scrollbar when infiniteScroll is enabled");
        [self setContentSize:CGSizeMake(numberOfPages_ * self.bounds.size.width * 3, self.bounds.size.height)];
        [pageContainerView_ setFrame:CGRectMake(0,0, self.contentSize.width, self.contentSize.height)];
        [self MB_recenterIfNecessary];
    } else {
        // set the content size so it can be scrollable
        [self setContentSize:CGSizeMake((numberOfPages_ * pageSize.width), self.bounds.size.height)];
        [pageContainerView_ setFrame:CGRectMake(0, 0, self.contentSize.width, self.contentSize.height)];
    }
    
    CGRect contentBounds = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(-preloadMargin_, -preloadMargin_, -preloadMargin_, -preloadMargin_));
    
    // Recycle all pages that are no longer visible
    for (UIView *page in pageContainerView_.subviews) {
        CGRect scaledPageFrame = [pageContainerView_ convertRect:page.frame toView:self];
		
        if (!CGRectIntersectsRect(scaledPageFrame, contentBounds)) {
            // Retain temporarily in case the page isn't recycled but thrown away
            [[page retain] autorelease];
			[self MB_recycleView:page];
            if ([delegate_ respondsToSelector:@selector(pageScrollView:pageDidDisappear:)]) {
                [delegate_ pageScrollView:self pageDidDisappear:page];
            }
        }
    }
    
	NSInteger firstNeededPage = MAX(0, floorf((contentBounds.origin.x) / pageSize.width));
	NSInteger lastNeededPage  = ceilf((CGRectGetMaxX(contentBounds)) / pageSize.width) - 1;
    
    if (!infiniteScroll) {
        lastNeededPage  = MIN((numberOfPages_ - 1), lastNeededPage);
    }
    
    if (numberOfPages_ > 0) {
        // Iterate through needed pages, adding any pages that are missing
        for (NSInteger item = firstNeededPage; item <= lastNeededPage; item++) {
            
            CGRect frame = CGRectMake(pageSize.width * item, 0.0, pageSize.width, pageSize.height);
            
            pageIndex = item % numberOfPages_;
            
            UIView *page = [self pageForIndex:pageIndex];
            
            if (!page) {
                page = [self MB_createPageForIndex:pageIndex];
            }
            // When the number of pages is less than 3, preloading the page would cause the page to disappear on one side while scrolling.
            // In that case only set the frame when it's inside the bounds
            if (numberOfPages_ > 2 || CGRectIntersectsRect(frame, self.bounds)) {
                [page setFrame:frame];
            }
        }
    }
    
    // Update our record of which pages are visible
    firstVisiblePage_ = firstNeededPage;
    lastVisiblePage_  = lastNeededPage;
}

- (void)setFrame:(CGRect)frame
{
    BOOL enabled = [UIView areAnimationsEnabled];
    
    [UIView setAnimationsEnabled:NO];
    
    CGRect oldFrame = self.frame;
    NSInteger page = [self currentPage];
    
    [super setFrame:frame];
    
    // Restore selected page when frame has changed
    if (oldFrame.size.width != frame.size.width) {
        [self MB_setCurrentPage:page animated:NO];
        [self setNeedsLayout];
    }
    [UIView setAnimationsEnabled:enabled];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return pageContainerView_;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	if ([delegate_ respondsToSelector:@selector(pageScrollViewWillBeginDragging:)]) {
		[delegate_ pageScrollViewWillBeginDragging:self];
	}
    if ([self isAnimating]) {
        [self stopAnimating];
        timerSuspended_ = YES;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (timerSuspended_) {
        [self startAnimating];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[self MB_checkPageChanged:self.contentOffset];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSUInteger)currentPage
{
	CGFloat pageWidth = self.bounds.size.width;
	NSUInteger page = floor((self.contentOffset.x - (pageWidth * 0.5)) / pageWidth) + 1;
    return numberOfPages_ > 0 ? (page % numberOfPages_) : 0;
}

- (void)setCurrentPage:(NSUInteger)currentPage animated:(BOOL)animated
{
    [self MB_setCurrentPage:currentPage animated:animated];
    if ([self isAnimating]) {
        // Restart timer for animation
        [self startAnimating];
    }
}

- (void)setCurrentPage:(NSUInteger)currentPage
{
    [self setCurrentPage:currentPage animated:NO];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Gestures

- (void)MB_touchUpInside:(UITapGestureRecognizer *)gesture
{
    if (self.numberOfPages > 0) {
        CGPoint point = [gesture locationInView:self];
        
        if (CGRectContainsPoint(self.bounds, point)) {
            if([delegate_ respondsToSelector:@selector(pageScrollView:didTouchItemAtIndex:)]) {
                [delegate_ pageScrollView:self didTouchItemAtIndex:self.currentPage];
            }
        } else {
            if (self.tapToScrollEnabled) {
                if (point.x < CGRectGetMinX(self.bounds)) {
                    [self MB_selectPreviousPageAnimated:YES];
                } else if (point.x > CGRectGetMaxX(self.bounds)) {
                    [self MB_selectNextPageAnimated:YES];
                }
            }
        }
    }
}

@end
