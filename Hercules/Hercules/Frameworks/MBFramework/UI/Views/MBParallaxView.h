//
//  MBParallaxView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 24/10/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The MBParallaxView class provides support for a parallax effect with its content. With paralax as the content offset of the view moves, the objects in the distance appear to move slower than the objects close to the camera.
 The parallax view can be set up to zoom its contents when the value specified in maximumParallaxValues is reached.
 */
@interface MBParallaxView : UIView

/**
 Adds a view to the end of the receiver’s list of subviews.
 @param view     The view to be added. After being added, this view appears on top of any other subviews.
 @param distance The 'distance' for the parralax effect.
 @discussion A value of 1 means maximum parallax (far away from the screen, object appears to stand still). Use a value of 0 for no parallax, (object is nearby).
 */
- (void)addSubview:(UIView *)view withParallaxFactor:(CGFloat)distance;
/**
 Inserts a view above another view in the view hierarchy.
 @param view     The view to insert. It’s removed from its superview if it’s not a sibling of siblingSubview.
 @param siblingSubview  The sibling view that will be behind the inserted view.
 @param distance The 'distance' for the parralax effect.
 @discussion A value of 1 means maximum parallax (far away from the screen, object appears to stand still). Use a value of 0 for no parallax, (object is nearby).
 */
- (void)insertSubview:(UIView *)view aboveSubview:(UIView *)siblingSubview withParallaxFactor:(CGFloat)distance;
/**
 Inserts a subview at the specified index.
 @param view     The view to insert. This value cannot be nil.
 @param index    The index in the array of the subviews property at which to insert the view. Subview indices start at 0 and cannot be greater than the number of subviews.
 @param distance The 'distance' for the parralax effect.
 @discussion A value of 1 means maximum parallax (far away from the screen, object appears to stand still). Use a value of 0 for no parallax, (object is nearby).
 */
- (void)insertSubview:(UIView *)view atIndex:(NSInteger)index withParallaxFactor:(CGFloat)distance;
/**
 Inserts a view below another view in the view hierarchy.
 @param view     The view to insert below another view. It’s removed from its superview if it’s not a sibling of siblingSubview.
 @param siblingSubview The sibling view that will be above the inserted view.
 @param distance The 'distance' for the parralax effect.
 @discussion A value of 1 means maximum parallax (far away from the screen, object appears to stand still). Use a value of 0 for no parallax, (object is nearby).
 */
- (void)insertSubview:(UIView *)view belowSubview:(UIView *)siblingSubview withParallaxFactor:(CGFloat)distance;
/**
 The point at which the origin of the parallax view is offset for the parallax effect.
 */
@property(nonatomic,assign) CGPoint contentOffset;
/**
 Returns the calculated offset for the parallax effect.
 */
@property(nonatomic,assign,readonly) CGPoint parallaxOffset;
/**
 Returns the content view of the cell object. (read-only)
 @discussion The content view of a MBParallaxView object is the default superview for content displayed by the parallax view.
 */
@property(nonatomic,readonly) UIView *contentView;
/**
 Specifies the minimum parallax factor that can be applied to the parallax view's content.
 */
@property(nonatomic,assign) CGPoint minimumParallaxValues;
/**
 Specifies the maximum parallax factor that can be applied to the parallax view's content.
 */
@property(nonatomic,assign) CGPoint maximumParallaxValues;
/**
 Specifies the minimum scale factor that can be applied to the parallax view's content.
 */
@property(nonatomic) CGFloat maximumZoomScale;
/**
 Specifies the maximum scale factor that can be applied to the parallax view's content.
 */
@property(nonatomic) CGFloat minimumZoomScale;

@end
