//
//  MBParallaxView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 24/10/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBParallaxView.h"
#import <objc/runtime.h>

@interface MBParallaxView ()
{
    CGFloat zoomScale_;
    CGFloat zoomRatio_;
}
@end

@implementation MBParallaxView

@synthesize contentOffset = contentOffset_;
@synthesize contentView = contentView_;
@synthesize minimumParallaxValues = minimumParallaxValues_;
@synthesize maximumParallaxValues = maximumParallaxValues_;
@synthesize maximumZoomScale = maximumZoomScale_;
@synthesize minimumZoomScale = minimumZoomScale_;
@synthesize parallaxOffset = parallaxOffset_;

static id MBParallaxView_distanceAssociationKey;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        contentView_ = [[[UIView alloc] initWithFrame:self.bounds] autorelease];
        [contentView_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
        [self addSubview:contentView_];
        [contentView_.layer setMask:[CALayer layer]];
        [contentView_.layer.mask setBackgroundColor:[[UIColor blackColor] CGColor]];
    
        self.minimumParallaxValues = CGPointMake(-CGFLOAT_MAX, -CGFLOAT_MAX);
        self.maximumParallaxValues = CGPointMake(CGFLOAT_MAX, CGFLOAT_MAX);
        self.maximumZoomScale = 1.0;
        self.minimumZoomScale = 1.0;
        zoomScale_ = 1.0;
        zoomRatio_ = 0.01;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_updateMaskLayer
{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    CGPoint offset = self.contentOffset;
    CGRect clipRect = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(offset.y, 0, 0, 0));
    [contentView_.layer.mask setFrame:clipRect];
    [CATransaction commit];
}

- (void)MB_setDistance:(CGFloat)distance forView:(UIView *)view
{
    // Set runtime association of object
    // param -  source object for association, association key, association value, policy of association
    objc_setAssociatedObject(view, &MBParallaxView_distanceAssociationKey, [NSNumber numberWithFloat:distance], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CGFloat)MB_distanceForView:(UIView *)view
{
    // Get runtime association of object
    // Param -  source object for association, association key, association value, policy of association
    NSNumber *distance = objc_getAssociatedObject(view, &MBParallaxView_distanceAssociationKey);
    return distance != nil ? [distance floatValue] : 0.0;
}

- (void)MB_updateParallaxValues
{
    CGPoint newParallaxOffset = CGPointZero;
    // Check offset limits
    newParallaxOffset.x = MIN(MAX(self.contentOffset.x, self.minimumParallaxValues.x), self.maximumParallaxValues.x);
    newParallaxOffset.y = MIN(MAX(self.contentOffset.y, self.minimumParallaxValues.y), self.maximumParallaxValues.y);
    
    if (!CGPointEqualToPoint(parallaxOffset_, newParallaxOffset)) {
        parallaxOffset_ = newParallaxOffset;
        
        CGFloat transformX = 0;
        CGFloat transformY = 0;
        CGFloat distance = 0;
        
        for (UIView *subview in contentView_.subviews) {
            distance = [self MB_distanceForView:subview];
            
            transformX = 0;
            transformY = parallaxOffset_.y * distance;
            
            [subview setTransform:CGAffineTransformMakeTranslation(transformX, transformY)];
        }
    }
    
    if (self.contentOffset.y > self.maximumParallaxValues.y) {
        CGFloat zoomScale = 1.0 - (self.contentOffset.y - self.maximumParallaxValues.y) * zoomRatio_;
        zoomScale = MAX(zoomScale, self.minimumZoomScale);
        [self setZoomScale:zoomScale];
    } else if (self.contentOffset.y < self.minimumParallaxValues.y) {
        CGFloat zoomScale = 1.0 + (self.minimumParallaxValues.y - self.contentOffset.y) * zoomRatio_;
        zoomScale = MIN(zoomScale, self.maximumZoomScale);
        [self setZoomScale:zoomScale];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)addSubview:(UIView *)view withParallaxFactor:(CGFloat)distance
{
    [self MB_setDistance:distance forView:view];
    [contentView_ addSubview:view];
}

- (void)insertSubview:(UIView *)view aboveSubview:(UIView *)siblingSubview withParallaxFactor:(CGFloat)distance
{
    [self MB_setDistance:distance forView:view];
    [contentView_ insertSubview:view aboveSubview:siblingSubview];
}

- (void)insertSubview:(UIView *)view atIndex:(NSInteger)index withParallaxFactor:(CGFloat)distance
{
    [self MB_setDistance:distance forView:view];
    [contentView_ insertSubview:view atIndex:index];
}

- (void)insertSubview:(UIView *)view belowSubview:(UIView *)siblingSubview withParallaxFactor:(CGFloat)distance
{
    [self MB_setDistance:distance forView:view];
    [contentView_ insertSubview:view belowSubview:siblingSubview];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self MB_updateMaskLayer];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setContentOffset:(CGPoint)contentOffset
{
    if (!CGPointEqualToPoint(contentOffset_, contentOffset)) {
        contentOffset_ = contentOffset;
        [self MB_updateParallaxValues];
        [self MB_updateMaskLayer];
    }
}

- (void)setZoomScale:(CGFloat)zoomScale
{
    if (zoomScale_ != zoomScale) {
        zoomScale_ = zoomScale;
        CGAffineTransform scale = CGAffineTransformMakeScale(zoomScale_, zoomScale_);
        // Zoom and translate back to the center of the view
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, (self.contentOffset.y * 0.5));
        CGAffineTransform transform = CGAffineTransformConcat(scale, translate);
        
        for (UIView *subview in contentView_.subviews) {
            [subview setTransform:transform];
        }
    }
}

@end
