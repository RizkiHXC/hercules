//
//  MBPickerViewCell.h
//  MBFramework
//
//  Created by Arno Woestenburg on 18/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The MBPickerViewCell class defines the attributes and behavior of the cells that appear in UIKit's UIPickerView objects. This class includes properties and methods for setting and managing cell content and background (including text, images, and custom views).
 */
@interface MBPickerViewCell : UIView

/**
 Returns the content view of the cell object. (read-only)
 @discussion The content view of a MBPickerViewCell object is the default superview for content displayed by the cell.
 */
@property(nonatomic, readonly, retain) UIView *contentView;

/**
 The inset or outset margins for the rectangle surrounding all of the view's content.
 */
@property(nonatomic) UIEdgeInsets contentEdgeInsets;

/**
 The inset or outset margins for the rectangle around the text label.
 */
@property(nonatomic) UIEdgeInsets titleEdgeInsets;

/**
 Returns the label used for the textual content of the picker cell. (read-only)
 @discussion Default style for this label attempts to match the default UIPickerView's title style.
 */
@property(nonatomic, readonly, retain) UILabel *textLabel;

@end
