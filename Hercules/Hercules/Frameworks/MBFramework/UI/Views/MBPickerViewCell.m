//
//  MBPickerViewCell.m
//  MBFramework
//
//  Created by Arno Woestenburg on 18/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBPickerViewCell.h"
#import "UIDevice+MBFramework.h"

@implementation MBPickerViewCell

@synthesize contentView = contentView_;
@synthesize contentEdgeInsets = contentEdgeInsets_;
@synthesize titleEdgeInsets = titleEdgeInsets_;
@synthesize textLabel = textLabel_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Content view
        contentView_ = [[[UIView alloc] init] autorelease];
        [self addSubview:contentView_];
        
        // Text label
        textLabel_ = [[[UILabel alloc] init] autorelease];
        [contentView_ addSubview:textLabel_];
        
        [self MB_initializeStyle];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_initializeStyle
{
    [textLabel_ setBackgroundColor:[UIColor clearColor]];
    [textLabel_ setTextColor:[UIColor blackColor]];
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    // Default to iOS basic styles
    if ([[UIDevice currentDevice] systemVersionLower:@"7"]) {
        titleEdgeInsets_ = UIEdgeInsetsMake(0.0f, 7.0f, 0.0f, 7.0f);
        [textLabel_ setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
    } else {
#endif
        // iOS 7+
        [textLabel_ setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.0f]];
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    }
#endif
    [textLabel_ setTextAlignment:NSTextAlignmentCenter];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    [contentView_ setFrame:UIEdgeInsetsInsetRect(self.bounds, contentEdgeInsets_)];
    [textLabel_ setFrame:UIEdgeInsetsInsetRect(contentView_.bounds, titleEdgeInsets_)];
}


@end
