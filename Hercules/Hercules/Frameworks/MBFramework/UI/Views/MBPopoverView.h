//
//  MBPopoverView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 08/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The style of the popover view
 */
typedef NS_ENUM(NSInteger, MBPopoverViewStyle) {
    /**
     A black translucent background with white contents.
     */
    MBPopoverViewStyleDefault,
    /**
     A white translucent background with dark gray contents.
     */
    MBPopoverViewStyleWhiteTranslucent,
};

/**
 Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBPopoverView : UIView

- (instancetype)initWithMessage:(NSString *)message;

- (void)show;
- (void)showOnView:(UIView *)view;
- (void)showOnView:(UIView *)view aboveSubview:(UIView *)subview;
- (void)dismiss;
- (void)dismissAnimated:(BOOL)animated;
- (void)setMessage:(NSString *)message;
- (void)setDismissAfterDelay:(NSTimeInterval)delay; // NSTimeInterval is always specified in seconds

@property(nonatomic) CGSize constrainedToSize;
@property(nonatomic,readonly) UIView *containerView;
/**
 The inset or outset margins for the rectangle surrounding all of the popover view's content.
 */
@property(nonatomic) UIEdgeInsets contentEdgeInsets;
@property(nonatomic,retain) UIView *accessoryView;
@property(nonatomic,readonly) UILabel *messageLabel;
@property(readwrite,nonatomic,retain) UIColor *color;
/**
 The style of the popover view. Default is MBPopoverViewStyleDefault, a black translucent background with white contents.
 */
@property(nonatomic,assign) MBPopoverViewStyle popoverViewStyle UI_APPEARANCE_SELECTOR;
/**
 The title’s text attributes.
 @discussion The properties set in the messageTextAttributes overrule the style settings made with the popoverViewStyle setting.
 */
@property(nonatomic,copy) NSDictionary *messageTextAttributes UI_APPEARANCE_SELECTOR;

@property(nonatomic,assign) BOOL allowTapToClose;
@property(nonatomic,readonly) UITapGestureRecognizer *tapGestureRecognizer;


@end