//
//  MBPopoverView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 08/11/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBPopoverView.h"
#import <QuartzCore/QuartzCore.h>

@interface MBPopoverView()
{
    UIView *centerView_;
    UIView *containerView_;
}
@property(nonatomic,retain) UILabel *messageLabel;

@end

@implementation MBPopoverView

@synthesize accessoryView = accessoryView_;
@synthesize color = color_;
@synthesize constrainedToSize = constrainedToSize_;
@synthesize containerView = containerView_;
@synthesize contentEdgeInsets = contentEdgeInsets_;
@synthesize messageLabel = messageLabel_;
@synthesize popoverViewStyle = popoverViewStyle_;
@synthesize tapGestureRecognizer = tapGestureRecognizer_;
@synthesize messageTextAttributes = messageTextAttributes_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithMessage:(NSString *)message
{
    self = [super init];
    if (self) {
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self setAllowTapToClose:NO];
        
        // Default settings, use class properties if you want different values.
        contentEdgeInsets_ = UIEdgeInsetsMake(15, 15, 15, 15);
        constrainedToSize_ = CGSizeMake(200, CGFLOAT_MAX);
        
        containerView_ = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 300)] autorelease];
        [containerView_.layer setCornerRadius:10];
        [self addSubview:containerView_];
        
        if (message.length > 0) {
            [self.messageLabel setText:message];
        }
        [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.1]];
        popoverViewStyle_ = MBPopoverViewStyleDefault;
        [self MB_updatePopoverViewStyle];
        
        tapGestureRecognizer_ = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MB_popoverViewTapped:)] autorelease];
        [self addGestureRecognizer:tapGestureRecognizer_];
    }
    return self;
}

- (void)dealloc
{
    [accessoryView_ release];
    [centerView_ release];
    [color_ release];
    [messageLabel_ release];
    [messageTextAttributes_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    CGRect containerRect = CGRectZero;
    CGFloat spacing = 5;
    
    CGSize maxLabelSize = CGSizeMake((constrainedToSize_.width - (contentEdgeInsets_.left + contentEdgeInsets_.right)), (constrainedToSize_.height - (contentEdgeInsets_.top + contentEdgeInsets_.bottom) - (CGRectGetMaxY(accessoryView_.frame) + spacing)));
    
    CGSize labelSize = [messageLabel_ sizeThatFits:maxLabelSize];
    
    containerRect.size.width = MAX(accessoryView_.bounds.size.width, labelSize.width) + contentEdgeInsets_.left + contentEdgeInsets_.right;
    containerRect.size.height = accessoryView_.bounds.size.height + contentEdgeInsets_.top + contentEdgeInsets_.bottom;
    if (labelSize.height > 0) {
        containerRect.size.height += labelSize.height + spacing;
    }
    
    if (centerView_) {
        CGRect centerRect = [self convertRect:centerView_.frame fromView:centerView_.superview];
        containerRect.origin.x = centerRect.origin.x + roundf((centerRect.size.width - containerRect.size.width) * 0.5);
        containerRect.origin.y = centerRect.origin.y + roundf((centerRect.size.height - containerRect.size.height) * 0.5);
    } else {
        containerRect.origin.x = roundf((self.bounds.size.width - containerRect.size.width) * 0.5);
        containerRect.origin.y = roundf((self.bounds.size.height - containerRect.size.height) * 0.5);
    }
    
    [containerView_ setFrame:containerRect];
    
    CGRect controlRect = CGRectMake(roundf((containerRect.size.width - accessoryView_.bounds.size.width) * 0.5), contentEdgeInsets_.top, accessoryView_.bounds.size.width, accessoryView_.bounds.size.height);
    
    [accessoryView_ setFrame:controlRect];

    if (messageLabel_) {
        controlRect.size = labelSize;
        if (accessoryView_) {
            controlRect.origin.y = CGRectGetMaxY(accessoryView_.frame) + spacing;
        } else {
            controlRect.origin.y = roundf((containerRect.size.height - labelSize.height) * 0.5);
        }
        controlRect.origin.x = roundf((containerRect.size.width - labelSize.width) * 0.5);
        [messageLabel_ setFrame:controlRect];
    }
}

- (void)removeFromSuperview
{
    [super removeFromSuperview];
    [centerView_ release];
    centerView_ = nil;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_updatePopoverViewStyle
{
    switch (popoverViewStyle_) {
        case MBPopoverViewStyleWhiteTranslucent:
            [containerView_ setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.95f]];
            self.color = [UIColor colorWithWhite:0.19f alpha:1.0f];
            break;
        default:
            // MBPopoverViewStyleDefault
            [containerView_ setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.8f]];
            self.color = [UIColor whiteColor];
            break;
    }
}

- (void)MB_applyMessageLabelStyle
{
    // Apply message text attributes that are set.
    UIColor *textColor = messageTextAttributes_[NSForegroundColorAttributeName];
    if (!textColor) {
        textColor = self.color;
    }
    [messageLabel_ setTextColor:textColor];
    
    UIFont *font = messageTextAttributes_[NSFontAttributeName];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:14.0f];
    }
    [messageLabel_ setFont:font];
    [messageLabel_ setTextAlignment:NSTextAlignmentCenter];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)show
{
    [self showOnView:nil];
}

- (void)showOnView:(UIView *)view
{
    if (!view) {
        //TODO this does not work in presented viewcontroller, no subviews are found
        // Find a default view when no view is passed.
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        view = [keyWindow.subviews firstObject];
    }
    [self setFrame:view.bounds];
    [view addSubview:self];
}

- (void)showOnView:(UIView *)view aboveSubview:(UIView *)subview
{
    [centerView_ release];
    centerView_ = [subview retain];
    [self showOnView:view];
}

- (void)dismiss {
    [self dismissAnimated:YES];
}

- (void)dismissAnimated:(BOOL)animated
{
    if (animated) {
        CGFloat alpha = self.alpha;
        [UIView animateWithDuration:0.3
                         animations:^{
                             [self setAlpha:0];
                         } completion:^(BOOL finished) {
                             [self removeFromSuperview];
                             [self setAlpha:alpha]; // Restore alpha
                         }];
        
    } else {
        [self removeFromSuperview];
    }
}

- (void)setMessage:(NSString *)message
{
    if (message.length > 0) {
        [self.messageLabel setText:message];
    } else {
        [self.messageLabel removeFromSuperview];
        self.messageLabel = nil;
    }
    [self setNeedsLayout];
}

- (void)setDismissAfterDelay:(NSTimeInterval)delay
{
    // NSTimeInterval is always specified in seconds
    // Cancel the delayed request and dismiss right away.
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismiss) object:nil];
    // Automatically dismiss after 2 seconds
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:delay];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setAccessoryView:(UIView *)accessoryView
{
    if (accessoryView_ != accessoryView) {
        [accessoryView_ removeFromSuperview];
        [accessoryView_ release];
        accessoryView_ = [accessoryView retain];
        [containerView_ addSubview:accessoryView_];
        [self setNeedsLayout];
    }
}

- (void)setColor:(UIColor *)color
{
    if (color_ != color) {
        [color_ release];
        color_ = [color retain];
        // Update label color if there is no text attribute color.
        if (messageLabel_ && !messageTextAttributes_[NSForegroundColorAttributeName]) {
            [messageLabel_ setTextColor:color_];
        }
    }
}

- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets
{
    if (!UIEdgeInsetsEqualToEdgeInsets(contentEdgeInsets, contentEdgeInsets_)) {
        contentEdgeInsets_ = contentEdgeInsets;
        [self setNeedsLayout];
    }
}

- (UILabel *)messageLabel
{
    if (!messageLabel_) {
        // Create on demand
        messageLabel_ = [[UILabel alloc] init];
        [messageLabel_ setBackgroundColor:[UIColor clearColor]];
        [messageLabel_ setNumberOfLines:0];
        [self MB_applyMessageLabelStyle];
        [containerView_ addSubview:messageLabel_];
    }
    return messageLabel_;
}

- (void)setPopoverViewStyle:(MBPopoverViewStyle)popoverViewStyle
{
    popoverViewStyle_ = popoverViewStyle;
    [self MB_updatePopoverViewStyle];
}

- (void)setMessageTextAttributes:(NSDictionary *)messageTextAttributes
{
    [messageTextAttributes_ release];
    messageTextAttributes_ = [messageTextAttributes copy];
    [self MB_applyMessageLabelStyle];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponder

- (void)MB_popoverViewTapped:(UITapGestureRecognizer *)sender
{
    if (self.allowTapToClose) {
        // When the popoverview is tapped, cancel the delayed request and dismiss right away.
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismiss) object:nil];
        [self dismiss];
    }
}


@end
