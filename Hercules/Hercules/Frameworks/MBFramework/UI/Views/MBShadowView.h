//
//  MBShadowView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 27/01/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Options to specify how the shadow is displayed.
 */
typedef NS_ENUM(NSInteger, MBShadowType)
{
    /**
     Outer shadow (drop shadow). This is the default option.
     */
    MBShadowTypeDefault,
    /**
     Inner shadow.
     */
    MBShadowTypeInnerShadow,
};

/**
 A shadow view object provides a view-based container for displaying a customizable shadow.
 */
@interface MBShadowView : UIView
{
    MBShadowType shadowType_;
}

+ (MBShadowView *)shadowViewWithType:(MBShadowType)shadowType frame:(CGRect)frame;

/**
 Initializes and returns a newly allocated ShadowView object with the specified frame rectangle.
 @param shadowType The type of shadow for the view. @seealso MBShadowType
 @param frame The frame rectangle for the view, measured in points. The origin of the frame is relative to the superview in which you plan to add it. This method uses the frame rectangle to set the center and bounds properties accordingly.
 @return An initialized ShadowView object or nil if the object couldn't be created.
 */
- (instancetype)initWithShadowType:(MBShadowType)shadowType frame:(CGRect)frame;

/**
 Specify how the shadow is displayed
 */
@property(nonatomic) MBShadowType shadowType;
/**
 The color of the shadow.
 */
@property(nonatomic,assign) UIColor *shadowColor;
/**
 The offset (in points) of the shadow.
 */
@property(nonatomic)        CGSize shadowOffset;
/**
 The opacity of the shadow.
 */
@property(nonatomic)        CGFloat shadowOpacity;
/**
 The blur radius (in points) used to render the shadow.
 */
@property(nonatomic)        CGFloat shadowRadius;
/**
 A Boolean that indicates whether the shadow is rendered as a bitmap before compositing. Default is YES.
 @discussion When the value of this property is YES, the layer is rendered as a bitmap in its local coordinate space and then composited to the destination with any other content.
 Set this value to YES to speed up rendering when the size of the view never or rarely changes.
 */
@property(nonatomic)        BOOL shouldRasterize;
/**
 The inset or outset margins for the shadow rectangle. The default value is UIEdgeInsetsZero.
 */
@property(nonatomic,assign) UIEdgeInsets shadowEdgeInsets;

@end
