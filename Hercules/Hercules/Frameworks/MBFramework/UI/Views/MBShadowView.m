//
//  MBShadowView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 27/01/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBShadowView.h"
#import <QuartzCore/QuartzCore.h>

@interface MBShadowView ()
{
    CAShapeLayer *shadowLayer_;
    CAShapeLayer *maskLayer_;
}

@end

@implementation MBShadowView

@synthesize shadowColor = shadowColor_;
@synthesize shadowEdgeInsets = shadowEdgeInsets_;
@synthesize shadowOffset = shadowOffset_;
@synthesize shadowOpacity = shadowOpacity_;
@synthesize shadowRadius = shadowRadius_;
@synthesize shadowType = shadowType_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (MBShadowView *)shadowViewWithType:(MBShadowType)shadowType frame:(CGRect)frame
{
    return [[[MBShadowView alloc] initWithShadowType:shadowType frame:frame] autorelease];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithShadowType:(MBShadowType)shadowType frame:(CGRect)frame
{
    shadowType_ = shadowType;
    self = [self initWithFrame:frame];
    if (self) {
        shadowEdgeInsets_ = UIEdgeInsetsZero;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setUserInteractionEnabled:NO];
        
        // Shadow layer
        shadowLayer_ = [CAShapeLayer layer];
        [shadowLayer_ setFrame:self.bounds];
        [self.layer addSublayer:shadowLayer_];
        
        // Mask layer
        maskLayer_ = [[CAShapeLayer layer] retain];
        [shadowLayer_ setMask:maskLayer_];
        
        // Default values
        [self setShadowColor:[UIColor blackColor]];
        [self setShadowOffset:CGSizeMake(0.0f, 0.0f)];
        [self setShadowRadius:5];
        [self setShadowOpacity:1.0f];
        
        [self setShadowType:shadowType_];
        [self setShouldRasterize:YES];
        
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [shadowLayer_ setFrame:self.bounds];
    
    switch (shadowType_) {
        case MBShadowTypeDefault:
            [self MB_layoutForOuterShadow];
            break;
        case MBShadowTypeInnerShadow:
            [self MB_layoutForInnerShadow];
            break;
        default:
            break;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_layoutForOuterShadow
{
    [shadowLayer_ setFillRule:kCAFillRuleNonZero];
    [maskLayer_ setFillRule:kCAFillRuleEvenOdd];    // Causes the inner region not be filled.
    
    CGPathRef path = [[UIBezierPath bezierPathWithRect:self.bounds] CGPath];
    [shadowLayer_ setPath:path];
    
    // Create the larger rectangle path.
    CGMutablePathRef maskPath = CGPathCreateMutable();
    CGPathAddRect(maskPath, NULL, CGRectInset(self.bounds, -20, -20));
    
    CGPathRef innerPath = [[UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.bounds, shadowEdgeInsets_)] CGPath];
    
    // Add the inner path so it's subtracted from the outer path.
    CGPathAddPath(maskPath, NULL, innerPath);
    CGPathCloseSubpath(maskPath);
    
    [maskLayer_ setPath:maskPath];
    CGPathRelease(maskPath);
}

- (void)MB_layoutForInnerShadow
{
    [self setClipsToBounds:YES];
    
    [shadowLayer_ setFillRule:kCAFillRuleEvenOdd];  // Causes the inner region not be filled.
    [maskLayer_ setFillRule:kCAFillRuleNonZero];
    
    // Create the larger rectangle path.
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectInset(self.bounds, -20, -20));
    
    CGPathRef innerPath = [[UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.bounds, shadowEdgeInsets_)] CGPath];
    
    // Add the inner path so it's subtracted from the outer path.
    CGPathAddPath(path, NULL, innerPath);
    CGPathCloseSubpath(path);
    
    [shadowLayer_ setPath:path];
    [maskLayer_ setPath:innerPath];
    CGPathRelease(path);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setShadowType:(MBShadowType)shadowType
{
    shadowType_ = shadowType;
    [self setClipsToBounds:(shadowType_ == MBShadowTypeInnerShadow)];
    [self setNeedsLayout];
}

- (void)setShadowColor:(UIColor *)shadowColor
{
    [shadowLayer_ setShadowColor:shadowColor.CGColor];
}

- (UIColor *)shadowColor
{
    return [UIColor colorWithCGColor:shadowLayer_.shadowColor];
}

- (void)setShadowOffset:(CGSize)shadowOffset
{
    [shadowLayer_ setShadowOffset:shadowOffset];
}

- (CGSize)shadowOffset
{
    return shadowLayer_.shadowOffset;
}

- (void)setShadowOpacity:(CGFloat)shadowOpacity
{
    [shadowLayer_ setShadowOpacity:shadowOpacity];
}

- (CGFloat)shadowOpacity
{
    return shadowLayer_.shadowOpacity;
}

- (void)setShadowRadius:(CGFloat)shadowRadius
{
    [shadowLayer_ setShadowRadius:shadowRadius];
}

- (CGFloat)shadowRadius
{
    return shadowLayer_.shadowRadius;
}

- (void)setShouldRasterize:(BOOL)shouldRasterize
{
    if (shouldRasterize) {
        [self.layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    }
    [self.layer setShouldRasterize:shouldRasterize];
}

- (BOOL)shouldRasterize
{
    return self.layer.shouldRasterize;
}

@end
