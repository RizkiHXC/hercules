//
//  MBStepProcessIndicator.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/26/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBStepProcessIndicator : UIView
{
    NSInteger selectedStep_;
    UIColor *activeTextColor_;
    UIColor *highlightTextColor_;
    UIColor *textColor_;
    UIFont  *textFont_;
    UIImage *backgroundImage_;
    UIImage *barActiveImage_;
    UIImage *barHighlightImage_;
    UIImage *emptyBarImage_;
    UIImage *emptyLeftImage_;
    UIImage *emptyMiddleImage_;
    UIImage *emptyRightImage_;
    UIImage *stateActiveImage_;
    UIImage *stateHighlightImage_;
}

/**
 *  Initializes and returns a newly allocated StepProcessIndicator object with the specified frame rectangle.
 *
 *  @param items An array of NSString objects (for step titles).
 *
 *  @param frame The frame rectangle for the view, measured in points. The origin of the frame is relative to the superview in which you plan to add it. This method uses the frame rectangle to set the center and bounds properties accordingly.
 *
 *  @return An initialized StepProcessIndicator object or nil if the object couldn't be created.
 */
- (instancetype)initWithItems:(NSArray *)items andFrame:(CGRect)frame;

@property(nonatomic,readonly) NSUInteger numberOfSteps;
@property(nonatomic,assign) NSInteger selectedStep;
@property(nonatomic,retain) UIImage *backgroundImage;
@property(nonatomic,retain) UIImage *emptyLeftImage;
@property(nonatomic,retain) UIImage *emptyMiddleImage;
@property(nonatomic,retain) UIImage *emptyRightImage;
@property(nonatomic,retain) UIImage *emptyBarImage;
@property(nonatomic,retain) UIImage *barActiveImage;
@property(nonatomic,retain) UIImage *barHighlightImage;
@property(nonatomic,retain) UIImage *stateActiveImage;
@property(nonatomic,retain) UIImage *stateHighlightImage;
@property(nonatomic,retain) UIColor *textColor;
@property(nonatomic,retain) UIColor *activeTextColor;
@property(nonatomic,retain) UIColor *highlightTextColor;
@property(nonatomic,retain) UIFont *textFont;

@end
