//
//  MBStepProcessIndicator.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/26/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBStepProcessIndicator.h"

@interface MBStepProcessIndicator ()
{
    NSArray *processItems_;
}
@end

@implementation MBStepProcessIndicator


@synthesize activeTextColor = activeTextColor_;
@synthesize backgroundImage = backgroundImage_;
@synthesize barActiveImage = barActiveImage_;
@synthesize barHighlightImage = barHighlightImage_;
@synthesize emptyBarImage = emptyBarImage_;
@synthesize emptyLeftImage = emptyLeftImage_;
@synthesize emptyMiddleImage = emptyMiddleImage_;
@synthesize emptyRightImage = emptyRightImage_;
@synthesize highlightTextColor = highlightTextColor_;
@synthesize selectedStep = selectedStep_;
@synthesize stateActiveImage = stateActiveImage_;
@synthesize stateHighlightImage = stateHighlightImage_;
@synthesize textColor = textColor_;
@synthesize textFont = textFont_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithItems:(NSArray *)items andFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        processItems_ = [items retain];
        [self MB_createDefaultImages];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    NSAssert(FALSE, @"Use initWithItems:andFrame: to create this control");
    return self;
}

- (void)dealloc
{
    [processItems_ release];
    [emptyLeftImage_ release];
    [emptyMiddleImage_ release];
    [emptyRightImage_ release];
    [emptyBarImage_ release];
    [barActiveImage_ release];
    [barHighlightImage_ release];
    [stateActiveImage_ release];
    [stateHighlightImage_ release];
    [textColor_ release];
    [activeTextColor_ release];
    [highlightTextColor_ release];
    [textFont_ release];
    [backgroundImage_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_createDefaultImages
{
    CGSize imageSize = CGSizeMake(36, 36);
    CGFloat scale = [[UIScreen mainScreen] scale];
    CGRect ellipseRect = CGRectMake(0, 0, 12, 12);
    ellipseRect.origin.x = roundf((imageSize.width - ellipseRect.size.width) * 0.5);
    ellipseRect.origin.y = roundf((imageSize.width - ellipseRect.size.width) * 0.5);
    CGRect barRect = CGRectMake(0, 0, 0, 4);
    barRect.origin.y = roundf(CGRectGetMidY(ellipseRect) - barRect.size.height*0.5);
    
    // Empty left image
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.333f, 0.333f, 0.333f, 1.0f);
    barRect.origin.x = roundf(CGRectGetMidX(ellipseRect));
    barRect.size.width = imageSize.width - barRect.origin.x;
    CGContextFillRect(context, barRect);
    CGContextFillEllipseInRect(context, ellipseRect);
    self.emptyLeftImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Empty middle image
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, scale);
    context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.333f, 0.333f, 0.333f, 1.0f);
    barRect.origin.x = 0;
    barRect.size.width = imageSize.width;
    CGContextFillRect(context, barRect);
    CGContextFillEllipseInRect(context, ellipseRect);
    self.emptyMiddleImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Empty right image
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, scale);
    context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.333f, 0.333f, 0.333f, 1.0f);
    barRect.origin.x = 0;
    barRect.size.width = imageSize.width - roundf(CGRectGetMidX(ellipseRect));
    CGContextFillRect(context, barRect);
    CGContextFillEllipseInRect(context, ellipseRect);
    self.emptyRightImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
  
    imageSize = CGSizeMake(3, 36);
    // Empty bar image
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, scale);
    context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.333f, 0.333f, 0.333f, 1.0f);
    barRect.origin.x = 0;
    barRect.size.width = imageSize.width;
    CGContextFillRect(context, barRect);
    self.emptyBarImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Active bar
    barRect.size.height = 2;
    barRect.origin.y = roundf(CGRectGetMidY(ellipseRect) - barRect.size.height*0.5);

    // Active bar image
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, scale);
    context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.1f, 0.1f, 0.1f, 1.0f);
    barRect.origin.x = 0;
    barRect.size.width = imageSize.width;
    CGContextFillRect(context, barRect);
    self.barActiveImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Active state image
    imageSize = CGSizeMake(36, 36);
    ellipseRect = UIEdgeInsetsInsetRect(ellipseRect, UIEdgeInsetsMake(2, 2, 2, 2));

    UIGraphicsBeginImageContextWithOptions(imageSize, NO, scale);
    context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.1f, 0.1f, 0.1f, 1.0f);
    CGContextFillEllipseInRect(context, ellipseRect);
    self.stateActiveImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Highlight state image
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, scale);
    context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.8f, 0.8f, 0.8f, 1.0f);
    CGContextFillEllipseInRect(context, UIEdgeInsetsInsetRect(ellipseRect, UIEdgeInsetsMake(-1, -1, -1, -1)));
    CGContextSetRGBFillColor(context, 0.1f, 0.1f, 0.1f, 1.0f);
    CGContextFillEllipseInRect(context, ellipseRect);
    self.stateHighlightImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect {
    
    NSInteger index = 0;
    NSInteger count = processItems_.count;
    
    CGRect drawRect = CGRectZero;
    
    [backgroundImage_ drawInRect:self.bounds];
    
    if (count >= 2) {
        UIEdgeInsets insets = UIEdgeInsetsMake(15, 40, 0, 40);
        CGRect contentRect = UIEdgeInsetsInsetRect(self.bounds, insets);
        CGSize bulletSize = emptyMiddleImage_.size;
        CGFloat bulletOffset = (contentRect.size.width - bulletSize.width) / (count - 1);
        
        drawRect.origin.y = contentRect.origin.y;
        drawRect.size = bulletSize;
        
        NSString *title = nil;
        
        CGRect textArea = CGRectZero;
        textArea.origin = CGPointMake(0, CGRectGetMaxY(drawRect) + 5);
        textArea.size.width = self.bounds.size.width / count;
        textArea.size.height = [@"Title" sizeWithFont:textFont_ constrainedToSize:CGSizeMake(textArea.size.width, CGFLOAT_MAX)].height;
        
        CGRect textRect = textArea;
        
        
        // Draw empty bullets and titles
        for (index = 0; index < count; index++) {

            drawRect.size = bulletSize;
            
           if (index == 0) {
               drawRect.origin.x = contentRect.origin.x;
               [emptyLeftImage_ drawInRect:drawRect];
            } else if (index == (count - 1)) {
                drawRect.origin.x = contentRect.origin.x + contentRect.size.width - bulletSize.width;
                [emptyRightImage_ drawInRect:drawRect];
            } else {
                drawRect.origin.x = contentRect.origin.x + (bulletOffset * index);
                [emptyMiddleImage_ drawInRect:drawRect];
            }
            
            if (index < (count - 1)) {
                drawRect.origin.x = contentRect.origin.x + (bulletOffset * index) + bulletSize.width;
                drawRect.size.width = bulletOffset - bulletSize.width;
                [emptyBarImage_ drawInRect:drawRect];
            }
            
            title = [processItems_ objectAtIndex:index];
            
            textRect.size = [title sizeWithFont:textFont_ constrainedToSize:textArea.size];
            textRect.origin.x = roundf((textArea.size.width * index) + ((textArea.size.width - textRect.size.width) * 0.5));
            // Used for drawing the text
            if (index == selectedStep_ && highlightTextColor_) {
                [highlightTextColor_ set];
            } else if (selectedStep_ > index && activeTextColor_) {
                [activeTextColor_ set];
            } else {
                [textColor_ set];
            }
            [title drawInRect:textRect withFont:textFont_];
        }
        
        // Draw state image connection lines
        for (index = 0; index < count; index++) {
            if (index < (count - 1)) {
                if (selectedStep_ > index) {
                    drawRect.origin.x = contentRect.origin.x + (bulletOffset * index) + (bulletSize.width * 0.5);
                    drawRect.size.width = bulletOffset;
                    
                    UIImage *barImage = barActiveImage_;
                    
                    if (index == (selectedStep_ - 1) && barHighlightImage_ != nil) {
                        barImage = barHighlightImage_;
                    }
                    [barImage drawInRect:drawRect];
                }
            }
        }
        
        // Draw state images
        for (index = 0; index < count; index++) {
            drawRect.size = bulletSize;
            
            if (index == 0) {
                drawRect.origin.x = contentRect.origin.x;
            } else if (index == (count - 1)) {
                drawRect.origin.x = contentRect.origin.x + contentRect.size.width - bulletSize.width;
            } else {
                drawRect.origin.x = contentRect.origin.x + (bulletOffset * index);
            }
            
            // Is item active
            if (selectedStep_ == index) {
                [stateHighlightImage_ drawInRect:drawRect];
            } else if (selectedStep_ > index) {
                [stateActiveImage_ drawInRect:drawRect];
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSUInteger)numberOfSteps
{
    return processItems_.count;
}

- (void)setSelectedStep:(NSInteger)selectedStep
{
    if (selectedStep >= processItems_.count) {
        // Raises an NSRangeException if index is greater than the number of elements in the array.
        [NSException raise:NSRangeException format:nil];
    }
    selectedStep_ = selectedStep;
    [self setNeedsDisplay];
}

//- (void)setState:(MBStepItemState)state forItem:(NSInteger)item {
//    if (item >= 0 && item < processItems_.count) {
//        [processItems_ replaceObjectAtIndex:item withObject:[NSNumber numberWithInteger:state]];
//        [self setNeedsDisplay];
//    }
//}



@end
