//
//  MBTabContainerView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/13/12.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifdef DEBUG
//#define SHOW_TABCONTAINERVIEW_LOGS // Enable to view NSLogs for this class
#endif

@class MBTabContainerView;

@protocol MBTabContainerViewDelegate <NSObject>

@optional
- (void)tabContainerView:(MBTabContainerView *)tabContainerView willDisplayPanel:(UIView *)panel atIndex:(NSUInteger)index;
- (void)tabContainerView:(MBTabContainerView *)tabContainerView didDisplayPanel:(UIView *)panel atIndex:(NSUInteger)index;
- (void)tabContainerView:(MBTabContainerView *)tabContainerView willEndDisplayingPanel:(UIView *)panel atIndex:(NSUInteger)index;
- (void)tabContainerView:(MBTabContainerView *)tabContainerView didEndDisplayingPanel:(UIView *)panel atIndex:(NSUInteger)index;
- (void)tabContainerView:(MBTabContainerView *)tabContainerView didSelectPanel:(UIView *)panel atIndex:(NSUInteger)index;
@end

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBTabContainerView : UIView <UIScrollViewDelegate>

- (void)addPanel:(UIView *)panel animated:(BOOL)animated;
- (void)insertPanel:(UIView *)panel atIndex:(NSUInteger)index animated:(BOOL)animated;
- (void)removeAllPanelsAnimated:(BOOL)animated;
- (void)removeLastPanelAnimated:(BOOL)animated;
- (void)removePanel:(UIView *)panel animated:(BOOL)animated;
- (void)removePanelAtIndex:(NSUInteger)index animated:(BOOL)animated;
- (void)replacePanelAtIndex:(NSUInteger)index withPanel:(UIView *)panel animated:(BOOL)animated;
- (void)setSelectedPanel:(UIView *)panel animated:(BOOL)animated;
- (UIView *)selectedPanel;
- (void)setSelectedIndex:(NSUInteger)index animated:(BOOL)animated;
- (NSUInteger)selectedIndex;
- (NSUInteger)indexForPanel:(UIView *)panel;
- (NSUInteger)numberOfPanels;
- (NSArray *)panels;
- (UIView *)panelAtIndex:(NSUInteger)index;

@property(nonatomic,readonly) UIView *contentView;
/**
 *  The object that acts as the delegate of the receiving tabcontainer view controller.
 *
 *  @discussion The delegate must adopt the MBTabContainerViewDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBTabContainerViewDelegate> delegate;
@property(nonatomic,readonly) UIScrollView *scrollView;

@end

