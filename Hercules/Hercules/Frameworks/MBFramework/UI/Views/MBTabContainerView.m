//
//  MBTabContainerView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/13/12.
//  Copyright (c) 2012 mediaBunker. All rights reserved.
//

#import "MBTabContainerView.h"

@interface MBTabContainerView ()
{
    NSTimeInterval animationDuration_;
    CGRect previousVisibleBounds_;
}
@end

@implementation MBTabContainerView

@synthesize contentView = contentView_;
@synthesize delegate = delegate_;
@synthesize scrollView = scrollView_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        scrollView_ = [[[UIScrollView alloc] initWithFrame:self.bounds] autorelease];
        [scrollView_ setPagingEnabled:YES];
        [scrollView_ setScrollEnabled:NO];
        [scrollView_ setDelegate:self];
        [self addSubview:scrollView_];
        
        contentView_ = [[[UIView alloc] initWithFrame:self.bounds] autorelease];
        [scrollView_ addSubview:contentView_];
        animationDuration_ = 0.3;
        previousVisibleBounds_ = CGRectZero;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)setFrame:(CGRect)frame
{
    // Frame is about to change; get current visible page
    NSInteger selectedIndex = [self selectedIndex];
    [super setFrame:frame];
    // Restore visible page
    if (selectedIndex != NSNotFound) {
        [self setSelectedIndex:selectedIndex animated:NO];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [scrollView_ setFrame:self.bounds];
    
    CGRect contentRect = CGRectMake(0, 0, (scrollView_.bounds.size.width * contentView_.subviews.count), scrollView_.bounds.size.height);
    [scrollView_ setContentSize:contentRect.size];
    
    [contentView_ setFrame:contentRect];

    CGRect panelRect = self.bounds;
    
    for (UIView *panel in contentView_.subviews) {
        [panel setFrame:panelRect];
        panelRect.origin.x += panelRect.size.width;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated
{
    BOOL nowVisible = NO;
    BOOL willBeVisible = NO;
    NSUInteger index = 0;
    CGRect visibleBounds = [scrollView_ convertRect:scrollView_.bounds toView:contentView_];
    CGRect bounds = CGRectMake(0, 0, scrollView_.bounds.size.width, scrollView_.bounds.size.height);
    CGRect nextVisibleBounds = CGRectOffset(bounds, contentOffset.x, contentOffset.y);
    
    for (UIView *panel in contentView_.subviews) {
        
        // Check if visible state has changed
        nowVisible = CGRectIntersectsRect(panel.frame, visibleBounds);
        willBeVisible = CGRectIntersectsRect(panel.frame, nextVisibleBounds);
        
        // Visible state has changed.
        if (willBeVisible != nowVisible) {
            
            if (!nowVisible && willBeVisible) {
                // From not visible to visible
#ifdef SHOW_TABCONTAINERVIEW_LOGS
                NSLog(@"willDisplayPanel at index %d", index);
#endif
                if ([delegate_ respondsToSelector:@selector(tabContainerView:willDisplayPanel:atIndex:)]) {
                    [delegate_ tabContainerView:self willDisplayPanel:panel atIndex:index];
                }
            } else  if (nowVisible && !willBeVisible) {
                // From visible to not visible
#ifdef SHOW_TABCONTAINERVIEW_LOGS
                NSLog(@"willEndDisplayingPanel at index %d", index);
#endif
                if ([delegate_ respondsToSelector:@selector(tabContainerView:willEndDisplayingPanel:atIndex:)]) {
                    [delegate_ tabContainerView:self willEndDisplayingPanel:panel atIndex:index];
                }
            }
        }
        index++;
    }
    
    [scrollView_ setContentOffset:contentOffset animated:animated];
}

- (void)MB_handleScrollViewDidScroll:(UIScrollView *)scrollView
{
    BOOL wasVisible = NO;
    BOOL nowVisible = NO;
    NSUInteger index = 0;
    CGRect visibleBounds = [scrollView_ convertRect:scrollView.bounds toView:contentView_];

    
    for (UIView *panel in contentView_.subviews) {
        
        // Check if visible state has changed
        wasVisible = CGRectIntersectsRect(panel.frame, previousVisibleBounds_);
        nowVisible = CGRectIntersectsRect(panel.frame, visibleBounds);
        
        // Visible state has changed.
        if (wasVisible != nowVisible) {
            
            if (!wasVisible && nowVisible) {
                // From not visible to visible
#ifdef SHOW_TABCONTAINERVIEW_LOGS
                NSLog(@"didDisplayPanel at index %d", index);
#endif
                if ([delegate_ respondsToSelector:@selector(tabContainerView:didDisplayPanel:atIndex:)]) {
                    [delegate_ tabContainerView:self didDisplayPanel:panel atIndex:index];
                }
            } else  if (wasVisible && !nowVisible) {
                // From visible to not visible
#ifdef SHOW_TABCONTAINERVIEW_LOGS
                NSLog(@"didEndDisplayingPanel at index %d", index);
#endif
                if ([delegate_ respondsToSelector:@selector(tabContainerView:didEndDisplayingPanel:atIndex:)]) {
                    [delegate_ tabContainerView:self didEndDisplayingPanel:panel atIndex:index];
                }
            }
        }
        index++;
    }
    // Save for next round
    previousVisibleBounds_ = visibleBounds;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Adding Panels

- (void)addPanel:(UIView *)panel animated:(BOOL)animated
{
    if (animated) {
        // Untested
        [panel setAlpha:0];
        [self.contentView addSubview:panel];
        [UIView animateWithDuration:animationDuration_
                         animations:^{
                             [panel setAlpha:1];
                             [self setNeedsLayout];
                         }];
    } else {
        [self.contentView addSubview:panel];
        [self setNeedsLayout];
    }
}

- (void)insertPanel:(UIView *)panel atIndex:(NSUInteger)index animated:(BOOL)animated
{
    if (animated) {
        // Untested
        [panel setAlpha:0];
        [self.contentView insertSubview:panel atIndex:index];
        [UIView animateWithDuration:animationDuration_
                         animations:^{
                             [panel setAlpha:1];
                             [self setNeedsLayout];
                         }];
    } else {
        [self.contentView insertSubview:panel atIndex:index];
        [self setNeedsLayout];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Removing Panels

- (void)removeAllPanelsAnimated:(BOOL)animated
{
    if (animated) {
        // Untested
        [UIView animateWithDuration:animationDuration_
                         animations:^{
                             for (UIView *panel in self.contentView.subviews) {
                                 [panel setAlpha:0];
                             }
                         }
                         completion:^(BOOL finished) {
                             for (UIView *panel in self.contentView.subviews) {
                                 [panel removeFromSuperview];
                             }
                             [self setNeedsLayout];
                         }];
    } else {
        for (UIView *panel in self.contentView.subviews) {
            [panel removeFromSuperview];
        }
        [self setNeedsLayout];
    }
}

- (void)removeLastPanelAnimated:(BOOL)animated
{
    [self removePanelAtIndex:self.contentView.subviews.count - 1 animated:animated];
}

- (void)removePanel:(UIView *)panel animated:(BOOL)animated
{
    if (panel.superview == self.contentView) {
        // Untested
        if (animated) {
            [UIView animateWithDuration:animationDuration_
                             animations:^{
                                 [panel setAlpha:0];
                             }
                             completion:^(BOOL finished) {
                                 [panel removeFromSuperview];
                                 [self setNeedsLayout];
                             }];
        } else {
            [panel removeFromSuperview];
            [self setNeedsLayout];
        }
    }
}

- (void)removePanelAtIndex:(NSUInteger)index animated:(BOOL)animated
{
    UIView *panel = [self.contentView.subviews objectAtIndex:index];
    [self removePanel:panel animated:animated];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Replacing Panels

- (void)replacePanelAtIndex:(NSUInteger)index withPanel:(UIView *)panel animated:(BOOL)animated
{
    UIView *oldPanel = [self.contentView.subviews objectAtIndex:index];
    
    if (animated) {
        // Untested
        [panel setAlpha:0];
        [self.contentView insertSubview:panel atIndex:index];
        [UIView animateWithDuration:animationDuration_
                         animations:^{
                             [oldPanel setAlpha:0];
                         }
                         completion:^(BOOL finished) {
                             [oldPanel removeFromSuperview];
                             [self setNeedsLayout];
                         }];
        
    } else {
        [self.contentView insertSubview:panel atIndex:index];
        [oldPanel removeFromSuperview];
        [self setNeedsLayout];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Managing Selected Panel

- (void)setSelectedPanel:(UIView *)panel animated:(BOOL)animated
{
    NSUInteger index = [self indexForPanel:panel];
    if (index != NSNotFound) {
        [self setSelectedIndex:index animated:animated];
    }
}

- (UIView *)selectedPanel
{
    UIView *result = nil;
    NSInteger index = [self selectedIndex];
    if (index != NSNotFound) {
        result = [self.contentView.subviews objectAtIndex:index];
    }
    return result;
}

- (void)setSelectedIndex:(NSUInteger)index animated:(BOOL)animated
{
    if ((NSInteger)index < 0 || index >= self.contentView.subviews.count) {
        [NSException raise:NSRangeException format:@"Index out of bounds"];
    }
    // 
    [self layoutIfNeeded];
    [self MB_setContentOffset:CGPointMake((scrollView_.bounds.size.width * index), 0) animated:animated];
}

- (NSUInteger)selectedIndex
{
    NSInteger index = 0;
	CGFloat panelWidth = scrollView_.bounds.size.width;
    
    index = (NSInteger)(floor((scrollView_.contentOffset.x - (panelWidth * 0.5)) / panelWidth) + 1);
    
    if (index < 0 || index >= self.contentView.subviews.count) {
        index = NSNotFound;
    }
	return index;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Querying panels

- (NSUInteger)indexForPanel:(UIView *)panel
{
    return [self.contentView.subviews indexOfObject:panel];
}

- (NSUInteger)numberOfPanels
{
    return self.contentView.subviews.count;
}

- (NSArray *)panels
{
    return self.contentView.subviews;
}

- (UIView *)panelAtIndex:(NSUInteger)index
{
    return [self.contentView.subviews objectAtIndex:index];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (delegate_) {
        [self MB_handleScrollViewDidScroll:scrollView];
    }
}

@end
