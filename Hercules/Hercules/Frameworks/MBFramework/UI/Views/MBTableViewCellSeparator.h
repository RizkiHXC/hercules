//
//  MBTableViewCellSeparator.h
//  MBFramework
//
//  Created by Arno Woestenburg on 22/01/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Separator view used for table cells separators.
 *  Is used as a replacement for UITableView's separatorStyle. Set separatorStyle to UITableViewCellSeparatorStyleNone on the TableView and add this view class to the table cell object.
 */
/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBTableViewCellSeparator : UIView

/**
 *  The color applied to the view.
 */
@property(nonatomic,retain) UIColor *color;
/**
 *  The inset values for the cell’s content.
 */
@property(nonatomic) UIEdgeInsets separatorInset;

@end
