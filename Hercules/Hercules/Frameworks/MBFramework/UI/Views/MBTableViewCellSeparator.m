//
//  MBTableViewCellSeparator.m
//  MBFramework
//
//  Created by Arno Woestenburg on 22/01/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBTableViewCellSeparator.h"

@implementation MBTableViewCellSeparator

@synthesize color = color_;
@synthesize separatorInset = separatorInset_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        color_ = [[UIColor colorWithRed:0.784 green:0.78 blue:0.8 alpha:1.0] retain]; // iOS 7 default: #c8c7cc
        separatorInset_ = UIEdgeInsetsZero;
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)dealloc
{
    [color_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect
{
    // Draw separator line
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, color_.CGColor);
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, CGRectGetMinX(self.bounds) + separatorInset_.left, CGRectGetMaxY(self.bounds));
    CGContextAddLineToPoint(context, CGRectGetMaxX(self.bounds), CGRectGetMaxY(self.bounds));
    CGContextStrokePath(context);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setColor:(UIColor *)color
{
    if (color_ != color) {
        [color_ release];
        color_ = [color retain];
        [self setNeedsDisplay];
    }
}

- (void)setSeparatorInset:(UIEdgeInsets)separatorInset
{
    if (!UIEdgeInsetsEqualToEdgeInsets(separatorInset_, separatorInset)) {
        separatorInset_ = separatorInset;
        [self setNeedsDisplay];
    }
}

@end
