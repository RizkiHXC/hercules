//
//  MBTableViewHeaderFooterView.h
//  MBFramework
//
//  Created by Arno Woestenburg on 21/02/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This type of view can be used to create a standard view with a background view and label. This class is typically used in combination with a table view or collection view to provide section header or footer view's.
 */
@interface MBTableViewHeaderFooterView : UIView

/**
 Sets the title for the view.
 @param title The title.
 */
- (void)setTitle:(NSString *)title;

/**
 The background view of the reciever.
 @discussion The background view is automatically resized to match the size of the view. This view is placed as a subview of the recciever's view behind all content. Default is nil.
 */
@property(nonatomic,retain) UIView *backgroundView;
/**
 Returns the label used for the title of the view. (read-only)
 */
@property(nonatomic,readonly) UILabel *titleLabel;
/**
 The inset or outset margins for the rectangle around the reciever's title text.
 */
@property(nonatomic,assign) UIEdgeInsets titleEdgeInsets;

/**
 The vertical alignment of content within the receiver. Default is UIControlContentVerticalAlignmentCenter.
 */
@property(nonatomic) UIControlContentVerticalAlignment contentVerticalAlignment;
/**
 The horizontal alignment of content within the receiver. Default is UIControlContentHorizontalAlignmentCenter.
 */
@property(nonatomic) UIControlContentHorizontalAlignment contentHorizontalAlignment;
 
@end