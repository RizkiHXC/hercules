//
//  MBTableViewHeaderFooterView.m
//  MBFramework
//
//  Created by Arno Woestenburg on 21/02/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//
 
#import "MBTableViewHeaderFooterView.h"

@implementation MBTableViewHeaderFooterView

@synthesize backgroundView = backgroundView_;
@synthesize titleLabel = titleLabel_;
@synthesize titleEdgeInsets = titleEdgeInsets_;
@synthesize contentVerticalAlignment = contentVerticalAlignment_;
@synthesize contentHorizontalAlignment = contentHorizontalAlignment_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        titleEdgeInsets_ = UIEdgeInsetsMake(5, 5, 5, 5);
        
        contentVerticalAlignment_ = UIControlContentVerticalAlignmentCenter;
        contentHorizontalAlignment_ = UIControlContentHorizontalAlignmentCenter;
        
        // Title label
        titleLabel_ = [[[UILabel alloc] init] autorelease];
        [titleLabel_ setTextAlignment:NSTextAlignmentCenter];
        [titleLabel_ setBackgroundColor:[UIColor clearColor]];
        [titleLabel_ setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
        [self addSubview:titleLabel_];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect textBounds = UIEdgeInsetsInsetRect(self.bounds, titleEdgeInsets_);
    CGRect textRect = CGRectZero;
    textRect.size = [titleLabel_ sizeThatFits:CGSizeMake(textBounds.size.width, CGFLOAT_MAX)];
    textRect.size.width = MIN(textRect.size.width, textBounds.size.width);
    
    switch (contentVerticalAlignment_) {
        case UIControlContentVerticalAlignmentTop:
            textRect.origin.y = CGRectGetMinY(textBounds);
            break;
        case UIControlContentVerticalAlignmentBottom:
            textRect.origin.y = CGRectGetMaxY(textBounds) - textRect.size.height;
            break;
        default:
            // UIControlContentVerticalAlignmentCenter
            textRect.origin.y = roundf(CGRectGetMidY(textBounds) - (textRect.size.height * 0.5));
            break;
    }

    switch (contentHorizontalAlignment_) {
        case UIControlContentHorizontalAlignmentLeft:
            textRect.origin.x = CGRectGetMinX(textBounds);
            break;
        case UIControlContentHorizontalAlignmentRight:
            textRect.origin.x = CGRectGetMaxX(textBounds) - textRect.size.width;
            break;
        default:
            // UIControlContentHorizontalAlignmentCenter
            textRect.origin.x = roundf(CGRectGetMidX(textBounds) - (textRect.size.width * 0.5));
            break;
    }
    [titleLabel_ setFrame:textRect];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGRect textBounds = UIEdgeInsetsInsetRect(CGRectMake(0, 0, size.width, size.height), titleEdgeInsets_);
    CGSize labelSize = CGSizeZero;
    if (titleLabel_.text.length == 0) {
        [titleLabel_ setText:@" "];
    }
    labelSize = [titleLabel_ sizeThatFits:CGSizeMake(textBounds.size.width, CGFLOAT_MAX)];
    labelSize.width = MIN(labelSize.width, textBounds.size.width);
    
    size.width = MAX(size.width, (labelSize.width + titleEdgeInsets_.left + titleEdgeInsets_.right));
    size.height = (labelSize.height + titleEdgeInsets_.top + titleEdgeInsets_.bottom);
    
    return size;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setTitle:(NSString *)title
{
    [self.titleLabel setText:title];
    [self setNeedsLayout];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setBackgroundView:(UIView *)backgroundView
{
    [backgroundView_ removeFromSuperview];
    backgroundView_ = backgroundView;
    
    if (backgroundView_) {
        [backgroundView_ setFrame:self.bounds];
        [backgroundView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        // Insert at the back
        [self insertSubview:backgroundView_ atIndex:0];
    }
}

- (void)setTitleEdgeInsets:(UIEdgeInsets)titleEdgeInsets
{
    if (!UIEdgeInsetsEqualToEdgeInsets(titleEdgeInsets, titleEdgeInsets_)) {
        titleEdgeInsets_ = titleEdgeInsets;
        [self setNeedsLayout];
    }
}

- (void)setContentHorizontalAlignment:(UIControlContentHorizontalAlignment)contentHorizontalAlignment
{
    if (contentHorizontalAlignment_ != contentHorizontalAlignment) {
        contentHorizontalAlignment_ = contentHorizontalAlignment;
        [self setNeedsLayout];
    }
}

- (void)setContentVerticalAlignment:(UIControlContentVerticalAlignment)contentVerticalAlignment
{
    if (contentVerticalAlignment_ != contentVerticalAlignment) {
        contentVerticalAlignment_ = contentVerticalAlignment;
        [self setNeedsLayout];
    }
}


@end
