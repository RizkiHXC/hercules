//
//  MBTitledToolbar.h
//  MBFramework
//
//  Created by Arno Woestenburg on 24/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 A UIKIt's UIToolbar based class that provides a title view.
 */
@interface MBTitledToolbar : UIToolbar

/**
 Set or get the toolbar's title label displayed in the center of the toolbar. Is automatically created when the property is called when is doesn't exist.
 */
@property(nonatomic,retain) UILabel *titleLabel;

/**
 A custom view displayed in the center of the navigation bar when the receiver is the top item. The default value returns a reference to the titleLabel.
 @discussion If you set this property to a custom title view, it is displayed instead of the title label.
 */
@property(nonatomic,retain) UIView *titleView;

/**
 The inset or outset margins for the rectangle around the title view.
 */
@property(nonatomic,assign) UIEdgeInsets titleEdgeInsets;

@end
