//
//  MBTitledToolbar.m
//  MBFramework
//
//  Created by Arno Woestenburg on 24/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBTitledToolbar.h"
#import "MBFramework.h"

@implementation MBTitledToolbar

@synthesize titleEdgeInsets = titleEdgeInsets_;
@synthesize titleLabel = titleLabel_;
@synthesize titleView = titleView_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        titleEdgeInsets_ = UIEdgeInsetsZero;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    UIView *titleView = self.titleView;
    if (titleView) {
        CGRect titleRect = CGRectZero;
        titleRect.size = [titleView sizeThatFits:CGSizeZero];
        if (titleRect.size.width == 0) {
            titleRect.size.width = titleView.bounds.size.width;
        }
        if (titleRect.size.height == 0) {
            titleRect.size.height = titleView.bounds.size.height;
        }
        CGRect bounds = UIEdgeInsetsInsetRect(self.bounds, titleEdgeInsets_);
        
        titleRect.origin.x = roundf(CGRectGetMidX(bounds) - (titleRect.size.width * 0.5));
        titleRect.origin.y = roundf(CGRectGetMidY(bounds) - (titleRect.size.height * 0.5));
        [titleView setFrame:titleRect];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_setTitleLabelStyle
{
    // Basic styles
    [titleLabel_ setBackgroundColor:[UIColor clearColor]];
    [titleLabel_ setTextAlignment:NSTextAlignmentCenter];
    
        // Default to iOS basic styles
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    if ([[UIDevice currentDevice] systemVersionLower:@"7"]) {
        [titleLabel_ setFont:[UIFont systemFontOfSize:16.0f]];
        [titleLabel_ setTextColor:[UIColor whiteColor]];
        [titleLabel_ setShadowColor:[UIColor blackColor]];
        [titleLabel_ setShadowOffset:CGSizeMake(0, -1)];

    } else {
#endif
        titleEdgeInsets_ = UIEdgeInsetsMake(4.0f, 0.0f, 0.0f, 0.0f);
        // iOS 7+
        [titleLabel_ setFont:[UIFont systemFontOfSize:14.0f]];
        
        switch (self.barStyle) {
            case UIBarStyleBlack:
                [titleLabel_ setTextColor:[UIColor lightGrayColor]];
                break;
            default:
                // UIBarStyleDefault
                [titleLabel_ setTextColor:[UIColor grayColor]];
                break;
        }
        
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    }
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIToolbar

- (void)setBarStyle:(UIBarStyle)barStyle
{
    [super setBarStyle:barStyle];
    [self MB_setTitleLabelStyle];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UILabel *)titleLabel
{
    if (!titleLabel_) {
        titleLabel_ = [[[UILabel alloc] init] autorelease];
        
        [self MB_setTitleLabelStyle];
        
        [self addSubview:titleLabel_];
    }
    return titleLabel_;
}

- (UIView *)titleView
{
    UIView *titleView = titleView_;
    if (!titleView) {
        titleView = titleLabel_;
    }
    return titleView;
}

@end
