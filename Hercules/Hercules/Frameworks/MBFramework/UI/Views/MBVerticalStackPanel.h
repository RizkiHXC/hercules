//
//  MBVerticalStackPanel.h
//  MBFramework
//
//  Created by Marco Jonker on 6/13/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBRectFunctions.h"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBVerticalStackPanel : UIView {
    MBHorizontalAlignment   defaultAlignment_;
    CGFloat                 defaultMargin_;
    UIEdgeInsets            contentInsets_;
}

@property (nonatomic, assign) MBHorizontalAlignment defaultAlignment;
@property (nonatomic, assign) CGFloat               defaultMargin;
@property (nonatomic, assign) UIEdgeInsets          contentInsets;

-(void)addView:(UIView*)view;
-(void)addView:(UIView*)view withMargin:(CGFloat)margin;
-(void)addView:(UIView*)view alignment:(MBHorizontalAlignment)alignment;
-(void)addView:(UIView*)view alignment:(MBHorizontalAlignment)alignment withMargin:(CGFloat)margin;
-(void)addMargin:(CGFloat)margin;
@end
