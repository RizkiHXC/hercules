//
//  MBVerticalStackPanel
//  MBFramework
//
//  Created by Marco Jonker on 6/13/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBVerticalStackPanel.h"

@implementation MBVerticalStackPanel

@synthesize defaultAlignment=defaultAlignment_;
@synthesize defaultMargin=defaultMargin_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 0)];
    if (self) {
        defaultAlignment_   = MBHorizontalAlignmentCenter;
        defaultMargin_      = 0;
        contentInsets_      = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(void)setContentInsets:(UIEdgeInsets)contentInsets {
    contentInsets_ = contentInsets;
    if(self.frame.size.height < (contentInsets_.top + contentInsets.bottom)) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, (contentInsets_.top + contentInsets.bottom) + self.frame.size.height);
    }
}

-(void)addView:(UIView*)view{
    [self addView:view alignment:defaultAlignment_ withMargin:defaultMargin_];
}

-(void)addView:(UIView*)view withMargin:(CGFloat)margin{
    [self addView:view alignment:defaultAlignment_ withMargin:margin];
}

-(void)addView:(UIView*)view alignment:(MBHorizontalAlignment)alignment{
    [self addView:view alignment:alignment withMargin:defaultMargin_];
}

-(void)addView:(UIView*)view alignment:(MBHorizontalAlignment)alignment withMargin:(CGFloat)margin{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height + margin + view.frame.size.height);
    
    switch(alignment) {
        case MBHorizontalAlignmentLeft:
            view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin;
            break;
        case MBHorizontalAlignmentRight:
            view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin;
            break;
        case MBHorizontalAlignmentCenter:
            view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
            break;
        case MBHorizontalStretch:
            view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth;
            break;
    }
    
    CGRect insetRect = UIEdgeInsetsInsetRect(self.bounds, contentInsets_);
    
    if(alignment == MBHorizontalStretch) {
        view.frame = CGRectMake(insetRect.origin.x, 0, insetRect.size.width, view.frame.size.height);
    }
    
    view.frame = [MBRectFunctions alignRect:view.bounds toRect:insetRect alignMode:alignment|MBAlignmentBottom];
    [self addSubview:view];
}

-(void)addMargin:(CGFloat)margin{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height + margin);
}

@end
