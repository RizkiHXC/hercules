//
//  MBError.h
//  MBFramework
//
//  Created by Marco Jonker on 5/29/13.
//
//

#import <Foundation/Foundation.h>

#define MBFrameworkErrorDomain @"MBFrameworkErrorDomain"

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBError : NSError
+(NSError*)createError:(NSString*)description withErrorCode:(NSInteger)code andError:(NSError*)baseError;
@end
