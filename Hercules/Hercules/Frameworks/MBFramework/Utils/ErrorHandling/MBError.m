//
//  MBError.m
//  MBFramework
//
//  Created by Marco Jonker on 5/29/13.
//
//

#import "MBError.h"

@implementation MBError

+(NSError*)createError:(NSString*)description withErrorCode:(NSInteger)code andError:(NSError*)baseError{
    NSDictionary *userInfo = nil;
    
    if(baseError != nil) {
        userInfo = @{ NSLocalizedDescriptionKey: description, NSUnderlyingErrorKey: baseError };
    } else {
        userInfo = @{ NSLocalizedDescriptionKey: description };
    }
    
    return [[[NSError alloc] initWithDomain:MBFrameworkErrorDomain code:code userInfo:userInfo]autorelease];
}

@end
