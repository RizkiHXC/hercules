//
//  MBErrorController.h
//  MBFramework
//
//  Created by Marco Jonker on 10/30/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBErrorController : NSObject

@property (nonatomic, retain) NSString* localizedErrorTableName;

+ (instancetype)sharedInstance;
- (NSError *)localizeError:(NSError*)error;
- (NSString *)stringIdentifierForError:(NSError*)error;

@end
