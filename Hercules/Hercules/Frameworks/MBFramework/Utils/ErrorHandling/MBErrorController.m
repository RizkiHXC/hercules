//
//  MBErrorController.m
//  MBFramework
//
//  Created by Marco Jonker on 10/30/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBErrorController.h"
#import "MBLog.h"

static id sharedInstance_ = nil;

@implementation MBErrorController

@synthesize localizedErrorTableName = localizedErrorTableName_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static functions

+ (instancetype)sharedInstance {
    @synchronized(self) {
        if(sharedInstance_ == nil)
            sharedInstance_ = [[self alloc] init];
    }
    return sharedInstance_;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        self.localizedErrorTableName = @"Errors";
    }
    return self;
}

- (void)dealloc
{
    sharedInstance_ = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public function

-(NSString*)stringIdentifierForError:(NSError*)error {
    NSAssert(false, @"MBErrorControllerProtocol protocol not implemented");
    return @"";
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

-(NSError*)MB_localizeError:(NSString*)identifier error:(NSError*)error {
    NSDictionary*   userInfo       = nil;
    NSDictionary*   errorUserInfo  = error.userInfo;
    NSError*        baseError      = nil;
    NSString*       descriptionKey = [identifier stringByAppendingString:@"Dsc"];
    NSString*       reasonKey      = [identifier stringByAppendingString:@"Rsn"];
    NSString*       suggestionKey  = [identifier stringByAppendingString:@"Sug"];
    NSString*       description    = NSLocalizedStringFromTable(descriptionKey, localizedErrorTableName_, desctiptionKey);
    
    //If the description was not found then return the original error
    if(reasonKey.length > 0) {
        if(errorUserInfo != nil) {
            baseError = [errorUserInfo valueForKey:NSUnderlyingErrorKey];
        }
        
        userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                    description, NSLocalizedDescriptionKey,
                    
                                    NSLocalizedStringFromTable(reasonKey, localizedErrorTableName_, reasonKey), NSLocalizedFailureReasonErrorKey,
                                    NSLocalizedStringFromTable(suggestionKey, localizedErrorTableName_, suggestionKey), NSLocalizedRecoverySuggestionErrorKey,
                                    baseError, NSUnderlyingErrorKey, nil];
        
        return [[[NSError alloc] initWithDomain:error.domain code:error.code userInfo:userInfo]autorelease];
    } else {
        MBLogDebug(@"Error localization not found for identifier: %@", identifier);
        return error;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(NSError*)localizeError:(NSError*)error {
    NSString* identifier = [self stringIdentifierForError:error];
    
    if(identifier != nil && identifier.length > 0) {
        return [self MB_localizeError:identifier error:error];
    } else {
        MBLogDebug(@"No string indentifier found for (domain: %@, code: %@): %@", error.domain, @(error.code), error.description);
        return error;
    }
}

@end
