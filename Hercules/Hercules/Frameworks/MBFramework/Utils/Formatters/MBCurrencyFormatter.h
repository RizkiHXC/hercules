//
//  MBCurrencyFormatter.h
//  MBFramework
//
//  Created by Arno Woestenburg on 18/06/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Instances of MBCurrencyFormatter create currency string representations of NSNumber objects, and convert textual representations of currency NSNumber objects. You can express the representation of currencies flexibly using pre-set format styles.
 */
@interface MBCurrencyFormatter : NSNumberFormatter

/**
 Returns a string representation of a given currency formatted using the receiver’s current settings.
 @param currency The currency value to format.
 @return A string representation of currency formatted using the receiver’s current settings.
 */
- (NSString *)stringFromCurrency:(NSNumber *)currency;

/**
 Returns the Zero decimal symbol for the receiver. Symbol is used when decimal value is zero for the currency 
 @discussion (For example, 10,00 formats to 10,- when zeroDecimalSymbol is '-')
 */
@property(nonatomic,copy) NSString *zeroDecimalSymbol;

@end
