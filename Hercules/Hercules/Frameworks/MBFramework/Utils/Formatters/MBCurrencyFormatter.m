//
//  MBCurrencyFormatter.m
//  MBFramework
//
//  Created by Arno Woestenburg on 18/06/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBCurrencyFormatter.h"

@implementation MBCurrencyFormatter

@synthesize zeroDecimalSymbol = zeroDecimalSymbol_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSFormatter

- (NSString *)stringFromCurrency:(NSNumber *)currency
{
    [self setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [self setNumberStyle:NSNumberFormatterCurrencyStyle];
    [self setRoundingMode:NSNumberFormatterRoundHalfUp];

    NSString *formattedCurrency = nil;

    // Dutch locale
    if ([self.locale.localeIdentifier isEqualToString:@"nl_NL"]) {
        // Change negative format
        [self setNegativeFormat:[NSString stringWithFormat:@"- %@", self.positiveFormat]];
    }
    
    formattedCurrency = [self stringFromNumber:currency];
    
    if (self.zeroDecimalSymbol) {
        // Replace ',00' with zeroDecimalSymbol
        NSString *decimals = [NSString stringWithFormat:@"%@00", [self decimalSeparator]];
        NSRange range = NSMakeRange(formattedCurrency.length - decimals.length, decimals.length);
        if ([[formattedCurrency substringWithRange:range] isEqualToString:decimals]) {
            decimals = [NSString stringWithFormat:@"%@%@", [self decimalSeparator], self.zeroDecimalSymbol];
            formattedCurrency = [formattedCurrency stringByReplacingCharactersInRange:range withString:decimals];
        }
    }
    // When formatted without currency symbol, trim whitespaces.
    if (self.currencySymbol.length == 0) {
        formattedCurrency = [formattedCurrency stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    return formattedCurrency;
}

- (NSString *)zeroDecimalSymbol
{
    NSString *zeroDecimalSymbol = zeroDecimalSymbol_;
    if (!zeroDecimalSymbol && [self.locale.localeIdentifier isEqualToString:@"nl_NL"]) {
        zeroDecimalSymbol = @"-";
    }
    return zeroDecimalSymbol;
}

@end
