//
//  MBPhoneNumberFormatter.h
//  MBFramework
//
//  Created by Arno Woestenburg on 09/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBPhoneNumberFormatter : NSFormatter
{
    
}

+ (NSString *)phoneNumberFormatFromTemplate:(NSString *)template options:(NSUInteger)opts locale:(NSLocale *)locale;

- (NSString *)groupingSeparator;
- (void)setGroupingSeparator:(NSString *)groupingSeparator;
- (NSLocale *)locale;
- (void)setLocale:(NSLocale *)locale;
- (NSString *)phoneNumberFormat;
- (void)setPhoneNumberFormat:(NSString *)format;
- (NSString *)stringFromNumber:(NSNumber *)number;


@end
