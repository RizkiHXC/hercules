//
//  MBPhoneNumberFormatter.m
//  MBFramework
//
//  Created by Arno Woestenburg on 09/04/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBPhoneNumberFormatter.h"

@interface MBPhoneNumberFormatter ()
{
    NSNumberFormatter *numberFormatter_;
    NSString *phoneNumberFormat_;
}
@end

@implementation MBPhoneNumberFormatter

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        numberFormatter_ = [[NSNumberFormatter alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [numberFormatter_ release];
    [phoneNumberFormat_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSString *)phoneNumberFormatFromTemplate:(NSString *)template options:(NSUInteger)opts locale:(NSLocale *)locale
{
    // Currenty no locale is supported. Add when needed
    return @"0000,0000";
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSString *)groupingSeparator
{
    return numberFormatter_.groupingSeparator;
}

- (void)setGroupingSeparator:(NSString *)groupingSeparator
{
    [numberFormatter_ setGroupingSeparator:groupingSeparator];
}

- (NSLocale *)locale
{
    return numberFormatter_.locale;
}

- (void)setLocale:(NSLocale *)locale
{
    [numberFormatter_ setLocale:locale];
}

- (NSString *)phoneNumberFormat
{
    NSString *format = nil;
    if (phoneNumberFormat_) {
        format = phoneNumberFormat_;
    } else {
        format = [MBPhoneNumberFormatter phoneNumberFormatFromTemplate:nil options:0 locale:self.locale];
    }
    return format;
}

- (void)setPhoneNumberFormat:(NSString *)format
{
    [phoneNumberFormat_ release];
    phoneNumberFormat_ = [format copy];
}

- (NSString *)stringFromNumber:(NSNumber *)number
{
    [numberFormatter_ setPositiveFormat:self.phoneNumberFormat];
    NSMutableString *formattedString = [NSMutableString stringWithString:[numberFormatter_ stringFromNumber:number]];
    [formattedString replaceOccurrencesOfString:@"," withString:self.groupingSeparator options:0 range:NSMakeRange(0, formattedString.length)];
    //NSLog(@"Number %@ : format : %@   result : %@", number, self.phoneNumberFormat, formattedString);
    return formattedString;
}


@end
