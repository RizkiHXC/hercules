//
//  MBTimeSpanFormatter.h
//  MBFramework
//
//  Created by Arno Woestenburg on 13/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Instances of MBTimeSpanFormatter create string representations of two NSDate objects with a timespan between them into a user-friendly string representation. 
 
 Date formats for user-visible strings should be configured using the styles configured by the user—use setDateStyle:, setTimeStyle:, and appropriate style constants (defined in NSDateFormatterStyle) to choose between them.
 */
@interface MBTimeSpanFormatter : NSFormatter

/**
 Returns a user-friendly string representation of a given date- or timespan formatted using the receiver’s current settings.
 @param fromDate Start date
 @param toDate   End date
 @return A user-friendly string representation of date- or timespan formatted using the receiver’s current settings.
 */
- (NSString *)stringFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate;

/**
 The locale for the receiver.
 */
@property(nonatomic,copy) NSLocale *locale;
/**
 The symbol used between the two formatting dates.
 */
@property(nonatomic,copy) NSString *untilSymbol;
/**
 The date style of the receiver.
 */
@property(nonatomic,assign) NSDateFormatterStyle dateStyle;
/**
 The time style of the receiver.
 */
//@property(nonatomic,assign) NSDateFormatterStyle timeStyle;
/**
 Controls if the year is excluded in the formatted string if it's the current year. Default is NO.
 */
@property(nonatomic,assign) BOOL excludeCurrentYear;

@end
