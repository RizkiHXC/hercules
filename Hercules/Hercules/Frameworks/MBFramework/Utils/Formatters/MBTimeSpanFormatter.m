//
//  MBTimeSpanFormatter.m
//  MBFramework
//
//  Created by Arno Woestenburg on 13/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBTimeSpanFormatter.h"

@interface MBTimeSpanFormatter ()
{
    NSDateFormatter *dateFormatter_;
}
@end

@implementation MBTimeSpanFormatter

@synthesize locale = locale_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.untilSymbol = @"-";
        self.dateStyle = NSDateFormatterNoStyle;
        self.excludeCurrentYear = NO;
        dateFormatter_ = [[NSDateFormatter alloc] init];
    }
    return self;
}

- (void)dealloc
{
    self.locale = nil;
    self.untilSymbol = nil;
    [dateFormatter_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSString *)MB_templateFromComponents:(NSCalendarUnit)unitFlags dateStyle:(NSDateFormatterStyle)dateStyle
{
    NSMutableString *template = [NSMutableString string];
    
    switch (self.dateStyle) {
        case NSDateFormatterNoStyle:
            // No output
            break;
        case NSDateFormatterShortStyle:
            // E.g. '08-12-13'
            if ((unitFlags & NSCalendarUnitYear) > 0) {
                [template appendString:@"yy"];
            }
            if ((unitFlags & NSCalendarUnitMonth) > 0) {
                [template appendString:@"MM"];
            }
            if ((unitFlags & NSCalendarUnitDay) > 0) {
                [template appendString:@"dd"];
            }
            break;
        case NSDateFormatterMediumStyle:
            // E.g. '8 dec. 2013'
            if ((unitFlags & NSCalendarUnitYear) > 0) {
                [template appendString:@"yyyy"];
            }
            if ((unitFlags & NSCalendarUnitMonth) > 0) {
                [template appendString:@"MMM"];
            }
            if ((unitFlags & NSCalendarUnitDay) > 0) {
                [template appendString:@"d"];
            }
            break;
        case NSDateFormatterLongStyle:
            // E.g. '8 december 2013'
            if ((unitFlags & NSCalendarUnitYear) > 0) {
                [template appendString:@"yyyy"];
            }
            if ((unitFlags & NSCalendarUnitMonth) > 0) {
                [template appendString:@"MMMM"];
            }
            if ((unitFlags & NSCalendarUnitDay) > 0) {
                [template appendString:@"d"];
            }
            break;
        case NSDateFormatterFullStyle:
            // E.g. 'zondag 8 december 2013'
            if ((unitFlags & NSCalendarUnitYear) > 0) {
                [template appendString:@"yyyy"];
            }
            if ((unitFlags & NSCalendarUnitMonth) > 0) {
                [template appendString:@"MMMM"];
            }
            if ((unitFlags & NSCalendarUnitDay) > 0) {
                [template appendString:@"eeeed"];
            }
            break;
        default:
            NSAssert(NO, @"Unhandled case");
            break;
    }
    return [NSDateFormatter dateFormatFromTemplate:template options:0 locale:self.locale];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSString *)stringFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate
{
    NSString *datespan = nil;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [dateFormatter_ setLocale:self.locale];
    
    NSCalendarUnit fromUnitFlags = NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear;
    NSCalendarUnit toUnitFlags = NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear;
    
    NSDateComponents *fromComponents = [calendar components:fromUnitFlags fromDate:fromDate];
    NSDateComponents *toComponents = [calendar components:toUnitFlags fromDate:toDate];
    
    // Same year?
    if (fromComponents.year == toComponents.year) {
        
        fromUnitFlags &= ~NSCalendarUnitYear;
        
        // Same month?
        if (fromComponents.month == toComponents.month) {

            fromUnitFlags &= ~NSCalendarUnitMonth;
            
            // Same day?
            if (fromComponents.day == toComponents.day) {
                fromUnitFlags = 0;
            }
        }
        
        // This year?
        if (self.excludeCurrentYear) {
            NSInteger currentYear = [calendar components:NSCalendarUnitYear fromDate:[NSDate date]].year;
            if (fromComponents.year == currentYear) {
                toUnitFlags &= ~NSCalendarUnitYear;
            }
        }
    }
    
    NSString *fromTemplate = nil;
    NSString *toTemplate = nil;
    
    if (fromUnitFlags > 0) {
        fromTemplate = [self MB_templateFromComponents:fromUnitFlags dateStyle:self.dateStyle];
    }
    toTemplate = [self MB_templateFromComponents:toUnitFlags dateStyle:self.dateStyle];
    
    if (fromTemplate) {
        [dateFormatter_ setDateFormat:fromTemplate];
        NSString *fromText = [dateFormatter_ stringFromDate:fromDate];
        [dateFormatter_ setDateFormat:toTemplate];
        NSString *toText = [dateFormatter_ stringFromDate:toDate];
        
        datespan = [NSString stringWithFormat:@"%@ %@ %@", fromText, self.untilSymbol, toText];
    } else {
        [dateFormatter_ setDateFormat:toTemplate];
        datespan = [dateFormatter_ stringFromDate:toDate];
    }
    return datespan;
}

- (NSLocale *)locale
{
    return (locale_ != nil ? locale_ : [NSLocale currentLocale]);
}

@end
