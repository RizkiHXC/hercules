//
//  MBURLFormatter.h
//  MBFramework
//
//  Created by Arno Woestenburg on 06/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The following constants specify predefined format styles for URL strings.
 */
typedef NS_ENUM(NSInteger, MBURLFormatterStyle)
{
    /**
     Specifies no style.
     */
    MBURLFormatterNoStyle,
    /**
     Specifies a short style, host without prefix. example: "apple.com"
     */
    MBURLFormatterShortStyle,
    /**
     Specifies medium style, host only. example: "www.apple.com"
     */
    MBURLFormatterMediumStyle,
    /**
     Specifies a full style with complete details. Example "http://www.apple.com/iphone/accessories/#iphone-5s-cases"
     */
    MBURLFormatterLongStyle,
    /**
     Same as MBURLFormatterLongStyle.
     */
    MBURLFormatterFullStyle = MBURLFormatterLongStyle,
};


/**
 The MBURLFormatter class implements a formatter object for generating URL strings.
 */
@interface MBURLFormatter : NSFormatter
{
    MBURLFormatterStyle urlStyle_;
}

/**
 Creates a string based on the specified URL.
 @param url An object representing a URL (Universal Resource Locator).
 @return A user-readable string that specifies the URL with the specified style.
 */
- (NSString *)stringFromURL:(NSURL *)url;

/**
 The preferred style for the URL string.
 */
@property(nonatomic,assign) MBURLFormatterStyle urlStyle;

@end
