//
//  MBURLFormatter.m
//  MBFramework
//
//  Created by Arno Woestenburg on 06/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBURLFormatter.h"

@implementation MBURLFormatter

@synthesize urlStyle = urlStyle_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        urlStyle_ = MBURLFormatterNoStyle;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSString *)stringFromURL:(NSURL *)url
{
    NSString *urlString = nil;
    switch (urlStyle_) {
        case MBURLFormatterNoStyle:
            urlString = [url absoluteString];
            break;
        case MBURLFormatterShortStyle:
        {
            urlString = url.host;
            NSString *prefix = @"www.";
            if ([urlString rangeOfString:prefix].location == 0) {
                urlString = [urlString substringWithRange:NSMakeRange(prefix.length, [urlString length] - prefix.length)];
            }
            break;
        }
        case MBURLFormatterMediumStyle:
            urlString = url.host;
            break;
        case MBURLFormatterLongStyle:
            urlString = [NSString stringWithFormat:@"%@%@", url.host, url.path];
            break;
        default:
            NSAssert(NO, @"Unhandled case");
            break;
    }
    return urlString;
}


@end
