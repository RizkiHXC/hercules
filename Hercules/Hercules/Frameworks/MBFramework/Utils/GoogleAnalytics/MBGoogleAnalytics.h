//
//  MBGoogleAnalytics.h
//  MBFramework
//
//  Created by Marco Jonker on 3/21/12.
//  Copyright (c) 2012 mediaBunker B.V. All rights reserved.

#import <Foundation/Foundation.h>

#import "GAI.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@class MBGoogleAnalytics;

// Protocol for ViewController
@protocol MBTrackedViewController <NSObject>

@property(nonatomic,retain) NSString *screenNameForTracker;

@optional
- (BOOL)shouldTrackView;

@end

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBGoogleAnalytics : NSObject

+ (instancetype)sharedInstance;

/**
 Initializes and returns a newly allocated GoogleAnalytics object with the specified tracking id.
 @param trackingId Tracker id.
 @param interval Tracker dispatch interval.
 @return An initialized GoogleAnalytics object or nil if the object couldn't be created.
 */
- (void)initWithTrackingId:(NSString*)trackingId dispatchInterval:(NSTimeInterval)interval;
- (void)trackAppStart;

- (void)trackViewController:(id<MBTrackedViewController>)viewController;
- (void)trackViewController:(id<MBTrackedViewController>)viewController withIdiom:(BOOL)idiom;
- (void)trackPageView:(NSString*)title;
- (void)trackPageViewForIdiom:(NSString *)screenName;
- (void)trackEventWithCategory:(NSString *)category action:(NSString *)action;
- (void)trackEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label;
- (void)trackEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber*)value;

@property(nonatomic,assign) GAILogLevel logLevel;
@property(nonatomic,readonly) BOOL hasTracker;

/* 
 When this is 'YES', no tracking information will be sent. Defaults is NO.
 */
@property(nonatomic,assign) BOOL dryRun;

@property(nonatomic,assign) bool trackInDebugMode DEPRECATED_MSG_ATTRIBUTE("No longer supported. To disable tracking, use setDryRun method");

@end

