//
//  MBGoogleAnalytics.m
//  MBFramework
//
//  Created by Marco Jonker on 3/21/12.
//  Copyright (c) 2012 mediaBunker B.V. All rights reserved.
//

#import "MBGoogleAnalytics.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

static id sharedInstance_ = nil;

@implementation MBGoogleAnalytics

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (instancetype)sharedInstance
{
    if (!sharedInstance_) {
        sharedInstance_ = [[self alloc] init];
    }
    return sharedInstance_;
}

/////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

/////////////////////////////////////////////
#pragma mark - Public functions

-(void)initWithTrackingId:(NSString*)trackingId dispatchInterval:(NSTimeInterval)interval
{
    GAI *gai = [GAI sharedInstance];
    // Optional: automatically track uncaught exceptions with Google Analytics.
    [gai setTrackUncaughtExceptions:YES];
    // Optional: set Google Analytics dispatch interval.
    [gai setDispatchInterval:interval];
    // Optional: set Logger to VERBOSE for debug information.
    [gai.logger setLogLevel:kGAILogLevelNone];
    // Create tracker instance.
    [gai trackerWithTrackingId:trackingId];
}


- (void)trackViewController:(id<MBTrackedViewController>)viewController
{
    [self trackViewController:viewController withIdiom:YES];
}

- (void)trackViewController:(id<MBTrackedViewController>)viewController withIdiom:(BOOL)idiom
{
    BOOL trackView = YES;
    
    if ([viewController respondsToSelector:@selector(shouldTrackView)]) {
        trackView = [viewController shouldTrackView];
    }
    if (trackView) {
#ifdef DEBUG
        NSAssert(viewController.screenNameForTracker.length > 0, @"%@: Forgot to provide screenNameForTracker?", [viewController class]);
#endif
        if (viewController.screenNameForTracker.length > 0) {
            if (idiom) {
                [self trackPageViewForIdiom:viewController.screenNameForTracker];
            } else {
                [self trackPageView:viewController.screenNameForTracker];
            }
        }
    }

}

- (void)trackPageView:(NSString *)title
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSAssert(tracker != nil, @"Tracker is not initialized");

    [tracker set:kGAIScreenName value:title];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)trackPageViewForIdiom:(NSString *)screenName
{
    if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
        screenName = [NSString stringWithFormat:@"/iPad/%@", screenName];
    } else {
        screenName = [NSString stringWithFormat:@"/iPhone/%@", screenName];
    }
    [self trackPageView:screenName];
}

- (void)trackEventWithCategory:(NSString *)category action:(NSString *)action
{
    [self trackEventWithCategory:category action:action label:nil value:nil];
}

- (void)trackEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label
{
    [self trackEventWithCategory:category action:action label:label value:nil];
}

- (void)trackEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSAssert(tracker != nil, @"Tracker is not initialized");
    // Setting Data on the Tracker.
    [tracker set:kGAIScreenName value:@"event"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:action
                                                           label:label
                                                           value:value] build]];
}

- (void)trackAppStart
{
    [self trackPageView:@"App-Start"];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (BOOL)hasTracker
{
    return ([[GAI sharedInstance] defaultTracker] != nil);
}

- (void)setDryRun:(BOOL)dryRun
{
    [[GAI sharedInstance] setDryRun:dryRun];
}

- (BOOL)dryRun
{
    return [[GAI sharedInstance] dryRun];
}

- (GAILogLevel)logLevel
{
    return [[[GAI sharedInstance]logger]logLevel];
}

- (void)setLogLevel:(GAILogLevel)logLevel
{
    return [[[GAI sharedInstance]logger]setLogLevel:logLevel];
}

@end
