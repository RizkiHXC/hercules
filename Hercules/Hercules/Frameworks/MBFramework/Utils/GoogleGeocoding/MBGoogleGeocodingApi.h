//
//  MBGoogleGeocodingApi.h
//  MBFramework
//
//  Created by Marco Jonker on 3/21/13.
//
//

#import <Foundation/Foundation.h>
#import "MBGoogleGeocodingSearchParameters.h"

@protocol MBGoogleGeocodingApiDelegate;

typedef NS_ENUM(NSInteger, MBGoogleGeocodingApiError)
{
    MBGoogleGeocodingApiInvalidDataError        = 1,
    MBGoogleGeocodingApiStatusError             = 2,
    MBGoogleGeocodingApiJsonDataParsingError    = 3,
    MBGoogleGeocodingApiNoDataError             = 4,
    MBGoogleGeocodingApiConnectionError         = 5,
};

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBGoogleGeocodingApi : NSObject  <NSURLConnectionDelegate> {
    NSString*           language_;
    bool                useHttps_;
	NSMutableData*      responseData_;
    NSURLConnection*    connection_;
    id<MBGoogleGeocodingApiDelegate> delegate_;
}

@property (nonatomic, retain) NSString*         language;
@property (nonatomic, assign) bool              useHttps;
@property (nonatomic, retain) NSMutableData*    responseData;
@property (nonatomic, retain) NSURLConnection*  connection;
@property (nonatomic, assign) id<MBGoogleGeocodingApiDelegate> delegate;

-(void)cancelRequest;
-(void)findLocationWithLatitude:(double)latitude andLongitude:(double)longitude;
-(void)findLocationWithSearchParameters:(MBGoogleGeocodingSearchParameters*)parameters;

@end

/**
 *  Protocol not yet (fully) documented. Documentation for this protocol is in progress.
 */
@protocol MBGoogleGeocodingApiDelegate <NSObject>
-(void)googleGeocodingApiDidReceiveResults:(NSArray*)results;
-(void)googleGeocodingApiDidReceiveError:(NSError*)error;
@end