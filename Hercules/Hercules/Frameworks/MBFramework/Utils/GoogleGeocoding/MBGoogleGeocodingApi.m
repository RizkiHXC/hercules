//
//  MBGoogleGeocodingApi.m
//  MBFramework
//
//  Created by Marco Jonker on 3/21/13.
//
//

#import "MBGoogleGeocodingApi.h"
#import "CJSONDeserializer.h"
#import "MBGoogleGeocodingLocation.h"
#import "MBError.h"

#define URL_GOOGLE_GEOCODING_API        @"http://maps.googleapis.com/maps/api/geocode/json?"
#define SECURE_URL_GOOGLE_GEOCODING_API @"https://maps.googleapis.com/maps/api/geocode/json?"

#define STATUS_OK                       @"OK"               // indicates that no errors occurred; the address was successfully parsed and at least one geocode was returned.
#define STATUS_ZERO_RESULTS             @"ZERO_RESULTS"     // indicates that the geocode was successful but returned no results. This may occur if the geocode was passed a non-existent address or a latlng in a remote location.
#define STATUS_OVER_QUERY_LIMIT         @"OVER_QUERY_LIMIT" // indicates that you are over your quota.
#define STATUS_REQUEST_DENIED           @"REQUEST_DENIED"   // indicates that your request was denied, generally because of lack of a sensor parameter.
#define STATUS_INVALID_REQUEST          @"INVALID_REQUEST"  // generally indicates that the query (address or latlng) is missing.
#define UNKNOWN_ERROR                   @"UNKNOWN_ERROR"    // indicates that the request could not be processed due to a server error. The request may succeed if you try again.

@interface MBGoogleGeocodingApi()
-(NSString*)MB_getBaseUrl;
-(void)MB_findLocationWithUrl:(NSString*)urlString;
-(void)MB_callDidReceiveResultsDelegate:(NSArray*)results;
-(void)MB_callDidReceiveErrorDelegate:(NSError*)error;
-(NSError*)MB_createError:(NSString*)description withErrorCode:(NSInteger)code andError:(NSError*)baseError;
@end

@implementation MBGoogleGeocodingApi

@synthesize language=language_;
@synthesize useHttps=useHttps_;
@synthesize connection=connection_;
@synthesize responseData=responseData_;
@synthesize delegate=delegate_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

-(instancetype)init {
    self = [super init];
    if(self != nil) {
        self.language = @"";
        self.useHttps = YES;
    }
    return self;
}

-(void)dealloc {
    [language_ release];
    
    if(connection_ != nil) {
        [connection_ cancel];
        [connection_ release];
    }
    
    if(responseData_ != nil) {
        [responseData_ release];
    }
    
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

-(NSString*)MB_getBaseUrl {
    if(useHttps_ == YES) {
        return SECURE_URL_GOOGLE_GEOCODING_API;
    } else {
        return URL_GOOGLE_GEOCODING_API;
    }
}

-(void)MB_findLocationWithUrl:(NSString*)urlString {
    NSURL* url = nil;
    
    if([language_ length] > 0) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&sensor=true&language=%@", urlString, language_]];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&sensor=true", urlString]];
    }
    
    [self cancelRequest];
    
    self.responseData = [NSMutableData data];
    self.connection   = [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
}

-(void)MB_callDidReceiveResultsDelegate:(NSArray*)results {
    if( [delegate_ respondsToSelector:@selector(googleGeocodingApiDidReceiveResults:)]){
        [delegate_ googleGeocodingApiDidReceiveResults:results];
    }
}

-(void)MB_callDidReceiveErrorDelegate:(NSError*)error {
    if( [delegate_ respondsToSelector:@selector(googleGeocodingApiDidReceiveError:)]){
        [delegate_ googleGeocodingApiDidReceiveError:error];
    }
}

-(NSError*)MB_createError:(NSString*)description withErrorCode:(NSInteger)code andError:(NSError*)baseError{
    return [MBError createError:description withErrorCode:code andError:baseError];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(void)findLocationWithLatitude:(double)latitude andLongitude:(double)longitude {
    NSString* urlString = [NSString stringWithFormat:@"%@latlng=%f,%f", [self MB_getBaseUrl], latitude, longitude];
    [self MB_findLocationWithUrl:urlString];
}

-(void)findLocationWithSearchParameters:(MBGoogleGeocodingSearchParameters*)parameters {
    NSString* urlString = [NSString stringWithFormat:@"%@address=%@", [self MB_getBaseUrl], [parameters getSearchParametersString]];
    [self MB_findLocationWithUrl:urlString];
}

-(void)cancelRequest {
    if(connection_ != nil) {
        [connection_ cancel];
        [connection_ release];
        connection_ = nil;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData_ setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)nsdata {
    [responseData_ appendData:nsdata];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    self.responseData = nil;
    NSString* errorDescription = [NSString stringWithFormat:@"GoogleGeocoding: Connection error (url:%@)", connection.currentRequest.URL];
    [self MB_callDidReceiveErrorDelegate:[self MB_createError:errorDescription withErrorCode:MBGoogleGeocodingApiConnectionError andError:error]];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if(responseData_ != nil && [responseData_ length] > 0) {
        NSError* error = nil;
        NSDictionary* dataStructure = [[CJSONDeserializer deserializer]deserializeAsDictionary:responseData_ error:&error];
        
        if(error == nil) {
            if([[dataStructure objectForKey:@"status"] isKindOfClass:[NSString class]]) {
               NSString* status = [dataStructure objectForKey:@"status"];
               
               if([status caseInsensitiveCompare:STATUS_OK] == NSOrderedSame) {
                   if([[dataStructure objectForKey:@"results"] isKindOfClass:[NSArray class]] ){
                       NSArray* results = [MBGoogleGeocodingLocation fromArray:[dataStructure objectForKey:@"results"]];
                       [self MB_callDidReceiveResultsDelegate:results];
                   } else {
                       NSString* errorDescription = [NSString stringWithFormat:@"GoogleGeocoding: Invalid result data (url:%@)", connection.currentRequest.URL];
                       [self MB_callDidReceiveErrorDelegate:[self MB_createError:errorDescription withErrorCode:MBGoogleGeocodingApiInvalidDataError andError:nil]];
                   }
               } else if ([status caseInsensitiveCompare:STATUS_ZERO_RESULTS] == NSOrderedSame) {
                   [self MB_callDidReceiveResultsDelegate:[NSArray array]];
               } else {
                   NSString* errorDescription = [NSString stringWithFormat:@"GoogleGeocoding: Status error (status:%@ url:%@)", status, connection.currentRequest.URL];
                   [self MB_callDidReceiveErrorDelegate:[self MB_createError:errorDescription withErrorCode:MBGoogleGeocodingApiStatusError andError:nil]];
               }
            } else {
                NSString* errorDescription = [NSString stringWithFormat:@"GoogleGeocoding: Invalid result data (url:%@)", connection.currentRequest.URL];
                [self MB_callDidReceiveErrorDelegate:[self MB_createError:errorDescription withErrorCode:MBGoogleGeocodingApiInvalidDataError andError:nil]];
           }
        }else {
            NSString* errorDescription = [NSString stringWithFormat:@"GoogleGeocoding: Error pasing json (url:%@)", connection.currentRequest.URL];
            [self MB_callDidReceiveErrorDelegate:[self MB_createError:errorDescription withErrorCode:MBGoogleGeocodingApiJsonDataParsingError andError:error]];
        }
    } else {
        NSString* errorDescription = [NSString stringWithFormat:@"GoogleGeocoding: No data received (url:%@)",connection.currentRequest.URL];
        [self MB_callDidReceiveErrorDelegate:[self MB_createError:errorDescription withErrorCode:MBGoogleGeocodingApiNoDataError andError:nil]];
    }
}

@end


