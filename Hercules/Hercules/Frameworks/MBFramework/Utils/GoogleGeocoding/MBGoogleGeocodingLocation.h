//
//  MBGoogleGeocodingLocation.h
//  MBFramework
//
//  Created by Marco Jonker on 3/21/13.
//
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBGoogleGeocodingLocation : NSObject {
    NSString* houseNumber_;
    NSString* houseNumberShort_;
    NSString* street_;
    NSString* streetShort_;
    NSString* zipCode_;
    NSString* zipCodeShort_;
    NSString* city_;
    NSString* cityShort_;
    NSString* country_;
    NSString* countryShort_;
    NSString* areaLevel1_;
    NSString* areaLevel1Short_;
    NSString* areaLevel2_;
    NSString* areaLevel2Short_;
    NSString* areaLevel3_;
    NSString* areaLevel3Short_;
    NSNumber* latitude_;
    NSNumber* longitude_;
    NSString* formattedAddress_;
}

@property (nonatomic, retain) NSString* houseNumber;
@property (nonatomic, retain) NSString* houseNumberShort;
@property (nonatomic, retain) NSString* street;
@property (nonatomic, retain) NSString* streetShort;
@property (nonatomic, retain) NSString* zipCode;
@property (nonatomic, retain) NSString* zipCodeShort;
@property (nonatomic, retain) NSString* city;
@property (nonatomic, retain) NSString* cityShort;
@property (nonatomic, retain) NSString* country;
@property (nonatomic, retain) NSString* countryShort;
@property (nonatomic, retain) NSString* areaLevel1;
@property (nonatomic, retain) NSString* areaLevel1Short;
@property (nonatomic, retain) NSString* areaLevel2;
@property (nonatomic, retain) NSString* areaLevel2Short;
@property (nonatomic, retain) NSString* areaLevel3;
@property (nonatomic, retain) NSString* areaLevel3Short;
@property (nonatomic, retain) NSNumber* latitude;
@property (nonatomic, retain) NSNumber* longitude;
@property (nonatomic, retain) NSString* formattedAddress;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
-(void)loadFromDictionary:(NSDictionary*)dictionary;
+(MBGoogleGeocodingLocation*)fromDictionary:(NSDictionary*)dictionary;
+(NSArray*)fromArray:(NSArray*)array;

@end
