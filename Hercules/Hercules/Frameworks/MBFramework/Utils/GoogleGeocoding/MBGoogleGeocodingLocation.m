//
//  MBGoogleGeocodingLocation.m
//  MBFramework
//
//  Created by Marco Jonker on 3/21/13.
//
//

#import "MBGoogleGeocodingLocation.h"

@interface  MBGoogleGeocodingLocation()
-(NSDictionary*)MB_findComponentForTye:(NSString*)type fromComponents:(NSArray*)components;
@end

@implementation MBGoogleGeocodingLocation

@synthesize houseNumber=houseNumber_;
@synthesize houseNumberShort=houseNumberShort_;
@synthesize street=street_;
@synthesize streetShort=streetShort_;
@synthesize zipCode=zipCode_;
@synthesize zipCodeShort=zipCodeShort_;
@synthesize city=city_;
@synthesize cityShort=cityShort_;
@synthesize country=country_;
@synthesize countryShort=countryShort_;
@synthesize areaLevel1=areaLevel1_;
@synthesize areaLevel1Short=areaLevel1Short_;
@synthesize areaLevel2=areaLevel2_;
@synthesize areaLevel2Short=areaLevel2Short_;
@synthesize areaLevel3=areaLevel3_;
@synthesize areaLevel3Short=areaLevel3Short_;
@synthesize latitude=latitude_;
@synthesize longitude=longitude_;
@synthesize formattedAddress=formattedAddress_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

-(instancetype)init {
    self = [super init];
    
    if(self != nil) {
        self.houseNumber = @"";
        self.houseNumberShort = @"";
        self.street = @"";
        self.streetShort = @"";
        self.zipCode = @"";
        self.zipCodeShort = @"";
        self.city = @"";
        self.cityShort = @"";
        self.country = @"";
        self.countryShort = @"";
        self.areaLevel1 = @"";
        self.areaLevel1Short = @"";
        self.areaLevel2 = @"";
        self.areaLevel2Short = @"";
        self.areaLevel3 = @"";
        self.areaLevel3Short = @"";
        self.latitude = nil;
        self.longitude = nil;
        self.formattedAddress = @"";
    }
    
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary*)dictionary {
    self = [self init];
    if(self != nil) {
        [self loadFromDictionary:dictionary];
    }
    
    return self;
}

-(void)dealloc {
    self.houseNumber = nil;
    self.houseNumberShort = nil;
    self.street = nil;
    self.streetShort = nil;
    self.zipCode = nil;
    self.zipCodeShort = nil;
    self.city = nil;
    self.cityShort = nil;
    self.country = nil;
    self.countryShort = nil;
    self.areaLevel1 = nil;
    self.areaLevel1Short = nil;
    self.areaLevel2 = nil;
    self.areaLevel2Short = nil;
    self.areaLevel3 = nil;
    self.areaLevel3Short = nil;
    self.latitude = nil;
    self.longitude = nil;
    self.formattedAddress = nil;
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private functions

-(NSDictionary*)MB_findComponentForTye:(NSString*)type fromComponents:(NSArray*)components {
    NSDictionary* result = nil;
    
    for(NSDictionary* component in components) {
        NSArray* types = nil;
        if([[component valueForKey:@"types"] isKindOfClass:[NSString class]]) {
            types = [NSArray arrayWithObject:[component valueForKey:@"types"]];
        } else {
            types = [component valueForKey:@"types"];
        }
        
        if([types containsObject:type]) {
            result = component;
        }
    }
    
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(void)loadFromDictionary:(NSDictionary*)dictionary {
    NSArray*      components = [dictionary valueForKey:@"address_components"];
    
    NSDictionary* component = [self MB_findComponentForTye:@"street_number" fromComponents:components];
    if(component != nil) {
        self.houseNumber         = [component valueForKey:@"long_name"];
        self.houseNumberShort    = [component valueForKey:@"short_name"];
    }
    
    component = [self MB_findComponentForTye:@"route" fromComponents:components];
    if(component != nil) {
        self.street         = [component valueForKey:@"long_name"];
        self.streetShort    = [component valueForKey:@"short_name"];
    }
    
    component  = [self MB_findComponentForTye:@"locality" fromComponents:components];
    if(component != nil) {
        self.city           = [component valueForKey:@"long_name"];
        self.cityShort      = [component valueForKey:@"short_name"];
    }
    
    component  = [self MB_findComponentForTye:@"administrative_area_level_3" fromComponents:components];
    if(component != nil) {
        self.areaLevel3     = [component valueForKey:@"long_name"];
        self.areaLevel3Short= [component valueForKey:@"short_name"];
    }
    
    component  = [self MB_findComponentForTye:@"administrative_area_level_2" fromComponents:components];
    if(component != nil) {
        self.areaLevel2     = [component valueForKey:@"long_name"];
        self.areaLevel2Short= [component valueForKey:@"short_name"];
    }
    
    component  = [self MB_findComponentForTye:@"administrative_area_level_1" fromComponents:components];
    if(component != nil) {
        self.areaLevel1     = [component valueForKey:@"long_name"];
        self.areaLevel1Short= [component valueForKey:@"short_name"];
    }
    
    component  = [self MB_findComponentForTye:@"country" fromComponents:components];
    if(component != nil) {
        self.country        = [component valueForKey:@"long_name"];
        self.countryShort   = [component valueForKey:@"short_name"];
    }
    
    component  = [self MB_findComponentForTye:@"postal_code" fromComponents:components];
    if(component != nil) {
        self.zipCode        = [component valueForKey:@"long_name"];
        self.zipCodeShort   = [component valueForKey:@"short_name"];
    }
    
    if([[dictionary valueForKey:@"formatted_address"]isKindOfClass:[NSString class]]) {
        self.formattedAddress = [dictionary valueForKey:@"formatted_address"];
    }
    
    if([[dictionary valueForKey:@"geometry"]isKindOfClass:[NSDictionary class]]) {
        NSDictionary* geometry = [dictionary valueForKey:@"geometry"];
        if([[geometry valueForKey:@"location"]isKindOfClass:[NSDictionary class]]) {
            NSDictionary* location = [geometry valueForKey:@"location"];
            self.latitude   = [location valueForKey:@"lat"];
            self.longitude  = [location valueForKey:@"lng"];
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public static functions

+(MBGoogleGeocodingLocation*)fromDictionary:(NSDictionary*)dictionary {
    MBGoogleGeocodingLocation* googleGeocodeingLocation = [[[MBGoogleGeocodingLocation alloc]initWithDictionary:dictionary]autorelease];
    return googleGeocodeingLocation;
}

+(NSArray*)fromArray:(NSArray*)array {
    NSMutableArray* googleGeocodingLocations = [[[NSMutableArray alloc]init]autorelease];
    
    for(NSDictionary* dictionary in array) {
        [googleGeocodingLocations addObject:[MBGoogleGeocodingLocation fromDictionary:dictionary]];
    }
    
    return googleGeocodingLocations;
}

@end
