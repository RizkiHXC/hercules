//
//  MBGoogleGeocodingSearchParameters.h
//  MBFramework
//
//  Created by Marco Jonker on 3/21/13.
//
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBGoogleGeocodingSearchParameters : NSObject {
    NSString* houseNumber_;
    NSString* street_;
    NSString* zipCode_;
    NSString* city_;
    NSString* state_;
    NSString* country_;
}

@property (nonatomic, retain) NSString* houseNumber;
@property (nonatomic, retain) NSString* street;
@property (nonatomic, retain) NSString* zipCode;
@property (nonatomic, retain) NSString* city;
@property (nonatomic, retain) NSString* state;
@property (nonatomic, retain) NSString* country;

-(NSString*)getSearchParametersString;

@end
