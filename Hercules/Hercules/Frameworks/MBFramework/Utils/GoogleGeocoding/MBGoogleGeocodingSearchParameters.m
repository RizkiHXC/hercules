//
//  MBGoogleGeocodingSearchParameters.m
//  MBFramework
//
//  Created by Marco Jonker on 3/21/13.
//
//

#import "MBGoogleGeocodingSearchParameters.h"

@implementation MBGoogleGeocodingSearchParameters

@synthesize houseNumber=houseNumber_;
@synthesize street=street_;
@synthesize zipCode=zipCode_;
@synthesize city=city_;
@synthesize state=state_;
@synthesize country=country_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public functions

-(NSString*)getSearchParametersString {
    NSMutableString*    searchString = [[[NSMutableString alloc]init]autorelease];
    NSString*           result       = @"";
    
    if(houseNumber_ != nil && [houseNumber_ length] > 0) {
        [searchString appendFormat:@"%@+", houseNumber_];
    }
    if(street_ != nil && [street_ length] > 0) {
        [searchString appendFormat:@"%@+", street_];
    }
    if(zipCode_ != nil && [zipCode_ length] > 0) {
        [searchString appendFormat:@"%@", zipCode_];
    }
    
    if([searchString length] > 0){
        [searchString appendFormat:@","];
    }
    
    if(city_ != nil && [city_ length] > 0) {
        [searchString appendFormat:@"%@,", city_];
    }
    if(state_ != nil && [state_ length] > 0) {
        [searchString appendFormat:@"%@,", state_];
    }
    if(country_ != nil && [country_ length] > 0) {
        [searchString appendFormat:@"%@,", country_];
    }
    if([searchString length] > 1) {
        result = [searchString substringToIndex:[searchString length] - 1];
    }
    
    return [result stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
