//
//  MBKeychainItem.h
//  MBFramework
//
//  Created by Arno Woestenburg on 1/7/13.
//
//

#import "KeychainItemWrapper.h"

/**
 MBKeychainItem is the base class for keychain item objects that interact with the iOS Keychain system.
    @warning You cannot use the MBKeychainItem class directly to instantiate a keychain object. It instead defines the common interface and behavioral structure for its subclasses.
 */
@interface MBKeychainItem : KeychainItemWrapper
{
    
}

/**
 Creates and returns a keychain item instance with the specified account and identifier.
 @param account    Account name.
 @param identifier Identifier for the service associated with this item.
 @return A newly created keychain item instance.
 */
+ (instancetype)keychainItemWithAccount:(NSString *)account identifier:(NSString *)identifier;

/**
 Creates and returns a keychain item object initialized with the specified parameters.
 *
 @param account     Account name.
 @param service     Represents the service associated with this item.
 @param accessGroup Access group. @see accessGroup property.
 @return A newly created keychain item instance.
 */
+ (instancetype)keychainItemWithAccount:(NSString *)account service:(NSString *)service accessGroup:(NSString *)accessGroup;

/**
 Deletes all keychain items accessible to this app.
 */
+ (void)clearKeychainItems;

/**
 Returns an MBKeychainItem object initialized  with the specified parameters.
 *
 @param account     Account name.
 @param service     Represents the service associated with this item.
 @param accessGroup Access group. @see accessGroup property.
 *
 @return A newly created MBKeychainItem instance.
 */
- (instancetype)initWithAccount:(NSString *)account service:(NSString *)service accessGroup:(NSString *)accessGroup;

/**
 Secured data stored in the keychain item.
 */
@property (nonatomic, assign) NSString *secureValueData;

/**
 Indicates when your application needs access to an item's data. You
 should choose the most restrictive option that meets your application's
 needs to allow the system to protect that item in the best way possible.
 See the "kSecAttrAccessible Value Constants" section for a list of
 values which can be specified.
 @warning IMPORTANT: This attribute is currently not supported for OS X keychain
 items, unless the kSecAttrSynchronizable attribute is also present. If
 both attributes are specified on either OS X or iOS, the value for the
 kSecAttrAccessible key may only be one whose name does not end with
 "ThisDeviceOnly", as those cannot sync to another device.
 */
@property(nonatomic,readonly) BOOL accessible;
/**
 Indicates which access group an item is in. The access
 groups that a particular application has membership in are determined by
 two entitlements for that application.  The application-identifier
 entitlement contains the application's single access group, unless
 there is a keychain-access-groups entitlement present.  The latter
 has as its value a list of access groups; the first item in this list
 is the default access group. Unless a specific access group is provided
 as the value of kSecAttrAccessGroup when SecItemAdd is called, new items
 are created in the application's default access group.  Specifying this
 attribute in SecItemCopyMatching, SecItemUpdate, or SecItemDelete calls
 limits the search to the specified access group (of which the calling
 application must be a member to obtain matching results.)  To share
 keychain items between multiple applications, each application must have
 a common group listed in its keychain-access-groups entitlement, and each
 must specify this shared access group name as the value for the
 kSecAttrAccessGroup key in the dictionary passed to SecItem functions.
 */
@property(nonatomic,retain) NSString *accessGroup;
/**
 A value that represents the date the item was created. (read-only)
 */
@property(nonatomic,readonly) NSDate *creationDate;
/**
 * A value that represents the last time the item was updated. (read-only)
 */
@property(nonatomic,readonly) NSDate *modificationDate;
/**
 Represents a user-visible string describing this particular kind of item (e.g., "disk image password").
 */
@property(nonatomic,retain) NSString *description;
/**
 Set or get the user-editable comment for this item.
 */
@property(nonatomic,retain) NSString *comment;
/**
 Set or get a value that represents the item's creator. 
 @discussion This number is the unsigned integer representation of a four-character code (e.g., 'aCrt').
 */
@property(nonatomic,retain) NSNumber *creator;
/**
 Set or get the item's type. 
 @discussion This number is the unsigned integer representation of a four-character code (e.g., 'aTyp').
 */
@property(nonatomic,retain) NSNumber *type;
/**
 Set or get the user-visible label for this item.
 */
@property(nonatomic,retain) NSString *label;
/**
 Set or get a value that indicates whether the item is invisible (i.e., should not be displayed.)
 */
@property(nonatomic,assign,getter=isInvisible) BOOL invisible;
/**
 Set or get a value that indicates whether there is a valid password associated with this keychain item. This is useful if your application doesn't want a password for some particular service to be stored in the keychain, but prefers that it always be entered by the user.
 */
@property(nonatomic,assign,getter=isNegative) BOOL negative;
/**
 Set or get a value of type CFNumberRef that denotes the authentication scheme for this item (see the kSecAttrAuthenticationType value constants below).
 */
@property(nonatomic,retain) NSNumber *authenticationType;
/**
 Value is one of kSecAttrKeyClassPublic, kSecAttrKeyClassPrivate or kSecAttrKeyClassSymmetric. (read-only)
 */
@property(nonatomic,readonly) NSString *keyClass;
/**
 The key's application label attribute. This is different from the label property (which is intended to be human-readable). This attribute is used to look up a key programmatically; in particular, for keys of class kSecAttrKeyClassPublic and kSecAttrKeyClassPrivate, the value of this attribute is the hash of the public key. (read-only)
 */
@property(nonatomic,readonly) NSString *applicationLabel;
/**
 Indicates whether the key in question will be stored permanently.
 */
@property(nonatomic,readonly,getter=isPermanent) BOOL permanent;
/**
 Contains private tag data.
 */
@property(nonatomic,readonly) NSData *applicationTag;
/**
 Indicates the algorithm associated with this key (Currently only the value 42 is supported, alternatively you can use kSecAttrKeyTypeRSA).
 */
@property(nonatomic,readonly) NSNumber *keyType;
/**
 Indicates the number of bits in this key.
 */
@property(nonatomic,readonly) NSNumber *keySizeInBits;
/**
 Indicates the effective number of bits in this key.
 @discussion For example, a DES key has a kSecAttrKeySizeInBits of 64, but a kSecAttrEffectiveKeySize of 56 bits.
 */
@property(nonatomic,readonly) NSNumber *effectiveKeySize;
/**
 Indicates whether the key in question can be used to encrypt data.
 */
@property(nonatomic,readonly) BOOL canEncrypt;
/**
 Indicates whether the key in question can be used to decrypt data.
 */
@property(nonatomic,readonly) BOOL canDecrypt;
/**
 Indicates whether the key in question can be used to derive another key.
 */
@property(nonatomic,readonly) BOOL canDerive;
/**
 Indicating whether the key in question can be used to create a digital signature.
 */
@property(nonatomic,readonly) BOOL canSign;
/**
 Indicates whether the key in question can be used to verify a digital signature.
 */
@property(nonatomic,readonly) BOOL canVerify;
/**
 Indicating whether the key in question can be used to wrap another key.
 */
@property(nonatomic,readonly) BOOL canWrap;
/**
 Indicates whether the key in question can be used to unwrap another key.
 */
@property(nonatomic,readonly) BOOL canUnwrap;

// Custom (non-standard) keys:

/**
 Set or get the expiration date for this item.
 */
@property(nonatomic,retain) NSDate *expirationDate;

@end
