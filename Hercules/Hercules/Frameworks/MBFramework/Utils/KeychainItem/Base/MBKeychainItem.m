//
//  MBKeychainItem.m
//  MBFramework
//
//  Created by Arno Woestenburg on 1/7/13.
//
//

#import "MBKeychainItem.h"

@interface MBKeychainItem ()
{
    id classType_;
}
@end

@implementation MBKeychainItem

static const NSString * kExpirationDate = @"MB_kExpirationDate";

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (instancetype)keychainItemWithAccount:(NSString *)account identifier:(NSString *)identifier
{
    return [[[self alloc] initWithAccount:account
                                  service:[NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier], identifier]
                              accessGroup:nil] autorelease];
}

+ (instancetype)keychainItemWithAccount:(NSString *)account service:(NSString *)service accessGroup:(NSString *)accessGroup
{
    return [[[self alloc] initWithAccount:account service:service accessGroup:accessGroup] autorelease];
}

+ (void)clearKeychainItems
{
    NSArray *secItemClasses = @[(__bridge id)kSecClassGenericPassword,
                                (__bridge id)kSecClassInternetPassword,
                                (__bridge id)kSecClassCertificate,
                                (__bridge id)kSecClassKey,
                                (__bridge id)kSecClassIdentity];
    for (id secItemClass in secItemClasses) {
        NSDictionary *spec = @{(__bridge id)kSecClass: secItemClass};
        SecItemDelete((__bridge CFDictionaryRef)spec);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        // Class type
        classType_ = [self classType];
        if (!classType_) {
            [NSException raise:NSInternalInconsistencyException format:@"You cannot use the MBKeychainItem class directly to instantiate a keychain object. You can use one of the subclasses for keychain item (MBKeychainItemCertificate, MBKeychainItemGenericPassword, MBKeychainItemIdentity, MBKeychainItemInternetPassword or MBKeychainItemKey) or you can override [MBKeychainItem %@] in a subclass.", NSStringFromSelector(_cmd)];
            return nil;
        }
    }
    return self;
}

// source: http://stackoverflow.com/questions/4891562/ios-keychain-services-only-specific-values-allowed-for-ksecattrgeneric-key
- (instancetype)initWithAccount:(NSString *)account service:(NSString *)service accessGroup:(NSString *)accessGroup
{
    self = [self init];
    if (self) {
        NSAssert(account != nil || service != nil, @"Both account and service are nil. Must specifiy at least one.");
        // Begin Keychain search setup. The genericPasswordQuery the attributes kSecAttrAccount and
        // kSecAttrService are used as unique identifiers differentiating keychain items from one another
        genericPasswordQuery = [[NSMutableDictionary alloc] init];
        
        [genericPasswordQuery setObject:(id)classType_ forKey:(id)kSecClass];
        
        if (account) {
            [genericPasswordQuery setObject:account forKey:(id)kSecAttrAccount];
        }
        if (service) {
            [genericPasswordQuery setObject:service forKey:(id)kSecAttrService];
        }
        
        // The keychain access group attribute determines if this item can be shared
        // amongst multiple apps whose code signing entitlements contain the same keychain access group.
        if (accessGroup != nil) {
#if !TARGET_IPHONE_SIMULATOR
            // Ignore the access group if running on the iPhone simulator.
            //
            // Apps that are built for the simulator aren't signed, so there's no keychain access group
            // for the simulator to check. This means that all apps can see all keychain items when run
            // on the simulator.
            //
            // If a SecItem contains an access group attribute, SecItemAdd and SecItemUpdate on the
            // simulator will return -25243 (errSecNoAccessForItem).
            [genericPasswordQuery setObject:accessGroup forKey:(id)kSecAttrAccessGroup];
#endif // !TARGET_IPHONE_SIMULATOR
        }
        
        // Use the proper search constants, return only the attributes of the first match.
        [genericPasswordQuery setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
        [genericPasswordQuery setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnAttributes];
        
        NSDictionary *tempQuery = [NSDictionary dictionaryWithDictionary:genericPasswordQuery];
        
        NSMutableDictionary *outDictionary = nil;
        
        if (! SecItemCopyMatching((CFDictionaryRef)tempQuery, (CFTypeRef *)&outDictionary) == noErr) {
            // Stick these default values into keychain item if nothing found.
            [self resetKeychainItem];
            
            //Adding the account and service identifiers to the keychain
            if (account) {
                [self setAccount:account];
            }
            if (service) {
                [self setService:service];
            }
            
            if (accessGroup != nil) {
#if !TARGET_IPHONE_SIMULATOR
                // Ignore the access group if running on the iPhone simulator.
                //
                // Apps that are built for the simulator aren't signed, so there's no keychain access group
                // for the simulator to check. This means that all apps can see all keychain items when run
                // on the simulator.
                //
                // If a SecItem contains an access group attribute, SecItemAdd and SecItemUpdate on the
                // simulator will return -25243 (errSecNoAccessForItem).
                [self setAccessGroup:accessGroup];
#endif // !TARGET_IPHONE_SIMULATOR
            }
        } else {
            // load the saved data from Keychain.
            self.keychainItemData = [super secItemFormatToDictionary:outDictionary];
        }
        
        [outDictionary release];
    }
    
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (id)classType
{
    return nil;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Data values

- (NSString *)secureValueData
{
    return [[[self objectForKey:(id)kSecValueData] copy] autorelease];
}

- (void)setSecureValueData:(NSString *)secureValueData
{
    [super setObject:secureValueData forKey:(id)kSecValueData];
}


//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Attributes

- (BOOL)accessible
{
    return [[self objectForKey:(id)kSecAttrAccessible] boolValue];
}

- (NSString *)accessGroup
{
    return [self objectForKey:(id)kSecAttrAccessGroup];
}

- (void)setAccessGroup:(NSString *)accessGroup
{
    [self setObject:accessGroup forKey:(id)kSecAttrAccessGroup];
}

- (NSDate *)creationDate
{
    return [self objectForKey:(id)kSecAttrCreationDate];
}

- (NSDate *)modificationDate
{
    return [self objectForKey:(id)kSecAttrModificationDate];
}

- (NSString *)description
{
    return [self objectForKey:(id)kSecAttrDescription];
}

- (void)setDescription:(NSString *)description
{
    [self setObject:description forKey:(id)kSecAttrDescription];
}

- (NSString *)comment
{
    return [self objectForKey:(id)kSecAttrComment];
}

- (void)setComment:(NSString *)comment
{
    [self setObject:comment forKey:(id)kSecAttrComment];
}

- (NSNumber *)creator
{
    return [self objectForKey:(id)kSecAttrCreator];
}

- (void)setCreator:(NSNumber *)creator
{
    [self setObject:creator forKey:(id)kSecAttrCreator];
}

- (NSNumber *)type
{
    return [self objectForKey:(id)kSecAttrType];
}

- (void)setType:(NSNumber *)type
{
    [self setObject:type forKey:(id)kSecAttrType];
}

- (NSString *)label
{
    return [self objectForKey:(id)kSecAttrLabel];
}

- (void)setLabel:(NSString *)label
{
    [self setObject:label forKey:(id)kSecAttrLabel];
}

- (BOOL)isInvisible
{
    return [[self objectForKey:(id)kSecAttrIsInvisible] boolValue];
}

- (void)setInvisible:(BOOL)invisible
{
    [self setObject:@(invisible) forKey:(id)kSecAttrIsInvisible];
}

- (BOOL)isNegative
{
    return [[self objectForKey:(id)kSecAttrIsNegative] boolValue];
}

- (void)setNegative:(BOOL)negative
{
    [self setObject:@(negative) forKey:(id)kSecAttrIsNegative];
}

- (NSString *)account
{
    return [self objectForKey:(id)kSecAttrAccount];
}

- (void)setAccount:(NSString *)account
{
    [self setObject:account forKey:(id)kSecAttrAccount];
}

- (NSString *)service
{
    return [self objectForKey:(id)kSecAttrService];
}

- (void)setService:(NSString *)service
{
    [self setObject:service forKey:(id)kSecAttrService];
}

- (NSData *)generic
{
    return [self objectForKey:(id)kSecAttrGeneric];
}

- (void)setGeneric:(NSData *)generic
{
    [self setObject:generic forKey:(id)kSecAttrGeneric];
}

- (NSString *)securityDomain
{
    return [self objectForKey:(id)kSecAttrSecurityDomain];
}

- (void)setSecurityDomain:(NSString *)securityDomain
{
    [self setObject:securityDomain forKey:(id)kSecAttrSecurityDomain];
}

- (NSString *)server
{
    return [self objectForKey:(id)kSecAttrServer];
}

- (void)setServer:(NSString *)server
{
    [self setObject:server forKey:(id)kSecAttrServer];
}

- (NSNumber *)protocol
{
    return [self objectForKey:(id)kSecAttrProtocol];
}

- (void)setProtocol:(NSNumber *)protocol
{
    [self setValue:protocol forKey:(id)kSecAttrProtocol];
}

- (NSNumber *)authenticationType
{
    return [self objectForKey:(id)kSecAttrAuthenticationType];
}

- (void)setAuthenticationType:(NSNumber *)authenticationType
{
    [self setObject:authenticationType forKey:(id)kSecAttrAuthenticationType];
}

- (NSNumber *)port
{
    return [self objectForKey:(id)kSecAttrPort];
}

- (void)setPort:(NSNumber *)port
{
    [self setValue:port forKey:(id)kSecAttrPort];
}

- (NSString *)path
{
    return [self objectForKey:(id)kSecAttrPath];
}

- (void)setPath:(NSString *)path
{
    [self setValue:path forKey:(id)kSecAttrPath];
}

- (NSData *)subject
{
    return [self objectForKey:(id)kSecAttrSubject];
}

- (NSData *)issuer
{
    return [self objectForKey:(id)kSecAttrIssuer];
}

- (NSData *)serialNumber
{
    return [self objectForKey:(id)kSecAttrSerialNumber];
}

- (NSData *)subjectKeyID
{
    return [self objectForKey:(id)kSecAttrSubjectKeyID];
}

- (NSData *)publicKeyHash
{
    return [self objectForKey:(id)kSecAttrPublicKeyHash];
}

- (NSNumber *)certificateType
{
    return [self objectForKey:(id)kSecAttrCertificateType];
}

- (NSNumber *)certificateEncoding
{
    return [self objectForKey:(id)kSecAttrCertificateEncoding];
}

- (NSString *)keyClass
{
    return [self objectForKey:(id)kSecAttrKeyClass];
}

- (NSString *)applicationLabel
{
    return [self objectForKey:(id)kSecAttrApplicationLabel];
}

- (BOOL)isPermanent
{
    return [[self objectForKey:(id)kSecAttrIsPermanent] boolValue];
}

- (NSData *)applicationTag
{
    return [self objectForKey:(id)kSecAttrApplicationTag];
}

- (NSNumber *)keyType
{
    return [self objectForKey:(id)kSecAttrKeyType];
}

- (NSNumber *)keySizeInBits
{
    return [self objectForKey:(id)kSecAttrKeySizeInBits];
}

- (NSNumber *)effectiveKeySize
{
    return [self objectForKey:(id)kSecAttrEffectiveKeySize];
}

- (BOOL)canEncrypt
{
    return [[self objectForKey:(id)kSecAttrCanEncrypt] boolValue];
}

- (BOOL)canDecrypt
{
    return [[self objectForKey:(id)kSecAttrCanDecrypt] boolValue];
}

- (BOOL)canDerive
{
    return [[self objectForKey:(id)kSecAttrCanDerive] boolValue];
}

- (BOOL)canSign
{
    return [[self objectForKey:(id)kSecAttrCanSign] boolValue];
}

- (BOOL)canVerify
{
    return [[self objectForKey:(id)kSecAttrCanVerify] boolValue];
}

- (BOOL)canWrap
{
    return [[self objectForKey:(id)kSecAttrCanWrap] boolValue];
}

- (BOOL)canUnwrap
{
    return [[self objectForKey:(id)kSecAttrCanUnwrap] boolValue];
}

- (NSDate *)expirationDate
{
    return [self objectForKey:(id)kExpirationDate];
}

- (void)setExpirationDate:(NSDate *)expirationDate
{
    [self setObject:expirationDate forKey:(id)kExpirationDate];
}


@end
