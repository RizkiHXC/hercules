//
//  MBKeychainItemCertificate.h
//  MBFramework
//
//  Created by Arno Woestenburg on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBKeychainItem.h"

/**
 The MBKeychainItemCertificate class provides a programmatic interface for interacting with the iOS Keychain system for certificate items.
 */
@interface MBKeychainItemCertificate : MBKeychainItem

/**
 Contains the X.500 subject name of a certificate. (read-only)
 */
@property(nonatomic,readonly) NSData *subject;
/**
 Contains the X.500 issuer name of a certificate. (read-only)
 */
@property(nonatomic,readonly) NSData *issuer;
/**
 Contains the serial number data of a certificate. (read-only)
 */
@property(nonatomic,readonly) NSData *serialNumber;
/**
 Contains the subject key ID of a certificate. (read-only)
 */
@property(nonatomic,readonly) NSData *subjectKeyID;
/**
 Contains the hash of a certificate's public key. (read-only)
 */
@property(nonatomic,readonly) NSData *publicKeyHash;
/**
 A value that denotes the certificate type (Currently only the value of this attribute must be equal to the version of the X509 certificate.  So 1 for v1 2 for v2 and 3 for v3 certificates). (read-only)
 */
@property(nonatomic,readonly) NSNumber *certificateType;
/**
 The item's certificate encoding. You use this key to get a value that denotes the certificate encoding (Currently only the value 3 meaning
 kSecAttrCertificateEncodingDER is supported). (read-only)
 */
@property(nonatomic,readonly) NSNumber *certificateEncoding;

@end
