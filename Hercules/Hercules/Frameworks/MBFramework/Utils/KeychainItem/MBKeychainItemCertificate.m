//
//  MBKeychainItemCertificate.m
//  MBFramework
//
//  Created by Arno Woestenburg on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBKeychainItemCertificate.h"

@implementation MBKeychainItemCertificate

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (id)classType
{
    return (id)kSecClassCertificate;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSData *)subject
{
    return [self objectForKey:(id)kSecAttrSubject];
}

- (NSData *)issuer
{
    return [self objectForKey:(id)kSecAttrIssuer];
}

- (NSData *)serialNumber
{
    return [self objectForKey:(id)kSecAttrSerialNumber];
}

- (NSData *)subjectKeyID
{
    return [self objectForKey:(id)kSecAttrSubjectKeyID];
}

- (NSData *)publicKeyHash
{
    return [self objectForKey:(id)kSecAttrPublicKeyHash];
}

- (NSNumber *)certificateType
{
    return [self objectForKey:(id)kSecAttrCertificateType];
}

- (NSNumber *)certificateEncoding
{
    return [self objectForKey:(id)kSecAttrCertificateEncoding];
}

@end
