//
//  MBKeychainItemGenericPassword.h
//  MBFramework
//
//  Created by Arno Woestenburg on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBKeychainItem.h"

/**
 The MBKeychainItemGenericPassword class provides a programmatic interface for interacting with the iOS Keychain system for generic password items.
 */
@interface MBKeychainItemGenericPassword : MBKeychainItem

/**
 Set or get an account name.
 */
@property(nonatomic,retain) NSString *account;
/**
 Set or get  the service associated with this item.
 */
@property(nonatomic,retain) NSString *service;
/**
 Set or get a value of NSData that contains a user-defined attribute.
 */
@property(nonatomic,retain) NSData *generic;

@end
