//
//  MBKeychainItemGenericPassword.m
//  MBFramework
//
//  Created by Arno Woestenburg on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBKeychainItemGenericPassword.h"

@implementation MBKeychainItemGenericPassword

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (id)classType
{
    return (id)kSecClassGenericPassword;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSString *)account
{
    return [self objectForKey:(id)kSecAttrAccount];
}

- (void)setAccount:(NSString *)account
{
    [self setObject:account forKey:(id)kSecAttrAccount];
}

- (NSString *)service
{
    return [self objectForKey:(id)kSecAttrService];
}

- (void)setService:(NSString *)service
{
    [self setObject:service forKey:(id)kSecAttrService];
}

- (NSData *)generic
{
    return [self objectForKey:(id)kSecAttrGeneric];
}

- (void)setGeneric:(NSData *)generic
{
    [self setObject:generic forKey:(id)kSecAttrGeneric];
}

@end
