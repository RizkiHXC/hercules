//
//  MBKeychainItemIdentity.m
//  MBFramework
//
//  Created by Arno Woestenburg on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBKeychainItemIdentity.h"

@implementation MBKeychainItemIdentity

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (id)classType
{
    return (id)kSecClassIdentity;
}


@end
