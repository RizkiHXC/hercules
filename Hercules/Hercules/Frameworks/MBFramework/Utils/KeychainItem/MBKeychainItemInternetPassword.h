//
//  MBKeychainItemInternetPassword.h
//  MBFramework
//
//  Created by Arno Woestenburg on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBKeychainItem.h"

/**
 The MBKeychainItemInternetPassword class provides a programmatic interface for interacting with the iOS Keychain system for Internet password items.
 */
@interface MBKeychainItemInternetPassword : MBKeychainItem

/**
 Set or get an account name.
 */
@property(nonatomic,retain) NSString *account;
/**
 Set or get the Internet security domain.
 */
@property(nonatomic,retain) NSString *securityDomain;
/**
 Set or get the server's domain name or IP address.
 */
@property(nonatomic,retain) NSString *server;
/**
 Set or get a value that denotes the protocol for this item (see the SecProtocolType enum in SecKeychainItem.h).
 */
@property(nonatomic,retain) NSNumber *protocol;
/**
 Set or get a value that represents an Internet port number.
 */
@property(nonatomic,retain) NSNumber *port;
/**
 Set or get the item's path attribute, typically this is the path component of the URL.
 */
@property(nonatomic,retain) NSString *path;

@end
