//
//  MBKeychainItemInternetPassword.m
//  MBFramework
//
//  Created by Arno Woestenburg on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBKeychainItemInternetPassword.h"

@implementation MBKeychainItemInternetPassword

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (id)classType
{
    return (id)kSecClassInternetPassword;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSString *)account
{
    return [self objectForKey:(id)kSecAttrAccount];
}

- (void)setAccount:(NSString *)account
{
    [self setObject:account forKey:(id)kSecAttrAccount];
}

- (NSString *)securityDomain
{
    return [self objectForKey:(id)kSecAttrSecurityDomain];
}

- (void)setSecurityDomain:(NSString *)securityDomain
{
    [self setObject:securityDomain forKey:(id)kSecAttrSecurityDomain];
}

- (NSString *)server
{
    return [self objectForKey:(id)kSecAttrServer];
}

- (void)setServer:(NSString *)server
{
    [self setObject:server forKey:(id)kSecAttrServer];
}

- (NSNumber *)protocol
{
    return [self objectForKey:(id)kSecAttrProtocol];
}

- (void)setProtocol:(NSNumber *)protocol
{
    [self setValue:protocol forKey:(id)kSecAttrProtocol];
}

- (NSNumber *)port
{
    return [self objectForKey:(id)kSecAttrPort];
}

- (void)setPort:(NSNumber *)port
{
    [self setValue:port forKey:(id)kSecAttrPort];
}

- (NSString *)path
{
    return [self objectForKey:(id)kSecAttrPath];
}

- (void)setPath:(NSString *)path
{
    [self setValue:path forKey:(id)kSecAttrPath];
}



@end
