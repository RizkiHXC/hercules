//
//  MBKeychainItemKey.h
//  MBFramework
//
//  Created by Arno Woestenburg on 22/08/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBKeychainItem.h"

/**
 The MBKeychainItemKey class provides a programmatic interface for interacting with the iOS Keychain system for key items.
 */
@interface MBKeychainItemKey : MBKeychainItem

@end
