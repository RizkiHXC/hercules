//
//  MBLayoutGuide.h
//  MBFramework
//
//  Created by Arno Woestenburg on 28/08/14.
//  Copyright (c) 2014 mediaBunker B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 This class can be implemented by the UIViewController properties topLayoutGuide and bottomLayoutGuide to support using Auto Layout with a view controller’s view, starting in iOS 7. You can use layout guides as layout items in the NSLayoutConstraint factory methods.
 */
@interface MBLayoutGuide : NSObject <UILayoutSupport>
{
    
}

/**
 Creates and initiliazes a new MBLayoutGuide instance.
 @param length The length, in points, of the portion of a view controller’s view that is overlaid by translucent or transparent UIKit bars.
 @return A newly created MBLayoutGuide object.
 */
+ (MBLayoutGuide *)layoutGuideWithLength:(CGFloat)length;

/**
 Creates and initiliazes a new MBLayoutGuide instance.
 @param length The length, in points, of the portion of a view controller’s view that is overlaid by translucent or transparent UIKit bars.
 @return A newly created MBLayoutGuide object.
 */
- (instancetype)initWithLenght:(CGFloat)length;

/**
 Provides the length, in points, of the portion of a view controller’s view that is overlaid by translucent or transparent UIKit bars.
 */
@property(nonatomic,assign) CGFloat length;

@end
