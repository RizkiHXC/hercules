//
//  MBLayoutGuide.m
//  MBFramework
//
//  Created by Arno Woestenburg on 28/08/14.
//  Copyright (c) 2014 mediaBunker B.V. All rights reserved.
//

#import "MBLayoutGuide.h"

@implementation MBLayoutGuide

@synthesize length = length_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (MBLayoutGuide *)layoutGuideWithLength:(CGFloat)length
{
    return [[[MBLayoutGuide alloc] initWithLenght:length] autorelease];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithLenght:(CGFloat)length
{
    self = [super init];
    if (self) {
        length_ = length;
    }
    return self;
}

@end
