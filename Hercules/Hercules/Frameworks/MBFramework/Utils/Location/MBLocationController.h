//
//  MBLocationController.h
//  MBFramework
//
//  Created by Arno Woestenburg on 08/05/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@class CLLocationManager;
@class CLLocation;
@class MBLocationController;

@protocol MBLocationControllerDelegate <CLLocationManagerDelegate>

@optional
- (void)locationController:(MBLocationController *)controller didUpdateLocation:(CLLocation *)location;

@end

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBLocationController : NSObject
{
    
}

- (BOOL)hasAccurateLocation;

@property(nonatomic,readonly) CLLocationManager *locationManager;
@property(nonatomic,retain,readonly) CLLocation *location;
@property(nonatomic,assign) NSTimeInterval maximumCacheLocationInterval;    // Maximum cache time in seconds
/**
 *  The object that acts as the delegate of the receiving input location controller.
 *
 *  @discussion The delegate must adopt the MBLocationControllerDelegate protocol. The delegate is not retained.
 */
@property(nonatomic,assign) id<MBLocationControllerDelegate> delegate;
@property(assign, nonatomic) BOOL stopLocationUpdatesAutomatically;         // Stop updating location when an accurate location is found

@end

