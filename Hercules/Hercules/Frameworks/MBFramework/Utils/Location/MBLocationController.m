//
//  MBLocationController.m
//  MBFramework
//
//  Created by Arno Woestenburg on 08/05/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//
 
#import "MBLocationController.h"
#import <CoreLocation/CoreLocation.h>

@interface MBLocationController () <CLLocationManagerDelegate>

@property(nonatomic,retain) CLLocation *location;

@end

@implementation MBLocationController

@synthesize locationManager = locationManager_;
@synthesize location = location_;
@synthesize delegate = delegate_;
@synthesize maximumCacheLocationInterval = maximumCacheLocationInterval_;
@synthesize stopLocationUpdatesAutomatically = stopLocationUpdatesAutomatically_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        locationManager_ = [[CLLocationManager alloc] init];
        [locationManager_ setDelegate:self];
        maximumCacheLocationInterval_ = 60.0; // Seconds
        stopLocationUpdatesAutomatically_ = YES;
    }
    return self;
}

- (void)dealloc
{
    [locationManager_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_updateLocation:(CLLocation *)location
{
    if ([delegate_ respondsToSelector:@selector(locationController:didUpdateLocation:)]) {
        [delegate_ locationController:self didUpdateLocation:location];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (BOOL)hasAccurateLocation
{
    CLLocationAccuracy locationAccuracy = location_.horizontalAccuracy;
    return (locationAccuracy > 0 && locationAccuracy <= self.locationManager.desiredAccuracy);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Method forwarding

- (BOOL)respondsToSelector:(SEL)aSelector
{
    // This class, or one of the delegates handle the selector.
    return ([super respondsToSelector:aSelector] || [delegate_ respondsToSelector:aSelector]);
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector
{
    NSMethodSignature *signature = [super methodSignatureForSelector:selector];
    if (!signature && [delegate_ respondsToSelector:selector]) {
        signature = [(NSObject *)delegate_ methodSignatureForSelector:selector];
    }
    return signature;
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    // Forward to delegates if implemented
    if ([delegate_ respondsToSelector:[invocation selector]]) {
        [invocation invokeWithTarget:delegate_];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setLocation:(CLLocation *)location
{
    [location_ release];
    location_ = [location retain];
    [self MB_updateLocation:location];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // If it's a relatively recent event, turn off updates to save power
    CLLocation *location = [locations lastObject];
    NSDate *eventDate = location.timestamp;
    
    if (location != nil && abs([eventDate timeIntervalSinceNow]) < maximumCacheLocationInterval_) {
        if (stopLocationUpdatesAutomatically_) {
            [manager stopUpdatingLocation];
        }
        // Last location update was shorter than X seconds ago.
        // Accept this value as current location and stop updating
        [self setLocation:location];
    }
}

 
@end
