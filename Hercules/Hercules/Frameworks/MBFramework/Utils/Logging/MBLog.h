//
//  MBLog.h
//  MBFramework
//
//  Created by Marco Jonker on 2/18/13.
//
//

#ifndef MBFramework_MBLog_h
#define MBFramework_MBLog_h

#ifdef DEBUG
    #define MBLogDebug(format, ...)  MBLog(format, ##__VA_ARGS__)
#else
    #define MBLogDebug(format, ...)  ((void)0)
#endif

#define MBLog(format, ...)  NSLog((@"[Line %d] %s " format), __LINE__,__PRETTY_FUNCTION__, ##__VA_ARGS__)

#endif
