//
//  MBDeviceCapabilities.h
//  MBFramework
//
//  Created by Arno Woestenburg on 12/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBDeviceCapabilities : NSObject

+ (BOOL)canPhoneCall;

@end
