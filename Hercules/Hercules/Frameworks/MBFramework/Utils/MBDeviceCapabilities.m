//
//  MBDeviceCapabilities.m
//  MBFramework
//
//  Created by Arno Woestenburg on 12/12/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBDeviceCapabilities.h"

@implementation MBDeviceCapabilities

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (BOOL)canPhoneCall
{
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]];
}

@end
