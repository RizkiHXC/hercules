//
//  MBGlobalDefines.h
//  MBFramework
//
//  Created by Marco Jonker on 5/29/13.
//  Copyright (c) 2013 Mediabunker. All rights reserved.
//

#ifndef MBFramework_MBGlobalDefines_h
#define MBFramework_MBGlobalDefines_h

#define MB_IOS_VERSION_EQUALHIGHER(_iosVersion) ([[[UIDevice currentDevice] systemVersion] floatValue] >= _iosVersion)
#define MB_IOS_VERSION_EQUAL(_iosVersion)       ([[[UIDevice currentDevice] systemVersion] floatValue] == _iosVersion)
#define MB_IOS_VERSION_LOWER(_iosVersion)       ([[[UIDevice currentDevice] systemVersion] floatValue] <  _iosVersion)
#define MB_IOS_VERSION_LOWEREQUAL(_iosVersion)  ([[[UIDevice currentDevice] systemVersion] floatValue] <= _iosVersion)

#endif
