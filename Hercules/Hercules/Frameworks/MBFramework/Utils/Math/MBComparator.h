//
//  MBComparator.h
//  MBFramework
//
//  Created by Arno Woestenburg on 22/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBComparator : NSObject

extern NSComparisonResult (^MBCompareInteger)(NSInteger, NSInteger);
extern NSComparisonResult (^MBCompareUnsignedInteger)(NSUInteger, NSUInteger);

@end
