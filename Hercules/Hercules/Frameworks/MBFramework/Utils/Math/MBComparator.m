//
//  MBComparator.m
//  MBFramework
//
//  Created by Arno Woestenburg on 22/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBComparator.h"

@implementation MBComparator

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

NSComparisonResult (^MBCompareInteger)(NSInteger, NSInteger) = ^(NSInteger value1, NSInteger value2)
{
    if (value1 > value2) {
        return NSOrderedDescending;
    } else if (value1 < value2) {
        return NSOrderedAscending;
    } else {
        return NSOrderedSame;
    }
};

NSComparisonResult (^MBCompareUnsignedInteger)(NSUInteger, NSUInteger) = ^(NSUInteger value1, NSUInteger value2)
{
    if (value1 > value2) {
        return NSOrderedDescending;
    } else if (value1 < value2) {
        return NSOrderedAscending;
    } else {
        return NSOrderedSame;
    }
};


@end
