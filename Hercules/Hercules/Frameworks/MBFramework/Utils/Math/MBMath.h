//
//  MBMath.h
//  MBFramework
//
//  Created by Arno Woestenburg on 01/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 These constants specify rounding behaviors.
 */
typedef NS_ENUM(NSUInteger, MBRoundingMode)
{
    /**
     Round towards positive infinity.
     */
    MBRoundingModeCeiling,
    /**
     Round towards negative infinity.
     */
    MBRoundingModeFloor,
    /**
     Round towards zero.
     */
    MBRoundingModeDown,
    /**
     Round away from zero.
     */
    MBRoundingModeUp,
    /**
     Round towards the nearest integer, or towards an even number if equidistant.
     */
    MBRoundingModeHalfEven,
    /**
     Round towards the nearest integer, or towards zero if equidistant.
     */
    MBRoundingModeHalfDown,
    /**
     Round towards the nearest integer, or away from zero if equidistant.
     */
    MBRoundingModeHalfUp
};

/**
 The Math class provides basic math functions.
 */
@interface MBMath : NSObject

/**
 Round value to closer unity
 @param value  value to round.
 @param closer rounds to nearest value of closer.
 @param mode   rounding mode.
 @see MBRoundingMode
 @return Rounded value
 */
+ (double)round:(double)value closer:(double)closer roundingMode:(MBRoundingMode)mode;
/**
 Round value to closer unity
 @param value  value to round.
 @param closer rounds to nearest value of closer.
 @param mode   rounding mode.
 @see MBRoundingMode
 @return Rounded value as integer
 */
+ (NSInteger)roundToInteger:(double)value closer:(double)closer roundingMode:(MBRoundingMode)mode;

@end
