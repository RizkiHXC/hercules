//
//  MBMath.m
//  MBFramework
//
//  Created by Arno Woestenburg on 01/07/14.
//  Copyright (c) 2014 Mediabunker. All rights reserved.
//

#import "MBMath.h"

@implementation MBMath

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (double)round:(double)value closer:(double)closer roundingMode:(MBRoundingMode)mode
{
    double result = 0.0;
    switch (mode) {
        case MBRoundingModeCeiling:
            // Round towards positive infinity.
            result = (ceil(value / closer) * closer);
            break;
        case MBRoundingModeFloor:
            // Round towards negative infinity.
            result = (floor(value / closer) * closer); // Untested
            break;
        case MBRoundingModeDown:
            // Round towards zero.
            
            break;
        case MBRoundingModeUp:
            // Round away from zero.
            
            break;
        case MBRoundingModeHalfEven:
            // Round towards the nearest integer, or towards an even number if equidistant.
            
            break;
        case MBRoundingModeHalfDown:
            // Round towards the nearest integer, or towards zero if equidistant.
            
            break;
        case MBRoundingModeHalfUp:
            // Round towards the nearest integer, or away from zero if equidistant.
            result = (round(value / closer) * closer);
            break;
        default:
            NSAssert(NO, @"Invalid case");
            break;
    }
    
    return result;
}

+ (NSInteger)roundToInteger:(double)value closer:(double)closer roundingMode:(MBRoundingMode)mode
{
    NSInteger result = (NSInteger)[MBMath round:value closer:closer roundingMode:mode];
    return result;
}

@end
