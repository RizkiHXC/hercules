//
//  MBRectFunctions.h
//  MBFramework
//
//  Created by Arno Woestenburg on 6/3/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBRectFunctions : NSObject

typedef NS_ENUM(NSInteger, MBAlignment) {
    MBAlignmentTop              = 1,
    MBAlignmentBottom           = 2,
    MBAlignmentLeft             = 4,
    MBAlignmentRight            = 8,
    MBAlignmentTopLeft          = (MBAlignmentTop | MBAlignmentLeft),
    MBAlignmentTopRight         = (MBAlignmentTop | MBAlignmentRight),
    MBAlignmentBottomLeft       = (MBAlignmentBottom | MBAlignmentLeft),
    MBAlignmentBottomRight      = (MBAlignmentBottom | MBAlignmentRight),
    MBAlignmentHorizontalCenter = (MBAlignmentLeft | MBAlignmentRight),
    MBAlignmentVerticalCenter   = (MBAlignmentTop | MBAlignmentBottom),
    MBAlignmentTopCenter        = (MBAlignmentTop | MBAlignmentHorizontalCenter),
    MBAlignmentBottomCenter     = (MBAlignmentBottom | MBAlignmentHorizontalCenter),
    MBAlignmentLeftCenter       = (MBAlignmentLeft | MBAlignmentVerticalCenter),
    MBAlignmentRightCenter      = (MBAlignmentRight | MBAlignmentVerticalCenter),
    MBAlignmentCenter           = (MBAlignmentHorizontalCenter | MBAlignmentVerticalCenter),
};

#define MBAlignmentMaskHorizontal   MBAlignmentHorizontalCenter
#define MBAlignmentMaskVertical     MBAlignmentVerticalCenter

typedef NS_ENUM(NSInteger, MBVerticalAlignment) {
    MBVerticalAlignmentTop      = MBAlignmentTop,
    MBVerticalAlignmentBottom   = MBAlignmentBottom,
    MBVerticalAlignmentCenter   = MBAlignmentVerticalCenter
};

typedef NS_ENUM(NSInteger, MBHorizontalAlignment) {
    MBHorizontalAlignmentLeft       = MBAlignmentLeft,
    MBHorizontalAlignmentRight      = MBAlignmentRight,
    MBHorizontalAlignmentCenter     = MBAlignmentHorizontalCenter,
    MBHorizontalStretch             = 16 //Move to MBAlignment
};

typedef NS_ENUM(NSInteger, MBFitMode) {
    MBFitModeScaleToFill        = 1,
    MBFitModeScaleAspectFit     = 2,
    MBFitModeScaleAspectFill    = 3,
};

+ (CGRect)alignRect:(CGRect)rect1 toRect:(CGRect)rect2 alignMode:(MBAlignment)mode;
+ (CGRect)alignRect:(CGRect)rect toPoint:(CGPoint)point alignMode:(MBAlignment)mode;
+ (CGRect)fitRect:(CGRect)rect1 insideRect:(CGRect)rect2 fitMode:(MBFitMode)mode;
+ (CGRect)insetRect:(CGRect)rect left:(CGFloat)left top:(CGFloat)top right:(CGFloat)right bottom:(CGFloat)bottom;

@end
