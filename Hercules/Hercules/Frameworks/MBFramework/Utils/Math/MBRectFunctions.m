//
//  MBRectFunctions.m
//  MBFramework
//
//  Created by Arno Woestenburg on 6/3/13.
//  Copyright (c) 2013 mediaBunker B.V. All rights reserved.
//

#import "MBRectFunctions.h"

#define MBHorizontalAlignmentMask   (MBAlignmentLeft | MBAlignmentRight)
#define MBVerticalAlignmentMask     (MBAlignmentTop | MBAlignmentBottom)

@implementation MBRectFunctions

+ (CGRect)alignRect:(CGRect)rect1 toRect:(CGRect)rect2 alignMode:(MBAlignment)mode {
    
    switch (mode & MBHorizontalAlignmentMask) {
        case MBAlignmentLeft:
            rect1.origin.x = rect2.origin.x;
            break;
        case MBAlignmentRight:
            rect1.origin.x = rect2.origin.x + rect2.size.width - rect1.size.width;
            break;
        case MBAlignmentHorizontalCenter:
            rect1.origin.x = rect2.origin.x + roundf((rect2.size.width - rect1.size.width) * 0.5);
            break;
        default:
            break;
    }
    
    switch (mode & MBVerticalAlignmentMask) {
        case MBAlignmentTop:
            rect1.origin.y = rect2.origin.y;
            break;
        case MBAlignmentBottom:
            rect1.origin.y = rect2.origin.y + rect2.size.height - rect1.size.height;
            break;
        case MBAlignmentVerticalCenter:
            rect1.origin.y = rect2.origin.y + roundf((rect2.size.height - rect1.size.height) * 0.5);
            break;
        default:
            break;
    }
    return rect1;
}

+ (CGRect)alignRect:(CGRect)rect toPoint:(CGPoint)point alignMode:(MBAlignment)mode {
    
    switch (mode & MBHorizontalAlignmentMask) {
        case MBAlignmentLeft:
            rect.origin.x = point.x;
            break;
        case MBAlignmentRight:
            rect.origin.x = point.x - rect.size.width;
            break;
        case MBAlignmentHorizontalCenter:
            rect.origin.x = point.x - roundf(rect.size.width * 0.5);
            break;
        default:
            break;
    }
    
    switch (mode & MBVerticalAlignmentMask) {
        case MBAlignmentTop:
            rect.origin.y = point.y;
            break;
        case MBAlignmentBottom:
            rect.origin.y = point.y - rect.size.height;
            break;
        case MBAlignmentVerticalCenter:
            rect.origin.y = point.y - roundf(rect.size.height * 0.5);
            break;
        default:
            break;
    }
    return rect;
}


+ (CGRect)fitRect:(CGRect)rect1 insideRect:(CGRect)rect2 fitMode:(MBFitMode)mode
{
    switch (mode) {
        case MBFitModeScaleToFill:
            rect1 = rect2;
            break;
        case MBFitModeScaleAspectFit:
        {
            CGFloat scale = 0.0f;
            if (rect1.size.width != 0.0f && rect1.size.height != 0.0f) {
                scale = MIN((rect2.size.width / rect1.size.width), (rect2.size.height / rect1.size.height));
            }
            rect1.size.width = roundf(rect1.size.width * scale);
            rect1.size.height = roundf(rect1.size.height * scale);
            rect1 = [MBRectFunctions alignRect:rect1 toRect:rect2 alignMode:MBAlignmentCenter];
            break;
        }
        case MBFitModeScaleAspectFill:
        {
            CGFloat scale = 0.0f;
            if (rect1.size.width != 0.0f && rect1.size.height != 0.0f) {
                scale = MAX((rect2.size.width / rect1.size.width), (rect2.size.height / rect1.size.height));
            }
            rect1.size.width = roundf(rect1.size.width * scale);
            rect1.size.height = roundf(rect1.size.height * scale);
            rect1 = [MBRectFunctions alignRect:rect1 toRect:rect2 alignMode:MBAlignmentCenter];
            break;
        }
        default:
            NSAssert(FALSE, @"Unhandled case");
            break;
    }
    return rect1;
}

+(CGRect)insetRect:(CGRect)rect left:(CGFloat)left top:(CGFloat)top right:(CGFloat)right bottom:(CGFloat)bottom {
    return UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(top, left, bottom, right));
}

@end
