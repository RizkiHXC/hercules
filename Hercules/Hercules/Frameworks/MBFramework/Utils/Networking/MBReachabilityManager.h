//
//  MBReachabilityManager.h
//  MBFramework
//
//  Created by Arno Woestenburg on 3/18/13.
//
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

#ifdef DEBUG
//#define SHOW_REACHABILITY_LOGS // Enable to show log for reachability (debug only)
#endif

/**
 *  Class not yet (fully) documented. Documentation for this class is in progress.
 */
@interface MBReachabilityManager : NSObject
{
    Reachability *internetReachable_;
    NSMutableDictionary *hostReachable_;
}

+ (instancetype)sharedInstance;

- (void)addObserver:(id)observer;
- (void)removeObserver:(id)observer;

- (NSDictionary *)hostReachable;
- (Reachability *)reachabilityWithHostName:(NSString *)hostname;
- (void)startNotifierForInternetConnection;
- (void)startNotifierForHostName:(NSString *)hostname;
- (void)stopNotifierForInternetConnection;
- (void)stopNotifierForHostName:(NSString *)hostname;
- (void)stopAllNotifiers;
- (NSArray *)reachabilityItems;

@property(nonatomic, readonly) Reachability *internetReachable;

@end
