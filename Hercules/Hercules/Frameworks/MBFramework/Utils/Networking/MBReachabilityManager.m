//
//  MBReachabilityManager.m
//  MBFramework
//
//  Created by Arno Woestenburg on 3/18/13.
//
//

#import "MBReachabilityManager.h"
#import "Reachability.h"

static id sharedInstance_ = nil;

@implementation MBReachabilityManager

@synthesize internetReachable = internetReachable_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

+ (instancetype)sharedInstance {
    if(!sharedInstance_) {
        sharedInstance_ = [[self alloc] init];
    }
    return sharedInstance_;
}

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

- (void)dealloc {
    [internetReachable_ release];
    [hostReachable_ release];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSDictionary *)hostReachable {
    return [NSDictionary dictionaryWithDictionary:hostReachable_];
}

- (Reachability *)reachabilityWithHostName:(NSString *)hostname {
    return [hostReachable_ objectForKey:hostname];
}

- (void)startNotifierForInternetConnection {
    [internetReachable_ release];
    internetReachable_ = [[Reachability reachabilityForInternetConnection] retain];
    [internetReachable_ startNotifier];
}

- (void)startNotifierForHostName:(NSString *)hostname {
    if (nil == hostReachable_) {
        hostReachable_ = [[NSMutableDictionary alloc] init];
    }
    Reachability *hostReachable = [Reachability reachabilityWithHostName:hostname];
    [hostReachable_ setObject:hostReachable forKey:hostname];
    [hostReachable startNotifier];
    //[hostReachable connectionRequired];
}

- (void)stopNotifierForInternetConnection {
    [internetReachable_ stopNotifier];
    [internetReachable_ release];
    internetReachable_ = nil;
}

- (void)stopNotifierForHostName:(NSString *)hostname {
    [hostReachable_ removeObjectForKey:hostname];
}

- (void)stopAllNotifiers {
    [self stopNotifierForInternetConnection];
    for (NSString *hostname in [hostReachable_ allKeys]) {
        [self stopNotifierForHostName:hostname];
    }
}

- (void)addObserver:(id)observer {
	[[NSNotificationCenter defaultCenter] addObserver:observer selector:NSSelectorFromString(@"reachabilityChanged:") name:kReachabilityChangedNotification object:nil];
}

- (void)removeObserver:(id)observer {
	[[NSNotificationCenter defaultCenter] removeObserver:observer name:kReachabilityChangedNotification object:nil];
}

- (NSArray *)reachabilityItems {
    NSMutableArray *items = [[[NSMutableArray alloc] init] autorelease];
    
    if (nil != internetReachable_) {
        [items addObject:internetReachable_];
    }
    
    [items addObjectsFromArray:[hostReachable_ allValues]];
    return items;
}

@end
