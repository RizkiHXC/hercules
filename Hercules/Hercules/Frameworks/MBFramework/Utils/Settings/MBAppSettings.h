//
//  MBAppSettings.h
//  MBFramework
//
//  Created by Arno Woestenburg on 9/26/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const MBAppSettingDidChangeNotification;

/**
 The MBAppSettings class provides a programmatic interface for interacting with the defaults system via the NSUserDefaults class. The defaults system allows an application to customize its behavior to match a user’s preferences.
 */
@interface MBAppSettings : NSObject

+ (instancetype)sharedInstance;

- (void)beginUpdates;
- (void)endUpdates;
- (void)updateUserDefaults;
- (void)notifyAppSettingsDidChangeForKey:(NSString *)defaultName;
- (void)updateAndNotifyForKey:(NSString *)defaultName;

- (id)objectForKey:(NSString *)defaultName;
- (NSString *)stringForKey:(NSString *)defaultName;
- (NSArray *)arrayForKey:(NSString *)defaultName;
- (NSDictionary *)dictionaryForKey:(NSString *)defaultName;
- (NSData *)dataForKey:(NSString *)defaultName;
- (NSArray *)stringArrayForKey:(NSString *)defaultName;
- (NSInteger)integerForKey:(NSString *)defaultName;
- (float)floatForKey:(NSString *)defaultName;
- (double)doubleForKey:(NSString *)defaultName;
- (BOOL)boolForKey:(NSString *)defaultName;
- (NSURL *)URLForKey:(NSString *)defaultName NS_AVAILABLE(10_6, 4_0);

- (void)setObject:(id)value forKey:(NSString *)defaultName;
- (void)setInteger:(NSInteger)value forKey:(NSString *)defaultName;
- (void)setFloat:(float)value forKey:(NSString *)defaultName;
- (void)setDouble:(double)value forKey:(NSString *)defaultName;
- (void)setBool:(BOOL)value forKey:(NSString *)defaultName;
- (void)setURL:(NSURL *)url forKey:(NSString *)defaultName NS_AVAILABLE(10_6, 4_0);

@property(nonatomic,readonly) NSUserDefaults *userDefaults;

@end
