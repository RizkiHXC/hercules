//
//  MBAppSettings.m
//  MBFramework
//
//  Created by Arno Woestenburg on 9/26/13.
//  Copyright (c) 2013 mediaBunker. All rights reserved.
//

#import "MBAppSettings.h"

static id sharedInstance_ = nil;

NSString *const MBAppSettingDidChangeNotification  = @"MBAppSettingDidChangeNotification";

@interface MBAppSettings ()
{
    BOOL updating_;
}

@end

@implementation MBAppSettings

@synthesize userDefaults = userDefaults_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (instancetype)sharedInstance
{
    if (!sharedInstance_) {
        sharedInstance_ = [[self alloc] init];
    }
    return sharedInstance_;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        userDefaults_   = [NSUserDefaults standardUserDefaults];
        updating_       = NO;
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)beginUpdates
{
    // Begin a series of method calls that insert, update or delete items of the receiver.
    updating_ = YES;
}

- (void)endUpdates
{
    // Conclude a series of method calls that insert, update or delete items of the receiver.
    if (updating_) {
        updating_ = NO;
        [userDefaults_ synchronize];
    }
}

- (void)updateUserDefaults
{
    if (!updating_) {
        [userDefaults_ synchronize];
    }
}

//- (id)valueForKey:(NSString *)key
//{
//    return [userDefaults_ objectForKey:key];
//}

- (void)notifyAppSettingsDidChangeForKey:(NSString *)defaultName
{
    [[NSNotificationCenter defaultCenter] postNotificationName:MBAppSettingDidChangeNotification object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:defaultName, @"key", nil]];
}

- (void)updateAndNotifyForKey:(NSString *)defaultName
{
    [self updateUserDefaults];
    [self notifyAppSettingsDidChangeForKey:defaultName];
}

- (id)objectForKey:(NSString *)defaultName
{
    return [userDefaults_ objectForKey:defaultName];
}

- (NSString *)stringForKey:(NSString *)defaultName
{
    return [userDefaults_ stringForKey:defaultName];
}

- (NSArray *)arrayForKey:(NSString *)defaultName
{
    return [userDefaults_ arrayForKey:defaultName];
}

- (NSDictionary *)dictionaryForKey:(NSString *)defaultName
{
    return [userDefaults_ dictionaryForKey:defaultName];
}

- (NSData *)dataForKey:(NSString *)defaultName
{
    return [userDefaults_ dataForKey:defaultName];
}

- (NSArray *)stringArrayForKey:(NSString *)defaultName
{
    return [userDefaults_ stringArrayForKey:defaultName];
}

- (NSInteger)integerForKey:(NSString *)defaultName
{
    return [userDefaults_ integerForKey:defaultName];
}

- (float)floatForKey:(NSString *)defaultName
{
    return [userDefaults_ floatForKey:defaultName];
}

- (double)doubleForKey:(NSString *)defaultName
{
    return [userDefaults_ doubleForKey:defaultName];
}

- (BOOL)boolForKey:(NSString *)defaultName
{
    return [userDefaults_ boolForKey:defaultName];
}

- (NSURL *)URLForKey:(NSString *)defaultName
{
    return [userDefaults_ URLForKey:defaultName];
}

- (void)setObject:(id)value forKey:(NSString *)defaultName
{
    [userDefaults_ setObject:value forKey:defaultName];
    [self updateUserDefaults];
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)defaultName
{
    [userDefaults_ setInteger:value forKey:defaultName];
    [self updateUserDefaults];
}

- (void)setFloat:(float)value forKey:(NSString *)defaultName
{
    [userDefaults_ setFloat:value forKey:defaultName];
    [self updateUserDefaults];
}

- (void)setDouble:(double)value forKey:(NSString *)defaultName
{
    [userDefaults_ setDouble:value forKey:defaultName];
    [self updateUserDefaults];
}

- (void)setBool:(BOOL)value forKey:(NSString *)defaultName
{
    [userDefaults_ setBool:value forKey:defaultName];
    [self updateUserDefaults];
}

- (void)setURL:(NSURL *)url forKey:(NSString *)defaultName
{
    [userDefaults_ setURL:url forKey:defaultName];
    [self updateUserDefaults];
}



@end
