//
//  MBStopwatchTimer.h
//  MBFramework
//
//  Created by Arno Woestenburg on 03/10/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Kinds of events possible for stopwatch objects.
 */
typedef NS_ENUM(NSUInteger, MBStopWatchEvent)
{
    /**
     Update time interval for the time measurement has elapsed.
     */
    MBStopWatchEventTimer,
};

/**
 The stopwatch class is used to measure the amount of time elapsed from a particular time when it is activated to the time when it is is deactivated.
 */
@interface MBStopwatchTimer : NSObject

/**
 Starts the timer. 
 @discussion New elapsed time is added to the previous elapsed time before the stop watch class was stopped. To start at zero, call reset first.
 */
- (void)start;
/**
 Stops the timer.
 */
- (void)stop;
/**
 Resets the elapsed time.
 */
- (void)reset;

/**
 Sets a target and action for a particular event (or events) to an internal dispatch table.
 @param target   The target object—that is, the object to which the action message is sent.
 @param selector A selector identifying an action message. It cannot be NULL.
 @param event    A bitmask specifying the stopwatch events for which the action message is sent.
 */
- (void)setTarget:(id)target selector:(SEL)selector forEvent:(MBStopWatchEvent)event;

/**
 Used to specify the total elapsed time interval, in seconds. (read-only)
 @discussion NSTimeInterval is always specified in seconds; it yields sub-millisecond precision over a range of 10,000 years.
 */
@property(nonatomic,readonly) NSTimeInterval elapsedTimeInterval;

/**
 Used to specify update time interval for the time measurement. Default is 100 milliseconds.
*/
@property(nonatomic,assign) NSTimeInterval updateTimeInterval;
/**
 A Boolean value indicating whether the stopwatch timer is running.
 */
@property(nonatomic,readonly,getter=isRunning) BOOL running;

@end
