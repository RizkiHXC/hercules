//
//  MBStopwatchTimer.m
//  MBFramework
//
//  Created by Arno Woestenburg on 03/10/14.
//  Copyright (c) 2014 mediaBunker. All rights reserved.
//

#import "MBStopwatchTimer.h"
#import <QuartzCore/QuartzCore.h>
#import <time.h>
#import <errno.h>
#import <sys/sysctl.h>

@interface MBStopwatchTimer ()
{
    NSTimer *runTimer_;
    NSTimeInterval lastTimeInterval_;
    NSTimeInterval startTimeInterval_;
    NSMapTable *eventDispatchTable_;
}
@end

@implementation MBStopwatchTimer

@synthesize updateTimeInterval = updateTimeInterval_;
@synthesize elapsedTimeInterval = elapsedTimeInterval_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        updateTimeInterval_ = 0.1;
        eventDispatchTable_ = [[NSMapTable alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [eventDispatchTable_ release];
    [self MB_stopRunTimer];
    [super dealloc];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_startRunTimer
{
    NSAssert(runTimer_ == nil, @"Time is already running");
    runTimer_ = [[NSTimer scheduledTimerWithTimeInterval:updateTimeInterval_ target:self selector:@selector(MB_timerIntervalElapsed:) userInfo:nil repeats:YES] retain];
}

- (void)MB_stopRunTimer
{
    [runTimer_ invalidate];
    [runTimer_ release];
    runTimer_ = nil;
}

- (void)MB_timerIntervalElapsed:(NSTimer *)sender
{
    [self MB_updateElapsedTimeInterval];
    [self MB_sendActionsForControlEvent:MBStopWatchEventTimer];
}

- (void)MB_sendActionsForControlEvent:(MBStopWatchEvent)event
{
    NSInvocation *invocation = [eventDispatchTable_ objectForKey:@(event)];
    if (invocation) {
        [invocation invoke];
    }
}

- (time_t)MB_systemUptime
{
    // A measure of the time the system has been working and available.
    struct timeval boottime;
    int mib[2] = {CTL_KERN, KERN_BOOTTIME};
    size_t size = sizeof(boottime);
    time_t now;
    time_t uptime = -1;
    
    (void)time(&now);
    
    if (sysctl(mib, 2, &boottime, &size, NULL, 0) != -1 && boottime.tv_sec != 0) {
        uptime = now - boottime.tv_sec;
    }
    return uptime;
}

- (void)MB_updateElapsedTimeInterval
{
    // Time interval since last start time, and previous measured time interval.
    NSTimeInterval currentTime = (NSTimeInterval)[self MB_systemUptime];
    elapsedTimeInterval_ = (NSTimeInterval)((currentTime - startTimeInterval_) + lastTimeInterval_);
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)start
{
    startTimeInterval_ = (NSTimeInterval)[self MB_systemUptime];
    [self MB_startRunTimer];
}

- (void)stop
{
    // Save elapsed time.
    lastTimeInterval_ = [self elapsedTimeInterval];
    [self MB_stopRunTimer];
}

- (void)reset
{
    // Reset elapsed time and start.
    lastTimeInterval_ = 0.0;
    startTimeInterval_ = 0.0;
    elapsedTimeInterval_ = 0.0;
    
    // If the timer is currently running (not nil), trigger a restart
    if (runTimer_) {
        // Restart.
        [self MB_stopRunTimer];
        [self start];
    }
}

- (void)setTarget:(id)target selector:(SEL)selector forEvent:(MBStopWatchEvent)event
{
    NSMethodSignature* signature = [target methodSignatureForSelector:selector];
    NSInvocation* invocation = [NSInvocation invocationWithMethodSignature:signature];
    [invocation setTarget:target];
    [invocation setSelector:selector];
    if (signature.numberOfArguments > 2) {
        [invocation setArgument:&self atIndex:2];
    }
    [eventDispatchTable_ setObject:invocation forKey:@(event)];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (BOOL)isRunning
{
    // If the timer exists this means that the timer is currently running.
    return (runTimer_ != nil);
}

@end
