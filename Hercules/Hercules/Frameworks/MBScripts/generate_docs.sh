#appledoc Xcode script
# Start constants
company="mediaBunker";
companyID="com.mediabunker";
companyURL="http://mediabunker.com";
target="iphoneos";
#target="macosx";
outputPath="${PROJECT_DIR}/Documentation/Help/docs/";
# End constants
"${PROJECT_DIR}/MBScripts/appledoc" \
--project-name "${PROJECT_NAME}" \
--project-company "${company}" \
--company-id "${companyID}" \
--docset-atom-filename "${company}.atom" \
--docset-feed-url "${companyURL}/${company}/%DOCSETATOMFILENAME" \
--docset-package-url "${companyURL}/${company}/%DOCSETPACKAGEFILENAME" \
--docset-fallback-url "${companyURL}/${company}" \
--output "${outputPath}" \
--publish-docset \
--docset-platform-family "${target}" \
--logformat xcode \
--keep-intermediate-files \
--no-repeat-first-par \
--no-warn-invalid-crossref \
--ignore build \
--ignore publish \
--ignore Frameworks \
--ignore .m \
--exit-threshold 2 \
"${PROJECT_DIR}"