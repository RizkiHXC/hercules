#!/bin/sh

#  mark_todo.sh
#  WMF
#
#  Created by Arno Woestenburg on 2/4/13.
#  Copyright (c) 2013 mediaBunker. All rights reserved.

KEYWORDS="TODO:|TODO |FIXME:|FIXME |\?\?\?:|\!\!\!:"
find "${SRCROOT}" \( -name "*.h" -or -name "*.m" \) -print0 | xargs -0 egrep --with-filename --line-number --only-matching "($KEYWORDS).*\$" | perl -p -e "s/($KEYWORDS)/ warning: \$1/"