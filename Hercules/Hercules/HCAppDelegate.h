//
//  HCAppDelegate.h
//  Hercules
//
//  Created by Rizki Calame on 13-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCMainViewController.h"

@interface HCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) HCMainViewController *mainViewController;


@end

