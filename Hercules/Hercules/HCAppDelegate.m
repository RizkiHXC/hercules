//
//  HCAppDelegate.m
//  Hercules
//
//  Created by Rizki Calame on 13-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCAppDelegate.h"
#import "Hercules.h"
#import "HCLocationManager.h"

@interface HCAppDelegate ()

@end

@implementation HCAppDelegate

@synthesize mainViewController = mainViewController_;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:@"AIzaSyA4C3FodCo3unAcTA1igfVN1Cz9-6Cn-a0"];
    
    // Apply global styles
    [HCStyle applyGlobalStyles];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    [self.window setBackgroundColor:[UIColor whiteColor]];
    
    // Main view controller
    HCMainViewController *mainViewController = [[[HCMainViewController alloc] init] autorelease];
    [self.window setRootViewController:mainViewController];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
