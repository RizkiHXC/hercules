//
//  UIBarButtonItem+Style.h
//  Hercules
//
//  Created by Rizki Calame on 22/04/15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Style)

+ (UIBarButtonItem *)hcMenuBarButtonItemWithTarget:(id)target action:(SEL)action;

@end
