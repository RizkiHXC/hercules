//
//  UIBarButtonItem+Style.m
//  Hercules
//
//  Created by Rizki Calame on 22/04/15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "UIBarButtonItem+Style.h"

@implementation UIBarButtonItem (Style)

+ (UIBarButtonItem *)hcMenuBarButtonItemWithTarget:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *normalImage = [UIImage imageNamed:@"cntrl_hamburger_menu"];
    UIImage *highlightImage = [UIImage imageNamed:@"cntrl_hamburger_menu"];
    if ([UIImage instancesRespondToSelector:@selector(imageWithRenderingMode:)]) {
        normalImage = [normalImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        highlightImage = [highlightImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    [button setImage:normalImage forState:UIControlStateNormal];
    [button setImage:highlightImage forState:UIControlStateHighlighted];
    
    [button sizeToFit];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
}

@end
