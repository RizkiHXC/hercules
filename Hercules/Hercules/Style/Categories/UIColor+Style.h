//
//  UIColor+Style.h
//  Hercules
//
//  Created by Rizki Calame on 26-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Style)

@end
