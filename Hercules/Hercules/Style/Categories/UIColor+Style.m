//
//  UIColor+Style.m
//  Hercules
//
//  Created by Rizki Calame on 26-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "UIColor+Style.h"

@implementation UIColor (Style)

+ (UIColor *)hcOctopusBlue
{
    return [UIColor colorWithRed:(10/255) green:(91/255) blue:(127/255) alpha:1];
}

+ (UIColor *)hcStarFishRed
{
    return [UIColor colorWithRed:(10/255) green:(91/255) blue:(127/255) alpha:1];
}

+ (UIColor *)hcOctopusBlue
{
    return [UIColor colorWithRed:(10/255) green:(91/255) blue:(127/255) alpha:1];
}

+ (UIColor *)hcOctopusBlue
{
    return [UIColor colorWithRed:(10/255) green:(91/255) blue:(127/255) alpha:1];
}

@end
