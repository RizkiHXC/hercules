//
//  HCStyle.h
//  Hercules
//
//  Created by Rizki Calame on 15-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIColor+Style.h"
#import "UIImage+MBFramework.h"
#import "UIBarButtonItem+Style.h"
#import "UIFont+Style.h"


@interface HCStyle : NSObject

+ (void)applyGlobalStyles;

+ (CGFloat)buttonHeight;
+ (CGFloat)marginBottom;
+ (CGFloat)marginLeft;
+ (CGFloat)marginRight;
+ (CGFloat)marginTop;
+ (CGFloat)spacingHorizontal;
+ (CGFloat)spacingHorizontalSmall;
+ (CGFloat)spacingVertical;
+ (CGFloat)spacingVerticalSmall;
+ (CGFloat)textFieldHeight;
+ (CGFloat)cornerRadius;

+ (CGSize)profileImageSize;
+ (CGSize)smallButtonSize;

+ (UIEdgeInsets)contentEdgeInsets;
+ (CGRect)contentRectForBounds:(CGRect)bounds;

@end
