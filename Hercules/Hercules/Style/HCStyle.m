//
//  HCStyle.m
//  Hercules
//
//  Created by Rizki Calame on 15-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCStyle.h"

@implementation HCStyle

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (void)applyGlobalStyles
{
    // UINavigationBar appearance
    id navigationBarAppearance = [UINavigationBar appearance];
    UIImage *barImage = [UIImage imageWithColor:[UIColor hcDeepSeaBlue] size:CGSizeMake(1, 1)];
    [navigationBarAppearance setBackgroundImage:barImage forBarMetrics:UIBarMetricsDefault];
    [navigationBarAppearance setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor hcWhite]}];
    [[UISearchBar appearance] setBarTintColor:[UIColor hcDeepSeaBlue]];
    
    // Remove black bottom border UINavigationBar
    [navigationBarAppearance setShadowImage:[[[UIImage alloc] init] autorelease]];
    
    // Status bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
//
//    // UIBarButtonItem appearance
//    id barButtonAppearance = [UIBarButtonItem appearance];
//    
//    NSDictionary *textAttributes = @{ NSFontAttributeName:[UIFont apItalicFontOfSize:[UIFont apNormalFontSize]], NSForegroundColorAttributeName:[UIColor apWhiteColor] };
//    [barButtonAppearance setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
//
//
//    // UISwitch appearance
//    id switchAppearance = [UISwitch appearance];
//    [switchAppearance setOnTintColor:[UIColor apCaribbeanGreenColor]];
//    
//    // MBPopoverView style
//    id popoverViewAppearance = [MBPopoverView appearance];
//    //[popoverViewAppearance setPopoverViewStyle:MBPopoverViewStyleWhiteTranslucent];
//    [popoverViewAppearance setMessageTextAttributes:@{NSFontAttributeName:[UIFont apRegularFontOfSize:[UIFont apNormalFontSize]]}];
//    
//    // TableView
//    id tableViewAppearance = [UITableView appearance];
//    [tableViewAppearance setSectionIndexColor:[UIColor apCaribbeanGreenColor]];
//    
//    // UIActivityIdicatorView
//    id activityIdicatorAppearance = [UIActivityIndicatorView appearance];
//    [activityIdicatorAppearance setColor:[UIColor apSilverChaliceColor]];
}

+ (CGFloat)buttonHeight           { return 50.0f; }
+ (CGFloat)marginBottom           { return 20.0f; }
+ (CGFloat)marginLeft             { return 14.0f; }
+ (CGFloat)marginRight            { return 14.0f; }
+ (CGFloat)marginTop              { return 20.0f; }
+ (CGFloat)spacingHorizontal      { return 15.0f; }
+ (CGFloat)spacingHorizontalSmall { return  5.0f; }
+ (CGFloat)spacingVertical        { return 15.0f; }
+ (CGFloat)spacingVerticalSmall   { return  5.0f; }
+ (CGFloat)textFieldHeight        { return 55.0f; }
+ (CGFloat)cornerRadius           { return  2.5f; }

+ (CGSize)profileImageSize        { return CGSizeMake(130.0f, 130.0f); }
+ (CGSize)smallButtonSize         { return CGSizeMake(83.0f, 27.0f); }

+ (UIEdgeInsets)contentEdgeInsets
{
    return UIEdgeInsetsMake([HCStyle marginTop], [HCStyle marginLeft], [HCStyle marginBottom], [HCStyle marginRight]);
}

+ (CGRect)contentRectForBounds:(CGRect)bounds
{
    return UIEdgeInsetsInsetRect(bounds, [HCStyle contentEdgeInsets]);
}

@end
