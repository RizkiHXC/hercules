//
//  HCLocationManager.h
//  Hercules
//
//  Created by Rizki Calame on 22/04/15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hercules.h"

@interface HCLocationManager : MBLocationController

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation *currentLocation;
@property (nonatomic, assign) MKCoordinateRegion currentRegion;

+ (instancetype)sharedManager;
- (void) startLocationManagerForOwnLocation;
- (void) stopLocationManagerForOwnLocation;

@end
