//
//  HCLocationManager.m
//  Hercules
//
//  Created by Rizki Calame on 22/04/15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCLocationManager.h"
#import <CoreLocation/CoreLocation.h>

static id sharedManager_ = nil;

@interface HCLocationManager () <CLLocationManagerDelegate>
{
    
}

@end

@implementation HCLocationManager

@synthesize locationManager = locationManager_;
@synthesize currentLocation = currentLocation_;
@synthesize delegate = delegate_;
@synthesize currentRegion = currentRegion_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

+ (instancetype)sharedManager
{
    if(!sharedManager_) {
        sharedManager_ = [[self alloc] init];
    }
    return sharedManager_;
}

- (id)init
{
    self = [super init];
    if (self){
        
        currentLocation_ = [[CLLocation alloc] init];
        locationManager_ = [[CLLocationManager alloc] init];
        
        [locationManager_ setDelegate:self];
        [locationManager_ setActivityType:CLActivityTypeFitness];
        [locationManager_ setDesiredAccuracy:kCLLocationAccuracyBest];
        [self setStopLocationUpdatesAutomatically:NO];
        
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self.locationManager requestAlwaysAuthorization];
        }
    }
    
    return self;
}

- (void)dealloc
{
    [locationManager_ setDelegate:nil];
    [locationManager_ release];
    [currentLocation_ release];
    [super dealloc];
}
//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void) stopLocationManagerForOwnLocation {
    [locationManager_ stopUpdatingLocation];
}

- (void) startLocationManagerForOwnLocation {
    [locationManager_ startUpdatingLocation];
}

- (void)notifyDidUpdateOwnLocation:(CLLocation*)ownLocation {
    if ([delegate_ respondsToSelector:@selector(locationController:didUpdateLocation:)]){
        [delegate_ performSelector:@selector(locationController:didUpdateLocation:) withObject:self withObject:ownLocation];
    }
}


- (CLLocation * )currentLocation
{
    CLLocation *newLocation = [[[CLLocation alloc] initWithLatitude:self.locationManager.location.coordinate.latitude longitude:self.locationManager.location.coordinate.longitude] autorelease];
    currentLocation_ = [newLocation retain];
    
    return currentLocation_;
}
/////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = [locations lastObject];
    currentLocation_ = newLocation;
    
    [self notifyDidUpdateOwnLocation:currentLocation_];
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusDenied) {
        //TODO : Check if we need to do something like this
    }
    else if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self startLocationManagerForOwnLocation];
    }
    
}


@end
