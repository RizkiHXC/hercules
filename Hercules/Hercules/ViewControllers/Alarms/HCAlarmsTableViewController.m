//
//  HCAlarmsTableViewController.m
//  Hercules
//
//  Created by Rizki Calame on 18-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCAlarmsTableViewController.h"
#import "Hercules.h"

@implementation HCAlarmsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Add right bar button item
    UIBarButtonItem *plusBarButton = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"cntrl_add_plus"] highlightedImage:[UIImage imageNamed:@"cntrl_add_plus"] target:self action:@selector(RC_rightBarButtonTouched:)];
    [self.navigationItem setRightBarButtonItem:plusBarButton];
}

- (void)RC_rightBarButtonTouched:(id)sender
{
    
}

@end
