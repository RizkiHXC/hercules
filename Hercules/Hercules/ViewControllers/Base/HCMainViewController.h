//
//  HCMainViewController.h
//  Hercules
//
//  Created by Rizki Calame on 14-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "MBSideMenuController.h"

@class HCNavigationController;

@interface HCMainViewController : MBSideMenuController

@property (nonatomic, retain) HCNavigationController *mapViewNavigationController;
@property (nonatomic, retain) HCNavigationController *alarmsNavigationController;
@property (nonatomic, retain) HCNavigationController *friendsNavigationController;
@property (nonatomic, retain) HCNavigationController *profileNavigationController;

@end
