//
//  HCMainViewController.m
//  Hercules
//
//  Created by Rizki Calame on 14-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCMainViewController.h"
#import "Hercules.h"
#import "HCBackgroundView.h"
#import "HCMapViewController.h"
#import "HCAlarmsTableViewController.h"
#import "HCFriendsTableViewController.h"
#import "HCProfileTableViewController.h"
#import "HCMenuTableViewCell.h"
#import "UIBarButtonItem+Style.h"

@interface HCMainViewController ()
{
    MBActivityIndicatorPopoverView *activityIndicatorPopoverView_;
}

@end

@implementation HCMainViewController

@synthesize mapViewNavigationController = mapViewNavigationController_;
@synthesize alarmsNavigationController = alarmsNavigationController_;
@synthesize friendsNavigationController = friendsNavigationController_;
@synthesize profileNavigationController = profileNavigationController_;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initializeViewControllers];
    [self RC_initializeStyle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)RC_initializeHeaderView
{
//    profileHeaderView_ = [[[APProfileHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.width, 75.0f)] autorelease];
//    [profileHeaderView_ sizeToFit];
//    [self.tableView setTableHeaderView:profileHeaderView_];
}

- (void)RC_initializeStyle
{
    [self setMenuParallaxValue:0.0f];
//    [self.tableView setBackgroundColor:[UIColor darkGrayColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundView:[[[HCBackgroundView alloc] initWithFrame:self.tableView.bounds] autorelease]];
}

- (void)showActivityIndicatorPopoverWithMessage:(NSString *)message
{
    if (!activityIndicatorPopoverView_) {
        MBImagesActivityIndicatorView *activityIndicator = [[[MBImagesActivityIndicatorView alloc] init] autorelease];
        activityIndicatorPopoverView_ = [[MBActivityIndicatorPopoverView alloc] initWithIndicator:activityIndicator andMessage:message];
        [activityIndicatorPopoverView_ setContentEdgeInsets:UIEdgeInsetsMake(25.0f, 40.0f, 25.0f, 40.0f)];
        [activityIndicatorPopoverView_ showOnView:self.view];
    } else {
        [activityIndicatorPopoverView_ setMessage:message];
    }
}

- (void)dismissActivityIndicatorPopoverAnimated:(BOOL)animated
{
    [activityIndicatorPopoverView_ dismissAnimated:animated];
    [activityIndicatorPopoverView_ release];
    activityIndicatorPopoverView_ = nil;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)initializeViewControllers
{
    NSMutableArray *viewControllers = [NSMutableArray array];
    
    // Map View
    HCMapViewController *mapViewController = [[[HCMapViewController alloc] init] autorelease];
    [mapViewController setTitle:@"Map"];
    mapViewNavigationController_ = [[HCNavigationController alloc] initWithRootViewController:mapViewController];
    [viewControllers addObject:mapViewNavigationController_];
    
    // Alarms
    HCAlarmsTableViewController *alarmsTableViewController = [[[HCAlarmsTableViewController alloc] init] autorelease];
    [alarmsTableViewController setTitle:@"Alarms"];
    alarmsNavigationController_ = [[HCNavigationController alloc] initWithRootViewController:alarmsTableViewController];
    [viewControllers addObject:alarmsNavigationController_];
    
    // Friends
    HCFriendsTableViewController *friendsTableViewController = [[[HCFriendsTableViewController alloc] init] autorelease];
    [friendsTableViewController setTitle:@"Friends"];
    friendsNavigationController_ = [[HCNavigationController alloc] initWithRootViewController:friendsTableViewController];
    [viewControllers addObject:friendsNavigationController_];
    
    // Profile
    HCProfileTableViewController *profileTableViewController = [[[HCProfileTableViewController alloc] init] autorelease];
    [profileTableViewController setTitle:@"Profile"];
    profileNavigationController_ = [[HCNavigationController alloc] initWithRootViewController:profileTableViewController];
    [viewControllers addObject:profileNavigationController_];
    
    // Dummy
    HCViewController *logOutViewController = [[[HCViewController alloc] init] autorelease];
    [logOutViewController setTitle:@"Log out"];
    [viewControllers addObject:logOutViewController];
    
    [self setViewControllers:viewControllers forSection:0];
    
    // Add menu buttons to all root view controllers.
    for (UIViewController *viewController in viewControllers) {
        if ([viewController isKindOfClass:[UINavigationController class]]) {
           UIViewController *rootViewController = [(UINavigationController *)viewController rootViewController];
            UIBarButtonItem *homeBarButton = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"cntrl_hamburger_menu"] highlightedImage:[UIImage imageNamed:@"cntrl_hamburger_menu"] target:self action:@selector(RC_menuBarButtonTouched:)];
            [rootViewController.navigationItem setLeftBarButtonItem:homeBarButton];
        }
    }
    // Menu width
//    CGRect menuButtonRect = [mapViewController.navigationItem.leftBarButtonItem frameInView:mapViewNavigationController_.view];
    [self setMenuWidth:mapViewController.view.size.width - [HCStyle spacingHorizontal] * 5];
    
    // Default starting ViewController is mapView
    [self setSelectedViewController:mapViewNavigationController_];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIResponder

- (void)RC_menuBarButtonTouched:(UIBarButtonItem *)sender
{
    [self revealMenuAnimated:YES];
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewCell

- (UITableViewCell *)tableView:(UITableView *)tableView menuCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HCMenuTableViewCell *menuCell = [[HCMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    UIViewController *viewController = [self viewControllerForItemAtIndexPath:indexPath];
    [menuCell.imageView setImage:viewController.tabBarItem.image];
    [menuCell.imageView setHighlightedImage:viewController.tabBarItem.selectedImage];
    
    return menuCell;
}

@end
