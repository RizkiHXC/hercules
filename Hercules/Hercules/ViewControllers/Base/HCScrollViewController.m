//
//  HCScrollViewController.m
//  Hercules
//
//  Created by Rizki Calame on 15-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCScrollViewController.h"

@implementation HCScrollViewController

@synthesize scrollView = scrollView_;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    scrollView_ = [[[UIScrollView alloc] initWithFrame:self.view.bounds] autorelease];
    [scrollView_ setDelegate:self];
    [self.view addSubview:scrollView_];
}

@end
