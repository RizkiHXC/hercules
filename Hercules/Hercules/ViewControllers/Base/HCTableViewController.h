//
//  HCTableViewController.h
//  Hercules
//
//  Created by Rizki Calame on 15-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCViewController.h"

@interface HCTableViewController : HCViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) UITableView *tableView;

@end
