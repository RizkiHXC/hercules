//
//  HCTableViewController.m
//  Hercules
//
//  Created by Rizki Calame on 15-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCTableViewController.h"

@implementation HCTableViewController

@synthesize tableView = tableView_;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // TableView
    tableView_ = [[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain] autorelease];
    [tableView_ setDelegate:self];
    [tableView_ setDataSource:self];
    [self.view addSubview:tableView_];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UITableViewControllerDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Override
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UITableViewControllerDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Override
    UITableViewCell *cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"] autorelease];
    
    return cell;
}

@end
