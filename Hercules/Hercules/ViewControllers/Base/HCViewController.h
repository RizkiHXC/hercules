//
//  ViewController.h
//  Hercules
//
//  Created by Rizki Calame on 13-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HCViewController : UIViewController

- (void)showActivityIndicatorPopoverWithMessage:(NSString *)message;
- (void)dismissActivityIndicatorPopoverAnimated:(BOOL)animated;

@end

