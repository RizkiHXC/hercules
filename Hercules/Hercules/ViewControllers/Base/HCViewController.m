//
//  ViewController.m
//  Hercules
//
//  Created by Rizki Calame on 13-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCViewController.h"
#import "Hercules.h"

@interface HCViewController ()
{
    MBActivityIndicatorPopoverView *activityIndicatorPopoverView_;
}

@end

@implementation HCViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Resets origin.y when navigationBar is translucent
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showActivityIndicatorPopoverWithMessage:(NSString *)message
{
    if (!activityIndicatorPopoverView_) {
        activityIndicatorPopoverView_ = [[MBActivityIndicatorPopoverView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite andMessage:message];
        [activityIndicatorPopoverView_ setContentEdgeInsets:UIEdgeInsetsMake(25.0f, 40.0f, 25.0f, 40.0f)];
        [activityIndicatorPopoverView_ showOnView:nil];
    } else {
        [activityIndicatorPopoverView_ setMessage:message];
    }
}

- (void)dismissActivityIndicatorPopoverAnimated:(BOOL)animated
{
    [activityIndicatorPopoverView_ dismissAnimated:animated];
    [activityIndicatorPopoverView_ release];
    activityIndicatorPopoverView_ = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController properties

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
