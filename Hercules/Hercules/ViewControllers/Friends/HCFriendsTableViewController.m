//
//  HCFriendsTableViewController.m
//  Hercules
//
//  Created by Rizki Calame on 18-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCFriendsTableViewController.h"
#import "Hercules.h"
#import "HCFriendsTableViewCell.h"

@interface HCFriendsTableViewController ()
{
    NSMutableArray *currentFriendsArray_;
    NSMutableArray *friendRequestsArray_;
}

@end

@implementation HCFriendsTableViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - NSObject

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        // TableView properties
        [self.tableView setTableFooterView:[[[UIView alloc] initWithFrame:CGRectZero] autorelease]];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        UIBarButtonItem *plusBarButton = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"cntrl_add_plus"] highlightedImage:[UIImage imageNamed:@"cntrl_add_plus"] target:self action:@selector(RC_rightBarButtonTouched:)];
        [self.navigationItem setRightBarButtonItem:plusBarButton];
        
        // Setup arrays holding friends and requests
        friendRequestsArray_ = [[NSMutableArray array] retain];
        currentFriendsArray_ = [[NSMutableArray array] retain];
        
        // Setup fake friends
        [self RC_initializeFakeFriends];
    }
    return self;
}

- (void)dealloc
{
    [currentFriendsArray_ release];
    [friendRequestsArray_ release];
    [super dealloc];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView reloadData];
}

- (void)RC_rightBarButtonTouched:(id)sender
{
    
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UITableViewControllerDelegate

- (void)RC_initializeFakeFriends
{
    HCUserProfile *friendOne = [[[HCUserProfile alloc] init] autorelease];
    [friendOne setFullName:@"Abel den Top"];
    [friendOne setEmailAddress:@"abel@dentop.com"];
    [friendOne setSafetyStatus:0];
    [friendOne setProfilePicturePath:@"http://media.makeadare.com/img/6160cf6aa/image_bceabb1299.jpg"];
    [currentFriendsArray_ addObject:friendOne];
    
    HCUserProfile *friendTwo = [[[HCUserProfile alloc] init] autorelease];
    [friendTwo setFullName:@"Michelle den Top"];
    [friendTwo setEmailAddress:@"michelle@dentop.com"];
    [friendTwo setSafetyStatus:2];
    [friendTwo setProfilePicturePath:@"http://media.makeadare.com/img/6160cf6aa/image_bceabb1299.jpg"];
    [currentFriendsArray_ addObject:friendTwo];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UITableViewControllerDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 2;
    
    if (section == 0) {
        rows = [currentFriendsArray_ count];
    }
    
    return rows;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    CGRect contentRect = [HCStyle contentRectForBounds:self.view.bounds];
//    CGRect viewRect = contentRect;
//    
//    viewRect.size.height = 50.0f;
//    
//    UIView *view = [[[UIView alloc] initWithFrame:viewRect] autorelease];
//    [view setBackgroundColor:[UIColor lightGrayColor]];
//    
//    return view;
//}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *string;
    if (section == 0) {
        string = @"Friends";
    } else {
        string = @"Friend request";
    }
    
    return string;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [HCFriendsTableViewCell preferredHeight];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UITableViewControllerDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        // Override
        HCFriendsTableViewCell *cell = [[[HCFriendsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FriendCell"] autorelease];
        [cell setUserProfile:[currentFriendsArray_ objectAtIndex:indexPath.row]];
        
        return cell;
    } else {
        // Override
        HCFriendsTableViewCell *cell = [[[HCFriendsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FriendRequest"] autorelease];
        
        return cell;
    }
}

@end
