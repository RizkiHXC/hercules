//
//  HCMapViewController.m
//  Hercules
//
//  Created by Rizki Calame on 17/04/15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCMapViewController.h"
#import "Hercules.h"
#import "HCPromptAlarmViewController.h"

@interface HCMapViewController () <UISearchBarDelegate, MBLocationControllerDelegate, GMSMapViewDelegate>
{
    
    GMSMapView *mapView_;
    UISearchBar *searchBar_;
    UIButton *alarmButton_;
    
    NSTimer *buttonHeldTimer_;
    BOOL shouldUseTimer_;
    NSTimeInterval interval_;
    
}

@end

@implementation HCMapViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // MapView
    CGRect contentRect = self.view.bounds;
    CGRect viewRect = contentRect;
    
    mapView_ = [GMSMapView mapWithFrame:viewRect camera:nil];
    [mapView_ setDelegate:self];
    [mapView_ setMyLocationEnabled:YES];
    [mapView_ setTrafficEnabled:YES];
    [self.view addSubview:mapView_];
    
    
    // Do any additional setup after loading the view, typically from a nib.
    [[HCLocationManager sharedManager] setDelegate:self];
    [[HCLocationManager sharedManager] startLocationManagerForOwnLocation];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:mapView_.myLocation.coordinate.latitude longitude:mapView_.myLocation.coordinate.longitude zoom:4];
    [mapView_ setCamera:camera];
    
    // Searchbar and container
    viewRect.size.height = [HCStyle buttonHeight];
    viewRect.origin.y = 0.0f - 1.0f;

    UIView *searchBarContainer = [[[UIView alloc] initWithFrame:viewRect] autorelease];
    [searchBarContainer setBackgroundColor:[UIColor hcDeepSeaBlue]];
    [searchBarContainer setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:searchBarContainer];
    
    searchBar_ = [[[UISearchBar alloc] initWithFrame:viewRect] autorelease];
    [searchBar_ setDelegate:self];
    [searchBar_ setPlaceholder:@"Where you going bud?"];
    [searchBar_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    [self.view addSubview:searchBar_];
    
    // Alarm button
    viewRect.size = CGSizeMake(80, 80);
    viewRect.origin.y = self.view.bounds.size.height - viewRect.size.height - [HCStyle spacingVertical];
    viewRect.origin.x = roundf(self.view.bounds.size.width / 2 - viewRect.size.width / 2);
    
    alarmButton_ = [UIButton buttonWithType:UIButtonTypeCustom];
    [alarmButton_ setFrame:viewRect];
    [alarmButton_ setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
    [alarmButton_ setBackgroundColor:[UIColor hcDeepSeaBlue]];
    [alarmButton_ setImage:[UIImage imageNamed:@"ico_shield_defender"] forState:UIControlStateNormal];
    [alarmButton_ addTarget:self action:@selector(RC_alarmButtonTouched:) forControlEvents:UIControlEventTouchDown];
    [alarmButton_ addTarget:self action:@selector(RC_alarmButtonReleased:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:alarmButton_];
    
    viewRect.origin = CGPointZero;
    viewRect.size.width = [HCStyle buttonHeight];
    viewRect.size.height = self.view.bounds.size.height;
    
    // Set panView left which can be used to handle touch events for the side menu
    UIView *panView = [[[UIView alloc] initWithFrame:viewRect] autorelease];
    [panView setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
    [panView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:panView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - Private

- (void)RC_setNewTimer
{
    interval_ = 3.0f;
    
    // Timer
    buttonHeldTimer_ = [NSTimer scheduledTimerWithTimeInterval:interval_
                                                        target:self
                                                      selector:@selector(RC_checkAlarmButtonHeld)
                                                      userInfo:nil
                                                       repeats:YES];
}

- (void)RC_killTimer
{
    interval_ = 0;
    [buttonHeldTimer_ invalidate];
}

- (void)RC_alarmButtonTouched:(id)sender
{
    shouldUseTimer_ = YES;
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [self RC_setNewTimer];
}

- (void)RC_alarmButtonReleased:(id)sender
{
    shouldUseTimer_ = NO;
    [self RC_killTimer];
    
    HCPromptAlarmViewController *viewController = [[[HCPromptAlarmViewController alloc] init] autorelease];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)RC_checkAlarmButtonHeld
{
    if (shouldUseTimer_) {
        // Vibrate the phone as feedback
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)locationController:(MBLocationController *)controller didUpdateLocation:(CLLocation *)location
{

}

@end
