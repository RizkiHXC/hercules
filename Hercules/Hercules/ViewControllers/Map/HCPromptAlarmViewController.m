//
//  HCPromptAlarmViewController.m
//  Hercules
//
//  Created by Rizki Calame on 26-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCPromptAlarmViewController.h"
#import "Hercules.h"

@interface HCPromptAlarmViewController ()
{
    UILabel *counterLabel_;
    NSTimer *timer_;
    NSInteger startingCount_;
}

@end

@implementation HCPromptAlarmViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - NSObject

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    }
    
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)viewDidLoad
{
    // Starting count
    startingCount_ = 10;
    
    // Set backgroundColor
    [self.view setBackgroundColor:[UIColor colorWithRed:(50/255) green:(50/255) blue:(50/255) alpha:.8]];
    
    CGRect contentRect = [HCStyle contentRectForBounds:self.view.bounds];
    CGRect viewRect = contentRect;
    
    // CounterLabel
    counterLabel_ = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    [counterLabel_ setFont:[UIFont fontWithName:@"Helvetica" size:180.0f]];
    [counterLabel_ setText:[NSString stringWithFormat:@"%ld", startingCount_]];
    [counterLabel_ setTextColor:[UIColor darkGrayColor]];
    [counterLabel_ setTextAlignment:NSTextAlignmentCenter];
    viewRect.size.height = [counterLabel_ sizeThatFits:CGSizeMake(viewRect.size.width, CGFLOAT_MAX)].height;
    viewRect.origin.y = self.view.size.height / 2 - viewRect.size.height;
    [counterLabel_ setFrame:viewRect];
    [counterLabel_ setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:counterLabel_];
    
    // Explanation text
    UILabel *explanationText = [[[UILabel alloc] initWithFrame:viewRect] autorelease];
    [explanationText setFont:[UIFont fontWithName:@"Helvetica" size:18.0f]];
    [explanationText setText:@"You have activated an alarm. When the timer runs out your status will be set to red."];
    [explanationText setTextColor:[UIColor darkGrayColor]];
    [explanationText setTextAlignment:NSTextAlignmentCenter];
    [explanationText setNumberOfLines:0];
    viewRect.size.height = [explanationText sizeThatFits:CGSizeMake(viewRect.size.width, CGFLOAT_MAX)].height;
    viewRect.origin.y = CGRectGetMaxY(counterLabel_.frame);
    [explanationText setFrame:viewRect];
    [explanationText setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:explanationText];
    
    // Cancel Button
    viewRect.size.width = self.view.bounds.size.width;
    viewRect.size.height = [HCStyle buttonHeight];
    viewRect.origin.y = CGRectGetMaxY(explanationText.frame) + [HCStyle spacingVertical];
    viewRect.origin.x = 0.0f;
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:viewRect];
    [cancelButton setBackgroundColor:[UIColor hcDeepSeaBlue]];
    [cancelButton setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
    [cancelButton setTitle:@"Stop timer" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(RC_cancelAlarmButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelButton];

    viewRect.origin.y = CGRectGetMaxY(cancelButton.frame) + [HCStyle spacingVertical];
    
    UIButton *sendAlarmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sendAlarmButton setFrame:viewRect];
    [sendAlarmButton setBackgroundColor:[UIColor hcStarFishRed]];
    [sendAlarmButton setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
    [sendAlarmButton setTitle:@"Send alarm immediately" forState:UIControlStateNormal];
    [sendAlarmButton addTarget:self action:@selector(RC_sendAlarmButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sendAlarmButton];
    
    timer_ = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(RC_timerDecrease) userInfo:nil repeats:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - Private

- (void)RC_timerDecrease
{
    if (startingCount_ > 0) {
        startingCount_ --;
        [counterLabel_ setText:[NSString stringWithFormat:@"%ld", startingCount_]];
    } else {
        [self RC_sendAlarm];
    }
}

- (void)RC_cancelAlarmButtonTouched:(id)sender
{
    [self RC_dismiss];
    [timer_ invalidate];
}

- (void)RC_sendAlarmButtonTouched:(id)sender
{
    [self RC_sendAlarm];
}

- (void)RC_sendAlarm
{
    [self showActivityIndicatorPopoverWithMessage:@"Sending alarm"];
    [self performSelector:@selector(RC_dismiss) withObject:nil afterDelay:1];
    [timer_ invalidate];
}

- (void)RC_dismiss
{
    [self dismissActivityIndicatorPopoverAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
