//
//  HCProfileTableViewController.m
//  Hercules
//
//  Created by Rizki Calame on 18-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCProfileTableViewController.h"
#import "HCStyle.h"
#import "HCProfileImageView.h"
#import "HCProfileTableViewCell.h"

@interface HCProfileTableViewController ()
{
    HCProfileImageView *profileImageView_;
}

@end

@implementation HCProfileTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView setTableHeaderView:[self RC_createTableViewHeader]];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    // Background Image
    UIImageView *backgroundImageView = [[[UIImageView alloc] initWithFrame:self.view.bounds] autorelease];
    [backgroundImageView setImage:[UIImage imageNamed:@"bg_profile_background"]];
    [backgroundImageView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.view insertSubview:backgroundImageView belowSubview:self.tableView];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - Private

- (UIView *)RC_createTableViewHeader
{
    CGRect contentRect = [HCStyle contentRectForBounds:self.view.bounds];
    CGRect viewRect = contentRect;
    
    viewRect.size.height = 200.0f;
    viewRect.size.width = self.view.bounds.size.width;
    
    UIView *view = [[[UIView alloc] initWithFrame:viewRect] autorelease];
    [view setBackgroundColor:[UIColor clearColor]];
    
    viewRect.size = [HCStyle profileImageSize];
    viewRect.origin.y = [HCStyle spacingVertical] * 2;
    viewRect.origin.x = roundf(view.bounds.size.width / 2 - viewRect.size.width / 2);
    
    // Profile Image View
    profileImageView_ = [[[HCProfileImageView alloc] initWithFrame:viewRect] autorelease];
//    [profileImageView_ setBackgroundImage:[UIImage imageWithColor:[UIColor redColor] size:CGSizeMake(1, 1)]];
    [profileImageView_ setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/7/000/212/32c/05fe90e.jpg"]]]];
    [view addSubview:profileImageView_];
    
    return view;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UITableViewControllerDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Override
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UITableViewControllerDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Override
    HCProfileTableViewCell *cell = [[[HCProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if (indexPath.row == 0) {
        [cell setCellType:HCProfileTableViewCellTypeName];
        [cell setShowSeparator:YES];
        [cell.detailTextLabel setText:@"Rizki Calame"];
    } else if (indexPath.row == 1) {
        [cell setCellType:HCProfileTableViewCellTypeEmail];
        [cell.detailTextLabel setText:@"rizki@rizkicalame.com"];
    }
    
    return cell;
}

@end
