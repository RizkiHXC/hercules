//
//  HCTableViewCell.h
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HCTableViewCell : UITableViewCell

+ (CGFloat)preferredHeight;

- (void)initializeStyle;

@end
