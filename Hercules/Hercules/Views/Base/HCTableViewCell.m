//
//  HCTableViewCell.m
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCTableViewCell.h"
#import "Hercules.h"

@implementation HCTableViewCell

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Class methods

+ (CGFloat)preferredHeight
{
    return 60.0f;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initializeStyle];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)initializeStyle
{
    [self setBackgroundColor:[UIColor clearColor]];
    [self setSelectedBackgroundView:[[[UIView alloc] init] autorelease]];
//    [self.selectedBackgroundView setBackgroundColor:[UIColor hcWhite]];
    
    // Text label
    [self.textLabel setTextColor:[UIColor hcWhite]];
    [self.textLabel setFont:[UIFont hcSemiboldFontOfSize:14.0f]];
    
    // Detail text label
    [self.detailTextLabel setTextColor:[UIColor hcWhite]];
    [self.detailTextLabel setFont:[UIFont hcLightFontOfSize:14.0f]];
}


@end
