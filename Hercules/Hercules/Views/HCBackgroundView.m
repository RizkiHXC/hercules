//
//  HCBackgroundView.m
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCBackgroundView.h"
#import "Hercules.h"

@implementation HCBackgroundView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_menu_background"]] autorelease];
        [backgroundView setContentMode:UIViewContentModeScaleAspectFill];
        [backgroundView setClipsToBounds:YES];
        
        [backgroundView setFrame:self.bounds];
        [backgroundView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self addSubview:backgroundView];
        [self MB_addMotionEffectToView:backgroundView];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_addMotionEffectToView:(UIView *)view
{
    CGSize motionValue = CGSizeMake(25.0f, 35.0f);
    UIInterpolatingMotionEffect *horizontalMotionEffect = [[[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis] autorelease];
    [horizontalMotionEffect setMinimumRelativeValue:@(motionValue.width)];
    [horizontalMotionEffect setMaximumRelativeValue:@(-motionValue.width)];
    
    UIInterpolatingMotionEffect *verticalMotionEffect = [[[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis] autorelease];
    [verticalMotionEffect setMinimumRelativeValue:@(motionValue.height)];
    [verticalMotionEffect setMaximumRelativeValue:@(-motionValue.height)];
    
    UIMotionEffectGroup *effectGroup = [[[UIMotionEffectGroup alloc] init] autorelease];
    [effectGroup setMotionEffects:@[ horizontalMotionEffect, verticalMotionEffect]];
    [view addMotionEffect:effectGroup];
    
    CGSize sizeRatio = CGSizeMake(((view.width + 2*motionValue.width) / view.width), ((view.height + 2*motionValue.height) / view.height));
    CGFloat scale = MAX(sizeRatio.height, sizeRatio.width);
    [view setTransform:CGAffineTransformMakeScale(scale, scale)];
}

@end
