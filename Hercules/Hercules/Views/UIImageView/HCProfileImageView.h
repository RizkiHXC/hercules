//
//  HCProfileImageView.h
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HCProfileImageView : UIImageView

@property(nonatomic,retain) UIImage *backgroundImage;
@property(nonatomic,retain) UIImage *placeholderImage;
@property(nonatomic,assign) UIEdgeInsets imageEdgeInsets;

@end
