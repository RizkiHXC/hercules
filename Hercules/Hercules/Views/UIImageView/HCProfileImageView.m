//
//  HCProfileImageView.m
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCProfileImageView.h"

@interface HCProfileImageView ()
{
    UIImageView *imageView_;
    UIImageView *backgroundImageView_;
    UIImageView *placeholderImageView_;
    
    CALayer *maskLayer_;
}

@end

@implementation HCProfileImageView

@synthesize imageEdgeInsets = imageEdgeInsets_;

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithFrame:(CGRect)frame
{
    if (CGSizeEqualToSize(frame.size, CGSizeZero)) {
        frame.size = CGSizeMake(100.0f, 100.0f);
    }
    
    self = [super initWithFrame:frame];
    if (self) {
        [self setContentMode:UIViewContentModeScaleAspectFill];
        [self setClipsToBounds:YES];
        
        // Background image
        backgroundImageView_ = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
        [backgroundImageView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self addSubview:backgroundImageView_];
        
        // Image view
        imageView_ = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
        [imageView_ setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [imageView_.layer setMasksToBounds:YES];
        [self addSubview:imageView_];
        
        // Placeholder
        placeholderImageView_ = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
        [placeholderImageView_ setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin];
        [self addSubview:placeholderImageView_];
        [placeholderImageView_ setHidden:self.image != nil ? YES : NO];
    }
    return self;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)MB_updateMaskLayer
{
    if (CGSizeEqualToSize(self.bounds.size, CGSizeZero) == NO) {
        CAShapeLayer *maskLayer = (CAShapeLayer *)imageView_.layer.mask;
        if (!maskLayer) {
            maskLayer = [CAShapeLayer layer];
            [imageView_.layer setMask:maskLayer];
        }
        UIBezierPath *ovalPath = [UIBezierPath bezierPathWithOvalInRect:UIEdgeInsetsInsetRect(self.bounds, imageEdgeInsets_)];
        [maskLayer setPath:ovalPath.CGPath];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self MB_updateMaskLayer];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    return backgroundImageView_.image.size;
}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIImageView

- (void)setImage:(UIImage *)image
{
    [placeholderImageView_ setHidden:YES];
    [imageView_ setImage:image];
    
    [self setNeedsLayout];
}

- (UIImage *)image
{
    return imageView_.image;
}

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    [backgroundImageView_ setImage:backgroundImage];
}

- (UIImage *)backgroundImage
{
    return backgroundImageView_.image;
}

- (void)setPlaceholderImage:(UIImage *)placeholderImage
{
    [placeholderImageView_ setImage:placeholderImage];
    [placeholderImageView_ sizeToFit];
    [placeholderImageView_ setCenter:self.center];
}

- (UIImage *)placeholderImage
{
    return placeholderImageView_.image;
}

- (void)setImageEdgeInsets:(UIEdgeInsets)imageEdgeInsets
{
    if (!UIEdgeInsetsEqualToEdgeInsets(imageEdgeInsets_, imageEdgeInsets)) {
        imageEdgeInsets_ = imageEdgeInsets;
        [self setNeedsLayout];
    }
}

@end
