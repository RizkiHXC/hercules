//
//  HCFriendsTableViewCell.h
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

@class HCUserProfile;

#import "HCTableViewCell.h"

@interface HCFriendsTableViewCell : HCTableViewCell

@property (nonatomic, retain) HCUserProfile *userProfile;

+ (CGFloat)preferredHeight;

@end
