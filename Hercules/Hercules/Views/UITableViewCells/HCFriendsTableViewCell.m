//
//  HCFriendsTableViewCell.m
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCFriendsTableViewCell.h"
#import "Hercules.h"

@interface HCFriendsTableViewCell ()
{
    HCProfileImageView *profilePicture_;
}

@end

@implementation HCFriendsTableViewCell

@synthesize  userProfile = userProfile_;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - Class methods

+ (CGFloat)preferredHeight
{
    return 100.0f;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - NSObject

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self RC_initializeStyle];
    }
    
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect viewRect = self.bounds;
    
    // Text Label
    viewRect.size.width = viewRect.size.width - CGRectGetMaxX(profilePicture_.frame) + [HCStyle spacingVertical];
    viewRect.size.height = [self.textLabel sizeThatFits:CGSizeMake(viewRect.size.width, CGFLOAT_MAX)].height;
    viewRect.origin.y = roundf([HCFriendsTableViewCell preferredHeight] / 2 - viewRect.size.height / 2);
    viewRect.origin.x = CGRectGetMaxX(profilePicture_.frame) + [HCStyle spacingHorizontal];
    [self.textLabel setFrame:viewRect];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - Private

- (void)RC_initializeStyle
{
    [self.textLabel setTextColor:[UIColor blackColor]];
    
    CGRect viewRect = self.bounds;
    viewRect.origin.y = [HCStyle spacingVertical];
    viewRect.origin.x = [HCStyle spacingHorizontal];
    viewRect.size.height = [HCFriendsTableViewCell preferredHeight] - [HCStyle spacingVertical] * 2;
    viewRect.size.width = viewRect.size.height;
    
    // Profile Picture
    profilePicture_ = [[[HCProfileImageView alloc] initWithFrame:viewRect] autorelease];
    [profilePicture_ setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin];
    [self addSubview:profilePicture_];
    
    viewRect.size.height = 1.0f;
    viewRect.origin.y = CGRectGetMaxY(self.bounds) - viewRect.size.height;
    viewRect.origin.x = CGRectGetMaxX(profilePicture_.frame);
    viewRect.size.width = self.bounds.size.width - viewRect.origin.x;
    
    // Separator
    MBTableViewCellSeparator *separator = [[[MBTableViewCellSeparator alloc] initWithFrame:viewRect] autorelease];
    [separator setColor:[UIColor lightGrayColor]];
    [separator setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin];
    [self addSubview:separator];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - Properties

- (void)setUserProfile:(HCUserProfile *)userProfile
{
    [userProfile_ release];
    userProfile_ = [userProfile retain];
    
    // Set textLabel
    [self.textLabel setText:userProfile_.fullName];
    
    // Set profile picture
    [profilePicture_ setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:userProfile_.profilePicturePath]]]];
    
    [self setNeedsLayout];
}

@end
