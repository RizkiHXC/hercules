//
//  HCMenuTableViewCell.m
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCMenuTableViewCell.h"
#import "Hercules.h"

@implementation HCMenuTableViewCell

- (void)initializeStyle
{
    [super initializeStyle];
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self.textLabel setFont:[UIFont hcRegularFontOfSize:13.0f]];
    [self.textLabel setTextColor:[UIColor hcWhite]];
    [self.textLabel setHighlightedTextColor:[UIColor hcBlack]];
    
    // Selected background
    MBGradientView *highlightView = [[[MBGradientView alloc] init] autorelease];
    [highlightView setGradientDirection:MBGradientViewDirectionHorizontal];
    [highlightView setLocations:@[@(0.5), @(0.95)]];
    UIColor *startColor = [[UIColor hcWhite] colorWithAlphaComponent:0.2f];
    UIColor *endColor = [[UIColor hcWhite] colorWithAlphaComponent:0.0f];
    [highlightView setColors:@[startColor, endColor]];
    [self setSelectedBackgroundView:highlightView];
}

@end
