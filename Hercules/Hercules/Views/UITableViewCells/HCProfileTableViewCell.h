//
//  HCProfileTableViewCell.h
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCTableViewCell.h"

typedef NS_ENUM(NSInteger, HCProfileTableViewCellType)
{
    HCProfileTableViewCellTypeName,
    HCProfileTableViewCellTypeEmail,
};

@interface HCProfileTableViewCell : HCTableViewCell

@property (nonatomic, assign) HCProfileTableViewCellType cellType;
@property (nonatomic, assign, getter=separatorShown) BOOL showSeparator;

@end
