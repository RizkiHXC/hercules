//
//  HCProfileTableViewCell.m
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCProfileTableViewCell.h"
#import "Hercules.h"

@implementation HCProfileTableViewCell

@synthesize cellType = cellType_;
@synthesize showSeparator = showSeparator_;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        
    }
    
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - Properties

- (void)setCellType:(HCProfileTableViewCellType)cellType
{
    switch (cellType) {
        case HCProfileTableViewCellTypeName:
            [self.textLabel setText:@"Name"];
            break;
        case HCProfileTableViewCellTypeEmail:
            [self.textLabel setText:@"E-mail"];
            break;
        default:
            break;
    }
}

- (void)setShowSeparator:(BOOL)showSeparator
{
    if (showSeparator) {
        CGRect viewRect = self.bounds;
        viewRect.size.height = 1.0f;
        viewRect.origin.y = CGRectGetMaxY(self.bounds) - viewRect.size.height;
        
        MBTableViewCellSeparator *separator = [[[MBTableViewCellSeparator alloc] initWithFrame:viewRect] autorelease];
        [separator setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
        [separator setColor:[UIColor hcWhite]];
        [self addSubview:separator];
    }
}
@end
