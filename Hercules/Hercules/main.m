//
//  main.m
//  Hercules
//
//  Created by Rizki Calame on 13-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HCAppDelegate class]));
    }
}
