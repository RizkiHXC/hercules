#!/bin/sh

#  publish_framework.sh
#  MBFramework
#
#  Created by Marco Jonker on 2/4/13.
#  Copyright (c) 2013 mediaBunker. All rights reserved.
#  Source: https://github.com/jverkoey/iOS-Framework

echo "================ Cleaning framework =================="

xcodebuild clean

rm -rf ./publish
mkdir ./publish

echo "================ Updating framework version =================="

devPlist="./MBFramework/MBFramework.plist"
frameworksFolder="/Volumes/Production/Production Resources/iOS frameworks/MBFramework-"
frameworkSourceFolder="./publish/Release-iphoneos/MBFramework.framework"
resourcesSourceFile="./publish/MBFrameworkResources.bundle"
newVersion=`/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "$devPlist" | /usr/bin/perl -pe 's/(\d+\.\d+\.\d+\.)(\d+)/$1.($2+1)/eg'`
destinationFolder="$frameworksFolder$newVersion"

/usr/libexec/PListBuddy -c "Set :CFBundleVersion $newVersion" "$devPlist"

# Update the build date
# Info: create a custom field of type Date with key name 'CFBuildDate' in the info.plist file
buildDate=$(date)
/usr/libexec/PlistBuddy -c "Set :CFBuildDate $buildDate" "$devPlist"

echo "================ Building framework version: $newVersion =================="

xcodebuild -target MBFramework
xcodebuild -target Framework
xcodebuild -target MBFrameworkResources CONFIGURATION_BUILD_DIR=./publish

echo "================ Copying framework version: $newVersion =================="

mkdir "$destinationFolder"
cp -r "$frameworkSourceFolder" "$destinationFolder"
cp -r "$resourcesSourceFile" "$destinationFolder"

echo "================ Publish finished version: $newVersion =================="


