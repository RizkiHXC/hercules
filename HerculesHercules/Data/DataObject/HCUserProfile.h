//
//  HCUserProfile.h
//  Hercules
//
//  Created by Rizki Calame on 23-04-15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HCUserProfile : NSObject

@property (nonatomic, retain) NSString *fullName;
@property (nonatomic, retain) NSString *emailAddress;
@property (nonatomic, assign) NSInteger safetyStatus;
@property (nonatomic, retain) NSString *profilePicturePath;

@end
