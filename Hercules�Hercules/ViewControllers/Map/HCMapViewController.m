//
//  HCMapViewController.m
//  Hercules
//
//  Created by Rizki Calame on 17/04/15.
//  Copyright (c) 2015 Rizki Calame. All rights reserved.
//

#import "HCMapViewController.h"

@implementation HCMapViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma-mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.view setBackgroundColor:[UIColor redColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
